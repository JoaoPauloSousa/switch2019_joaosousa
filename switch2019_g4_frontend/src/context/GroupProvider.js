import React, {useReducer} from "react";
import {Provider} from './GroupContext'
import groupReducer from "./GroupReducer";
import PropTypes from "prop-types";

const initialState = {
    info: {
        loading: true,
        error: null,
        data: {},
    },
    transactions: {
        loading: false,
        error: null,
        data: [],
        isSubmitted: 'no'
    },
    categories: {
        loading: false,
        error: null,
        data: [],
        isSubmitted: 'no'
    },
    accounts: {
        loading: false,
        error: null,
        data: [],
        isSubmitted: 'no'
    },
    members: {
        loading: false,
        error: null,
        data: [],
        isSubmitted: 'no'
    },
    transactionsFilters: {
        hasFilters: 'no',
        accountID: null,
        initialDate: '',
        finalDate: ''
    },
};

const GroupProvider = (props) => {
    const [state, dispatch] = useReducer(groupReducer, initialState);
    return (
        <Provider value={{
            state,
            dispatch
        }}>
            {props.children}
        </Provider>
    )
};

GroupProvider.propTypes = {
    children: PropTypes.node,
};


export default GroupProvider