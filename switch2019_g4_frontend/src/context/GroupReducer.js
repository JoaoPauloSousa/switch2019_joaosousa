import {
    CLEAR_TRANSACTIONS_FILTERS,
    FETCH_GROUP_ACCOUNTS_AFTER_POST,
    FETCH_GROUP_ACCOUNTS_FAILURE,
    FETCH_GROUP_ACCOUNTS_STARTED,
    FETCH_GROUP_ACCOUNTS_SUCCESS,
    FETCH_GROUP_CATEGORIES_AFTER_POST,
    FETCH_GROUP_CATEGORIES_FAILURE,
    FETCH_GROUP_CATEGORIES_STARTED,
    FETCH_GROUP_CATEGORIES_SUCCESS,
    FETCH_GROUP_INFO_FAILURE,
    FETCH_GROUP_INFO_STARTED,
    FETCH_GROUP_INFO_SUCCESS,
    FETCH_GROUP_MEMBERS_AFTER_POST,
    FETCH_GROUP_MEMBERS_FAILURE,
    FETCH_GROUP_MEMBERS_STARTED,
    FETCH_GROUP_MEMBERS_SUCCESS,
    FETCH_GROUP_TRANSACTIONS_AFTER_POST,
    FETCH_GROUP_TRANSACTIONS_FAILURE,
    FETCH_GROUP_TRANSACTIONS_STARTED,
    FETCH_GROUP_TRANSACTIONS_SUCCESS,
    FILTER_TRANSACTIONS,
    TRANSACTIONS_ACCOUNTID_FILTER,
    TRANSACTIONS_FINALDATE_FILTER,
    TRANSACTIONS_INITIALDATE_FILTER,
} from "./GroupActions";


function groupReducer(state, action) {
    switch (action.type) {
        case FETCH_GROUP_CATEGORIES_STARTED:
            return {
                ...state,
                categories: {
                    loading: true,
                    error: null,
                    data: []
                }
            };
        case FETCH_GROUP_CATEGORIES_SUCCESS:
            return {
                ...state,
                categories: {
                    loading: false,
                    error: null,
                    data: [...action.payload.data]
                }
            };
        case FETCH_GROUP_CATEGORIES_FAILURE:
            return {
                ...state,
                categories: {
                    loading: false,
                    error: action.payload.error,
                    data: [],
                }
            };
        case FETCH_GROUP_CATEGORIES_AFTER_POST:
            return {
                ...state,
                categories: {
                    loading: false,
                    error: null,
                    data: [],
                    isSubmitted: 'yes'
                }
            };

        case FETCH_GROUP_TRANSACTIONS_STARTED:
            return {
                ...state,
                transactions: {
                    loading: true,
                    error: null,
                    data: []
                }
            };
        case FETCH_GROUP_TRANSACTIONS_SUCCESS:
            return {
                ...state,
                transactions: {
                    loading: false,
                    error: null,
                    data: [...action.payload.data]
                }
            };
        case FETCH_GROUP_TRANSACTIONS_FAILURE:
            return {
                ...state,
                transactions: {
                    loading: false,
                    error: action.payload.error,
                    data: []
                }
            };

        case FETCH_GROUP_TRANSACTIONS_AFTER_POST:
            return {
                ...state,
                transactions: {
                    loading: false,
                    error: null,
                    data: [],
                    isSubmitted: 'yes'
                }
            };

        case FETCH_GROUP_ACCOUNTS_STARTED:
            return {
                ...state,
                accounts: {
                    loading: true,
                    error: null,
                    data: []
                }
            };
        case FETCH_GROUP_ACCOUNTS_SUCCESS:
            return {
                ...state,
                accounts: {
                    loading: false,
                    error: null,
                    data: [...action.payload.data]
                }
            };
        case FETCH_GROUP_ACCOUNTS_FAILURE:
            return {
                ...state,
                accounts: {
                    loading: false,
                    error: action.payload.error,
                    data: []
                }
            };
        case FETCH_GROUP_ACCOUNTS_AFTER_POST:
            return {
                ...state,
                accounts: {
                    loading: false,
                    error: null,
                    data: [],
                    isSubmitted: 'yes'
                }
            };

        case FETCH_GROUP_MEMBERS_STARTED:
            return {
                ...state,
                members: {
                    loading: true,
                    error: null,
                    data: []
                }
            };
        case FETCH_GROUP_MEMBERS_SUCCESS:
            return {
                ...state,
                members: {
                    loading: false,
                    error: null,
                    data: [...action.payload.data]
                }
            };
        case FETCH_GROUP_MEMBERS_FAILURE:
            return {
                ...state,
                members: {
                    loading: false,
                    error: action.payload.error,
                    data: []
                }
            };
        case FETCH_GROUP_MEMBERS_AFTER_POST:
            return {
                ...state,
                members: {
                    loading: false,
                    error: null,
                    data: [],
                    isSubmitted: 'yes'
                }
            };

        //-------------------------- FILTER_TRANSACTIONS -------------------------

        case TRANSACTIONS_ACCOUNTID_FILTER:
            //Altera o accountID do transactionsFilters do estado
            return {
                ...state,
                transactionsFilters: {
                    ...state.transactionsFilters,
                    accountID: action.accountID
                }
            };
        case TRANSACTIONS_INITIALDATE_FILTER:
            //Altera o initialDate do transactionsFilters do estado
            return {
                ...state,
                transactionsFilters: {
                    ...state.transactionsFilters,
                    initialDate: action.initialDate
                }
            };
        case TRANSACTIONS_FINALDATE_FILTER:
            //Altera o finalDate do transactionsFilters do estado
            return {
                ...state,
                transactionsFilters: {
                    ...state.transactionsFilters,
                    finalDate: action.finalDate
                }
            };
        case FILTER_TRANSACTIONS:
            //Retorna o hasFilters como 'yes'
            return {
                ...state,
                transactionsFilters: {
                    ...state.transactionsFilters,
                    hasFilters: 'yes'
                }
            };
        case CLEAR_TRANSACTIONS_FILTERS:
            return {
                ...state,
                transactionsFilters: {
                    hasFilters: 'no',
                    accountID: null,
                    initialDate: '',
                    finalDate: ''
                }
            };

        //-------------------------- FETCH_PERSON_INFO -------------------------

        case FETCH_GROUP_INFO_STARTED:
            return {
                ...state,
                info: {
                    loading: true,
                    error: null,
                    data: {}
                }
            };
        case FETCH_GROUP_INFO_SUCCESS:
            return {
                ...state,
                info: {
                    loading: false,
                    error: null,
                    data: {...action.payload.data}
                }
            };
        case FETCH_GROUP_INFO_FAILURE:
            return {
                ...state,
                info: {
                    loading: false,
                    error: action.payload.error,
                    data: {}
                }
            };

        default:
            return state
    }
}

export default groupReducer