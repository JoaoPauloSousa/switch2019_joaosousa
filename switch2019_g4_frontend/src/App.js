import React from 'react';
import './App.css';
import SignIn from "./pages/SignIn";

import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import AppContext from "./context/AppContext";

import PrivateRoute from './PrivateRoute';
import Home from "./pages/Home";
import Groups from "./pages/Groups";

function App() {
    const {state} = React.useContext(AppContext);
    const {isLogged} = state;

    if (isLogged === true)
        return (
            <div>
                <Router>
                    <Switch>
                        <PrivateRoute exact path='/home' component={Home}/>
                        <PrivateRoute path='/groups/:groupId' component={Groups}/>
                    </Switch>
                </Router>
            </div>


        );
    else {
        return (
            <Router>
                <div className="App">
                    <Route component={SignIn}/>
                </div>
            </Router>
        );
    }

}

export default App;