import React, {useContext} from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import PersonContext from "../context/PersonContext";
import {changeTransactionsFiltersAccountID} from "../context/PersonActions";

export default function FreeSolo() {
    const accounts = useContext(PersonContext).state.accounts.data;
    const {dispatch} = useContext(PersonContext);

    function handleChange(event, value) {
        dispatch(changeTransactionsFiltersAccountID(value))
    }

    //TODO: clear account after clear filter button is clicked;
    //TODO: change filters and click "apply filters" without clicking "clear filters" button
    //TODO: add denomination to display in list of accounts
    return (
        <div style={{width: 300}}>
            <Autocomplete
                freeSolo
                id="free-solo-2-demo"
                disableClearable
                options={accounts.map((option) => option.accountID)}
                onChange={handleChange}
                renderInput={(params) => (
                    <TextField
                        {...params}
                        label="Account"
                        margin="normal"
                        variant="outlined"
                        InputProps={{...params.InputProps, type: 'search'}}
                    />
                )}
            />
        </div>
    );
}