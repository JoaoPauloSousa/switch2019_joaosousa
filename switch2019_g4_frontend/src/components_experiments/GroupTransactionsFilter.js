import React, {useContext} from "react";
import Button from "@material-ui/core/Button";
import {
    changeTransactionsFiltersFinalDate,
    changeTransactionsFiltersInitialDate,
    clearTransactionsFilters,
    filterTransactions
} from "../context/GroupActions";
import moment from "moment";
import FreeSoloGroup from "./FreeSoloGroup";
import DatePickerInlineGroup from "./DatePickerInlineGroup";
import GroupContext from "../context/GroupContext";
import Grid from "@material-ui/core/Grid";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        direction: "row",
        justify: "space-evenly",
        alignItems: "center"
    },
    control: {
        align: "center"
    },
}));

function PersonTransactionsFilter() {
    const {dispatch} = useContext(GroupContext);
    const classes = useStyles();

    function handleSubmit(event) {
        event.preventDefault();
        console.log("click")
        dispatch(filterTransactions())
    }

    function handleClear(event) {
        event.preventDefault();
        dispatch(clearTransactionsFilters())
        dispatch(changeTransactionsFiltersInitialDate((moment(Date()).format('YYYY-MM-DD')) + " 00:00:00"))
        dispatch(changeTransactionsFiltersFinalDate((moment(Date()).format('YYYY-MM-DD')) + " 23:59:59"))
    }

    return (
        <div>
            <Grid container className={classes.root} spacing={2}>
                <Grid item>
                    <DatePickerInlineGroup id={'initialDate'} name="initialDate" label="From"/>
                </Grid>
                <Grid item>
                    <DatePickerInlineGroup id={'finalDate'} name="finalDate" label="To"/>
                </Grid>
                <Grid item>
                    {/*<RadioButtonsGroup/>*/}
                    <FreeSoloGroup id={'accountIDFilter'} name="accountID"/>
                </Grid>
                <Grid item>
                    <Button id="applyFiltersBtn" variant="contained" color="primary" onClick={handleSubmit}>
                        Apply filters
                    </Button>
                </Grid>
                <Grid item>
                    <Button id="clearFiltersBtn" variant="contained" color="primary" onClick={handleClear}>
                        Clear filters
                    </Button>
                </Grid>
            </Grid>
        </div>
    )
}

export default PersonTransactionsFilter