import React, {useContext, useEffect} from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import {lighten, makeStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
// import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
// import DeleteIcon from '@material-ui/icons/Delete';
// import FilterListIcon from '@material-ui/icons/FilterList';
import PlaylistAddIcon from '@material-ui/icons/PlaylistAdd';
import PersonContext from "../context/PersonContext";
import {changeGroupId, fetchGroupsFailure, fetchGroupsStarted, fetchGroupsSuccess} from "../context/PersonActions";
import AppContext from "../context/AppContext";
import {useHistory} from "react-router-dom";
import useModal from "./useModal";
import Modal from "./Modal";
import Skeleton from "@material-ui/lab/Skeleton";
// import Button from '@material-ui/core/Button';
// import FilterButton from "./FilterButton";
// import DatePicker from "@material-ui/pickers";
// import DatePicker from './DatePicker';


function descendingComparator(a, b, orderBy) {
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
}

function getComparator(order, orderBy) {
    return order === 'desc'
        ? (a, b) => descendingComparator(a, b, orderBy)
        : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
        const order = comparator(a[0], b[0]);
        if (order !== 0) return order;
        return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
}


function EnhancedTableHead(props) {
    const {headCellsGroups} = useContext(PersonContext);
    const headCells = headCellsGroups;

    const {classes, onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort} = props;
    const createSortHandler = (property) => (event) => {
        onRequestSort(event, property);
    };

    return (
        <TableHead>
            <TableRow>
                {/*<TableCell padding="checkbox">*/}
                {/*    <Checkbox*/}
                {/*        indeterminate={numSelected > 0 && numSelected < rowCount}*/}
                {/*        checked={rowCount > 0 && numSelected === rowCount}*/}
                {/*        onChange={onSelectAllClick}*/}
                {/*        inputProps={{ 'aria-label': 'select all desserts' }}*/}
                {/*    />*/}
                {/*</TableCell>*/}
                {headCells.map((headCell) => (
                    <TableCell
                        style={{
                            backgroundColor: "#3f51b5",
                            color: "white",
                            fontWeight: "bold"
                        }}
                        key={headCell.id}
                        align={'center'}
                        padding={headCell.disablePadding ? 'none' : 'default'}
                        sortDirection={orderBy === headCell.id ? order : false}
                    >
                        <TableSortLabel
                            style={{
                                color: "white",
                                fontWeight: "bold"
                            }}
                            active={orderBy === headCell.id}
                            direction={orderBy === headCell.id ? order : 'asc'}
                            onClick={createSortHandler(headCell.id)}
                        >
                            {headCell.label}
                            {orderBy === headCell.id ? (
                                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
                            ) : null}
                        </TableSortLabel>
                    </TableCell>
                ))}
            </TableRow>
        </TableHead>
    );
}

EnhancedTableHead.propTypes = {
    classes: PropTypes.object.isRequired,
    numSelected: PropTypes.number.isRequired,
    onRequestSort: PropTypes.func.isRequired,
    onSelectAllClick: PropTypes.func.isRequired,
    order: PropTypes.oneOf(['asc', 'desc']).isRequired,
    orderBy: PropTypes.string.isRequired,
    rowCount: PropTypes.number.isRequired,
};

const useToolbarStyles = makeStyles((theme) => ({
    root: {
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(1),
    },
    highlight:
        theme.palette.type === 'light'
            ? {
                color: theme.palette.secondary.main,
                backgroundColor: lighten(theme.palette.secondary.light, 0.85),
            }
            : {
                color: theme.palette.text.primary,
                backgroundColor: theme.palette.secondary.dark,
            },
    title: {
        flex: '1 1 100%',
    },
}));

const EnhancedTableToolbar = (props) => {
    const classes = useToolbarStyles();
    const {numSelected} = props;
    const {isShowing, toggle} = useModal();

    return (
        <Toolbar
            className={clsx(classes.root, {
                [classes.highlight]: numSelected > 0,
            })}
        >
            {/*{numSelected > 0 ? (*/}
            {/*    <Typography className={classes.title} color="inherit" variant="subtitle1" component="div">*/}
            {/*        {numSelected} selected*/}
            {/*    </Typography>*/}
            {/*) : (*/}
            {/*    <Typography className={classes.title} variant="h6" id="tableTitle" component="div">*/}
            {/*        Transactions*/}
            {/*    </Typography>*/}
            {/*)}*/}

            {/*{numSelected > 0 ? (*/}
            {/*    <Tooltip title="Delete">*/}
            {/*        <IconButton aria-label="delete">*/}
            {/*            <DeleteIcon />*/}
            {/*        </IconButton>*/}
            {/*    </Tooltip>*/}
            {/*) : (*/}
            <div>
                <div>
                    <Tooltip title="Add Group">
                        <IconButton variant="contained" color="primary">
                            <PlaylistAddIcon onClick={toggle}/>
                            <Modal
                                isShowing={isShowing}
                                hide={toggle}
                                aggregate='groups'
                            />
                        </IconButton>
                    </Tooltip>
                </div>
            </div>

            {/*)}*/}
        </Toolbar>
    );
};

EnhancedTableToolbar.propTypes = {
    numSelected: PropTypes.number.isRequired,
};

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
    paper: {
        width: '100%',
        marginBottom: theme.spacing(2),
    },
    table: {
        minWidth: 750,
    },
    visuallyHidden: {
        border: 0,
        clip: 'rect(0 0 0 0)',
        height: 1,
        margin: -1,
        overflow: 'hidden',
        padding: 0,
        position: 'absolute',
        top: 20,
        width: 1,
    },
}));

export default function EnhancedTable() {
    let history = useHistory();
    const classes = useStyles();
    const {state, dispatch} = useContext(PersonContext);
    const {username} = useContext(AppContext).state;
    const {groups} = state;
    const {data, isSubmitted} = groups;
    const rows = data;
    const [order, setOrder] = React.useState('asc');
    const [orderBy, setOrderBy] = React.useState('type');
    const [selected, setSelected] = React.useState([]);
    const [page, setPage] = React.useState(0);
    const [dense, setDense] = React.useState(true);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);
    const groupsLink = state.info.data._links.groupsURI.href;


    useEffect(() => {
        dispatch(fetchGroupsStarted());
        fetch(`${groupsLink}`)
            .then(res => res.json()).then(res => dispatch(fetchGroupsSuccess(res.groupsDTOs)))
            .catch(err => dispatch(fetchGroupsFailure(err.message)))
    }, [isSubmitted]);


    const handleRequestSort = (event, property) => {
        const isAsc = orderBy === property && order === 'asc';
        setOrder(isAsc ? 'desc' : 'asc');
        setOrderBy(property);
    };

    const handleSelectAllClick = (event) => {
        if (event.target.checked) {
            const newSelecteds = rows.map((n) => n.description);
            setSelected(newSelecteds);
            return;
        }
        setSelected([]);
    };

    const handleClick = (event, description) => {
        const selectedIndex = selected.indexOf(description);
        let newSelected = [];

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected, description);
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1));
        } else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                selected.slice(0, selectedIndex),
                selected.slice(selectedIndex + 1),
            );
        }

        setSelected(newSelected);
    };

    const handleClickID = (event, groupID, description) => {
        dispatch(changeGroupId(groupID, description));
        history.push(`/groups/${groupID}`);
    };


    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const handleChangeDense = (event) => {
        //setDense(event.target.checked);
    };

    const isSelected = (description) => selected.indexOf(description) !== -1;


    const emptyRows = rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);

    return (
        <div className={classes.root}>
            <Paper className={classes.paper}>
                <EnhancedTableToolbar numSelected={selected.length}/>
                <TableContainer>
                    <Table
                        className={classes.table}
                        aria-labelledby="tableTitle"
                        size={dense ? 'small' : 'medium'}
                        aria-label="enhanced table"
                    >
                        <EnhancedTableHead
                            classes={classes}
                            numSelected={selected.length}
                            order={order}
                            orderBy={orderBy}
                            onSelectAllClick={handleSelectAllClick}
                            onRequestSort={handleRequestSort}
                            rowCount={rows.length}
                        />
                        <TableBody>
                            {stableSort(rows, getComparator(order, orderBy))
                                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                .map((row, index) => {
                                    const isItemSelected = isSelected(row.description);
                                    const labelId = `enhanced-table-checkbox-${index}`;

                                    if (state.loading) {
                                        return (
                                            <div>
                                                <Skeleton/>
                                                <Skeleton animation={true}/>
                                                <Skeleton animation="wave"/>
                                            </div>
                                        )
                                    } else {
                                        return (
                                            <TableRow
                                                hover
                                                // onClick={(event) => handleClick(event, row.description)}
                                                onClick={(event) => handleClickID(event, row.groupID, row.description)}
                                                style={{cursor: 'pointer'}}
                                            >
                                                {/*<TableCell padding="checkbox">*/}
                                                {/*    <Checkbox*/}
                                                {/*        checked={isItemSelected}*/}
                                                {/*        inputProps={{'aria-labelledby': labelId}}*/}
                                                {/*    />*/}
                                                {/*</TableCell>*/}
                                                {/*<TableCell align="right" component="th" id={labelId} scope="row"*/}
                                                {/*           padding="none">{row.dateTime}</TableCell>*/}
                                                <TableCell align="center">{row.groupID}</TableCell>
                                                <TableCell align="center">{row.description}</TableCell>
                                                <TableCell align="center">{row.creationDate}</TableCell>
                                            </TableRow>
                                        )
                                    }
                                })}
                            {emptyRows > 0 && (
                                <TableRow style={{height: (dense ? 33 : 53) * emptyRows}}>
                                    <TableCell colSpan={7}/>
                                </TableRow>
                            )}
                        </TableBody>
                    </Table>
                </TableContainer>
                <TablePagination
                    rowsPerPageOptions={[5, 10, 25]}
                    component="div"
                    count={rows.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                />
            </Paper>
            {/*<FormControlLabel*/}
            {/*    control={<Switch checked={dense} onChange={handleChangeDense}/>}*/}
            {/*    label="Dense padding"*/}
            {/*/>*/}
        </div>
    );
}