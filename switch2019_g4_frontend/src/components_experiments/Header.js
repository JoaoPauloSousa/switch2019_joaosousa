import React, {Component} from 'react';
import logo from '../logo.png';

export default class Header extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="row">
                <div className="logo">
                    <img src={logo} width="400"/>
                </div>
            </div>
        );
    }
}