import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import EnhancedTransactionsTable from "./EnhancedGroupTransactionsTable";


const useStyles = makeStyles((theme) => ({
    root: {
        width: '80%',
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        flexBasis: '33.33%',
        flexShrink: 0,
    },
    secondaryHeading: {
        fontSize: theme.typography.pxToRem(15),
        color: theme.palette.text.secondary,
    },
}));

export default function ControlledExpansionPanels(props) {
    const classes = useStyles();
    const [expanded, setExpanded] = React.useState(false);

    const handleChange = (panel) => (event, isExpanded) => {
        setExpanded(isExpanded ? panel : false);
    };

    return (
        <div className={classes.root}>
            <ExpansionPanel expanded={expanded === 'panel1'} onChange={handleChange('panel1')}>
                <ExpansionPanelSummary
                    expandIcon={<ExpandMoreIcon/>}
                    aria-controls="panel1bh-content"
                    id="panel1bh-header"
                >
                    <Typography className={classes.heading} variant="h6">{props.name}</Typography>
                    {/*<Typography className={classes.secondaryHeading}>I am an expansion panel</Typography>*/}

                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <EnhancedTransactionsTable aggregate={props.aggregate}/>
                    {/*<EnhancedAccountsTable></EnhancedAccountsTable>*/}
                    {/*<DenseTable></DenseTable>*/}
                </ExpansionPanelDetails>
            </ExpansionPanel>
        </div>
    );
}

// export default ExpansionPanel;