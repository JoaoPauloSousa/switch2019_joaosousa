import React, {useContext, useState} from "react";
import {fetchGroupsAfterPost} from "../context/PersonActions";
import moment from "moment";
import PersonContext from "../context/PersonContext";
import SnackbarContext from "../context/SnackbarContext";
import Input from "@material-ui/core/Input";
import {
    GROUP_CREATION_FAILED,
    GROUP_SUCCESSFULLY_CREATED,
    showErrorSnackbar,
    showSuccessfulSnackbar
} from "../context/SnackbarActions";

function FormGroups() {

    const {dispatch} = useContext(PersonContext);
    const snackDispatch = useContext(SnackbarContext).dispatch;
    const createGroupLink = useContext(PersonContext).state.info.data._links.createGroupURI.href;


    const [inputData, setInputData] = useState({
        groupID: "",
        description: "",
        creationDate: moment().format("YYYY-MM-DD"),
        groupLedgerID: ""
    });

    function handleChange(event) {
        const {name, value} = event.target;
        setInputData(prevInputData => {
            return {
                ...prevInputData,
                [name]: value
            }
        })
    }

    function handleSubmit(event) {
        event.preventDefault();

        // This makes the groupLedgerID of the input the same as the groupID
        inputData.groupLedgerID = inputData.groupID;

        fetch(`${createGroupLink}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(inputData)
        }).then(res => res.json()).then(res => {
            console.log(res);
            res.message === undefined ?
                snackDispatch(showSuccessfulSnackbar(GROUP_SUCCESSFULLY_CREATED)) :
                snackDispatch(showErrorSnackbar(GROUP_CREATION_FAILED + res.message))
        })
            .catch(error => {
                console.log(error);
                snackDispatch(showErrorSnackbar(GROUP_CREATION_FAILED + error.message))
            }).then(() => {
            setInputData(prevInputData => {
                return {
                    groupID: "",
                    description: "",
                    creationDate: moment().format("YYYY-MM-DD"),
                    groupLedgerID: ""
                }
            });
            dispatch(fetchGroupsAfterPost())
        })

    }

    return (
        <form onSubmit={handleSubmit} id="groupForm">
            <div className="ModalHeader">
                <p>Create new group</p>
            </div>
            <div>
                <label className="labelForm" htmlFor="groupID">Group Id:</label><br/>
                <Input type="number" name="groupID" value={inputData.groupID}
                       onChange={handleChange} required/>
            </div>
            <br/>
            <div>
                <label className="labelForm" htmlFor="description">Description:</label>
                <br/>
                <Input type="text" name="description" value={inputData.description}
                       onChange={handleChange} required/>
            </div>
            <br/>
            <button className="buttonForm" id="submitGroup">Submit</button>
        </form>
    )
}

export default FormGroups