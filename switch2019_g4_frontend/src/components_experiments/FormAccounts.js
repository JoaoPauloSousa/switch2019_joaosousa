import React, {useContext, useState} from "react";
import {fetchPersonAccountsAfterPost, URL_API} from "../context/PersonActions";
import AppContext from "../context/AppContext";
import PersonContext from "../context/PersonContext";
import SnackbarContext from "../context/SnackbarContext";
import Input from "@material-ui/core/Input";
import {
    ACCOUNT_CREATION_FAILED,
    ACCOUNT_SUCCESSFULLY_CREATED,
    showErrorSnackbar,
    showSuccessfulSnackbar
} from "../context/SnackbarActions";

function FormAccounts() {

    const {state} = useContext(AppContext);
    const {username} = state;
    const {dispatch} = useContext(PersonContext);
    const snackDispatch = useContext(SnackbarContext).dispatch;

    const [inputData, setInputData] = useState({
        accountID: "",
        denomination: "",
        description: "",
    });

    function handleChange(event) {
        const {name, value} = event.target;
        setInputData(prevInputData => {
            return {
                ...prevInputData,
                [name]: value
            }
        })
    }

    function handleSubmit(event) {
        event.preventDefault();
        fetch(`${URL_API}/people/${username}/accounts`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(inputData)
        }).then(res => res.json()).then(res => {
            res.message === undefined ?
                snackDispatch(showSuccessfulSnackbar(ACCOUNT_SUCCESSFULLY_CREATED)) :
                snackDispatch(showErrorSnackbar(ACCOUNT_CREATION_FAILED + res.message));
            console.log(res)
        })
            .catch(error => {
                console.log(error);
                snackDispatch(showErrorSnackbar(ACCOUNT_CREATION_FAILED + error.message))
            }).then(() => {
                setInputData(prevInputData => {
                    return {
                        accountID: "",
                        denomination: "",
                        description: "",
                    }
                });
                dispatch(fetchPersonAccountsAfterPost())
            }
        )
    }

    return (
        <form onSubmit={handleSubmit} id="accountForm">
            <div
                className="ModalHeader">
                <p>Add new Account</p>
            </div>

            <div>
                <label className="labelForm" htmlFor="accountID">Account Id:</label><br/>
                <Input type="number" name="accountID" value={inputData.accountID}
                       onChange={handleChange} required/>
            </div>
            <br/>
            <div>
                <label className="labelForm" htmlFor="denomination">Denomination:</label>
                <br/>
                <Input type="text" name="denomination" value={inputData.denomination}
                       onChange={handleChange} required/>
            </div>
            <br/>
            <div>
                <label className="labelForm" htmlFor="description">Description:</label>
                <br/>
                <Input type="text" name="description" value={inputData.description}
                       onChange={handleChange} required/>
            </div>
            <br/>
            <button
                className="buttonForm"
                id="submitAccount">Submit
            </button>
        </form>
    )
}

export default FormAccounts;