import React, {useContext, useState} from "react";
import {fetchGroupMembersAfterPost, URL_API} from "../context/GroupActions";
import PersonContext from "../context/PersonContext";
import GroupContext from "../context/GroupContext";
import SnackbarContext from "../context/SnackbarContext";
import Input from "@material-ui/core/Input";
import {
    MEMBER_ADDITION_FAILED,
    MEMBER_SUCCESSFULLY_ADDED,
    showErrorSnackbar,
    showSuccessfulSnackbar
} from "../context/SnackbarActions";

function FormGroupMembers() {

    const {groupId} = useContext(PersonContext).state;
    const {dispatch} = useContext(GroupContext);
    const snackDispatch = useContext(SnackbarContext).dispatch;
    const [inputData, setInputData] = useState({
        personEmail: ""
    });

    function handleChange(event) {
        const {name, value} = event.target;
        setInputData(prevInputData => {
            return {
                ...prevInputData,
                [name]: value
            }
        })
    }

    function handleSubmit(event) {
        event.preventDefault();
        fetch(`${URL_API}/groups/${groupId}/members`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(inputData)
        }).then(res => res.json()).then(res => {
            console.log(res);
            res.message === undefined ?
                snackDispatch(showSuccessfulSnackbar(MEMBER_SUCCESSFULLY_ADDED)) :
                snackDispatch(showErrorSnackbar(MEMBER_ADDITION_FAILED + res.message))
        })
            .catch(error => {
                console.log(error);
                snackDispatch(showErrorSnackbar(MEMBER_ADDITION_FAILED + error.message))
            }).then(() => {
            setInputData(prevInputData => {
                return {
                    personEmail: ""
                }
            });
            dispatch(fetchGroupMembersAfterPost())
        })

    }

    return (
        <form onSubmit={handleSubmit} id="groupMemberForm">
            <div className="ModalHeader">
                <p>Add new Member</p>
            </div>
            <div>
                <label className="labelForm"
                       htmlFor="personEmail">Email:</label>
                <br/>
                <Input type="text" name="personEmail" value={inputData.personEmail}
                       onChange={handleChange} required/>
            </div>
            <br/>
            <button className="buttonForm"
                    id="submitNewMember">Submit
            </button>
        </form>
    )
}

export default FormGroupMembers;