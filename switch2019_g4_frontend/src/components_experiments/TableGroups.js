import React, {useContext, useEffect} from "react";
import PersonContext from "../context/PersonContext";
import {fetchGroupsFailure, fetchGroupsStarted, fetchGroupsSuccess, URL_API} from "../context/PersonActions";
import TableGroupsHeader from "./TableGroupsHeader";
import TableGroupsBody from "./TableGroupsBody";

function TableGroups(props) {
    const {state, dispatch} = useContext(PersonContext);
    const {groups} = state;
    const {loading, error, data} = groups;

    useEffect(() => {
        dispatch(fetchGroupsStarted());
        fetch(`${URL_API}/people/${props.user}/groups`)
            .then(res => res.json())
            .then(res => dispatch(fetchGroupsSuccess(res.groupsDTOs)))
            .catch(err => dispatch(fetchGroupsFailure(err.message)))
    }, []);

    if (loading === true) {
        return (<h1>Loading ....</h1>);
    } else {
        if (error !== null) {
            return (<h1>Error ....</h1>);
        } else {
            if (data.length > 0) {
                return (
                    <div>
                        <table border="1">
                            <TableGroupsHeader/>
                            <TableGroupsBody/>
                        </table>
                    </div>
                );
            } else {
                return (<h1>No data ....</h1>);
            }
        }
    }
}

export default TableGroups