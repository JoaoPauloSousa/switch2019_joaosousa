package project.model;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.entities.CreationDate;
import project.model.entities.shared.Designation;

import java.time.format.DateTimeParseException;

import static org.junit.jupiter.api.Assertions.*;

class CreationDateTest {

    /**
     * Test for CreationDate Constructor
     * Happy case (valid BirthDate)
     */
    @Test
    @DisplayName("Test for CreationDate constructor - Happy case")
    void CreationDateConstructorHappyCaseTest() {
        CreationDate football = new CreationDate("1993-03-15");
        assertTrue(football instanceof CreationDate);
    }

    /**
     * Test for CreationDate Constructor
     * Exception with invalid BirthDate - null case
     */
    @Test
    @DisplayName("Test for the constructor - Ensure NullPointerException with null BirthDate")
    void CreationDateConstructorEnsureExceptionWithNullTest() {
        assertThrows(NullPointerException.class, () -> {
            CreationDate football = new CreationDate(null);
        });
    }

    /**
     * Test for CreationDate Constructor
     * Exception with invalid BirthDate - empty String
     */
    @Test
    @DisplayName("Test for the constructor - Ensure DateTimeParseException with empty BirthDate")
    void CreationDateConstructorEnsureExceptionWithEmptyTest() {
        assertThrows(DateTimeParseException.class, () -> {
            CreationDate football = new CreationDate("");
        });
    }

    /**
     * Test for CreationDate Constructor
     * Exception with invalid BirthDate - only whitespaces
     */
    @Test
    @DisplayName("Test for the constructor - Ensure DateTimeParseException with only whitespaces BirthDate")
    void CreationDateConstructorEnsureExceptionWithOnlyWhitespacesTest() {
        assertThrows(DateTimeParseException.class, () -> {
            CreationDate football = new CreationDate("     ");
        });
    }

    /**
     * Test for CreationDate Constructor
     * Exception with invalid BirthDate - illegal pattern date
     */
    @Test
    @DisplayName("Test for the constructor - Ensure DateTimeParseException with illegal pattern BirthDate")
    void CreationDateConstructorEnsureExceptionWithIllegalPatternTest() {
        assertThrows(DateTimeParseException.class, () -> {
            CreationDate football = new CreationDate("12-12-2000");
        });
    }

    /**
     * Test for CreationDate equals
     * Happy case
     */
    @Test
    @DisplayName("Test for equals - Ensure true with same Object")
    void CreationDateEqualsSameObjectTest() {
        //Arrange
        CreationDate football = new CreationDate("1993-03-15");
        //Act
        boolean result = football.equals(football);
        //Assert
        assertTrue(result);
    }

    /**
     * Test for CreationDate equals
     * Happy case
     */
    @Test
    @DisplayName("Test for equals - Ensure true with objects with same attribute")
    void CreationDateEqualsSameObjectsTest() {
        //Arrange
        CreationDate football = new CreationDate("1993-03-15");
        CreationDate volley = new CreationDate("1993-03-15");
        //Act
        boolean result = volley.equals(football);
        //Assert
        assertTrue(result);
    }

    /**
     * Test for CreationDate equals
     * Null object
     */
    @Test
    @DisplayName("Test for equals - Ensure false comparing a valid object with a null object")
    void CreationDateNullTest() {
        //Arrange
        CreationDate football = null;
        CreationDate volley = new CreationDate("1993-03-15");
        //Act
        boolean result = volley.equals(football);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for CreationDate equals
     * Null object
     */
    @Test
    @DisplayName("Test for equals - Ensure false with objects from different classes")
    void CreationDateObjectsDifferentClassTest() {
        //Arrange
        CreationDate football = new CreationDate("1993-03-15");
        Designation food = new Designation("Pizza");
        //Act
        boolean result = football.equals(food);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for CreationDate hashcode
     * Happy case
     */
    @Test
    @DisplayName("Test for hashcode - Happy Case")
    void CreationDateHashCodeEnsureTrueTest() {
        CreationDate football = new CreationDate("2019-03-15");
        int expectedHash = 4135150;
        int actualHash = football.hashCode();
        assertEquals(expectedHash, actualHash);
    }

    /**
     * Test for CreationDate hashcode
     * Ensure Not Equals
     */
    @Test
    @DisplayName("Test for hashcode - Ensure Not Equals")
    void CreationDateHashCodeEnsureNotEqualsTest() {
        CreationDate football = new CreationDate("1993-03-15");
        int expectedHash = -1111;
        int actualHash = football.hashCode();
        assertNotEquals(expectedHash, actualHash);
    }


    /**
     * Test for CreationDate method toString
     * Happy Case
     */
    @Test
    @DisplayName("Test toString - Ensure Equals")
    void CreationDateToStringHappyCaseTest() {
        CreationDate football = new CreationDate("1993-03-15");
        String expected = "CreationDate{creationDate='1993-03-15'}";
        String result = football.toString();
        assertEquals(expected, result);
    }

    /**
     * Test for CreationDate method toString
     * Ensure Not Equals
     */
    @Test
    @DisplayName("Test for toString - Ensure Equals")
    void CreationDateToStringEnsureNotEqualsTest() {
        CreationDate football = new CreationDate("1993-03-15");
        String expected = "asdfg";
        String result = football.toString();
        assertNotEquals(expected, result);
    }

    /**
     * Test for CreationDate method toStringDTO
     * Happy Case
     */
    @Test
    @DisplayName("Test for toStringDTO - Happy Case")
    void CreationDateToStringDTOHappyCaseTest() {
        CreationDate football = new CreationDate("1993-03-15");
        String expected = "1993-03-15";
        String result = football.toStringDTO();
        assertEquals(expected, result);
    }

    /**
     * Test for CreationDate method toStringDTO
     * Ensure Not Equals
     */
    @Test
    @DisplayName("Test for group creation date - Ensure Equals")
    void CreationDateToStringDTOEnsureNotEqualsTest() {
        CreationDate football = new CreationDate("1993-03-15");
        String expected = "asdfg";
        String result = football.toStringDTO();
        assertNotEquals(expected, result);
    }

}