package project.model;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.exceptions.InvalidFieldException;
import project.model.entities.Categories;
import project.model.entities.shared.Category;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class CategoriesTest {
    /**
     * Test for constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for Categories constructor - HappyCase")
    void CategoriesConstructorHappyCaseTest() {
        Categories categories = new Categories();
        assertTrue(categories instanceof Categories);
    }

    /**
     * Test for addCategory
     * Happy case
     */
    @Test
    @DisplayName("Test for addCategory - Happy Case")
    void addCategoryToEmptyCategoriesTest() {
        // ARRANGE
        Categories categories = new Categories();
        String expected = "Categories{categories=[Category{designation=Designation{designation='rent'}}]}";
        // ACT
        boolean result = categories.addCategory("rent");
        String actual = categories.toString();
        // ASSERT
        assertTrue(result);
        assertEquals(expected, actual);
    }

    /**
     * Test for addCategory
     * Sad case
     */
    @Test
    @DisplayName("Test for addCategory - Ensure false if category already exists")
    void addCategoryAlreadyExistentToCategoriesTest() {
        // ARRANGE
        Categories categories = new Categories();
        String expected = "Categories{categories=[Category{designation=Designation{designation='rent'}}]}";
        // ACT
        categories.addCategory("rent");
        boolean result = categories.addCategory("rent");
        String actual = categories.toString();
        // ASSERT
        assertFalse(result);
        assertEquals(expected, actual);
    }

    /**
     * Test for addCategory
     * Trying to add a category after already having one on the list of categories
     */
    @Test
    @DisplayName("Test for addCategory - Ensure true for already filled listOfCategories")
    void addSeveralCategoriesToCategoriesTest() {
        // ARRANGE
        Categories categories = new Categories();
        String expected = "Categories{categories=[Category{designation=Designation{designation='rent'}}, " +
                "Category{designation=Designation{designation='food'}}]}";
        // ACT
        categories.addCategory("rent");
        boolean result = categories.addCategory("food");
        String actual = categories.toString();
        // ASSERT
        assertTrue(result);
        assertEquals(expected, actual);
    }

    /**
     * Test for addCategory
     * Test exception for null category
     */
    @Test
    @DisplayName("Test for addCategory - Ensure exception for null category ")
    void addNullCategoryTest() {
        // ARRANGE
        Categories categories = new Categories();
        // ACT
        // ASSERT
        assertThrows(InvalidFieldException.class, () -> {
            categories.addCategory(null);
        });
    }

    /**
     * Test for hasCategory
     * Ensure true
     */
    @Test
    @DisplayName("Test for hasCategory - Ensure true ")
    void hasCategoryEnsureTrueTest() {
        // ARRANGE
        Categories categories = new Categories();
        categories.addCategory("Groceries");
        Category newCategory = new Category("Groceries");
        String expected = "Categories{categories=[Category{designation=Designation{designation='Groceries'}}]}";
        // ACT
        boolean actual = categories.hasCategory(newCategory);
        String result = categories.toString();
        // ASSERT
        assertTrue(actual);
        assertEquals(expected, result);
    }

    /**
     * Test for hasCategory
     * Ensure false
     */
    @Test
    @DisplayName("Test for hasCategory - Ensure false ")
    void hasCategoryEnsureFalseTest() {
        // ARRANGE
        Categories categories = new Categories();
        categories.addCategory("Groceries");
        Category newCategory = new Category("Pharmacy");
        String expected = "Categories{categories=[Category{designation=Designation{designation='Groceries'}}]}";
        // ACT
        boolean actual = categories.hasCategory(newCategory);
        String result = categories.toString();
        // ASSERT
        assertFalse(actual);
        assertEquals(expected, result);
    }

    /**
     * Test for hasCategory
     * Ensure exception with null
     */
    @Test
    @DisplayName("Test for hasCategory - Ensure exception with null category")
    void hasCategoryNullCategoryTest() {
        // ARRANGE
        Categories categories = new Categories();
        // ACT
        // ASSERT
        assertThrows(InvalidFieldException.class, () -> {
            categories.hasCategory(null);
        });
    }

    /**
     * Test for toString - empty HashSet
     */
    @Test
    @DisplayName("Test for toString - empty HashSet ")
    void toStringEmptyCategoriesTest() {
        // ARRANGE
        Categories categories = new Categories();
        String expected = "Categories{categories=[]}";
        // ACT
        String result = categories.toString();
        // ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for toString - one category
     */
    @Test
    @DisplayName("Test for toString - one category ")
    void toStringOneCategoryTest() {
        // ARRANGE
        Categories categories = new Categories();
        categories.addCategory("rent");
        String expected = "Categories{categories=[Category{designation=Designation{designation='rent'}}]}";
        // ACT
        String result = categories.toString();
        // ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for toString - Several categories
     */
    @Test
    @DisplayName("Test for toString - several categories ")
    void toStringSeveralCategoriesTest() {
        // ARRANGE
        Categories categories = new Categories();
        categories.addCategory("rent");
        categories.addCategory("food");
        String expected = "Categories{categories=[Category{designation=Designation{designation='rent'}}, " +
                "Category{designation=Designation{designation='food'}}]}";
        // ACT
        String result = categories.toString();
        // ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for getCategories
     * Happy Case
     */
    @Test
    @DisplayName("Test for getCategories ")
    void getCategoriesTest() {
        // ARRANGE
        Categories categories = new Categories();
        categories.addCategory("rent");
        categories.addCategory("food");

        Set<Category> expected = new HashSet<>();
        Category rent = new Category("rent");
        Category food = new Category("food");
        expected.add(rent);
        expected.add(food);

        // ACT
        Set<Category> result = categories.getCategoriesValue();

        // ASSERT
        assertEquals(expected, result);
    }



}