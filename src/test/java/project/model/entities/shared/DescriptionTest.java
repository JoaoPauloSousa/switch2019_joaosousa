package project.model.entities.shared;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.exceptions.InvalidFieldException;
import project.model.entities.account.Account;

import static org.junit.jupiter.api.Assertions.*;

class DescriptionTest {

    /**
     * Test for AccountDescription constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for DescriptionAccount constructor - HappyCase")
    void accountDescriptionHappyCaseTest() {
        Description description = new Description("Home");
        assertTrue(description instanceof Description);
    }

    /**
     * Test for AccountDescription constructor
     * Exception with Null
     */
    @Test
    @DisplayName("Test for DescriptionAccount constructor - null test")
    void accountDescriptionNullTest() {
        assertThrows(InvalidFieldException.class, () -> {
            Description description = new Description(null);
        });
    }

    /**
     * Test for AccountDescription constructor
     * Exception with Empty String
     */
    @Test
    @DisplayName("Test for DescriptionAccount constructor - empty test")
    void accountDescriptionEmptyTest() {
        assertThrows(InvalidFieldException.class, () -> {
            Description description = new Description("");
        });
    }

    /**
     * Test for AccountDescription constructor
     * Exception with Only Spaces String
     */
    @Test
    @DisplayName("Test for DescriptionAccount constructor - Only Spaces test")
    void accountDescriptionOnlySpacesTest() {
        assertThrows(InvalidFieldException.class, () -> {
            Description description = new Description("      ");
        });
    }

    /**
     * Test for AccountDescription toString
     * HappyCase
     */
    @Test
    @DisplayName("Test for AccountDescription toString - HappyCase")
    void AccountDescriptionToStringHappyCaseTest() {
        //Arrange
        Description newDescription = new Description("Esta é a conta principal do utilizador");
        String expected = "Description{description='Esta é a conta principal do utilizador'}";
        //Act
        String actual = newDescription.toString();
        //Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for AccountDescription equals
     * Happy case
     */
    @Test
    @DisplayName("Test for AccountDescription - HappyCase")
    void accountDescriptionEqualsHappyCaseTest() {
        //Arrange
        Description desc = new Description("Home");
        Description desc2 = new Description("Home");
        //Act
        boolean result = desc.equals(desc2);
        //Assert
        assertTrue(result);
    }

    /**
     * Test for AccountDescription equals
     * Ensure false with null object
     */
    @Test
    @DisplayName("Test for AccountDescription - Null object")
    void accountDescriptionEqualsNullTest() {
        //Arrange
        Description desc = new Description("Home");
        Description desc2 = null;
        //Act
        boolean result = desc.equals(desc2);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for AccountDescription equals
     * Ensure false with diferent object
     */
    @Test
    @DisplayName("Test for AccountDescription - Diferent Objects")
    void accountDescriptionEqualsDifferentObjectTest() {
        //Arrange
        Description desc = new Description("Family");
        AccountID accountID = new AccountID("1");
        Account acc = new Account(accountID, "Home", "Family");
        //Act
        boolean result = desc.equals(acc);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for AccountDescription equals
     * Ensure if is same object
     */
    @Test
    @DisplayName("Test for AccountDescription - Diferent Objects")
    void accountDescriptionEqualsSameObjectTest() {
        //Arrange
        Description desc = new Description("Home");
        //Act
        boolean result = desc.equals(desc);
        //Assert
        assertTrue(result);
    }

    /**
     * Test for getDescription
     * Happy Case
     */
    @Test
    void getDescriptionHappyCaseTest() {
        //Arrange
        Description description = new Description("description");
        String expected = "description";
        //Act
        String result = description.getDescriptionValue();
        //Assert
        assertEquals(expected,result);
    }

    /**
     * Test for getDescription
     * Ensure not equals
     */
    @Test
    void getDescriptionEnsureNotEqualsTest() {
        //Arrange
        Description description = new Description("description");
        String expected = "asdf";
        //Act
        String result = description.getDescriptionValue();
        //Assert
        assertNotEquals(expected,result);
    }
}