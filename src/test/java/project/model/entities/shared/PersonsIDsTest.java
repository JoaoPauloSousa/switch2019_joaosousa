package project.model.entities.shared;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class PersonsIDsTest {

    PersonsIDs personsIDs;
    PersonID anaID;
    PersonID ruiID;
    PersonID susanaID;

    @BeforeEach
    public void init() {
        personsIDs = new PersonsIDs();
        anaID = new PersonID("1@switch.pt");
        ruiID = new PersonID("2@switch.pt");
        susanaID = new PersonID("3@switch.pt");
    }

    /**
     * Test for PersonID Constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for PersonID constructor - Happy case")
    void PersonIDConstructorHappyCaseTest() {
        assertTrue(personsIDs instanceof PersonsIDs);
    }

    @Test
    @DisplayName("Test for addPersonID - Happy Case")
    void addPersonIDHappyCaseTest() {
        //ARRANGE
        String stringExpected = "Persons{persons=[PersonID{personID=1@switch.pt}]}";
        Integer sizeExpected = 1;
        //ACT
        Boolean actual = personsIDs.addPersonID(anaID);
        String stringResult = personsIDs.toString();
        Integer sizeResult = personsIDs.size();
        //ASSERT
        assertTrue(actual);
        assertEquals(stringExpected, stringResult);
        assertEquals(sizeExpected, sizeResult);
    }

    @Test
    @DisplayName("Test for addPersonID - Add more than one PersonID- Happy Case")
    void addMoreThanOnePersonIDHappyCaseTest() {
        //ARRANGE
        String stringExpected = "Persons{persons=[PersonID{personID=1@switch.pt}, PersonID{personID=2@switch.pt}]}";
        Integer sizeExpected = 2;
        //ACT
        personsIDs.addPersonID(ruiID);
        Boolean actual = personsIDs.addPersonID(anaID);
        String stringResult = personsIDs.toString();
        Integer sizeResult = personsIDs.size();
        //ASSERT
        assertTrue(actual);
        assertEquals(stringExpected, stringResult);
        assertEquals(sizeExpected, sizeResult);
    }

    @Test
    @DisplayName("Test for addPersonID - Ensure false if the the personID to be added already exists")
    void addPersonIDEnsureFalseIfPersonIDAlreadyExistsTest() {
        //ARRANGE
        String stringExpected = "Persons{persons=[PersonID{personID=2@switch.pt}]}";
        Integer sizeExpected = 1;
        //ACT
        personsIDs.addPersonID(ruiID);
        Boolean actual = personsIDs.addPersonID(ruiID);
        String stringResult = personsIDs.toString();
        Integer sizeResult = personsIDs.size();
        //ASSERT
        assertFalse(actual);
        assertEquals(stringExpected, stringResult);
        assertEquals(sizeExpected, sizeResult);
    }

    @Test
    @DisplayName("Test for containsPersonID - HappyCase")
    void containsPersonIDHappyCaseTest() {
        //ARRANGE
        //ACT
        personsIDs.addPersonID(ruiID);
        Boolean actual = personsIDs.containsPersonID(ruiID);
        //ASSERT
        assertTrue(actual);
    }

    @Test
    @DisplayName("Test for containsPersonID - More than one personID in the set - HappyCase")
    void containsPersonIDMoreThanOneHappyCaseTest() {
        //ARRANGE
        //ACT
        personsIDs.addPersonID(ruiID);
        personsIDs.addPersonID(susanaID);
        personsIDs.addPersonID(anaID);
        Boolean actual = personsIDs.containsPersonID(ruiID);
        //ASSERT
        assertTrue(actual);
    }

    @Test
    @DisplayName("Test for containsPersonID - PersonID dont exist in the list text")
    void notContainsPersonIDHappyCaseTest() {
        //ARRANGE
        //ACT
        personsIDs.addPersonID(susanaID);
        personsIDs.addPersonID(anaID);
        Boolean actual = personsIDs.containsPersonID(ruiID);
        //ASSERT
        assertFalse(actual);
    }

    @Test
    @DisplayName("Test for size method - Empty List")
    void sizeEmptyListTest() {
        //ARRANGE
        Integer expected = 0;
        //ACT
        Integer result = personsIDs.size();
        //ASSERT
        assertEquals(expected, result);
    }

    @Test
    @DisplayName("Test for size method - Filled List")
    void sizeFilledListTest() {
        //ARRANGE
        personsIDs.addPersonID(anaID);
        personsIDs.addPersonID(ruiID);
        personsIDs.addPersonID(susanaID);
        Integer expected = 3;
        //ACT
        Integer result = personsIDs.size();
        //ASSERT
        assertEquals(expected, result);
    }

    @Test
    @DisplayName("Test for size method - Ensure False")
    void sizeFilledListEnsureFalseTest() {
        //ARRANGE
        personsIDs.addPersonID(anaID);
        personsIDs.addPersonID(ruiID);
        personsIDs.addPersonID(susanaID);
        Integer expected = 1;
        //ACT
        Integer result = personsIDs.size();
        //ASSERT
        assertNotEquals(expected, result);
    }

    @Test
    @DisplayName("Test for size method - Filled List")
    void getPersonsIDSHappyCaseTest() {
        //ARRANGE
        personsIDs.addPersonID(anaID);
        personsIDs.addPersonID(ruiID);
        personsIDs.addPersonID(susanaID);
        Set<PersonID> expected = new HashSet<>();
        expected.add(ruiID);
        expected.add(susanaID);
        expected.add(anaID);
        //ACT
        Set<PersonID> result = personsIDs.getPersonIDs();
        //ASSERT
        assertEquals(expected, result);
    }

    @Test
    @DisplayName("Test for size method - EmptyList")
    void getPersonsIDSEmptyListTest() {
        //ARRANGE
        Set<PersonID> expected = new HashSet<>();
        //ACT
        Set<PersonID> result = personsIDs.getPersonIDs();
        //ASSERT
        assertEquals(expected, result);
    }

    @Test
    @DisplayName("Test for size method - EmptyList")
    void getPersonsIDSEnsureNotEqualsTest() {
        //ARRANGE
        personsIDs.addPersonID(anaID);
        Set<PersonID> expected = new HashSet<>();
        //ACT
        Set<PersonID> result = personsIDs.getPersonIDs();
        //ASSERT
        assertNotEquals(expected, result);
    }

    @Test
    @DisplayName("Test for to string - empty list")
    void toStringEmptyListTest() {
        //ARRANGE
        String expected = "Persons{persons=[]}";
        //ACT
        String result = personsIDs.toString();
        //ASSERT
        assertEquals(expected,result);
    }

    @Test
    @DisplayName("Test for to string - filled list")
    void toStringFilledListTest() {
        //ARRANGE
        personsIDs.addPersonID(susanaID);
        personsIDs.addPersonID(anaID);
        String expected = "Persons{persons=[PersonID{personID=1@switch.pt}, PersonID{personID=3@switch.pt}]}";
        //ACT
        String result = personsIDs.toString();
        //ASSERT
        assertEquals(expected,result);
    }

}


