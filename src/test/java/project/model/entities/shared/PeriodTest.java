package project.model.entities.shared;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.exceptions.InvalidFieldException;
import project.model.entities.account.Account;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class PeriodTest {

    /**
     * Test for constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for the constructor - Happy Case")
    void constructorHappyCaseTest() {
        Period period = new Period("2020-01-01 00:00:00", "2021-01-01 00:00:00");
        assertTrue(period instanceof Period);
    }

    /**
     * Test for constructor
     * Ensure exception for invalid initialDateTime format
     */
    @Test
    @DisplayName("Test for the constructor - Ensure exception for invalid initialDateTime format")
    void constructorEnsureExceptionInvalidInitialDateTimeFormatTest() {
        assertThrows(InvalidFieldException.class, () -> {
            new Period("2020-01-01", "2021-01-01 00:00:00");
        });
    }

    /**
     * Test for constructor
     * Ensure exception for invalid finalDateTime format
     */
    @Test
    @DisplayName("Test for the constructor - Ensure exception for invalid finalDateTime format")
    void constructorEnsureExceptionInvalidFinalDateTimeFormatTest() {
        assertThrows(InvalidFieldException.class, () -> {
            new Period("2020-01-01 00:00:00", "2021-01-01");
        });
    }

    /**
     * Test for constructor
     * Ensure exception for invalid period
     */
    @Test
    @DisplayName("Test for the constructor - Ensure exception for invalid period")
    void constructorEnsureExceptionInvalidPeriodTest() {
        assertThrows(InvalidFieldException.class, () -> {
            new Period("2020-01-01 00:00:00", "2019-01-01 00:00:00");
        });
    }

    /**
     * Test for constructor
     * Ensure exception for invalid period - Equal dates
     */
    @Test
    @DisplayName("Test for the constructor - Ensure exception if dates are equal")
    void constructorEnsureExceptionEqualDatesPeriodTest() {
        assertThrows(InvalidFieldException.class, () -> {
            new Period("2020-01-01 00:00:00", "2020-01-01 00:00:00");
        });
    }

    /**
     * Test for isDateTimeWithinPeriod method
     * Ensure true when date is within the period limits
     */
    @Test
    void isDateTimeWithinPeriodEnsureTrueWhenDateIsWithinPeriodLimitsTest() {
        //Arrange
        LocalDateTime localDateTime = LocalDateTime.of(2050,01,01,00,00,00);
        Period period = new Period("2000-01-01 00:00:00", "2100-01-01 00:00:00");

        //Act
        boolean actual = period.isDateTimeWithinPeriod(localDateTime);

        //Assert
        assertTrue(actual);
    }

    /**
     * Test for isDateTimeWithinPeriod method
     * Ensure true when date is equals the lower period limit
     */
    @Test
    void isDateTimeWithinPeriodEnsureTrueWhenDateEqualsLowerLimitTest() {
        //Arrange
        LocalDateTime localDateTime = LocalDateTime.of(2000,01,01,00,00,00);
        Period period = new Period("2000-01-01 00:00:00", "2100-01-01 00:00:00");

        //Act
        boolean actual = period.isDateTimeWithinPeriod(localDateTime);

        //Assert
        assertTrue(actual);
    }

    /**
     * Test for isDateTimeWithinPeriod method
     * Ensure true when date is equals the higher period limit
     */
    @Test
    void isDateTimeWithinPeriodEnsureTrueWhenDateEqualsHigherLimitTest() {
        //Arrange
        LocalDateTime localDateTime = LocalDateTime.of(2100,01,01,00,00,00);
        Period period = new Period("2000-01-01 00:00:00", "2100-01-01 00:00:00");

        //Act
        boolean actual = period.isDateTimeWithinPeriod(localDateTime);

        //Assert
        assertTrue(actual);
    }

    /**
     * Test for isDateTimeWithinPeriod method
     * Ensure false when date is below the lower period limit
     */
    @Test
    void isDateTimeWithinPeriodEnsureTrueWhenDateIsBelowLowerLimitTest() {
        //Arrange
        LocalDateTime localDateTime = LocalDateTime.of(1900,01,01,00,00,00);
        Period period = new Period("2000-01-01 00:00:00", "2100-01-01 00:00:00");

        //Act
        boolean actual = period.isDateTimeWithinPeriod(localDateTime);

        //Assert
        assertFalse(actual);
    }

    /**
     * Test for isDateTimeWithinPeriod method
     * Ensure false when date is above the higher period limit
     */
    @Test
    void isDateTimeWithinPeriodEnsureTrueWhenDateIsAboveTheHigherLimitTest() {
        //Arrange
        LocalDateTime localDateTime = LocalDateTime.of(2500,01,01,00,00,00);
        Period period = new Period("2000-01-01 00:00:00", "2100-01-01 00:00:00");

        //Act
        boolean actual = period.isDateTimeWithinPeriod(localDateTime);

        //Assert
        assertFalse(actual);
    }

    /**
     * Test for equals
     * Ensure true with same instance
     */
    @Test
    @DisplayName("Test for equals - Ensure true with same instance")
    void equalsHappyCaseTest() {
        Period mainA = new Period("2019-01-01 00:00:00", "2020-01-01 00:00:00");
        boolean result = mainA.equals(mainA);
        assertTrue(result);
    }

    /**
     * Test for equals
     * Ensure false with null object
     */
    @Test
    @DisplayName("Test for equals - Ensure false with null object")
    void equalsEnsureFalseWithNullTest() {
        Period mainA = new Period("2019-01-01 00:00:00", "2020-01-01 00:00:00");
        Period mainB = null;
        boolean result = mainA.equals(mainB);
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false with different class object
     */
    @Test
    @DisplayName("Test for equals - Ensure false with different class object")
    void equalsEnsureFalseWithDifferentClassObjectTest() {
        Period mainA = new Period("2019-01-01 00:00:00", "2020-01-01 00:00:00");
        AccountID accountBID = new AccountID("1");
        Account mainB = new Account(accountBID, "wallet", "coins");
        boolean result = mainA.equals(mainB);
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure true with same attributes
     */
    @Test
    @DisplayName("Test for equals - Ensure true with same attributes")
    void equalsEnsureTrueWithSameAttributesTest() {
        Period mainA = new Period("2019-01-01 00:00:00", "2021-01-01 00:00:00");
        Period mainB = new Period("2019-01-01 00:00:00", "2021-01-01 00:00:00");
        boolean result = mainA.equals(mainB);
        assertTrue(result);
    }

    /**
     * Test for equals
     * Ensure false with different initialDateTime
     */
    @Test
    @DisplayName("Test for equals - Ensure false with different initialDateTime")
    void equalsEnsureFalseWithDifferentInitialDateTimeTest() {
        Period mainA = new Period("2019-01-01 00:00:00", "2021-01-01 00:00:00");
        Period mainB = new Period("2020-01-01 00:00:00", "2021-01-01 00:00:00");
        boolean result = mainA.equals(mainB);
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false with different finalDateTime
     */
    @Test
    @DisplayName("Test for equals - Ensure false with different finalDateTime")
    void equalsEnsureFalseWithDifferentFinalDateTimeTest() {
        Period mainA = new Period("2019-01-01 00:00:00", "2021-01-01 00:00:00");
        Period mainB = new Period("2019-01-01 00:00:00", "2020-01-01 00:00:00");
        boolean result = mainA.equals(mainB);
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false with different attributes
     */
    @Test
    @DisplayName("Test for equals - Ensure false with different attributes")
    void equalsEnsureFalseWithDifferentAttributesTest() {
        Period mainA = new Period("2019-01-01 00:00:00", "2021-01-01 00:00:00");
        Period mainB = new Period("2020-01-01 00:00:00", "2022-01-01 00:00:00");
        boolean result = mainA.equals(mainB);
        assertFalse(result);
    }

    /**
     * Test for hashcode
     * Happy case
     */
    @Test
    @DisplayName("Test for hashcode - Ensure true with same attributes")
    void hashCodeEnsureTrueTest() {
        Period mainA = new Period("2019-01-01 00:00:00", "2020-01-01 00:00:00");
        int expectedHash = 132322273;
        int actualHash = mainA.hashCode();
        assertEquals(expectedHash, actualHash);
    }

    /**
     * Test for toString
     * Ensure true
     */
    @Test
    @DisplayName("Test for toString - Ensure true")
    void toStringEnsureTrueTest() {
        // Arrange
        Period mainA = new Period("2019-01-01 00:00:00", "2020-01-01 00:00:00");
        String expected = "Period{initialDateTime=2019-01-01T00:00, finalDateTime=2020-01-01T00:00}";

        // Act
        String actual = mainA.toString();

        // Assert
        assertEquals(expected, actual);
    }

}