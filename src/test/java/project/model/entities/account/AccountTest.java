package project.model.entities.account;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.exceptions.InvalidFieldException;
import project.model.entities.shared.AccountID;
import project.model.entities.shared.Category;
import project.model.entities.shared.Description;

import static org.junit.jupiter.api.Assertions.*;


class AccountTest {

    /**
     * Test for constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for constructor - HappyCase")
    void constructorHappyCaseTest() {
        AccountID id = new AccountID("7");
        Account main = new Account(id, "Main", "overheads");
        assertTrue(main instanceof Account);
    }

    /**
     * Test for constructor
     * Exception with null denomination
     */
    @Test
    @DisplayName("Test for constructor - Ensure exception with null Denomination")
    void constructorEnsureExceptionWithNullDenominationTest() {
        assertThrows(InvalidFieldException.class, () -> {
            AccountID id = new AccountID("7");
            Account main = new Account(id, null, "overheads");
        });
    }

    /**
     * Test for constructor
     * Exception with null description
     */
    @Test
    @DisplayName("Test for constructor - Ensure exception with null description")
    void constructorEnsureExceptionWithNullDescriptionTest() {
        assertThrows(InvalidFieldException.class, () -> {
            AccountID id = new AccountID("7");
            Account main = new Account(id, "overheads", null);
        });
    }

    /**
     * Test for constructor
     * Exception with null id
     */
    @Test
    @DisplayName("Test for constructor - Ensure exception with null id")
    void constructorEnsureExceptionWithNullIdTest() {
        assertThrows(InvalidFieldException.class, () -> {
            Account main = new Account(null, "overheads", "overheads");
        });
    }

    /**
     * Test for toString
     * Happy Case
     */
    @Test
    @DisplayName("Test for toString - Happy Case")
    void toStringHappyCaseTest() {
        //Arrange
        AccountID id = new AccountID("7");
        Account newAccount = new Account(id, "Conta Principal", "Esta é a conta principal do utilizador");
        String expected = "Account{id=AccountID{accountID=7}denomination=Denomination{denomination='Conta Principal'}, description=Description{description='Esta é a conta principal do utilizador'}}";
        //Act
        String actual = newAccount.toString();

        //Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for getDenomination
     * Happy case
     */
    @Test
    @DisplayName("Test for getDenomination - HappyCase")
    void getDenominationHappyCaseTest() {
        //Arrange
        AccountID id = new AccountID("7");
        Account main = new Account(id, "Main", "overheads");
        Denomination expected = new Denomination("Main");

        //Act
        Denomination result = main.getDenomination();

        //Arrange
        assertEquals(expected,result);
    }

    /**
     * Test for getDescription
     * Happy case
     */
    @Test
    @DisplayName("Test for getDescription- HappyCase")
    void getDescriptionHappyCaseTest() {
        //Arrange
        AccountID id = new AccountID("7");
        Account main = new Account(id, "Main", "overheads");
        Description expected = new Description("overheads");

        //Act
        Description result = main.getDescription();

        //Arrange
        assertEquals(expected,result);
    }

    /**
     * Test for equals
     * Happy case
     */
    @Test
    @DisplayName("Test forEquals - Same denomination & description")
    void equalsHappyCaseTest() {
        //Arrange
        AccountID id = new AccountID("7");
        Account mainA = new Account(id, "Overheads", "overheads");
        //Act
        boolean result = mainA.equals(mainA);
        //Assert
        assertTrue(result);
    }

    /**
     * Test for equals
     * Ensure false with null object
     */
    @Test
    @DisplayName("Test forEquals - Comparing two instances of Account being one of them null")
    void equalsEnsureFalseWithNullTest() {
        //Arrange
        AccountID id = new AccountID("7");
        Account mainA = new Account(id, "Main", "overheads");
        Account mainB = null;
        //Act
        boolean result = mainA.equals(mainB);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure two objects from different classes are different
     */
    @Test
    @DisplayName("Test forEquals - Confirm that two objects from different classes are not the same")
    void equalsObjectsFromDifferentClasses() {
        //Arrange
        AccountID id = new AccountID("7");
        Account mainA = new Account(id, "Main", "overheads");
        Category mainB = new Category("overheads");
        //Act
        boolean result = mainA.equals(mainB);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure true with same attributes
     */
    @Test
    @DisplayName("Test forEquals - Ensure two objets with same id are equal")
    void equalsEnsureTrueWithSameAttributesTest() {
        //Arrange
        AccountID id = new AccountID("7");
        Account mainA = new Account(id, "Main", "overheads");
        Account mainB = new Account(id, "Main1", "overheads1");
        //Act
        boolean result = mainA.equals(mainB);
        //Assert
        assertTrue(result);
    }

    /**
     * Test for equals
     * Ensure false with different denomination
     */
    @Test
    @DisplayName("Test forEquals - Different denomination")
    void equalsEnsureFalseWithDifferentDenominationTest() {
        //Arrange
        AccountID id = new AccountID("7");
        AccountID id1 = new AccountID("8");
        Account mainA = new Account(id, "Main", "overheads");
        Account mainB = new Account(id1, "NotMain", "overheads");
        //Act
        boolean result = mainA.equals(mainB);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false with different description
     */
    @Test
    @DisplayName("Test forEquals - Different description")
    void equalsEnsureFalseWithDifferentDescriptionTest() {
        //Arrange
        AccountID id = new AccountID("7");
        AccountID id1 = new AccountID("8");
        Account mainA = new Account(id, "Main", "overheads");
        Account mainB = new Account(id1, "Main", "personal");
        //Act
        boolean result = mainA.equals(mainB);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for hashcode
     * Happy Case
     */
    @Test
    @DisplayName("Test for hashcode - Ensure true with same attributes")
    void hashCodeEnsureTrueTest() {
        //arrange
        AccountID id = new AccountID("7");
        Account main = new Account(id, "Main", "overheads");
        int expected = -685154709;
        //act
        int actual = main.hashCode();
        //assert
        assertEquals(expected, actual);
    }

    /**
     * Test for toString
     * Ensure works
     */
    @Test
    @DisplayName("Test for toString - Ensure works")
    void toStringEnsureTrueTest() {
        //arrange
        AccountID id = new AccountID("7");
        Account main = new Account(id, "Main", "overheads");
        String expected = "Account{id=AccountID{accountID=7}denomination=Denomination{denomination='Main'}, description=Description{description='overheads'}}";
        //act
        String actual = main.toString();
        //assert
        assertEquals(expected, actual);
    }

    /**
     * Test for getAccountID
     * Ensure works
     */
    @Test
    @DisplayName("Test for getAccountID - Ensure works")
    void getAccountIDEnsureTrueTest() {
        //arrange
        AccountID id = new AccountID("7");
        Account main = new Account(id, "Main", "overheads");
        AccountID expected = id;

        //act
        AccountID result = main.getID();

        //assert
        assertEquals(expected, result);
    }

    /**
     * Test false for sameAs Method
     */
    @Test
    @DisplayName("Test false for sameAs Method")
    void accountSameAsFalse() {
        //Arrange
        AccountID id = new AccountID("7");
        Account main = new Account(id, "Main", "overheads");
        Denomination newDenomination = new Denomination("Conta Principal");

        //Act
        boolean result = main.sameAs(newDenomination);

        //Assert
        assertFalse(result);
    }

}
