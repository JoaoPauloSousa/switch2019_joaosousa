package project.model.entities.account;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.exceptions.InvalidFieldException;
import project.model.entities.group.GroupID;

import static org.junit.jupiter.api.Assertions.*;

class DenominationTest {

    /**
     * Test for Constructor
     * Happy case (valid Denomination)
     */
    @Test
    @DisplayName("Test for constructor - Happy case")
    void constructorHappyCaseTest() {
        Denomination account1 = new Denomination("Personal");
        assertTrue(account1 instanceof Denomination);
    }

    /**
     * Test for Constructor
     * Exception with invalid Denomination - null case
     */
    @Test
    @DisplayName("Test for constructor - Ensure exception with null Denomination")
    void constructorEnsureExceptionWithNullDenominationTest() {
        assertThrows(InvalidFieldException.class, () -> {
            new Denomination(null);
        });
    }

    /**
     * Test for Constructor
     * Exception with invalid Denomination - empty String
     */
    @Test
    @DisplayName("Test for constructor - Ensure exception with empty Denomination")
    void constructorEnsureExceptionWithEmptyDenominationTest() {
        assertThrows(InvalidFieldException.class, () -> {
            new Denomination("");
        });
    }

    /**
     * Test for Constructor
     * Exception with invalid Denomination - only whitespaces
     */
    @Test
    @DisplayName("Test for constructor - Ensure exception with only whitespaces Denomination")
    void constructorEnsureExceptionWithOnlyWhitespacesDenominationTest() {
        assertThrows(InvalidFieldException.class, () -> {
            new Denomination("     ");
        });
    }

    /**
     * Test for getDenominatio
     * HappyCase
     */
    @Test
    @DisplayName("Test for getDenomination - HappyCase")
    void getDenominationHappyCaseTest() {
        //Arrange
        Denomination newDenomination = new Denomination("Conta Principal");
       String expected = "Conta Principal";

        //Act
        String actual = newDenomination.getDenominationValue();
        //Assert
        assertEquals(expected, actual);
    }



    /**
     * Test for toString
     * HappyCase
     */
    @Test
    @DisplayName("Test for toString - HappyCase")
    void toStringHappyCaseTest() {
        //Arrange
        Denomination newDenomination = new Denomination("Conta Principal");
        String expected = "Denomination{denomination='Conta Principal'}";
        //Act
        String actual = newDenomination.toString();
        //Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for toStringDTO
     * HappyCase
     */
    @Test
    @DisplayName("Test for toStringDTO - HappyCase")
    void toStringDTOHappyCaseTest() {
        //Arrange
        Denomination newDenomination = new Denomination("Conta Principal");
        String expected = "Conta Principal";
        //Act
        String actual = newDenomination.toStringDTO();
        //Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for equals
     * Happy case
     */
    @Test
    @DisplayName("Test for equals - HappyCase")
    void equalsHappyCaseTest() {
        //Arrange
        Denomination denom1 = new Denomination("Home");
        Denomination denom2 = new Denomination("Home");
        //Act
        boolean result = denom1.equals(denom2);
        //Assert
        assertTrue(result);
    }

    /**
     * Test for equals
     * Ensure false with null object
     */
    @Test
    @DisplayName("Test for equals - Ensure false with null")
    void equalsEnsureFalseWithNullTest() {
        //Arrange
        Denomination denom1 = new Denomination("Home");
        Denomination denom2 = null;
        //Act
        boolean result = denom1.equals(denom2);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false with diferent object
     */
    @Test
    @DisplayName("Test for equals - Ensure false with object from another class")
    void equalsEnsureFalseWithObjectFromAnotherClassTest() {
        //Arrange
        Denomination denom1 = new Denomination("Home");
        GroupID groupID = new GroupID("1");
        //Act
        boolean result = denom1.equals(groupID);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure true if is same object
     */
    @Test
    @DisplayName("Test for equals - Ensure true with same object")
    void equalsEnsureTrueWithSameObjectTest() {
        //Arrange
        Denomination denom1 = new Denomination("Home");
        //Act
        boolean result = denom1.equals(denom1);
        //Assert
        assertTrue(result);
    }

    /**
     * Test for hashcode
     * Ensure works
     */
    @Test
    @DisplayName("Test for hashCode - Ensure works")
    void hashCodeEnsureWorksTest() {
        //Arrange
        Denomination denom1 = new Denomination("Home");
        int expected = 2255134;
        //Act
        int result = denom1.hashCode();
        //Assert
        assertEquals(expected, result);
    }

}