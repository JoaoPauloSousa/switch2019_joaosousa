package project.model.entities.ledger;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.exceptions.InvalidFieldException;
import project.model.entities.shared.Designation;

import static org.junit.jupiter.api.Assertions.*;

class AmountTest {

    /**
     * Test for Amount Constructor
     * Happy Case
     */
    @Test
    void AmountHappyCaseTest() {
        Amount result = new Amount("100");
        assertTrue(result instanceof Amount);
    }

    /**
     * Test for Amount Constructor
     * Exception with invalid amount (amount <=0)
     */
    @Test
    void AmountWithNotPositiveAmountTest() {
        assertThrows(InvalidFieldException.class, () -> {
            Amount amount = new Amount("-100");
        });
    }

    /**
     * Test for Amount equals
     * Happy case
     */
    @Test
    @DisplayName("Test for Amount equals - Ensure true with same instance")
    void equalsHappyCase() {
        Amount amount = new Amount("100");
        boolean result = amount.equals(amount);
        assertTrue(result);
    }

    /**
     * Test for Amount equals
     * Ensure false with null object
     */
    @Test
    @DisplayName("Test for Amount equals - Ensure false with null")
    void equalsEnsureFalseWithNullObject() {
        Amount amountA = new Amount("100");
        Amount amountB = null;
        boolean result = amountA.equals(amountB);
        assertFalse(result);
    }

    /**
     * Test for Amount equals
     * Ensure false different class objects
     */
    @Test
    @DisplayName("Test for Amount equals - Ensure false different class objects")
    void equalsEnsureFalseWithDifferentClassObjects() {
        Amount amount = new Amount("100");
        Designation designation = new Designation("Service payment");
        boolean result = amount.equals(designation);
        assertFalse(result);
    }

    /**
     * Test for Amount hashCode
     * Happy Case
     */
    @Test
    @DisplayName("Test for Amount HashCode - Happy case")
    void hashCodeHappyCase() {
        Amount amount = new Amount("100");
        int expected = 1079574559;
        int result = amount.hashCode();
        assertEquals(expected, result);
    }

    /**
     * Test for toString
     * Happy Case
     */
    @Test
    @DisplayName("Test for toString - Happy case")
    void toStringHappyCase() {
        Amount amount = new Amount("100");
        String expected = "Amount{amount=100.0}";
        String result = amount.toString();
        assertEquals(expected, result);
    }

    @Test
    void getAmountHappyCaseTest() {
        //ARRANGE
        Amount amount = new Amount("100");
        double expected = 100;
        //ACT
        double result = amount.getAmountValue();
        //ASSERT
        assertEquals(expected, result);
    }

    @Test
    void setAmountHappyCaseTest() {
        //ARRANGE
        Amount amount = new Amount("100");
        double expected = 200;
        amount.setAmountValue(expected);
        //ACT
        double result = amount.getAmountValue();
        //ASSERT
        assertEquals(expected, result);
    }

}