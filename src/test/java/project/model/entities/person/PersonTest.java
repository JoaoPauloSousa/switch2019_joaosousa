package project.model.entities.person;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.exceptions.InvalidFieldException;
import project.model.entities.shared.AccountID;
import project.model.entities.shared.Category;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.PersonID;

import static org.junit.jupiter.api.Assertions.*;

class PersonTest {

    private PersonID personID;
    private PersonID personID2;
    private PersonID personID3;
    private PersonID personID4;
    private PersonID personID5;
    private PersonID personID6;
    private LedgerID ledgerID;
    private LedgerID ledgerID2;
    private LedgerID ledgerID3;
    private LedgerID ledgerID4;
    private LedgerID ledgerID5;
    private LedgerID ledgerID6;
    private AccountID accountID;

    @BeforeEach
    public void init() {
        personID = new PersonID("100@switch.pt");
        personID2 = new PersonID("2121@switch.pt");
        personID3 = new PersonID("21323@switch.pt");
        personID4 = new PersonID("21321@switch.pt");
        personID5 = new PersonID("6868664@switch.pt");
        personID6 = new PersonID("204818121@switch.pt");
        ledgerID = new LedgerID("121");
        ledgerID2 = new LedgerID("121");
        ledgerID3 = new LedgerID("6453");
        ledgerID4 = new LedgerID("686543");
        ledgerID5 = new LedgerID("645335321");
        ledgerID6 = new LedgerID("681240911");
        accountID = new AccountID("56454154");

    }

    /**
     * Test for Person Constructor
     * Happy case (valid name, valid birthdate)
     */
    @Test
    @DisplayName("Test for Person constructor - Happy case")
    void constructorHappyCaseTest() {
        // ARRANGE
        PersonID idPerson = new PersonID("4@switch.pt");
        LedgerID idLedger = new LedgerID("3");
        Person elsa = new Person(idPerson, "Elsa", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, idLedger);

        // ACT
        // ASSERT
        assertTrue(elsa instanceof Person);
    }

    /**
     * Test for Person Constructor
     * Exception with null PersonID
     */
    @Test
    @DisplayName("Test for the constructor - Ensure IllegalArgumentException with null Id")
    void constructorEnsureExceptionWithNullIDTest() {
        // ARRANGE
        LedgerID idLedger = new LedgerID("4");

        // ACT
        // ASSERT
        assertThrows(NullPointerException.class, () -> {
            Person elsa = new Person(null, "joao", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, idLedger);
        });
    }

    /**
     * Test for Person Constructor
     * Exception with null LedgerID
     */
    @Test
    @DisplayName("Test for the constructor - Ensure IllegalArgumentException with null LedgerId")
    void constructorEnsureExceptionWithNullLedgerIDTest() {
        // ARRANGE
        PersonID idPerson = new PersonID("4@switch.pt");

        // ACT
        // ASSERT
        assertThrows(NullPointerException.class, () -> {
            Person elsa = new Person(idPerson, "joao", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, null);
        });
    }

    /**
     * Test for Person Constructor
     * Exception with invalid name
     */
    @Test
    @DisplayName("Test for the constructor - Ensure exception with invalid name")
    void constructorEnsureExceptionWithInvalidNameTest() {
        // ARRANGE
        PersonID idPerson = new PersonID("4@switch.pt");
        LedgerID idLedger = new LedgerID("3");

        // ACT
        // ASSERT
        assertThrows(InvalidFieldException.class, () -> {
            Person elsa = new Person(idPerson, null, "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, idLedger);
        });
    }

    /**
     * Test for Person Constructor
     * Exception with invalid address
     */
    @Test
    @DisplayName("Test for the constructor - Ensure exception with invalid address")
    void constructorEnsureExceptionWithInvalidAddressTest() {
        // ARRANGE
        PersonID idPerson = new PersonID("4@switch.pt");
        LedgerID idLedger = new LedgerID("3");

        // ACT
        // ASSERT
        assertThrows(InvalidFieldException.class, () -> {
            Person elsa = new Person(idPerson, "Elsa", null, "Porto", "1991-12-22", null, null, idLedger);
        });
    }

    /**
     * Test for Person Constructor
     * Exception with invalid birthplace
     */
    @Test
    @DisplayName("Test for the constructor - Ensure exception with invalid birthplace")
    void constructorEnsureExceptionWithInvalidBirthPlaceTest() {
        // ARRANGE
        PersonID idPerson = new PersonID("4@switch.pt");
        LedgerID idLedger = new LedgerID("3");

        // ACT
        // ASSERT
        assertThrows(InvalidFieldException.class, () -> {
            Person elsa = new Person(idPerson, "Elsa", "Rua Dr. Roberto Frias s/n", null, "1991-12-22", null, null, idLedger);
        });
    }

    /**
     * Test for Person Constructor
     * Exception with invalid birthdate
     */
    @Test
    @DisplayName("Test for the constructor - Ensure IllegalArgumentException with invalid birthdate")
    void constructorEnsureExceptionWithInvalidBirthdateTest() {
        // ARRANGE
        PersonID idPerson = new PersonID("4@switch.pt");
        LedgerID idLedger = new LedgerID("3");

        // ACT
        // ASSERT
        assertThrows(NullPointerException.class, () -> {
            Person elsa = new Person(idPerson, "Elsa", "Rua Dr. Roberto Frias s/n", "Porto", null, null, null, idLedger);
        });
    }

    /**
     * Test for Person equals
     * Ensure false with different name
     */
    @Test
    @DisplayName("Test for the Person Equals - Ensure false with different name")
    void equalsEnsureFalseWithDifferentNameTest() {
        // ARRANGE
        PersonID idPersonA = new PersonID("4@switch.pt");
        LedgerID idLedgerA = new LedgerID("3");
        PersonID idPersonB = new PersonID("5@switch.pt");
        LedgerID idLedgerB = new LedgerID("7");
        Person elsa = new Person(idPersonA, "Elsa", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, idLedgerA);
        Person antonio = new Person(idPersonB, "Antonio", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, idLedgerB);

        // ACT
        // ASSERT
        assertFalse(elsa.equals(antonio));
    }

    /**
     * Test for Person equals
     * Ensure false with different address
     */
    @Test
    @DisplayName("Test for the Person Equals - Ensure false with different address")
    void equalsEnsureFalseWithDifferentAddressTest() {
        // ARRANGE
        PersonID idPersonA = new PersonID("4@switch.pt");
        LedgerID idLedgerA = new LedgerID("3");
        PersonID idPersonB = new PersonID("5@switch.pt");
        LedgerID idLedgerB = new LedgerID("1");
        Person elsaA = new Person(idPersonA, "Elsa", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, idLedgerA);
        Person elsaB = new Person(idPersonB, "Elsa", "Rua das Valongas, 49", "Porto", "1991-12-22", null, null, idLedgerB);

        // ACT
        // ASSERT
        assertFalse(elsaA.equals(elsaB));
    }

    /**
     * Test for Person equals
     * Ensure false with different birthplace
     */
    @Test
    @DisplayName("Test for the Person Equals - Ensure false with different birthplace")
    void equalsEnsureFalseWithDifferentBirthplaceTest() {
        // ARRANGE
        PersonID idPersonA = new PersonID("4@switch.pt");
        LedgerID idLedgerA = new LedgerID("3");
        PersonID idPersonB = new PersonID("5@switch.pt");
        LedgerID idLedgerB = new LedgerID("1");
        Person elsaA = new Person(idPersonA, "Elsa", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, idLedgerA);
        Person elsaB = new Person(idPersonB, "Elsa", "Rua Dr. Roberto Frias s/n", "Paranhos", "1991-12-22", null, null, idLedgerB);

        // ACT
        // ASSERT
        assertFalse(elsaA.equals(elsaB));
    }

    /**
     * Test for Person equals
     * Ensure false with different birthdate
     */
    @Test
    @DisplayName("Test for the Person Equals - Ensure false with different birthdate")
    void equalsEnsureFalseWithDifferentBirthdateTest() {
        // ARRANGE
        PersonID idPersonA = new PersonID("4@switch.pt");
        LedgerID idLedgerA = new LedgerID("3");
        PersonID idPersonB = new PersonID("5@switch.pt");
        LedgerID idLedgerB = new LedgerID("1");
        Person elsaA = new Person(idPersonA, "Elsa", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, idLedgerA);
        Person elsaB = new Person(idPersonB, "Elsa", "Rua Dr. Roberto Frias s/n", "Porto", "1990-10-10", null, null, idLedgerB);

        // ACT
        // ASSERT
        assertFalse(elsaA.equals(elsaB));
    }

    /**
     * Test for Person equals
     * Ensure false with different mother
     */
    @Test
    @DisplayName("Test for the Person Equals - Ensure false with different mother")
    void equalsEnsureFalseWithDifferentMotherTest() {
        // ARRANGE
        PersonID idMother = new PersonID("4@switch.pt");
        LedgerID idLedger = new LedgerID("3");
        PersonID idElsa = new PersonID("5@switch.pt");
        LedgerID idLedger1 = new LedgerID("1");
        PersonID idElsaB = new PersonID("7@switch.pt");
        LedgerID idLedger2 = new LedgerID("8");
        Person elsa = new Person(idElsa, "Elsa", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", idMother, null, idLedger1);
        Person elsaB = new Person(idElsaB, "Elsa", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, idLedger2);

        // ACT
        // ASSERT
        assertFalse(elsa.equals(elsaB));
    }

    /**
     * Test for Person equals
     * Ensure false with different father
     */
    @Test
    @DisplayName("Test for the Person Equals - Ensure false with different father")
    void equalsEnsureFalseWithDifferentFatherTest() {
        // ARRANGE
        PersonID idFather = new PersonID("4@switch.pt");
        LedgerID idLedger = new LedgerID("3");
        PersonID idElsa = new PersonID("5@switch.pt");
        LedgerID idLedger1 = new LedgerID("1");
        PersonID idElsaB = new PersonID("7@switch.pt");
        LedgerID idLedger2 = new LedgerID("8");
        Person elsa = new Person(idElsa, "Elsa", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, idFather, idLedger1);
        Person elsaB = new Person(idElsaB, "Elsa", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, idLedger2);

        // ACT
        // ASSERT
        assertFalse(elsa.equals(elsaB));
    }

    /**
     * Test for Person equals
     * Ensure false with person being null
     */
    @Test
    @DisplayName("Test for the Person Equals - Ensure false with Person being null")
    void equalsEnsureFalseWithNullTest() {
        // ARRANGE
        PersonID idElsaA = new PersonID("5@switch.pt");
        LedgerID idLedger = new LedgerID("1");
        Person elsaA = new Person(idElsaA, "Elsa", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, idLedger);
        Person elsaB = null;

        // ACT
        // ASSERT
        assertFalse(elsaA.equals(elsaB));
    }

    /**
     * Test for the Person Equals - Ensure false with different class
     */
    @Test
    @DisplayName("Test for the Person Equals - Ensure false with different class")
    void equalsEnsureFalseWithOtherClassTest() {
        // ARRANGE
        PersonID idElsaA = new PersonID("5@switch.pt");
        LedgerID idLedger = new LedgerID("1");

        Person elsaA = new Person(idElsaA, "Elsa", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, idLedger);
        Category saude = new Category("Dentista");

        // ACT
        // ASSERT
        assertFalse(elsaA.equals(saude));
    }

    /**
     * Test for the Person Equals - Ensure false with different class - method sameAs
     */
    @Test
    @DisplayName("Test for the Person Equals - Ensure false with different class - method sameAs")
    void equalsEnsureFalseWithOtherClassTestMethodSameAs() {
        // ARRANGE
        PersonID idElsaA = new PersonID("5@switch.pt");
        LedgerID idLedger = new LedgerID("1");
        Person elsaA = new Person(idElsaA, "Elsa", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, idLedger);
        Category saude = new Category("Dentista");

        // ACT
        // ASSERT
        assertFalse(elsaA.sameAs(saude));
    }

    /**
     * Test for the Person Equals - Happy Case
     */
    @Test
    @DisplayName("Test for the Person Equals - Happy Case")
    void equalsHappyCaseTest() {
        // ARRANGE
        PersonID idElsaA = new PersonID("5@switch.pt");
        LedgerID idLedger = new LedgerID("1");
        Person elsaA = new Person(idElsaA, "Elsa", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, idLedger);
        Person elsaB = new Person(idElsaA, "Elsa", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, idLedger);

        // ACT
        // ASSERT
        assertTrue(elsaA.equals(elsaB));
    }

    /**
     * Test for the Person Equals
     * Ensure true with same object
     */
    @Test
    @DisplayName("Test for the Person Equals - Ensure true with same object")
    void equalsEnsureTrueWithSameObjectTest() {
        // ARRANGE
        PersonID idElsaA = new PersonID("5@switch.pt");
        LedgerID idLedger = new LedgerID("1");
        Person elsa = new Person(idElsaA, "Elsa", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, idLedger);

        // ACT
        // ASSERT
        assertTrue(elsa.equals(elsa));
    }

    /**
     * Test for addSibling
     * Happy case
     */
    @DisplayName("Test for addSibling - Happy Case")
    @Test
    void addSiblingHappyCaseTest() {
        // ARRANGE
        PersonID idFather = new PersonID("4@switch.pt");
        PersonID idMother = new PersonID("5@switch.pt");
        PersonID idOldestChild = new PersonID("1@switch.pt");
        LedgerID idLedger2 = new LedgerID("2");
        Person oldestChild = new Person(idOldestChild, "Pedro", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", idMother, idFather, idLedger2);
        PersonID idYoungestChild = new PersonID("9@switch.pt");
        LedgerID idLedger3 = new LedgerID("2");
        Person youngestChild = new Person(idYoungestChild, "Ana", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", idMother, idFather, idLedger3);

        // ACT
        boolean result = oldestChild.addSibling(youngestChild);

        // ASSERT
        assertTrue(result);
    }

    /**
     * Test for addSibling
     * Ensure false if duplicate
     */
    @DisplayName("Test for addSibling - Ensure false if duplicate")
    @Test
    void addSiblingEnsureFalseIfDuplicateTest() {
        // ARRANGE
        PersonID idFather = new PersonID("4@switch.pt");
        PersonID idMother = new PersonID("5@switch.pt");
        PersonID idOldestChild = new PersonID("1@switch.pt");
        LedgerID idLedger2 = new LedgerID("2");
        Person oldestChild = new Person(idOldestChild, "Pedro", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", idMother, idFather, idLedger2);
        PersonID idYoungestChild = new PersonID("9@switch.pt");
        LedgerID idLedger3 = new LedgerID("2");
        Person youngestChild = new Person(idYoungestChild, "Ana", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", idMother, idFather, idLedger3);
        oldestChild.addSibling(youngestChild);

        // ACT
        boolean result = oldestChild.addSibling(youngestChild);

        // ASSERT
        assertFalse(result);
    }

    /**
     * Test for isSibling
     * Ensure true with same mother
     */
    @Test
    @DisplayName("Test for isSibling - Ensure true with same mother")
    void isSiblingEnsureTrueWithSameMotherTest() {
        // ARRANGE
        Person andre = new Person(personID4, "andre", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", personID, personID2, ledgerID4);
        Person joana = new Person(personID5, "joana", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", personID, personID3, ledgerID5);

        // ACT
        boolean result = andre.isSibling(joana);

        // ASSERT
        assertTrue(result);
    }

    /**
     * Test for isSibling
     * Ensure true with same father
     */
    @Test
    @DisplayName("Test for isSibling - Ensure true with same father")
    void isSiblingEnsureTrueWithSameFatherTest() {
        // ARRANGE
        Person andre = new Person(personID4, "andre", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", personID3, personID2, ledgerID4);
        Person joana = new Person(personID5, "joana", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", personID, personID2, ledgerID4);

        // ACT
        boolean result = andre.isSibling(joana);

        // ASSERT
        assertTrue(result);
    }

    /**
     * Test for isSibling
     * Happy case included in sibling list
     */
    @Test
    @DisplayName("Test for isSibling - Ensure true if B included in sibling list of A")
    void isSiblingEnsureTrueIncludedSiblingsListATest() {
        // ARRANGE
        Person andre = new Person(personID5, "andre", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", personID, personID2, ledgerID5);
        Person joana = new Person(personID6, "joana", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", personID4, personID3, ledgerID6);

        // ACT
        andre.addSibling(joana);
        boolean result = andre.isSibling(joana);

        // ASSERT
        assertTrue(result);
    }

    /**
     * Test for isSibling
     * Happy case included in other sibling list
     */
    @Test
    @DisplayName("Test for isSibling - Ensure true if A included in sibling list of B")
    void isSiblingEnsureTrueIncludedSiblingsListBTest() {
        // ARRANGE
        Person andre = new Person(personID5, "andre", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", personID, personID2, ledgerID5);
        Person joana = new Person(personID6, "joana", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", personID4, personID3, ledgerID6);

        // ACT
        andre.addSibling(joana);
        boolean result = joana.isSibling(andre);
        // ASSERT
        assertTrue(result);
    }

    /**
     * Test for isSibling
     * Ensure false
     */
    @Test
    @DisplayName("Test for isSibling - Ensure false (not same mother, father and they are not included in siblings list ")
    void isSiblingEnsureFalseCaseTest() {
        // ARRANGE
        Person maria = new Person(personID4, "maria", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, ledgerID4);
        Person andre = new Person(personID5, "andre", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", personID, personID2, ledgerID5);
        Person joana = new Person(personID6, "joana", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", personID4, personID3, ledgerID6);

        // ACT
        andre.addSibling(maria);
        boolean result = andre.isSibling(joana);

        // ASSERT
        assertFalse(result);
    }

    /**
     * Test for isSibling
     * Ensure false if father is null
     */
    @Test
    @DisplayName("Test for isSibling - Ensure false if father is null")
    void isSiblingEnsureFalseWithNullFatherTest() {
        // ARRANGE
        Person andre = new Person(personID4, "andre", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", personID, null, ledgerID4);
        Person joana = new Person(personID5, "joana", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", personID, personID3, ledgerID5);
        andre.addSibling(joana);
        joana.addSibling(andre);

        // ACT
        boolean result = andre.isSibling(joana);

        // ASSERT
        assertFalse(result);
    }

    /**
     * Test for isSibling
     * Ensure false if mother is null
     */
    @Test
    @DisplayName("Test for isSibling - Ensure false if mother is null")
    void isSiblingEnsureFalseIfMotherIsNullTest() {
        // ARRANGE
        Person andre = new Person(personID4, "andre", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, personID2, ledgerID4);
        Person joana = new Person(personID5, "joana", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", personID, personID2, ledgerID4);
        andre.addSibling(joana);
        joana.addSibling(andre);

        // ACT
        boolean result = andre.isSibling(joana);

        // ASSERT
        assertFalse(result);
    }

    /**
     * Test for isFather
     * Happy Case
     */
    @Test
    @DisplayName("Test for hasFather - Happy Case")
    void isFatherHappyCase() {
        // ARRANGE
        Person joao = new Person(personID, "joao", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, ledgerID);
        Person joana = new Person(personID3, "joana", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", personID2, personID, ledgerID3);

        // ACT
        boolean result = joana.hasFather(joao.getID());

        // ASSERT
        assertTrue(result);

    }

    /**
     * Test for isFather
     * Ensure false result
     */
    @Test
    @DisplayName("Test for hasFather - Ensure false")
    void isFatherEnsureFalse() {
        // ARRANGE
        Person paulo = new Person(personID3, "Paulo", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, ledgerID3);
        Person joana = new Person(personID4, "joana", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", personID2, personID, ledgerID4);

        // ACT
        boolean result = joana.hasFather(paulo.getID());

        // ASSERT
        assertFalse(result);

    }

    /**
     * Test for isMother
     * Happy Case
     */
    @Test
    @DisplayName("Test for hasMother - Happy Case")
    void isMotherHappyCase() {
        // ARRANGE
        Person maria = new Person(personID2, "maria", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, ledgerID2);
        Person joana = new Person(personID3, "joana", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", personID2, personID, ledgerID3);

        // ACT
        boolean result = joana.hasMother(maria.getID());

        // ASSERT
        assertTrue(result);
    }

    /**
     * Test for isMother
     * Ensure false result
     */
    @Test
    @DisplayName("Test for hasMother - Ensure False")
    void isMotherEnsureFalse() {
        // ARRANGE
        Person isabel = new Person(personID3, "isabel", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, ledgerID3);
        Person joana = new Person(personID4, "joana", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", personID2, personID3, ledgerID4);

        // ACT
        boolean result = joana.hasMother(isabel.getID());

        // ASSERT
        assertFalse(result);
    }

    /**
     * Test for addCategory
     * Happy Case
     */
    @Test
    @DisplayName("Test for addCategory - Happy case")
    void addCategoryHappyCaseTest() {
        // ARRANGE
        Person andre = new Person(personID3, "andre", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", personID, personID2, ledgerID);

        // ACT
        boolean result = andre.addCategory("health");

        // ASSERT
        assertTrue(result);

    }

    /**
     * Test for addCategory
     * Ensure exception
     */
    @Test
    @DisplayName("Test for addCategory - Ensure exception")
    void addCategoryEnsureExceptionTest() {
        // ARRANGE
        Person andre = new Person(personID3, "andre", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", personID, personID2, ledgerID3);

        // ASSERT
        assertThrows(InvalidFieldException.class, () -> {
            andre.addCategory(null);
        });

    }

    /**
     * Test for addAccount
     * Happy Case
     */
    @Test
    @DisplayName("Test for addAccount - Happy case")
    void addAccountHappyCaseTest() {
        // ARRANGE
        Person andre = new Person(personID3, "andre", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", personID, personID2, ledgerID);

        // ACT
        boolean result = andre.addAccount(accountID);

        // ASSERT
        assertTrue(result);

    }

    /**
     * Test for addAccount
     * Ensure false
     */
    @Test
    @DisplayName("Test for addAccount - Ensure false")
    void addAccountEnsureFalseTest() {
        // ARRANGE
        Person andre = new Person(personID3, "andre", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", personID, personID2, ledgerID3);
        andre.addAccount(accountID);

        // ACT
        boolean result = andre.addAccount(accountID);

        // ASSERT
        assertFalse(result);

    }

    /**
     * Test for addAccount
     * Ensure exception
     */
    @Test
    @DisplayName("Test for addAccount - Ensure exception")
    void addAccountEnsureExceptionTest() {
        // ARRANGE
        Person andre = new Person(personID3, "andre", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", personID, personID2, ledgerID3);

        // ASSERT
        assertThrows(InvalidFieldException.class, () -> {
            andre.addAccount(null);
        });

    }

    /**
     * Test for hasAccountID
     * Ensure true
     */
    @Test
    @DisplayName("Test for hasAccountID - Ensure true")
    void hasAccountTest() {
        // ARRANGE
        Person elsa = new Person(personID, "Elsa", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, ledgerID);
        // ACT
        elsa.addAccount(accountID);
        boolean actual = elsa.hasAccountID(accountID);

        // ASSERT
        assertTrue(actual);
    }

    /**
     * Test for hasAccountID
     * Ensure false
     */
    @Test
    @DisplayName("Test for hasAccountID - Ensure False")
    void hasAccountFalseTest() {
        // ARRANGE
        Person elsa = new Person(personID, "Elsa", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, ledgerID);
        // ACT
        boolean actual = elsa.hasAccountID(accountID);

        // ASSERT
        assertFalse(actual);
    }

    /**
     * Test for Person hashcode
     * Happy case
     */
    @Test
    @DisplayName("Test for hashcode - Happy case")
    void hashcodeHappyCaseTest() {
        Person mainA = new Person(personID, "Maria", "Rua Dr. Roberto Frias s/n", "Porto", "1987-04-17", null, null, ledgerID);
        int expectedHash = -1608300302;
        int actualHash = mainA.hashCode();
        assertEquals(expectedHash, actualHash);
    }

    /**
     * Test for Person toString
     * Ensure True
     */
    @Test
    @DisplayName("Test for Person toString - Happy case")
    void toStringHappyCaseTest() {
        // ARRANGE
        Person andre = new Person(personID3, "andre", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", personID, personID2, ledgerID3);
        String expected = "Person{siblingsIDs=Persons{persons=[]}," +
                " accountsIDs=AccountsIDs{AccountsIDs=[]}, " +
                "id=PersonID{personID=21323@switch.pt}," +
                " categories=Categories{categories=[]}," +
                " name=Name{name='andre'}, " +
                "address=Address{address='Rua Dr. Roberto Frias s/n'}, " +
                "birthPlace=Birthplace{birthplace='Porto'}," +
                " birthDate=Birthdate{birthdate='1991-12-22'}, " +
                "motherID=PersonID{personID=100@switch.pt}, " +
                "fatherID=PersonID{personID=2121@switch.pt}, " +
                "ledgerID=LedgerID{ledgerID=6453}}";
        // ACT
        String actual = andre.toString();
        // ASSERT
        assertEquals(expected, actual);
    }

//    /**
//     * Test for getBalanceWithinPeriod
//     * Ensure correct balance with all transactions within timeframe
//     */
//    @Test
//    @DisplayName("Test for getBalanceWithinPeriod - Ensure correct balance with all transactions within timeframe")
//    void getBalanceWithinPeriodEnsureCorrectBalanceWithAllTransactionsWithinTimeframeTest() {
//        // ARRANGE
//        Person isabel = new Person(personID, "isabel", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, ledgerID);
//        Person paulo = new Person(personID2, "Paulo", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, ledgerID2);
//        Person andre = new Person(personID3, "andre", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", personID, personID2, ledgerID3);
//
//        Account smithsGroceriesAccount = new Account(AccountID, "Smith's Groceries", "Account just for Smith family's groceries");
//        andre.addAccount(AccountID);
//
//        Account zeCreditAccount = new Account(AccountID2, "Zé & Associados", "Current account of the 'A Loja do Zé' grocery shop");
//        andre.addAccount(AccountID2);
//
//        Category newCategory = new Category("Groceries");
//        andre.addCategory("Groceries");
//
//        Period period = new Period("2019-01-01 00:00:00", "2020-12-31 00:00:00");
//
//        andre.addTransaction(20, Type.DEBIT, "2020-01-01 00:00:00", "Bags for plastic", newCategory, AccountID, AccountID2);
//        andre.addTransaction(100, Type.CREDIT, "2020-01-10 00:00:00", "Selling of plastic", newCategory, AccountID, AccountID2);
//
//        double expected = 80;
//
//        // ACT
//        double actual = andre.getBalanceWithinPeriod(period);
//
//        // ASSERT
//        assertEquals(expected, actual);
//    }
//
//    /**
//     * Test for getTransactionOfAccountWithinPeriod
//     * Happy Case
//     */
//    @Test
//    @DisplayName("Test for getTransactionsOfAccountWithinPeriod - HappyCase")
//    void getTransactionsOfAccountWithinPeriodHappyCaseTest() {
//        // ARRANGE
//        Person isabel = new Person(PersonID, "isabel", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, LedgerID);
//        Person paulo = new Person(PersonID2, "Paulo", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, LedgerID);
//        Person andre = new Person(PersonID3, "andre", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", PersonID, PersonID2, LedgerID);
//
//        Account smithsGroceriesAccount = new Account(AccountID, "Smith's Groceries", "Account just for Smith family's groceries");
//        andre.addAccount(AccountID);
//
//        Account zeCreditAccount = new Account(AccountID2, "Zé & Associados", "Current account of the 'A Loja do Zé' grocery shop");
//        andre.addAccount(AccountID2);
//
//        Category newCategory = new Category("Groceries");
//        andre.addCategory("Groceries");
//
//        andre.addTransaction(20, Type.DEBIT, "2020-01-01 00:00:00", "Bags for plastic", newCategory, AccountID, AccountID2);
//        andre.addTransaction(100, Type.CREDIT, "2020-01-10 00:00:00", "Selling of plastic", newCategory, AccountID, AccountID2);
//
//        Period period = new Period("2019-01-01 00:00:00", "2021-01-01 00:00:00");
//
//        Set<Transaction> expected = new HashSet<>();
//        expected.add(new Transaction(20, Type.DEBIT, "2020-01-01 00:00:00", "Bags for plastic", newCategory, AccountID, AccountID2));
//        expected.add(new Transaction(100, Type.CREDIT, "2020-01-10 00:00:00", "Selling of plastic", newCategory, AccountID, AccountID2));
//
//        // ACT
//
//        Set<Transaction> actual = new HashSet<>();
//        actual = andre.getTransactionsOfAccountWithinPeriod(AccountID2, period);
//
//        // ASSERT
//        assertEquals(expected, actual);
//    }
//
//    /**
//     * Test for getTransactionOfAccountWithinPeriod
//     * Ensure transactions outside the period are not in the result
//     */
//    @Test
//    @DisplayName("Test for getTransactionsOfAccountWithinPeriod - Ensure period filter is working")
//    void getTransactionsOfAccountWithinPeriodEnsurePeriodFilterTest() {
//        // ARRANGE
//        Person isabel = new Person(PersonID, "isabel", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, LedgerID);
//        Person paulo = new Person(PersonID2, "Paulo", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, LedgerID2);
//        Person andre = new Person(PersonID3, "andre", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", PersonID, PersonID2, LedgerID3);
//
//        Account smithsGroceriesAccount = new Account(AccountID, "Smith's Groceries", "Account just for Smith family's groceries");
//        andre.addAccount(AccountID);
//
//        Account zeCreditAccount = new Account(AccountID2, "Zé & Associados", "Current account of the 'A Loja do Zé' grocery shop");
//        andre.addAccount(AccountID2);
//
//        Category newCategory = new Category("Groceries");
//        andre.addCategory("Groceries");
//
//        andre.addTransaction(20, Type.DEBIT, "2023-01-01 00:00:00", "Bags for plastic", newCategory, AccountID, AccountID2);
//        andre.addTransaction(100, Type.CREDIT, "2020-01-10 00:00:00", "Selling of plastic", newCategory, AccountID, AccountID2);
//
//        Period period = new Period("2019-01-01 00:00:00", "2021-01-01 00:00:00");
//
//        Set<Transaction> expected = new HashSet<>();
//        expected.add(new Transaction(100, Type.CREDIT, "2020-01-10 00:00:00", "Selling of plastic", newCategory, AccountID, AccountID2));
//
//        // ACT
//
//        Set<Transaction> actual;
//        actual = andre.getTransactionsOfAccountWithinPeriod(AccountID2, period);
//
//        // ASSERT
//        assertEquals(expected, actual);
//    }
//
//    /**
//     * Test for getTransactionOfAccountWithinPeriod
//     * Ensure transactions without the requested account are not in the result
//     */
//    @Test
//    @DisplayName("Test for getTransactionsOfAccountWithinPeriod - Ensure account filter is working")
//    void getTransactionsOfAccountWithinPeriodEnsureAccountFilterTest() {
//        // ARRANGE
//        Person isabel = new Person(PersonID, "isabel", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, LedgerID);
//        Person paulo = new Person(PersonID2, "Paulo", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, LedgerID2);
//        Person andre = new Person(PersonID3, "andre", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", PersonID, PersonID2, LedgerID3);
//
//        Account smithsGroceriesAccount = new Account(AccountID, "Smith's Groceries", "Account just for Smith family's groceries");
//        andre.addAccount(AccountID);
//
//        Account zeCreditAccount = new Account(AccountID2, "Zé & Associados", "Current account of the 'A Loja do Zé' grocery shop");
//        andre.addAccount(AccountID2);
//
//        Account testAccount = new Account(AccountID3, "Teste", "Conta a testar como input");
//        andre.addAccount(AccountID3);
//
//        Category newCategory = new Category("Groceries");
//        andre.addCategory("Groceries");
//
//        andre.addTransaction(20, Type.DEBIT, "2020-01-01 00:00:00", "Bags for plastic", newCategory, AccountID, AccountID2);
//        andre.addTransaction(100, Type.CREDIT, "2020-01-10 00:00:00", "Selling of plastic", newCategory, AccountID3, AccountID2);
//
//        Period period = new Period("2019-01-01 00:00:00", "2021-01-01 00:00:00");
//
//        Set<Transaction> expected = new HashSet<>();
//        expected.add(new Transaction(100, Type.CREDIT, "2020-01-10 00:00:00", "Selling of plastic", newCategory, AccountID3, AccountID2));
//
//        // ACT
//
//        Set<Transaction> actual = new HashSet<>();
//        actual = andre.getTransactionsOfAccountWithinPeriod(AccountID3, period);
//
//        // ASSERT
//        assertEquals(expected, actual);
//    }
//
//    /**
//     * Test for getTransactionOfAccountWithinPeriod
//     * Ensure empty HashSet result when none of the transactions are fullfill the input parameters
//     */
//    @Test
//    @DisplayName("Test for getTransactionsOfAccountWithinPeriod - Ensure empty result test")
//    void getTransactionsOfAccountWithinPeriodEnsureEmptyResultTest() {
//        // ARRANGE
//        Person isabel = new Person(PersonID, "isabel", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, LedgerID);
//        Person paulo = new Person(PersonID2, "Paulo", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, LedgerID2);
//        Person andre = new Person(PersonID3, "andre", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", PersonID, PersonID2, LedgerID3);
//
//        Account smithsGroceriesAccount = new Account(AccountID, "Smith's Groceries", "Account just for Smith family's groceries");
//        andre.addAccount(AccountID);
//
//        Account zeCreditAccount = new Account(AccountID2, "Zé & Associados", "Current account of the 'A Loja do Zé' grocery shop");
//        andre.addAccount(AccountID2);
//
//        Account testAccount = new Account(AccountID3, "Teste", "Conta a testar como input");
//        andre.addAccount(AccountID3);
//
//        Category newCategory = new Category("Groceries");
//        andre.addCategory("Groceries");
//
//        andre.addTransaction(20, Type.DEBIT, "2025-01-01 00:00:00", "Bags for plastic", newCategory, AccountID, AccountID2);
//        andre.addTransaction(100, Type.CREDIT, "2020-01-10 00:00:00", "Selling of plastic", newCategory, AccountID, AccountID2);
//
//        Period period = new Period("2019-01-01 00:00:00", "2021-01-01 00:00:00");
//
//        Set<Transaction> expected = new HashSet<>();
//
//        // ACT
//
//        Set<Transaction> actual = new HashSet<>();
//        actual = andre.getTransactionsOfAccountWithinPeriod(AccountID3, period);
//
//        // ASSERT
//        assertEquals(expected, actual);
//    }
//
//    /**
//     * Test for getTransactionOfAccountWithinPeriod
//     * Ensure empty HashSet result when the given account is not in the person's listOfAccounts
//     */
//    @Test
//    @DisplayName("Test for getTransactionsOfAccountWithinPeriod - Ensure empty result test")
//    void getTransactionsOfAccountWithinPeriodAccountNotInTheListTest() {
//        // ARRANGE
//        Person isabel = new Person(PersonID, "isabel", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, LedgerID);
//        Person paulo = new Person(PersonID2, "Paulo", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, LedgerID2);
//        Person andre = new Person(PersonID3, "andre", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", PersonID, PersonID2, LedgerID3);
//
//        Account smithsGroceriesAccount = new Account(AccountID, "Smith's Groceries", "Account just for Smith family's groceries");
//        andre.addAccount(AccountID);
//
//        Account zeCreditAccount = new Account(AccountID2, "Zé & Associados", "Current account of the 'A Loja do Zé' grocery shop");
//        andre.addAccount(AccountID2);
//
//        Account testAccount = new Account(AccountID3, "Teste", "Conta a testar como input");
//
//        Category newCategory = new Category("Groceries");
//        andre.addCategory("Groceries");
//
//        andre.addTransaction(20, Type.DEBIT, "2025-01-01 00:00:00", "Bags for plastic", newCategory, AccountID, AccountID2);
//        andre.addTransaction(100, Type.CREDIT, "2020-01-10 00:00:00", "Selling of plastic", newCategory, AccountID, AccountID2);
//
//        Period period = new Period("2019-01-01 00:00:00", "2021-01-01 00:00:00");
//
//        Set<Transaction> expected = new HashSet<>();
//
//        // ACT
//
//        Set<Transaction> actual = new HashSet<>();
//        actual = andre.getTransactionsOfAccountWithinPeriod(AccountID3, period);
//
//        // ASSERT
//        assertEquals(expected, actual);
//    }
//
//    /**
//     * Test for getTransactionOfAccountWithinPeriod
//     * Test null account given by parameter
//     */
//    @Test
//    @DisplayName("Test for getTransactionsOfAccountWithinPeriod - Null account test")
//    void getTransactionsOfAccountWithinPeriodEnsureNullAccountTest() {
//        // ARRANGE
//        Person isabel = new Person(PersonID, "isabel", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, LedgerID);
//        Person paulo = new Person(PersonID2, "Paulo", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, LedgerID2);
//        Person andre = new Person(PersonID3, "andre", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", PersonID, PersonID2, LedgerID3);
//
//        Account smithsGroceriesAccount = new Account(AccountID, "Smith's Groceries", "Account just for Smith family's groceries");
//        andre.addAccount(AccountID);
//
//        Account zeCreditAccount = new Account(AccountID2, "Zé & Associados", "Current account of the 'A Loja do Zé' grocery shop");
//        andre.addAccount(AccountID2);
//
//        Account testAccount = null;
//        andre.addAccount(null);
//
//        Category newCategory = new Category("Groceries");
//        andre.addCategory("Groceries");
//
//        andre.addTransaction(20, Type.DEBIT, "2025-01-01 00:00:00", "Bags for plastic", newCategory, AccountID, AccountID2);
//        andre.addTransaction(100, Type.CREDIT, "2020-01-10 00:00:00", "Selling of plastic", newCategory, AccountID, AccountID2);
//
//        Period period = new Period("2019-01-01 00:00:00", "2021-01-01 00:00:00");
//
//        Set<Transaction> expected = new HashSet<>();
//
//        // ACT
//        // ASSERT
//        assertThrows(IllegalArgumentException.class, () -> {
//            andre.getTransactionsOfAccountWithinPeriod(null, period);
//        });
//    }
//
//    /**
//     * Test for getTransactionsWithinPeriod
//     * Ensure works with all Transactions Within Time frame
//     */
//    @Test
//    @DisplayName("Test for getTransactionsWithinPeriod - Ensure works with all Transactions Within Time frame")
//    void getTransactionsWithinPeriodEnsureWorksWithAllTransactionsWithinTimeframeTest() {
//        // ARRANGE
//        Person isabel = new Person(PersonID, "isabel", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, LedgerID);
//        Person paulo = new Person(PersonID2, "Paulo", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, LedgerID2);
//        Person andre = new Person(PersonID3, "andre", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", PersonID, PersonID2, LedgerID3);
//
//        Account smithsGroceriesAccount = new Account(AccountID, "Smith's Groceries", "Account just for Smith family's groceries");
//        andre.addAccount(AccountID);
//
//        Account zeCreditAccount = new Account(AccountID2, "Zé & Associados", "Current account of the 'A Loja do Zé' grocery shop");
//        andre.addAccount(AccountID2);
//
//        Category newCategory = new Category("Groceries");
//        andre.addCategory("Groceries");
//
//        andre.addTransaction(20, Type.DEBIT, "2020-01-01 00:00:00", "Bags for plastic", newCategory, AccountID, AccountID2);
//        andre.addTransaction(100, Type.CREDIT, "2020-01-10 00:00:00", "Selling of plastic", newCategory, AccountID, AccountID2);
//
//        Set<Transaction> expected = new HashSet<>();
//        expected.add(new Transaction(20, Type.DEBIT, "2020-01-01 00:00:00", "Bags for plastic", newCategory, AccountID, AccountID2));
//        expected.add(new Transaction(100, Type.CREDIT, "2020-01-10 00:00:00", "Selling of plastic", newCategory, AccountID, AccountID2));
//
//        Period period = new Period("2019-01-01 00:00:00", "2021-01-01 00:00:00");
//
//        // ACT
//        Set<Transaction> actual = andre.getTransactionsWithinPeriod(period);
//
//        // ASSERT
//        assertEquals(expected, actual);
//    }
//
//    /**
//     * Test for getTransactionsOfPersonAndRespectiveGroupsWithinPeriod
//     * Happy case
//     */
//    @Test
//    @DisplayName("Test for getTransactionsOfPersonAndRespectiveGroupsWithinPeriod - Happy case")
//    void getTransactionsOfPersonAndRespectiveGroupsWithinPeriodHappyCaseTest() {
//        // ARRANGE
//        Person maria = new Person(PersonID, "Maria", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, LedgerID);
//        Person paulo = new Person(PersonID2, "Paulo", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, LedgerID2);
//        Person andre = new Person(PersonID3, "Andre", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", PersonID, PersonID2, LedgerID3);
//
//        Account smithsGroceriesAccount = new Account(AccountID,"Smith's Groceries", "Account just for Smith family's groceries");
//        andre.addAccount(AccountID);
//
//        Account zeCreditAccount = new Account(AccountID2,"Zé & Associados", "Current account of the 'A Loja do Zé' grocery shop");
//        andre.addAccount(AccountID2);
//
//        Category newCategory = new Category("Groceries");
//        andre.addCategory("Groceries");
//
//        andre.addTransaction(25, Type.DEBIT, "2020-01-07 00:00:00", "Groceries for the week", newCategory, AccountID, AccountID2);
//        andre.addTransaction(149.49, Type.DEBIT, "2018-12-31 23:59:59", "New year's champagne", newCategory, AccountID, AccountID2);
//
//
//        Group switchGroup = new Group(GroupID,"Andre's switch group", "2019-10-07", andre, LedgerID4);
//
//        Account switchGroupAccount = new Account(AccountID4,"Current", "ProgrammersGroup's current account");
//        switchGroup.addAccountID(AccountID4);
//
//        Account computerStoreAccount = new Account(AccountID5,"ScreenTime", "Account for switchGroup's technology purchases at ScreenTime");
//        switchGroup.addAccountID(AccountID5);
//
//        Category electronicsCategory = new Category("Electronics");
//        switchGroup.addCategory("Electronics");
//
//        switchGroup.addTransaction(100, Type.DEBIT, "2019-10-01 00:00:00", "Electronic equipment", electronicsCategory, AccountID4, AccountID5);
//        switchGroup.addTransaction(380.00, Type.DEBIT, "1999-12-31 23:59:59", "Y2K software fix", electronicsCategory, AccountID4, AccountID5);
//
//        Group afterLunchGroup = new Group(GroupID2,"Andre's after lunch group", "2019-10-02", andre,LedgerID5);
//
//        Account afterLunchAccount = new Account(AccountID6,"Budget", "Account for afterLunchGroup's budget for coffee");
//        afterLunchGroup.addAccountID(AccountID6);
//
//        Account coffeeAccount = new Account(AccountID7,"Zespresso", "Account just for afterLunchGroup coffee purchases");
//        afterLunchGroup.addAccountID(AccountID7);
//
//        Category coffeeCategory = new Category("Coffee");
//        afterLunchGroup.addCategory("Coffee");
//
//        afterLunchGroup.addTransaction(20, Type.DEBIT, "2020-01-01 00:00:00", "Coffee for the month", coffeeCategory, AccountID6, AccountID7);
//        afterLunchGroup.addTransaction(10, Type.DEBIT, "2018-08-01 00:00:00", "Coffee for August", coffeeCategory, AccountID6, AccountID7);
//
//
//        Groups thisGroups = new Groups();
//        thisGroups.addGroupID(afterLunchGroup);
//        thisGroups.addGroupID(switchGroup);
//
//        Period period = new Period("2019-01-01 00:00:00", "2021-01-01 00:00:00");
//
//        Set<Transaction> expected = new HashSet<>();
//        expected.add(new Transaction(25, Type.DEBIT, "2020-01-07 00:00:00", "Groceries for the week", newCategory, smithsGroceriesAccount, zeCreditAccount));
//        expected.add(new Transaction(100, Type.DEBIT, "2019-10-01 00:00:00", "Electronic equipment", electronicsCategory, switchGroupAccount, computerStoreAccount));
//        expected.add(new Transaction(20, Type.DEBIT, "2020-01-01 00:00:00", "Coffee for the month", coffeeCategory, afterLunchAccount, coffeeAccount));
//
//        // ACT
//        Set<Transaction> actual = andre.getTransactionsOfPersonAndRespectiveGroupsWithinPeriod(thisGroups, period);
//
//        // ASSERT
//        assertEquals(expected, actual);
//    }
//
//    /**
//     * Test for getTransactionsOfPersonAndRespectiveGroupsWithinPeriod
//     * List of groups has another group from a different person
//     */
//    @Test
//    @DisplayName("Test for getTransactionsOfPersonAndRespectiveGroupsWithinPeriod - List of groups has another group from a different person")
//    void getTransactionsOfPersonAndRespectiveGroupsWithinPeriodSecondHappyCaseTest() {
//        // ARRANGE
//        Person maria = new Person("Maria", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null);
//        Person paulo = new Person("Paulo", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null);
//        Person andre = new Person("Andre", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", maria, paulo);
//        Person joao = new Person("João", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", maria, paulo);
//
//
//        Account smithsGroceriesAccount = new Account("Smith's Groceries", "Account just for Smith family's groceries");
//        andre.addAccount("Smith's Groceries", "Account just for Smith family's groceries");
//
//        Account zeCreditAccount = new Account("Zé & Associados", "Current account of the 'A Loja do Zé' grocery shop");
//        andre.addAccount("Zé & Associados", "Current account of the 'A Loja do Zé' grocery shop");
//
//        Category newCategory = new Category("Groceries");
//        andre.addCategory("Groceries");
//
//        andre.addTransaction(25, Type.DEBIT, "2020-01-07 00:00:00", "Groceries for the week", newCategory, smithsGroceriesAccount, zeCreditAccount);
//
//
//        Group switchGroup = new Group("Andre's switch group", "2019-10-07", andre);
//
//        Account switchGroupAccount = new Account("Current", "ProgrammersGroup's current account");
//        switchGroup.addAccount("Current", "ProgrammersGroup's current account");
//
//        Account computerStoreAccount = new Account("ScreenTime", "Account for switchGroup's technology purchases at ScreenTime");
//        switchGroup.addAccount("ScreenTime", "Account for switchGroup's technology purchases at ScreenTime");
//
//        Category electronicsCategory = new Category("Electronics");
//        switchGroup.addCategory("Electronics");
//
//        switchGroup.addTransaction(100, Type.DEBIT, "2019-10-01 00:00:00", "Electronic equipment", electronicsCategory, switchGroupAccount, computerStoreAccount);
//
//
//        Group afterLunchGroup = new Group("João's after lunch group", "2019-10-02", joao);
//
//        Account afterLunchAccount = new Account("Budget", "Account for afterLunchGroup's budget for coffee");
//        afterLunchGroup.addAccount("Budget", "Account for afterLunchGroup's budget for coffee");
//
//        Account coffeeAccount = new Account("Zespresso", "Account just for afterLunchGroup coffee purchases");
//        afterLunchGroup.addAccount("Zespresso", "Account just for afterLunchGroup coffee purchases");
//
//        Category coffeeCategory = new Category("Coffee");
//        afterLunchGroup.addCategory("Coffee");
//
//        afterLunchGroup.addTransaction(20, Type.DEBIT, "2020-01-10 00:00:00", "Coffee for the month", coffeeCategory, afterLunchAccount, coffeeAccount);
//
//        Groups thisGroups = new Groups();
//        thisGroups.addGroup(afterLunchGroup);
//        thisGroups.addGroup(switchGroup);
//
//        Period period = new Period("2019-01-01 00:00:00", "2021-01-01 00:00:00");
//
//        Set<Transaction> expected = new HashSet<>();
//        expected.add(new Transaction(25, Type.DEBIT, "2020-01-07 00:00:00", "Groceries for the week", newCategory, smithsGroceriesAccount, zeCreditAccount));
//        expected.add(new Transaction(100, Type.DEBIT, "2019-10-01 00:00:00", "Electronic equipment", electronicsCategory, switchGroupAccount, computerStoreAccount));
//
//        // ACT
//        Set<Transaction> actual = andre.getTransactionsOfPersonAndRespectiveGroupsWithinPeriod(thisGroups, period);
//
//        // ASSERT
//        assertEquals(expected, actual);
//    }
//
//    /**
//     * Test for getTransactionsOfPersonAndRespectiveGroupsWithinPeriod
//     * Period with no transactions
//     */
//    @Test
//    @DisplayName("Test for getTransactionsOfPersonAndRespectiveGroupsWithinPeriod - Period with no transactions")
//    void getTransactionsOfPersonAndRespectiveGroupsWithinPeriodNoOccurrencesTest() {
//        // ARRANGE
//        Person maria = new Person("Maria", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null);
//        Person paulo = new Person("Paulo", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null);
//        Person andre = new Person("Andre", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", maria, paulo);
//
//        Account smithsGroceriesAccount = new Account("Smith's Groceries", "Account just for Smith family's groceries");
//        andre.addAccount("Smith's Groceries", "Account just for Smith family's groceries");
//
//        Account zeCreditAccount = new Account("Zé & Associados", "Current account of the 'A Loja do Zé' grocery shop");
//        andre.addAccount("Zé & Associados", "Current account of the 'A Loja do Zé' grocery shop");
//
//        Category newCategory = new Category("Groceries");
//        andre.addCategory("Groceries");
//
//        andre.addTransaction(25, Type.DEBIT, "2020-01-07 00:00:00", "Groceries for the week", newCategory, smithsGroceriesAccount, zeCreditAccount);
//
//
//        Group switchGroup = new Group("Andre's switch group", "2019-10-07", andre);
//
//        Account switchGroupAccount = new Account("Current", "ProgrammersGroup's current account");
//        switchGroup.addAccount("Current", "ProgrammersGroup's current account");
//
//        Account computerStoreAccount = new Account("ScreenTime", "Account for switchGroup's technology purchases at ScreenTime");
//        switchGroup.addAccount("ScreenTime", "Account for switchGroup's technology purchases at ScreenTime");
//
//        Category electronicsCategory = new Category("Electronics");
//        switchGroup.addCategory("Electronics");
//
//        switchGroup.addTransaction(100, Type.DEBIT, "2019-10-01 00:00:00", "Electronic equipment", electronicsCategory, switchGroupAccount, computerStoreAccount);
//
//
//        Group afterLunchGroup = new Group("Andre's after lunch group", "2019-10-02", andre);
//
//        Account afterLunchAccount = new Account("Budget", "Account for afterLunchGroup's budget for coffee");
//        afterLunchGroup.addAccount("Budget", "Account for afterLunchGroup's budget for coffee");
//
//        Account coffeeAccount = new Account("Zespresso", "Account just for afterLunchGroup coffee purchases");
//        afterLunchGroup.addAccount("Zespresso", "Account just for afterLunchGroup coffee purchases");
//
//        Category coffeeCategory = new Category("Coffee");
//        afterLunchGroup.addCategory("Coffee");
//
//        afterLunchGroup.addTransaction(20, Type.DEBIT, "2020-01-10 00:00:00", "Coffee for the month", coffeeCategory, afterLunchAccount, coffeeAccount);
//
//        Groups thisGroups = new Groups();
//        thisGroups.addGroup(afterLunchGroup);
//        thisGroups.addGroup(switchGroup);
//
//        Period period = new Period("2018-01-01 00:00:00", "2018-12-31 00:00:00");
//
//        Set<Transaction> expected = new HashSet<>();
//
//        // ACT
//        Set<Transaction> actual = andre.getTransactionsOfPersonAndRespectiveGroupsWithinPeriod(thisGroups, period);
//
//        // ASSERT
//        assertEquals(expected, actual);
//    }
//
//    /**
//     * Test for getTransactionsOfPersonAndRespectiveGroupsWithinPeriod
//     * Empty ledgers
//     */
//    @Test
//    @DisplayName("Test for getTransactionsOfPersonAndRespectiveGroupsWithinPeriod - Empty ledgers")
//    void getTransactionsOfPersonAndRespectiveGroupsWithinPeriodFromEmptyLedgersTest() {
//        // ARRANGE
//        Person maria = new Person("Maria", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null);
//        Person paulo = new Person("Paulo", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null);
//        Person andre = new Person("Andre", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", maria, paulo);
//
//        Account smithsGroceriesAccount = new Account("Smith's Groceries", "Account just for Smith family's groceries");
//        andre.addAccount("Smith's Groceries", "Account just for Smith family's groceries");
//
//        Account zeCreditAccount = new Account("Zé & Associados", "Current account of the 'A Loja do Zé' grocery shop");
//        andre.addAccount("Zé & Associados", "Current account of the 'A Loja do Zé' grocery shop");
//
//        Category newCategory = new Category("Groceries");
//        andre.addCategory("Groceries");
//
//
//        Group switchGroup = new Group("Andre's switch group", "2019-10-07", andre);
//
//        Account switchGroupAccount = new Account("Current", "ProgrammersGroup's current account");
//        switchGroup.addAccount("Current", "ProgrammersGroup's current account");
//
//        Account computerStoreAccount = new Account("ScreenTime", "Account for switchGroup's technology purchases at ScreenTime");
//        switchGroup.addAccount("ScreenTime", "Account for switchGroup's technology purchases at ScreenTime");
//
//        Category electronicsCategory = new Category("Electronics");
//        switchGroup.addCategory("Electronics");
//
//
//        Group afterLunchGroup = new Group("Andre's after lunch group", "2019-10-02", andre);
//
//        Account afterLunchAccount = new Account("Budget", "Account for afterLunchGroup's budget for coffee");
//        afterLunchGroup.addAccount("Budget", "Account for afterLunchGroup's budget for coffee");
//
//        Account coffeeAccount = new Account("Zespresso", "Account just for afterLunchGroup coffee purchases");
//        afterLunchGroup.addAccount("Zespresso", "Account just for afterLunchGroup coffee purchases");
//
//        Category coffeeCategory = new Category("Coffee");
//        afterLunchGroup.addCategory("Coffee");
//
//
//        Groups thisGroups = new Groups();
//        thisGroups.addGroup(afterLunchGroup);
//        thisGroups.addGroup(switchGroup);
//
//        Period period = new Period("2019-01-01 00:00:00", "2021-01-01 00:00:00");
//
//        Set<Transaction> expected = new HashSet<>();
//
//        // ACT
//        Set<Transaction> actual = andre.getTransactionsOfPersonAndRespectiveGroupsWithinPeriod(thisGroups, period);
//
//        // ASSERT
//        assertEquals(expected, actual);
//    }

}
