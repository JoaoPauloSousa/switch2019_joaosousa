package project.model.entities.person;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.exceptions.InvalidFieldException;
import project.model.entities.account.Account;
import project.model.entities.person.Name;
import project.model.entities.shared.AccountID;

import static org.junit.jupiter.api.Assertions.*;

public class NameTest {
    /**
     * Test for Name Constructor
     * Happy case (valid Name)
     */
    @Test
    @DisplayName("Test for Name constructor - Happy case")
    void NameConstructorHappyCaseTest() {
        Name joao = new Name("Joao");
        assertTrue(joao instanceof Name);
    }

    /**
     * Test for Name Constructor
     * Exception with invalid Name - null case
     */
    @Test
    @DisplayName("Test for the constructor - Ensure exception with null Name")
    void NameConstructorEnsureExceptionWithNullNameTest() {
        assertThrows(InvalidFieldException.class, () -> {
            Name joao = new Name(null);
        });
    }

    /**
     * Test for Name Constructor
     * Exception with invalid Name - empty String
     */
    @Test
    @DisplayName("Test for the constructor - Ensure exception with empty Name")
    void NameConstructorEnsureExceptionWithEmptyNameTest() {
        assertThrows(InvalidFieldException.class, () -> {
            Name joao = new Name("");
        });
    }

    /**
     * Test for Name Constructor
     * Exception with invalid Name - only whitespaces
     */
    @Test
    @DisplayName("Test for the constructor - Ensure exception with only whitespaces Name")
    void NameConstructorEnsureExceptionWithOnlyWhitespacesNameTest() {
        assertThrows(InvalidFieldException.class, () -> {
            Name joao = new Name("     ");
        });
    }

    /**
     * Test for Name equals
     * Ensure true with same instance
     */
    @Test
    @DisplayName("Test Name for equals - Ensure true with same instance")
    void nameEqualsHappyCaseTest() {
        Name mainA = new Name("joao");
        boolean result = mainA.equals(mainA);
        assertTrue(result);
    }

    /**
     * Test for Name equals
     * Ensure false with null object
     */
    @Test
    @DisplayName("Test Name for equals - Ensure false with null object")
    void nameEqualsEnsureFalseWithNullTest() {
        Name mainA = new Name("joao");
        Name mainB = null;
        boolean result = mainA.equals(mainB);
        assertFalse(result);
    }

    /**
     * Test for Name equals
     * Ensure false with different class object
     */
    @Test
    @DisplayName("Test Name for equals - Ensure false with different class object")
    void nameEqualsEnsureFalseWithDifferentClassObjectTest() {
        Name mainA = new Name("joao");
        AccountID id = new AccountID("5");
        Account mainB = new Account(id, "wallet", "coins");
        boolean result = mainA.equals(mainB);
        assertFalse(result);
    }

    /**
     * Test for Name equals
     * Ensure true with same attributes
     */
    @Test
    @DisplayName("Test Name for equals - Ensure true with same attributes")
    void nameEqualsEnsureTrueWithSameAttributesTest() {
        Name mainA = new Name("Joao");
        Name mainB = new Name("Joao");
        boolean result = mainA.equals(mainB);
        assertTrue(result);
    }

    /**
     * Test for Name hashcode
     * Happy case
     */
    @Test
    @DisplayName("Test for hashcode - Ensure true with same attributes")
    void nameHashCodeEnsureTrueTest() {
        Name mainA = new Name("joao");
        int expectedHash = 3267666;
        int actualHash = mainA.hashCode();
        assertEquals(expectedHash, actualHash);
    }

    /**
     * Test for toString
     * Ensure true
     */
    @Test
    @DisplayName("Test for toString - Ensure true")
    void toStringEnsureTrueTest() {
        // Arrange
        Name mainA = new Name("Joao");
        String expected = "Name{name='Joao'}";

        // Act
        String actual = mainA.toString();

        // Assert
        assertEquals(expected, actual);
    }
}
