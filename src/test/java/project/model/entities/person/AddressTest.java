package project.model.entities.person;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.exceptions.InvalidFieldException;
import project.model.entities.person.Address;
import project.model.entities.shared.AccountID;

import static org.junit.jupiter.api.Assertions.*;

class AddressTest {

    /**
     * Test for Address Constructor
     * Happy case (valid Address)
     */
    @Test
    @DisplayName("Test for Address constructor - Happy case")
    void AddressConstructorHappyCaseTest() {
        Address northStreet = new Address("North Street");
        assertTrue(northStreet instanceof Address);
    }

    /**
     * Test for Address Constructor
     * Exception with invalid Address - null case
     */
    @Test
    @DisplayName("Test for the constructor - Ensure exception with null Address")
    void AddressConstructorEnsureExceptionWithNullAddressTest() {
        assertThrows(InvalidFieldException.class, () -> {
            Address address = new Address(null);
        });
    }

    /**
     * Test for Address Constructor
     * Exception with invalid Address - empty String
     */
    @Test
    @DisplayName("Test for the constructor - Ensure exception with empty Address")
    void AddressConstructorEnsureExceptionWithEmptyAddressTest() {
        assertThrows(InvalidFieldException.class, () -> {
            Address address = new Address("");
        });
    }

    /**
     * Test for Address Constructor
     * Exception with invalid Address - only whitespaces
     */
    @Test
    @DisplayName("Test for the constructor - Ensure exception with only whitespaces Address")
    void AddressConstructorEnsureExceptionWithOnlyWhitespacesAddressTest() {
        assertThrows(InvalidFieldException.class, () -> {
            Address address = new Address("     ");
        });
    }

    /**
     * Test for Address equals
     * Ensure true with same instance
     */
    @Test
    @DisplayName("Test Address for equals - Ensure true with same instance")
    void AddressEqualsHappyCaseTest() {
        Address northStreet = new Address("North Street");
        boolean result = northStreet.equals(northStreet);
        assertTrue(result);
    }

    /**
     * Test for Address equals
     * Ensure false with null object
     */
    @Test
    @DisplayName("Test Address for equals - Ensure false with null object")
    void AddressEqualsEnsureFalseWithNullTest() {
        Address northStreet = new Address("North Street");
        Address nullStreet = null;
        boolean result = northStreet.equals(nullStreet);
        assertFalse(result);
    }

    /**
     * Test for Address equals
     * Ensure false with different class object
     */
    @Test
    @DisplayName("Test Address for equals - Ensure false with different class object")
    void AddressEqualsEnsureFalseWithDifferentClassObjectTest() {
        Address northStreet = new Address("North Street");
        AccountID accountID = new AccountID("100");
        boolean result = northStreet.equals(accountID);
        assertFalse(result);
    }

    /**
     * Test for Address equals
     * Ensure true with same attributes
     */
    @Test
    @DisplayName("Test Address for equals - Ensure true with same attributes")
    void AddressEqualsEnsureTrueWithSameAttributesTest() {
        Address northStreet = new Address("Rua Norte");
        Address otherNorthStreet = new Address("Rua Norte");
        boolean result = northStreet.equals(otherNorthStreet);
        assertTrue(result);
    }

    /**
     * Test for Address hashcode
     * Happy case
     */
    @Test
    @DisplayName("Test for hashcode - Ensure true with same attributes")
    void AddressHashCodeEnsureTrueTest() {
        Address mainA = new Address("address");
        int expectedHash = -1147692013;
        int actualHash = mainA.hashCode();
        assertEquals(expectedHash, actualHash);
    }

    /**
     * Test for toString
     * Ensure true
     */
    @Test
    @DisplayName("Test for toString - Ensure true")
    void toStringEnsureTrueTest() {
        // Arrange
        Address northStreet = new Address("Rua Norte");
        String expected = "Address{address='Rua Norte'}";

        // Act
        String actual = northStreet.toString();

        //Assert
        assertEquals(expected, actual);
    }
}