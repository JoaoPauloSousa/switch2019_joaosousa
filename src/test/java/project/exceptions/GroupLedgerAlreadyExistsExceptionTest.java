package project.exceptions;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class GroupLedgerAlreadyExistsExceptionTest {

    /**
     * Test for constructor filled
     * Happy case
     */
    @Test
    @DisplayName("Test for constructor filled - Happy case")
    void constructorFilledTest() {
        //ARRANGE
        GroupLedgerAlreadyExistsException ex = new GroupLedgerAlreadyExistsException("This is an error message");

        // ACT
        //ASSERT
        assertTrue(ex instanceof GroupLedgerAlreadyExistsException);
    }

    /**
     * Test for constructor unfilled
     * Happy case
     */
    @Test
    @DisplayName("Test for constructor unfilled - Happy case")
    void constructorUnfilledTest() {
        //ARRANGE
        GroupLedgerAlreadyExistsException ex = new GroupLedgerAlreadyExistsException();

        // ACT
        //ASSERT
        assertTrue(ex instanceof GroupLedgerAlreadyExistsException);
    }

    /**
     * Test for getMessage with filled constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for getMessage with filled constructor - Happy case")
    void getMessageFilledConstructorHappyCase() {
        //ARRANGE
        GroupLedgerAlreadyExistsException ex = new GroupLedgerAlreadyExistsException("This is an error message");
        String expected = "This is an error message";

        // ACT
        String actual = ex.getMessage();

        //ASSERT
        assertEquals(expected, actual);
    }

    /**
     * Test for getMessage with unfilled constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for getMessage with unfilled constructor - Happy case")
    void getMessageUnfilledConstructorHappyCase() {
        //ARRANGE
        GroupLedgerAlreadyExistsException ex = new GroupLedgerAlreadyExistsException();
        String expected = "Group ledger already exists";

        // ACT
        String actual = ex.getMessage();

        //ASSERT
        assertEquals(expected, actual);
    }

}