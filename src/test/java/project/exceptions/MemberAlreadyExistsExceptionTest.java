package project.exceptions;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MemberAlreadyExistsExceptionTest {

    /**
     * Test for constructor filled
     * Happy case
     */
    @Test
    @DisplayName("Test for constructor filled - Happy case")
    void constructorFilledTest() {
        //ARRANGE
        MemberAlreadyExistsException ex = new MemberAlreadyExistsException("This is an error message");

        // ACT
        //ASSERT
        assertTrue(ex instanceof MemberAlreadyExistsException);
    }

    /**
     * Test for constructor unfilled
     * Happy case
     */
    @Test
    @DisplayName("Test for constructor unfilled - Happy case")
    void constructorUnfilledTest() {
        //ARRANGE
        MemberAlreadyExistsException ex = new MemberAlreadyExistsException();

        // ACT
        //ASSERT
        assertTrue(ex instanceof MemberAlreadyExistsException);
    }

    /**
     * Test for getMessage with filled constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for getMessage with filled constructor - Happy case")
    void getMessageFilledConstructorHappyCase() {
        //ARRANGE
        MemberAlreadyExistsException ex = new MemberAlreadyExistsException("This is an error message");
        String expected = "This is an error message";

        // ACT
        String actual = ex.getMessage();

        //ASSERT
        assertEquals(expected, actual);
    }

    /**
     * Test for getMessage with unfilled constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for getMessage with unfilled constructor - Happy case")
    void getMessageUnfilledConstructorHappyCase() {
        //ARRANGE
        MemberAlreadyExistsException ex = new MemberAlreadyExistsException();
        String expected = "Member already exists";

        // ACT
        String actual = ex.getMessage();

        //ASSERT
        assertEquals(expected, actual);
    }

}