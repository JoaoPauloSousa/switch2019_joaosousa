package project.infrastructure.repositories;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import project.model.entities.ledger.Ledger;
import project.model.entities.shared.LedgerID;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Transactional
@SpringBootTest
class LedgerRepositoryDBTest {

    @Autowired
    LedgerRepositoryDB ledgerRepositoryDB;

    @BeforeEach
    public void init() {

    }

    /**
     * Test for constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for LedgerRepository constructor - HappyCase")
    void LedgerRepositoryConstructorHappyCaseTest() {
        assertTrue(ledgerRepositoryDB instanceof LedgerRepositoryDB);
    }

    /**
     * Test for findById
     * Happy case
     */
    @Test
    @DisplayName("Test for findById - HappyCase")
    void findByIdHappyCaseTest() {
        // Arrange
        LedgerID ledgerID1 = new LedgerID("1");
        Ledger ledger = new Ledger(ledgerID1);
        ledgerRepositoryDB.save(ledger);

        Optional<Ledger> expected = Optional.of(ledger);

        // Act
        Optional<Ledger> actual = ledgerRepositoryDB.findById(ledgerID1);

        // Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for findById
     * Ensure works with null test
     */
    @Test
    @DisplayName("Test for findById - Ensure works with null test")
    void findByIdEnsureWorksWithNullTest() {
        // Arrange
        Optional<Ledger> expected = Optional.empty();

        // Act
        Optional<Ledger> actual = ledgerRepositoryDB.findById(null);

        // Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for save
     * Happy case
     */
    @Test
    @DisplayName("Test for PersonRepository save - HappyCase")
    void saveHappyCaseTest() {
        // Arrange
        LedgerID ledgerID1 = new LedgerID("1");
        Ledger expected = new Ledger(ledgerID1);

        // Act
        Ledger actual = ledgerRepositoryDB.save(expected);

        // Assert
        assertEquals(expected, actual);
    }
}
