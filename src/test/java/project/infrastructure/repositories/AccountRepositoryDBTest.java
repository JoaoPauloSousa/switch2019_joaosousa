package project.infrastructure.repositories;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import project.model.entities.account.Account;
import project.model.entities.shared.AccountID;
import project.model.specifications.repositories.AccountRepository;

import javax.transaction.Transactional;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@Transactional
public class AccountRepositoryDBTest {

    @Autowired
    private AccountRepository repository;

    /**
     * Test for constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for AccountRepository constructor - HappyCase")
    void AccountRepositoryConstructorHappyCaseTest() {
        assertTrue(repository instanceof AccountRepository);
    }

    /**
     * Test for save method
     * Happy case
     */
    @Test
    @DisplayName("Test for save Method on AccountRepositoryDB - Happy case")
    void saveHappyCaseTest() {

        //ARRANGE
        AccountID accountID = new AccountID("1212");
        Account account = new Account(accountID, "bla", "blabla");

        Account expected = new Account(new AccountID("1212"), "bla", "blabla");

        //ACT
        Account result = repository.save(account);

        //ASSERT
        assertEquals(expected, result);

    }

    /**
     * Test for findByID- AccountID
     * <p>
     * Happy case
     */
    @Test
    @DisplayName("Test for findByID - Happy case")
    void findByIdHappyCaseTest() {

        //ARRANGE
        AccountID accountID = new AccountID("1212");
        Account account = new Account(accountID, "bla", "blabla");

        repository.save(account);

        Optional<Account> expected = Optional.of(account);

        //ACT
        Optional<Account> result = repository.findById(accountID);

        //ASSERT
        assertEquals(expected, result);

    }

    /**
     * Test for findById
     * Ensure works with null test
     */
    @Test
    @DisplayName("Test for findById- Ensure works with null test")
    void findByIdEnsureWorksWithNullTest() {

        //ARRANGE
        Optional<Account> expected = Optional.empty();

        //ACT
        Optional<Account> result = repository.findById(null);

        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for existsById
     * <p>
     * Happy case
     */
    @Test
    @DisplayName("Test for existsById Method- Ensure true")
    void existsByIdHappyCaseTest() {
        //ARRANGE
        AccountID accountID = new AccountID("1212");
        Account account = new Account(accountID, "bla", "blabla");

        repository.save(account);

        //ACT
        boolean result = repository.existsById(accountID);

        //ASSERT
        assertTrue(result);

    }

}