package project.controllers.web;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.application.services.CreateCategoryForPersonService;
import project.dto.CreateCategoryForPersonAssembler;
import project.dto.CreateCategoryForPersonRequestDTO;
import project.dto.CreateCategoryForPersonResponseDTO;
import project.dto.CreateCategoryInfoDTO;
import project.exceptions.CategoryAlreadyExistsException;
import project.exceptions.InvalidFieldException;
import project.exceptions.PersonNotFoundException;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
public class CreateCategoryForPersonRESTControllerTest {

    @Autowired
    private CreateCategoryForPersonService service;

    @Autowired
    private CreateCategoryForPersonRESTController controller;

    /**
     * createCategoryForPerson - Happy Case
     */
    @Test
    @DisplayName("Test for createCategoryForPerson - Happy Case")
    public void createCategoryForPersonHappyCaseTest() {

        //ARRANGE
        String personID = "51010@switch.com";
        String designation = "Relva";

        CreateCategoryInfoDTO infoDTO = new CreateCategoryInfoDTO(designation);
        CreateCategoryForPersonRequestDTO requestDTO = CreateCategoryForPersonAssembler.mapToRequestDTO(personID, designation);
        CreateCategoryForPersonResponseDTO expected = new CreateCategoryForPersonResponseDTO(personID, designation);

        Mockito.when(service.createCategoryForPerson(requestDTO)).thenReturn(expected);

        //ACT
        Object result = controller.createCategoryForPerson(infoDTO, personID).getBody();

        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Ensure that a category is not added if already exists in the person"
     */
    @Test
    @DisplayName("ensure that a category is not added if already exists in the person")
    public void createCategoryForPersonEnsureCategoryCantBeAddedIfAlreadyExistInPersonTest() {

        //ARRANGE
        String personID = "51010@switch.com";
        String designation = "Arbitros";

        CreateCategoryInfoDTO infoDTO = new CreateCategoryInfoDTO(designation);
        CreateCategoryForPersonRequestDTO requestDTO = CreateCategoryForPersonAssembler.mapToRequestDTO(personID, designation);

        Mockito.when(service.createCategoryForPerson(requestDTO)).thenThrow(CategoryAlreadyExistsException.class);

        //ACT
        //ASSERT
        Assertions.assertThrows(CategoryAlreadyExistsException.class, () -> {
            controller.createCategoryForPerson(infoDTO, personID);
        });
    }

    /**
     * Ensure that a category is not added to a person that is not in the repository
     */
    @Test
    @DisplayName("ensure that a category is not added to a person that is not in the repository")
    public void createCategoryForPersonEnsureCategoryIsNotAddedToAPersonThatIsNotInTheRepositoryTest() {

        //ARRANGE
        String personID = "51010@switch.com";
        String designation = "Comida";

        CreateCategoryInfoDTO infoDTO = new CreateCategoryInfoDTO(designation);
        CreateCategoryForPersonRequestDTO requestDTO = CreateCategoryForPersonAssembler.mapToRequestDTO(personID, designation);

        Mockito.when(service.createCategoryForPerson(requestDTO)).thenThrow(PersonNotFoundException.class);

        //ACT
        //ASSERT
        Assertions.assertThrows(PersonNotFoundException.class, () -> {
            controller.createCategoryForPerson(infoDTO, personID);
        });
    }

    /**
     * ensure that a exception is thrown when the category input is invalid
     */
    @Test
    @DisplayName("ensure that a exception is thrown when the category input is invalid")
    public void createCategoryForPersonEnsureExceptionWhenTheCategoryInputIsInvalidTest() {

        //ARRANGE
        String personID = "51010@switch.com";
        String designation = "";

        CreateCategoryInfoDTO infoDTO = new CreateCategoryInfoDTO(designation);
        CreateCategoryForPersonRequestDTO requestDTO = CreateCategoryForPersonAssembler.mapToRequestDTO(personID, designation);

        Mockito.when(service.createCategoryForPerson(requestDTO)).thenThrow(InvalidFieldException.class);

        //ACT
        //ASSERT
        Assertions.assertThrows(InvalidFieldException.class, () -> {
            controller.createCategoryForPerson(infoDTO, personID);
        });
    }

}
