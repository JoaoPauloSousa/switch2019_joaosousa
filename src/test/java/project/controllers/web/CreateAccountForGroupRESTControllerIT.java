package project.controllers.web;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.util.UriComponentsBuilder;
import project.dto.CreateAccountForGroupInfoDTO;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CreateAccountForGroupRESTControllerIT extends AbstractTest {

    @Override
    @BeforeAll
    public void setUp() {
        super.setUp();
    }

    /**
     * createAccountForGroup - Happy case
     *
     * @throws Exception JsonProcessingException
     */
    @Test
    @DisplayName("CreateAccountForGroup - Happy case")
    public void createAccountForGroupHappyCaseTest() throws Exception {

        // Global variables to be used throughout the test
        String accountID = "7002";
        String accountDenomination = "shopping";
        String accountDescription = "food";
        String personID = "7001@switch.pt";
        String groupID = "7001";
        String groupDescription = "isep students";

        // STAGE 1 - verify if account exists - ERROR 404 Account Not Found
        // ARRANGE
        Map<String, Object> pathVariableMap1 = new HashMap<>();
        pathVariableMap1.put("accountID", accountID);

        String uri1 = UriComponentsBuilder.fromUriString("")
                .path("/accounts/{accountID}")
                .buildAndExpand(pathVariableMap1)
                .toUriString();

        int expectedStatus1 = 404;
        JSONObject expectedContent1 = new JSONObject()
                .put("status", "NOT_FOUND")
                .put("message", "Account not found")
                .put("errors", new JSONArray(Collections.singletonList("Account not found")));

        // ACT
        MvcResult mvcResult1 = mvc.perform(MockMvcRequestBuilders.get(uri1)).andReturn();
        int actualStatus1 = mvcResult1.getResponse().getStatus();
        JSONObject actualContent1 = new JSONObject(mvcResult1.getResponse().getContentAsString());

        // ASSERT
        assertEquals(expectedStatus1, actualStatus1);
        JSONAssert.assertEquals(expectedContent1, actualContent1, true);

        // STAGE 2 - create account
        // ARRANGE
        Map<String, Object> pathVariableMap2 = new HashMap<>();
        pathVariableMap2.put("personID", personID);
        pathVariableMap2.put("groupID", groupID);

        String uri2 = UriComponentsBuilder.fromUriString("")
                .path("/people/{personID}/groups/{groupID}/accounts")
                .buildAndExpand(pathVariableMap2)
                .toUriString();

        CreateAccountForGroupInfoDTO infoDTO = new CreateAccountForGroupInfoDTO(accountID, accountDenomination, accountDescription);
        String inputJson = super.mapToJson(infoDTO);

        int expectedStatus2 = 201;
        JSONObject expectedContent2 = new JSONObject()
                .put("groupID", groupID)
                .put("groupDescription", groupDescription)
                .put("accountID", accountID)
                .put("accountDenomination", accountDenomination)
                .put("_links", new JSONObject()
                        .put("self", new JSONObject().
                                put("href", "http://localhost/accounts/" + accountID))
                        .put("groupAccounts", new JSONObject().
                                put("href", "http://localhost/groups/" + groupID + "/accounts"))
                        .put("group", new JSONObject().
                                put("href", "http://localhost/groups/" + groupID)));

        // ACT
        MvcResult mvcResult2 = mvc.perform(MockMvcRequestBuilders.post(uri2).contentType
                (MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        int actualStatus2 = mvcResult2.getResponse().getStatus();
        JSONObject actualContent2 = new JSONObject(mvcResult2.getResponse().getContentAsString());

        // ASSERT
        assertEquals(expectedStatus2, actualStatus2);
        JSONAssert.assertEquals(expectedContent2, actualContent2, true);

        // STAGE 3 - verify if account exists - OK 200
        // ARRANGE
        int expectedStatus3 = 200;
        JSONObject expectedContent3 = new JSONObject()
                .put("denomination", accountDenomination)
                .put("description", accountDescription)
                .put("accountID", accountID);

        // ACT
        MvcResult mvcResult3 = mvc.perform(MockMvcRequestBuilders.get(uri1)).andReturn();
        int actualStatus3 = mvcResult3.getResponse().getStatus();
        JSONObject actualContent3 = new JSONObject(mvcResult3.getResponse().getContentAsString());

        // ASSERT
        assertEquals(expectedStatus3, actualStatus3);
        JSONAssert.assertEquals(expectedContent3, actualContent3, true);

    }

    /**
     * Method createAccountForGroup
     * group doesn't exist in group repository
     *
     * @throws Exception JsonProcessingException
     */
    @Test
    @DisplayName("CreateAccountForGroup - group doesn't exist in the group repository")
    public void createAccountForGroupRepositoryDoesntContainGroupTest() throws Exception {
        //ARRANGE
        Map<String, Object> pathVariableMap = new HashMap<>();
        pathVariableMap.put("personID", "7001@switch.pt");
        pathVariableMap.put("groupID", "7003");

        String uri = UriComponentsBuilder.fromUriString("")
                .path("/people/{personID}/groups/{groupID}/accounts")
                .buildAndExpand(pathVariableMap)
                .toUriString();

        int expectedStatus = 404;
        JSONObject expectedContent = new JSONObject()
                .put("status", "NOT_FOUND")
                .put("message", "Group not found")
                .put("errors", new JSONArray(Collections.singletonList("Group not found")));

        String accountID = "7005";
        String denomination = "shopping";
        String description = "food";

        CreateAccountForGroupInfoDTO infoDTO = new CreateAccountForGroupInfoDTO(accountID, denomination, description);
        String inputJson = super.mapToJson(infoDTO);

        //ACT
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();
        JSONObject actualContent = new JSONObject(mvcResult.getResponse().getContentAsString());

        // ASSERT
        assertEquals(expectedStatus, actualStatus);
        JSONAssert.assertEquals(expectedContent, actualContent, true);
    }

    /**
     * Method createAccountForGroup
     * Account already exists in the account repository
     *
     * @throws Exception JsonProcessingException
     */
    @Test
    @DisplayName("CreateAccountForGroup - account already exists in the account repository")
    public void createAccountForGroupAlreadyExistsInTheAccountRepository() throws Exception {
        //ARRANGE
        Map<String, Object> pathVariableMap = new HashMap<>();
        pathVariableMap.put("personID", "7001@switch.pt");
        pathVariableMap.put("groupID", "7001");

        String uri = UriComponentsBuilder.fromUriString("")
                .path("/people/{personID}/groups/{groupID}/accounts")
                .buildAndExpand(pathVariableMap)
                .toUriString();

        int expectedStatus = 422;
        JSONObject expectedContent = new JSONObject()
                .put("status", "UNPROCESSABLE_ENTITY")
                .put("message", "Account already exists")
                .put("errors", new JSONArray(Collections.singletonList("Account already exists")));

        String accountID = "70010";
        String denomination = "shopping";
        String description = "food";

        CreateAccountForGroupInfoDTO infoDTO = new CreateAccountForGroupInfoDTO(accountID, denomination, description);
        String inputJson = super.mapToJson(infoDTO);

        //ACT
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();
        JSONObject actualContent = new JSONObject(mvcResult.getResponse().getContentAsString());

        // ASSERT
        assertEquals(expectedStatus, actualStatus);
        JSONAssert.assertEquals(expectedContent, actualContent, true);

    }

    /**
     * Method createAccountForGroup
     * Person trying to create account is not manager of the group
     *
     * @throws Exception JsonProcessingException
     */
    @Test
    @DisplayName("CreateAccountForGroup - personID is not in group's list of managers")
    public void createAccountForGroupAccountPersonIsNotManagerOfTheGroupTest() throws Exception {
        String personID = "7000@switch.pt";

        //ARRANGE
        Map<String, Object> pathVariableMap = new HashMap<>();
        pathVariableMap.put("personID", personID);
        pathVariableMap.put("groupID", "7001");

        String uri = UriComponentsBuilder.fromUriString("")
                .path("/people/{personID}/groups/{groupID}/accounts")
                .buildAndExpand(pathVariableMap)
                .toUriString();

        int expectedStatus = 422;
        JSONObject expectedContent = new JSONObject()
                .put("status", "UNPROCESSABLE_ENTITY")
                .put("message", "The person with PersonID{personID=" + personID + "} doesn't have manager privileges")
                .put("errors", new JSONArray(Collections.singletonList("The person with PersonID{personID=" + personID + "} doesn't have manager privileges")));

        String accountID = "7077";
        String denomination = "shopping";
        String description = "food";

        CreateAccountForGroupInfoDTO infoDTO = new CreateAccountForGroupInfoDTO(accountID, denomination, description);
        String inputJson = super.mapToJson(infoDTO);

        //ACT
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();
        JSONObject actualContent = new JSONObject(mvcResult.getResponse().getContentAsString());

        // ASSERT
        assertEquals(expectedStatus, actualStatus);
        JSONAssert.assertEquals(expectedContent, actualContent, true);

    }

    /**
     * Method createAccountForGroup
     * Trying to create account with empty denomination;
     *
     * @throws Exception Input 'denomination' is invalid!
     */
    @Test
    @DisplayName("CreateAccountForGroup - Account denomination cant be null")
    public void createAccountForGroupAccountDenominationCantBeEmptyTest() throws Exception {
        //ARRANGE
        Map<String, Object> pathVariableMap = new HashMap<>();
        pathVariableMap.put("personID", "7001@switch.pt");
        pathVariableMap.put("groupID", "7001");

        String uri = UriComponentsBuilder.fromUriString("")
                .path("/people/{personID}/groups/{groupID}/accounts")
                .buildAndExpand(pathVariableMap)
                .toUriString();

        int expectedStatus = 400;
        JSONObject expectedContent = new JSONObject()
                .put("status", "BAD_REQUEST")
                .put("message", "Input 'denomination' is invalid!")
                .put("errors", new JSONArray(Collections.singletonList("Input 'denomination' is invalid!")));

        String accountID = "7077";
        String denomination = "";
        String description = "food";

        CreateAccountForGroupInfoDTO infoDTO = new CreateAccountForGroupInfoDTO(accountID, denomination, description);
        String inputJson = super.mapToJson(infoDTO);

        //ACT
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();
        JSONObject actualContent = new JSONObject(mvcResult.getResponse().getContentAsString());

        // ASSERT
        assertEquals(expectedStatus, actualStatus);
        JSONAssert.assertEquals(expectedContent, actualContent, true);

    }

    /**
     * Test for method getAccountByID
     * Happy Case
     *
     * @throws Exception JsonProcessingException
     */
    @Test
    @DisplayName("getAccountByID - Happy case")
    public void getAccountByIDHappyCaseTest() throws Exception {

        // ARRANGE
        Map<String, Object> pathVariableMap = new HashMap<>();
        pathVariableMap.put("accountID", "7001");

        String uri = UriComponentsBuilder.fromUriString("")
                .path("/accounts/{accountID}")
                .buildAndExpand(pathVariableMap)
                .toUriString();

        int expectedStatus = 200;
        JSONObject expectedContent = new JSONObject()
                .put("denomination", "football")
                .put("description", "sports")
                .put("accountID", "7001");

        // ACT
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();
        JSONObject actualContent = new JSONObject(mvcResult.getResponse().getContentAsString());

        // ASSERT
        assertEquals(expectedStatus, actualStatus);
        JSONAssert.assertEquals(expectedContent, actualContent, true);
    }

    /**
     * Test for method getAccountByID
     * Ensure error when the account doesn't exist in account repository
     *
     * @throws Exception JsonProcessingException
     */
    @Test
    @DisplayName("getAccountByID - Ensure error if account doesn't exist in repo")
    public void getAccountByIDEnsureErrorWhenAccountDoesNotExistInRepoTest() throws Exception {

        // ARRANGE
        Map<String, Object> pathVariableMap = new HashMap<>();
        pathVariableMap.put("accountID", "7077");

        String uri = UriComponentsBuilder.fromUriString("")
                .path("/accounts/{accountID}")
                .buildAndExpand(pathVariableMap)
                .toUriString();

        int expectedStatus = 404;
        JSONObject expectedContent = new JSONObject()
                .put("status", "NOT_FOUND")
                .put("message", "Account not found")
                .put("errors", new JSONArray(Collections.singletonList("Account not found")));

        // ACT
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();
        JSONObject actualContent = new JSONObject(mvcResult.getResponse().getContentAsString());

        // ASSERT
        assertEquals(expectedStatus, actualStatus);
        JSONAssert.assertEquals(expectedContent, actualContent, true);
    }

    /**
     * Test for method getAccountsByGroupID
     * Happy Case
     *
     * @throws Exception JsonProcessingException
     */
    @Test
    @DisplayName("getAccountsByGroupID - Happy case")
    public void getAccountsByGroupIDHappyCaseTest() throws Exception {

        // ARRANGE
        Map<String, Object> pathVariableMap = new HashMap<>();
        pathVariableMap.put("groupID", "7002");

        String uri = UriComponentsBuilder.fromUriString("")
                .path("/groups/{groupID}/accounts")
                .buildAndExpand(pathVariableMap)
                .toUriString();

        int expectedStatus = 200;
        JSONObject expectedContent = new JSONObject()
                .put("accounts", new JSONArray(Arrays.asList(
                        new JSONObject()
                                .put("_links", new JSONObject()
                                        .put("_self", new JSONObject().
                                                put("href", "http://localhost/accounts/7009")))
                                .put("denomination", "party")
                                .put("description", "party")
                                .put("accountID", "7009")
                        ))
                );

        // ACT
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();
        JSONObject actualContent = new JSONObject(mvcResult.getResponse().getContentAsString());

        // ASSERT
        assertEquals(expectedStatus, actualStatus);
        JSONAssert.assertEquals(expectedContent, actualContent, true);
    }

    /**
     * Test for method getAccountsByGroupID
     * Ensure error when the group doesn't exist in group repository
     *
     * @throws Exception JsonProcessingException
     */
    @Test
    @DisplayName("getAccountsByGroupID - Ensure error if group doesn't exist in repo")
    public void getAccountsByGroupIDEnsureErrorIdGroupDoesNotExistInRepoTest() throws Exception {

        // ARRANGE
        Map<String, Object> pathVariableMap = new HashMap<>();
        pathVariableMap.put("groupID", "8000");

        String uri = UriComponentsBuilder.fromUriString("")
                .path("/groups/{groupID}/accounts")
                .buildAndExpand(pathVariableMap)
                .toUriString();

        int expectedStatus = 404;
        JSONObject expectedContent = new JSONObject()
                .put("status", "NOT_FOUND")
                .put("message", "Group not found")
                .put("errors", new JSONArray(Collections.singletonList("Group not found")));

        // ACT
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();
        JSONObject actualContent = new JSONObject(mvcResult.getResponse().getContentAsString());

        // ASSERT
        assertEquals(expectedStatus, actualStatus);
        JSONAssert.assertEquals(expectedContent, actualContent, true);
    }
}