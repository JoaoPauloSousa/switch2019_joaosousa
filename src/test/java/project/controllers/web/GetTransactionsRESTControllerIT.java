package project.controllers.web;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;
import project.dto.CreateTransactionInfoDTO;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Transactional
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class GetTransactionsRESTControllerIT extends AbstractTest {

    @Override
    @BeforeAll
    public void setUp() {
        super.setUp();
    }

    /**
     * Test for method getTransactionsByPersonID - Happy case
     */
    @Test
    @DisplayName("getTransactionsByPersonID - Happy Case")
    void getTransactionsByPersonIDHappyCaseTest() throws Exception {
        // Note: If a feature to create a transaction for person is ever added,
        // use the uri of the controller to mock a post request.
        //  Refer to test method getTransactionsByGroupIDHappyCaseTest
        //  to see an example of how to do that test with the uri and add the missing STAGES:
        //
        // STAGE 1 - Verify if the person's transaction list is empty at test beginning
        // STAGE 2 - Create some people transactions

        String debitAccountIDStr = ("1001");

        //STAGE 3 - get filtered transactions

        String uriGetTransactionsFilter = "/people/1001@switch.pt/transactions";

        //Arrange
        String expectedContent3 = "{\"transactions\":[{\"amount\":\"500.0\",\"dateTime\":\"2000-01-01T23:20:58\",\"type\":\"1\",\"description\":\"Pacote de Oreos\",\"category\":\"comida e bebida\",\"debitAccountID\":\"1001\",\"creditAccountID\":\"1003\"},{\"amount\":\"400.0\",\"dateTime\":\"2010-01-01T11:39:23\",\"type\":\"-1\",\"description\":\"Pacote de Belgas\",\"category\":\"comida e bebida\",\"debitAccountID\":\"1001\",\"creditAccountID\":\"1003\"}]}";
        int expectedStatus3 = 200;

        //Act
        MvcResult mvcResult3 = mvc.perform(MockMvcRequestBuilders.get(uriGetTransactionsFilter)
                .param("accountID", debitAccountIDStr)
                .param("initialDate", "1999-01-01 00:00:00")
                .param("finalDate", "2021-01-01 00:00:00"))
                .andReturn();
        String actualContent3 = mvcResult3.getResponse().getContentAsString();
        int actualStatus3 = mvcResult3.getResponse().getStatus();

        //Assert
        assertEquals(expectedContent3, actualContent3);
        assertEquals(expectedStatus3, actualStatus3);

    }

    /**
     * Test for method getTransactionsByGroupID - Happy case
     */
    @Test
    @DisplayName("getTransactionsByGroupID - Happy Case")
    void getTransactionsByGroupIDHappyCaseTest() throws Exception {

        //STAGE 1 - Verify if the group's transaction list is empty at test beginning

        //Arrange
        String uriGetTransactions = "/people/8101@switch.pt/groups/8101/transactions";
        String expectedContent1 = "{\"transactions\":[]}";
        int expectedStatus1 = 200;

        //Act
        MvcResult mvcResult1 = mvc.perform(MockMvcRequestBuilders.get(uriGetTransactions)).andReturn();
        String actualContent1 = mvcResult1.getResponse().getContentAsString();
        int actualStatus1 = mvcResult1.getResponse().getStatus();

        //Assert
        assertEquals(expectedContent1, actualContent1);
        assertEquals(expectedStatus1, actualStatus1);

        //STAGE 2 - Create some groups transactions

        String uri = "/people/8101@switch.pt/groups/8101/transactions";

        String type = "Debit";
        String description = "jantar switch";
        String categoryDesignation = "comida";
        String debitAccountID = "8101";
        String creditAccountID = "8102";

        CreateTransactionInfoDTO infoDTO2 = new CreateTransactionInfoDTO("3000", "2010-01-01 00:00:00", type, description,
                categoryDesignation, debitAccountID, creditAccountID);
        String inputJson2 = super.mapToJson(infoDTO2);
        mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson2)).andReturn();

        //STAGE 3 - get filtered transactions

        String uriGetTransactionsFilter = "/people/8101@switch.pt/groups/8101/transactions";

        //Arrange
        String expectedContent3 = "{\"transactions\":[{\"amount\":\"3000.0\",\"dateTime\":\"2010-01-01T00:00\",\"type\":\"-1\",\"description\":\"jantar switch\",\"category\":\"comida\",\"debitAccountID\":\"8101\",\"creditAccountID\":\"8102\"}]}";
        int expectedStatus3 = 200;

        //Act
        MvcResult mvcResult3 = mvc.perform(MockMvcRequestBuilders.get(uriGetTransactionsFilter)
                .param("accountID", debitAccountID)
                .param("initialDate", "2000-01-01 00:00:00")
                .param("finalDate", "2016-01-01 00:00:00"))
                .andReturn();
        String actualContent3 = mvcResult3.getResponse().getContentAsString();
        int actualStatus3 = mvcResult3.getResponse().getStatus();

        //Assert
        assertEquals(expectedContent3, actualContent3);
        assertEquals(expectedStatus3, actualStatus3);


    }

    /**
     * Test for method getTransactionsOfAccountWithinPeriodByPersonID - Happy case
     */
    @Test
    @DisplayName("getTransactionsOfAccountWithinPeriodByPersonID - Happy Case")
    void getTransactionsOfAccountWithinPeriodByPersonIDHappyCaseTest() throws Exception {
        // Note: If a feature to create a transaction for person is ever added,
        // use the uri of the controller to mock a post request.
        //  Refer to test method getTransactionsOfAccountWithinPeriodByGroupIDHappyCaseTest
        //  to see an example of how to do that test with the uri and add the missing STAGES:
        //
        // STAGE 1 - Verify if the person's transaction list is empty at test beginning
        // STAGE 2 - Create some people transactions

        String debitAccountIDStr = ("1001");

        //STAGE 3 - get filtered transactions

        String uriGetTransactionsFilter = "/people/1001@switch.pt/transactionsFilter";

        //Arrange
        String expectedContent3 = "{\"transactions\":[{\"amount\":\"400.0\",\"dateTime\":\"2010-01-01T11:39:23\",\"type\":\"-1\",\"description\":\"Pacote de Belgas\",\"category\":\"comida e bebida\",\"debitAccountID\":\"1001\",\"creditAccountID\":\"1003\"},{\"amount\":\"500.0\",\"dateTime\":\"2000-01-01T23:20:58\",\"type\":\"1\",\"description\":\"Pacote de Oreos\",\"category\":\"comida e bebida\",\"debitAccountID\":\"1001\",\"creditAccountID\":\"1003\"}]}";
        int expectedStatus3 = 200;

        //Act
        MvcResult mvcResult3 = mvc.perform(MockMvcRequestBuilders.get(uriGetTransactionsFilter)
                .param("accountID", debitAccountIDStr)
                .param("initialDate", "1999-01-01 00:00:00")
                .param("finalDate", "2021-01-01 00:00:00"))
                .andReturn();
        String actualContent3 = mvcResult3.getResponse().getContentAsString();
        int actualStatus3 = mvcResult3.getResponse().getStatus();

        //Assert
        assertEquals(expectedContent3, actualContent3);
        assertEquals(expectedStatus3, actualStatus3);
    }

    /**
     * Test for method getTransactionsOfAccountWithinPeriodByGroupID - Happy case
     */
    @Test
    @DisplayName("getTransactionsOfAccountWithinPeriodByGroupID - Happy Case")
    void getTransactionsOfAccountWithinPeriodByGroupIDHappyCaseTest() throws Exception {

        //STAGE 1 - Verify if the group's transaction list is empty at test beginning

        //Arrange
        String uriGetTransactions = "/people/8101@switch.pt/groups/8101/transactions";
        String expectedContent1 = "{\"transactions\":[]}";
        int expectedStatus1 = 200;

        //Act
        MvcResult mvcResult1 = mvc.perform(MockMvcRequestBuilders.get(uriGetTransactions)).andReturn();
        String actualContent1 = mvcResult1.getResponse().getContentAsString();
        int actualStatus1 = mvcResult1.getResponse().getStatus();

        //Assert
        assertEquals(expectedContent1, actualContent1);
        assertEquals(expectedStatus1, actualStatus1);

        //STAGE 2 - Create some groups transactions

        String uri = "/people/8101@switch.pt/groups/8101/transactions";

        String type = "Debit";
        String description = "jantar switch";
        String categoryDesignation = "comida";
        String debitAccountID = "8101";
        String creditAccountID = "8102";

        CreateTransactionInfoDTO infoDTO2 = new CreateTransactionInfoDTO("3000", "2010-01-01 00:00:00", type, description,
                categoryDesignation, debitAccountID, creditAccountID);
        String inputJson2 = super.mapToJson(infoDTO2);
        mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson2)).andReturn();

        //STAGE 3 - get filtered transactions

        String uriGetTransactionsFilter = "/people/8101@switch.pt/groups/8101/transactionsFilter";

        //Arrange
        String expectedContent3 = "{\"transactions\":[{\"amount\":\"3000.0\",\"dateTime\":\"2010-01-01T00:00\",\"type\":\"-1\",\"description\":\"jantar switch\",\"category\":\"comida\",\"debitAccountID\":\"8101\",\"creditAccountID\":\"8102\"}]}";
        int expectedStatus3 = 200;

        //Act
        MvcResult mvcResult3 = mvc.perform(MockMvcRequestBuilders.get(uriGetTransactionsFilter)
                .param("accountID", debitAccountID)
                .param("initialDate", "2000-01-01 00:00:00")
                .param("finalDate", "2016-01-01 00:00:00"))
                .andReturn();
        String actualContent3 = mvcResult3.getResponse().getContentAsString();
        int actualStatus3 = mvcResult3.getResponse().getStatus();

        //Assert
        assertEquals(expectedContent3, actualContent3);
        assertEquals(expectedStatus3, actualStatus3);
    }

}