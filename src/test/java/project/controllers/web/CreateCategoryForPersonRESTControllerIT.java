package project.controllers.web;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;
import project.dto.CreateCategoryInfoDTO;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Transactional
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CreateCategoryForPersonRESTControllerIT extends AbstractTest {

    @Override
    @BeforeAll
    public void setUp() {
        super.setUp();
    }

    /**
     * createcategoryForPersonRestController - Happy Case
     * Status 200.
     *
     * @throws Exception JsonProcessingException
     */
    @Test
    @DisplayName("Test for createCategoryForPersonRESTController - Happy Case")
    public void createCategoryForPersonRestControllerHappyCaseTest() throws Exception {
        //ArrangePOST
        String uriPOST = "/people/51010@switch.pt/categories";
        String designationPOST = "Equipamentos";

        CreateCategoryInfoDTO infoDTO = new CreateCategoryInfoDTO(designationPOST);
        String inputJson = super.mapToJson(infoDTO);
        MvcResult mvcResultPOST = mvc.perform(MockMvcRequestBuilders.post(uriPOST).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        //ActPOST
        int statusPOST = mvcResultPOST.getResponse().getStatus();
        String contentPOST = mvcResultPOST.getResponse().getContentAsString();

        //AssertPOST
        assertEquals(201, statusPOST);
        assertEquals("{\"personID\":\"51010@switch.pt\",\"_links\":{\"self\":{\"href\":\"http://localhost/people/51010@switch.pt/categories\"}}}", contentPOST);

        //ArrangeGET
        String uriGET = "/people/51010@switch.pt/categories";

        MvcResult mvcResultGET = mvc.perform(MockMvcRequestBuilders.get(uriGET).contentType(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        //ActGET
        int statusGET = mvcResultGET.getResponse().getStatus();
        String contentGET = mvcResultGET.getResponse().getContentAsString();

        //AssertGET
        assertEquals(200, statusGET);
        assertEquals("{\"categories\":[{\"designation\":\"Equipamentos\"},{\"designation\":\"Cerveja\"}]}", contentGET);
    }

    /**
     * createCategoryForPerson
     * ensure that a category is not added if category already exist in the group
     * return Message Error - "Category already exists"
     *
     * @throws Exception JsonProcessingException
     */
    @Test
    @DisplayName("ensure that a category is not added if already exists in the group")
    public void createCategoryForPersonEnsureCategoryCantBeAddedIfAlreadyExistInThePersonTest() throws Exception {

        String uri = "/people/51010@switch.pt/categories";
        String designation = "Cerveja";

        String expected = "{\"status\":\"UNPROCESSABLE_ENTITY\",\"message\":\"Category already exists\",\"errors\":[\"Category already exists\"]}";

        CreateCategoryInfoDTO infoDTO = new CreateCategoryInfoDTO(designation);
        String inputJson = super.mapToJson(infoDTO);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(422, status);

        String content = mvcResult.getResponse().getContentAsString();
        assertEquals(expected, content);
    }

    /**
     * createCategoryForPerson
     * ensure that a category is not added to a group that is not in the repository
     * return Message Error - "Group not found"
     *
     * @throws Exception JsonProcessingException
     */
    @Test
    @DisplayName("ensure that a category is not added to a group that is not in the repository")
    public void createCategoryForPersonEnsureCategoryIsNotAddedToAPersonThatIsNotInTheRepositoryTest() throws Exception {
        String uri = "/people/1@switch.pt/categories";
        String designation = "Comida";

        String expected = "{\"status\":\"NOT_FOUND\",\"message\":\"Person not found\",\"errors\":[\"Person not found\"]}";

        CreateCategoryInfoDTO infoDTO = new CreateCategoryInfoDTO(designation);
        String inputJson = super.mapToJson(infoDTO);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(404, status);

        String content = mvcResult.getResponse().getContentAsString();
        assertEquals(expected, content);
    }

    /**
     * createCategoryForPerson
     * ensure that a exception is thrown when the category input is invalid
     * return Message Error - ""
     *
     * @throws Exception JsonProcessingException
     */
    @Test
    @DisplayName("ensure that a exception is thrown when the category input is invalid")
    public void createCategoryForPersonEnsureExceptionWhenTheCategoryInputIsInvalidTest() throws Exception {

        String uri = "/people/51010@switch.pt/categories";
        String designation = "";

        String expected = "{\"status\":\"BAD_REQUEST\",\"message\":\"Input 'designation' is invalid!\",\"errors\":[\"Input 'designation' is invalid!\"]}";

        CreateCategoryInfoDTO infoDTO = new CreateCategoryInfoDTO(designation);
        String inputJson = super.mapToJson(infoDTO);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(400, status);

        String content = mvcResult.getResponse().getContentAsString();
        assertEquals(expected, content);
    }

}
