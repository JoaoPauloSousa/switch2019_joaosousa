package project.controllers.web;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;
import project.dto.CreateCategoryInfoDTO;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Transactional
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CreateCategoryForGroupRESTControllerIT extends AbstractTest {

    @Override
    @BeforeAll
    public void setUp() {
        super.setUp();
    }

    /**
     * createCategoryForGroupRestController - Happy Case
     * Status 200.
     *
     * @throws Exception JsonProcessingException
     */
    @Test
    @DisplayName("Test for createCategoryForGroupRESTController - Happy Case")
    public void createCategoryForGroupRestControllerHappyCaseTest() throws Exception {
        //ArrangePOST
        String uriPOST = "/people/51010@switch.pt/groups/510200/categories";
        String designationPOST = "Equipamentos";

        CreateCategoryInfoDTO infoDTO = new CreateCategoryInfoDTO(designationPOST);
        String inputJson = super.mapToJson(infoDTO);
        MvcResult mvcResultPOST = mvc.perform(MockMvcRequestBuilders.post(uriPOST).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        //ActPOST
        int statusPOST = mvcResultPOST.getResponse().getStatus();
        String contentPOST = mvcResultPOST.getResponse().getContentAsString();

        //AssertPOST
        assertEquals(201, statusPOST);
        assertEquals("{\"groupID\":\"510200\",\"groupDescription\":\"Futebol\",\"newCategory\":\"Equipamentos\",\"_links\":{\"self\":{\"href\":\"http://localhost/groups/510200/categories\"}}}", contentPOST);

        //ArrangeGET
        String uriGET = "/groups/510200/categories";

        MvcResult mvcResultGET = mvc.perform(MockMvcRequestBuilders.get(uriGET).contentType(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        //ActGET
        int statusGET = mvcResultGET.getResponse().getStatus();
        String contentGET = mvcResultGET.getResponse().getContentAsString();

        //AssertGET
        assertEquals(200, statusGET);
        assertEquals("{\"categories\":[{\"designation\":\"Equipamentos\"},{\"designation\":\"Arbitros\"}]}", contentGET);
    }

    /**
     * createCategoryForGroup
     * ensure that a category is not added if category already exist in the group
     * return Message Error - "Category already exists"
     *
     * @throws Exception JsonProcessingException
     */
    @Test
    @DisplayName("ensure that a category is not added if already exists in the group")
    public void createCategoryForGroupEnsureCategoryCantBeAddedIfAlreadyExistInTheGroupTest() throws Exception {

        String uri = "/people/51010@switch.pt/groups/510200/categories";
        String designation = "Arbitros";

        String expected = "{\"status\":\"UNPROCESSABLE_ENTITY\",\"message\":\"Category already exists\",\"errors\":[\"Category already exists\"]}";

        CreateCategoryInfoDTO infoDTO = new CreateCategoryInfoDTO(designation);
        String inputJson = super.mapToJson(infoDTO);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(422, status);

        String content = mvcResult.getResponse().getContentAsString();
        assertEquals(expected, content);
    }

    /**
     * createCategoryForGroup
     * ensure that a category is not added to a group that is not in the repository
     * return Message Error - "Group not found"
     *
     * @throws Exception JsonProcessingException
     */
    @Test
    @DisplayName("ensure that a category is not added to a group that is not in the repository")
    public void createCategoryForGroupEnsureCategoryIsNotAddedToAGroupThatIsNotInTheRepositoryTest() throws Exception {
        String uri = "/people/51010@switch.pt/groups/510300/categories";
        String designation = "Comida";

        String expected = "{\"status\":\"NOT_FOUND\",\"message\":\"Group not found\",\"errors\":[\"Group not found\"]}";

        CreateCategoryInfoDTO infoDTO = new CreateCategoryInfoDTO(designation);
        String inputJson = super.mapToJson(infoDTO);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(404, status);

        String content = mvcResult.getResponse().getContentAsString();
        assertEquals(expected, content);
    }

    /**
     * createCategoryForGroup
     * Ensure Person Is Manager Of The Group
     * return Message Error "Person isn't manager of this group"
     *
     * @throws Exception JsonProcessingException
     */
    @Test
    @DisplayName("Ensure Person Is Manager Of The Group ")
    public void createCategoryForGroupEnsurePersonIsManagerOfTheGroupTest() throws Exception {
        String uri = "/people/5101@switch.pt/groups/510200/categories";
        String designation = "Comida";

        String expected = "{\"status\":\"FORBIDDEN\",\"message\":\"Person is not manager of the group\",\"errors\":[\"Person is not manager of the group\"]}";

        CreateCategoryInfoDTO infoDTO = new CreateCategoryInfoDTO(designation);
        String inputJson = super.mapToJson(infoDTO);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(403, status);

        String content = mvcResult.getResponse().getContentAsString();
        assertEquals(expected, content);
    }

    /**
     * createCategoryForGroup
     * ensure that a exception is thrown when the category input is invalid
     * return Message Error - ""
     *
     * @throws Exception JsonProcessingException
     */
    @Test
    @DisplayName("ensure that a exception is thrown when the category input is invalid")
    public void createCategoryForGroupEnsureExceptionWhenTheCategoryInputIsInvalidTest() throws Exception {

        String uri = "/people/51010@switch.pt/groups/510200/categories";
        String designation = "";

        String expected = "{\"status\":\"BAD_REQUEST\",\"message\":\"Input 'designation' is invalid!\",\"errors\":[\"Input 'designation' is invalid!\"]}";

        CreateCategoryInfoDTO infoDTO = new CreateCategoryInfoDTO(designation);
        String inputJson = super.mapToJson(infoDTO);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(400, status);

        String content = mvcResult.getResponse().getContentAsString();
        assertEquals(expected, content);
    }

}
