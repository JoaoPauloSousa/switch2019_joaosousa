package project.controllers.web;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.CreateTransactionForGroupAssembler;
import project.dto.CreateTransactionForGroupRequestDTO;
import project.dto.CreateTransactionForGroupResponseDTO;
import project.dto.CreateTransactionInfoDTO;
import project.exceptions.AccountNotFoundException;
import project.exceptions.GroupNotFoundException;
import project.exceptions.InvalidFieldException;
import project.exceptions.LedgerNotFoundException;
import project.frameworkddd.IUSCreateTransactionForGroupService;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
class CreateTransactionForGroupRESTControllerTest {

    @Autowired
    private IUSCreateTransactionForGroupService service;

    @Autowired
    private CreateTransactionForGroupRESTController controller;

    /**
     * Test for method createTransactionForGroup
     * Happy Case
     */
    @Test
    void createTransactionForGroupHappyCaseTest() {
        //Arrange
        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO("1995.55", "2000-01-01 12:00:00", "Debit",
                "transaction description", "category designation", "1000", "2000");
        String personId = "10@switch.com";
        String groupId = "100";

        CreateTransactionForGroupRequestDTO requestDTO = CreateTransactionForGroupAssembler.mapToRequestDTO(personId,
                groupId, infoDTO);
        CreateTransactionForGroupResponseDTO responseDTO = new CreateTransactionForGroupResponseDTO(groupId, "group " +
                "description", "1995.55", "Debit", "transaction description");

        Mockito.when(service.createTransactionForGroup(requestDTO)).thenReturn(responseDTO);

        //Act
        Object actualBody = controller.createTransactionForGroup(infoDTO, personId, groupId).getBody();
        Object actualStatus = controller.createTransactionForGroup(infoDTO, personId, groupId).getStatusCode();

        //Assert
        assertEquals(responseDTO, actualBody);
        assertEquals(HttpStatus.CREATED, actualStatus);

    }

    /**
     * Test for method createTransactionForGroup
     * Ensure throws GroupNotFoundException
     */
    @Test
    void createTransactionForGroupGroupEnsureThrowsGroupNotFoundExceptionTest() {
        //Arrange
        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO("1995.55", "2000-01-01 12:00:00", "Debit",
                "transaction description", "category designation", "1000", "2000");
        String personId = "20@switch.com";
        String groupId = "200";

        CreateTransactionForGroupRequestDTO requestDTO = CreateTransactionForGroupAssembler.mapToRequestDTO(personId,
                groupId, infoDTO);

        Mockito.when(service.createTransactionForGroup(requestDTO)).thenThrow(GroupNotFoundException.class);

        //Act
        //Assert
        assertThrows(GroupNotFoundException.class, () -> {
            controller.createTransactionForGroup(infoDTO, personId, groupId);
        });

    }

    /**
     * Test for method createTransactionForGroup
     * Ensure throws LedgerNotFoundException
     */
    @Test
    void createTransactionForGroupEnsureThrowsLedgerNotFoundExceptionTest() {
        //Arrange
        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO("1995.55", "2000-01-01 12:00:00", "Debit",
                "transaction description", "category designation", "1000", "2000");
        String personId = "30@switch.com";
        String groupId = "300";

        CreateTransactionForGroupRequestDTO requestDTO = CreateTransactionForGroupAssembler.mapToRequestDTO(personId,
                groupId, infoDTO);

        Mockito.when(service.createTransactionForGroup(requestDTO)).thenThrow(LedgerNotFoundException.class);

        //Act
        //Assert
        assertThrows(LedgerNotFoundException.class, () -> {
            controller.createTransactionForGroup(infoDTO, personId, groupId);
        });

    }

    /**
     * Test for method createTransactionForGroup
     * Ensure throws AccountNotFoundException
     */
    @Test
    void createTransactionForGroupEnsureThrowsAccountNotFoundExceptionTest() {
        //Arrange
        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO("1995.55", "2000-01-01 12:00:00", "Debit",
                "transaction description", "category designation", "1000", "2000");
        String personId = "40@switch.com";
        String groupId = "400";

        CreateTransactionForGroupRequestDTO requestDTO = CreateTransactionForGroupAssembler.mapToRequestDTO(personId,
                groupId, infoDTO);

        Mockito.when(service.createTransactionForGroup(requestDTO)).thenThrow(AccountNotFoundException.class);

        //Act
        //Assert
        assertThrows(AccountNotFoundException.class, () -> {
            controller.createTransactionForGroup(infoDTO, personId, groupId);
        });

    }

    /**
     * Test for method createTransactionForGroup
     * Ensure throws InvalidFieldException
     */
    @Test
    void createTransactionForGroupEnsureThrowsInvalidFieldExceptionTest() {
        //Arrange
        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO("1995.55", "2000-01-01 12:00:00", "Debit",
                "transaction description", "category designation", "1000", "2000");
        String personId = "50@switch.com";
        String groupId = "500";

        CreateTransactionForGroupRequestDTO requestDTO = CreateTransactionForGroupAssembler.mapToRequestDTO(personId,
                groupId, infoDTO);

        Mockito.when(service.createTransactionForGroup(requestDTO)).thenThrow(InvalidFieldException.class);

        //Act
        //Assert
        assertThrows(InvalidFieldException.class, () -> {
            controller.createTransactionForGroup(infoDTO, personId, groupId);
        });

    }

}