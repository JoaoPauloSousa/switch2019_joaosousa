//package project.controllers;
//
//import org.junit.jupiter.api.Test;
//import project.model.domainentities.account.Account;
//import project.model.domainentities.shared.Category;
//import project.model.domainentities.person.Person;
//import project.model.domainentities.ledger.Type;
//
//import static org.junit.jupiter.api.Assertions.assertFalse;
//import static org.junit.jupiter.api.Assertions.assertTrue;
//
//class CreateTransactionPersonControllerTest {
//
//    /**
//     * Test for creating a personal transaction  controller
//     * Happy case
//     */
//    @Test
//    void createTransactionPersonHappyCaseTest() {
//        //Arrange
//        CreateTransactionPersonController control = new CreateTransactionPersonController();
//        Person Rui = new Person("Rui", "Porto", "Porto", "1999-02-02", null, null);
//        Rui.addCategory("bills");
//        Rui.addAccount("gas","gas");
//        Rui.addAccount("water","water");
//        Category bills = new Category("bills");
//        Account gas = new Account("gas","gas");
//        Account water = new Account("water","water");
//
//        //Act
//        boolean result = control.createTransaction(Rui, 100, Type.DEBIT, "2019-02-23 00:00:00", "Supermarket", bills, gas, water);
//        //Assert
//        assertTrue(result);
//    }
//
//    /**
//     * Test for creating a personal transaction controller
//     * False if try to add an equal transaction already existent
//     */
//    @Test
//    void createTransactionPersonNotTrueTest() {
//        //Arrange
//        CreateTransactionPersonController control = new CreateTransactionPersonController();
//        Person Rui = new Person("Rui", "Porto", "Porto", "1999-02-02", null, null);
//        Rui.addCategory("bills");
//        Rui.addAccount("gas", "gas");
//        Rui.addAccount("water", "water");
//        Category bills = new Category("bills");
//        Account gas = new Account("gas", "gas");
//        Account water = new Account("water", "water");
//        Rui.addTransaction(100, Type.DEBIT, "2019-02-23 00:00:00", "Supermarket", bills, gas, water);
//
//        //Act
//        boolean result = control.createTransaction(Rui, 100, Type.DEBIT, "2019-02-23 00:00:00", "Supermarket", bills, gas, water);
//        ;
//        //Assert
//        assertFalse(result);
//    }
//}