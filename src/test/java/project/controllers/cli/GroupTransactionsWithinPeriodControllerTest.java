//package project.controllers;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.DisplayName;
//import org.junit.jupiter.api.Test;
//import project.model.domainentities.account.Account;
//import project.model.domainentities.group.Group;
//import project.model.domainentities.ledger.Transaction;
//import project.model.domainentities.ledger.Type;
//import project.model.domainentities.person.Person;
//import project.model.domainentities.shared.Category;
//import project.model.domainentities.shared.Period;
//
//import java.util.HashSet;
//import java.util.Set;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//
//class GroupTransactionsWithinPeriodControllerTest {
//
//    GroupTransactionsWithinPeriodController control;
//    Person joao;
//    Group g1;
//    Category c1;
//    Category c2;
//    Account debit;
//    Account credit;
//    Account debit2;
//    Account credit2;
//
//    @BeforeEach
//    public void arrange() {
//        control = new GroupTransactionsWithinPeriodController();
//        joao = new Person("João", "Travessa de santa Bárbara", "Aveiro", "1987-04-17", null, null);
//        g1 = new Group("Saude", "2018-01-26", joao);
//
//        c1 = new Category("health");
//        c2 = new Category("medication");
//
//        debit = new Account("pills", "pay pills");
//        credit = new Account("pharmacy", "pharmacy from main street");
//        debit2 = new Account("health", "pay medical expenses");
//        credit2 = new Account("hospital", "hospital bills");
//        g1.addCategory("health");
//        g1.addCategory("medication");
//        g1.addAccount("pills", "pay pills");
//        g1.addAccount("health", "pay medical expenses");
//        g1.addAccount("hospital", "hospital bills");
//        g1.addAccount("pharmacy", "pharmacy from main street");
//    }
//
//    /**
//     * Test for getGroupTransactionsWithinPeriod
//     * Happy case
//     */
//    @Test
//    @DisplayName("Test for GroupTransactionsControllerTest - HappyCase")
//    void GroupTransactionsWithinPeriodControllerHappyCaseTest() {
//        //ARRANGE
//        Transaction one = new Transaction(20.50, Type.DEBIT, "2020-02-25 00:00:00", "health", c1, debit, credit);
//        Transaction two = new Transaction(30.50, Type.DEBIT, "2020-03-25 00:00:00", "gym", c2, debit2, credit2);
//        g1.addTransaction(20.50, Type.DEBIT, "2020-02-25 00:00:00", "health", c1, debit, credit);
//        g1.addTransaction(30.50, Type.DEBIT, "2020-03-25 00:00:00", "gym", c2, debit2, credit2);
//        g1.addTransaction(50.50, Type.DEBIT, "2021-03-25 00:00:00", "groceries", c2, debit2, credit2);
//        g1.addTransaction(1000.50, Type.DEBIT, "2021-01-25 00:00:00", "electronics", c1, debit, credit);
//        Set<Transaction> transactions = new HashSet<>();
//        transactions.add(one);
//        transactions.add(two);
//        Period period = new Period("2020-01-01 00:00:00", "2020-12-31 00:00:00");
//        String expected = transactions.toString();
//
//        //ACT
//        String result = control.getGroupTransactionsWithinPeriod(g1, period);
//
//        //ASSERT
//        assertEquals(expected, result);
//    }
//
//    /**
//     * Test for getGroupTransactionsWithinPeriod
//     * No occurrences
//     */
//    @Test
//    @DisplayName("Test for GroupTransactionsControllerTest - No occurrences")
//    void GroupTransactionsWithinPeriodControllerNoOccurrencesTest() {
//        //ARRANGE
//        g1.addTransaction(20.50, Type.DEBIT, "2019-02-25 00:00:00", "health", c1, debit, credit);
//        g1.addTransaction(30.50, Type.DEBIT, "2021-03-25 00:00:00", "gym", c2, debit2, credit2);
//        Period period = new Period("2020-01-01 00:00:00", "2020-12-31 00:00:00");
//        Set<Transaction> transactions = new HashSet<>();
//        String expected = transactions.toString();
//
//        //ACT
//        String result = control.getGroupTransactionsWithinPeriod(g1, period);
//
//        //ASSERT
//        assertEquals(expected, result);
//    }
//
//    /**
//     * Test for getGroupTransactionsWithinPeriod
//     * Ensure if limit date is included
//     */
//    @Test
//    @DisplayName("Test for GroupTransactionsControllerTest - Limit date case")
//    void GroupTransactionsWithinPeriodControllerLimitDateTest() {
//        //ARRANGE
//        Transaction one = new Transaction(20.50, Type.DEBIT, "2020-12-31 00:00:00", "health", c1, debit, credit);
//        Transaction two = new Transaction(30.50, Type.DEBIT, "2020-03-25 00:00:00", "gym", c2, debit2, credit2);
//        g1.addTransaction(20.50, Type.DEBIT, "2020-12-31 00:00:00", "health", c1, debit, credit);
//        g1.addTransaction(30.50, Type.DEBIT, "2020-03-25 00:00:00", "gym", c2, debit2, credit2);
//        Period period = new Period("2020-01-01 00:00:00", "2020-12-31 00:00:00");
//        Set<Transaction> transactions = new HashSet<>();
//        transactions.add(one);
//        transactions.add(two);
//        String expected = transactions.toString();
//
//        //ACT
//        String result = control.getGroupTransactionsWithinPeriod(g1, period);
//
//        //ASSERT
//        assertEquals(expected, result);
//    }
//
//    /**
//     * Test for getGroupTransactionsWithinPeriod
//     * Ensure if start date is included
//     */
//    @Test
//    @DisplayName("Test for GroupTransactionsControllerTest - Start date case")
//    void GroupTransactionsWithinPeriodControllerStartDateTest() {
//        //ARRANGE
//        Transaction one = new Transaction(20.50, Type.DEBIT, "2020-01-01 00:00:00", "health", c1, debit, credit);
//        Transaction two = new Transaction(30.50, Type.DEBIT, "2020-03-25 00:00:00", "gym", c2, debit2, credit2);
//        g1.addTransaction(20.50, Type.DEBIT, "2020-01-01 00:00:00", "health", c1, debit, credit);
//        g1.addTransaction(30.50, Type.DEBIT, "2020-03-25 00:00:00", "gym", c2, debit2, credit2);
//        Period period = new Period("2020-01-01 00:00:00", "2020-12-31 00:00:00");
//        Set<Transaction> transactions = new HashSet<>();
//        transactions.add(one);
//        transactions.add(two);
//        String expected = transactions.toString();
//
//        //ACT
//        String result = control.getGroupTransactionsWithinPeriod(g1, period);
//
//        //ASSERT
//        assertEquals(expected, result);
//    }
//
//}