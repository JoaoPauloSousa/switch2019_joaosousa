//package project.controllers;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.DisplayName;
//import org.junit.jupiter.api.Test;
//import project.model.domainentities.account.Account;
//import project.model.domainentities.group.Group;
//import project.model.domainentities.ledger.Transaction;
//import project.model.domainentities.ledger.Type;
//import project.model.domainentities.person.Person;
//import project.model.domainentities.shared.Category;
//import project.model.domainentities.shared.Period;
//
//import java.util.HashSet;
//import java.util.Set;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertNotEquals;
//
//class GetTransactionsOfAccountWithinPeriodControllerTest {
//
//    private GetTransactionsOfAccountWithinPeriodController control;
//
//    private Person Manuel;
//
//    private Account personalAccount1;
//    private Account personalAccount2;
//
//    private Group group1;
//
//    private Account groupAccount1;
//    private Account groupAccount2;
//
//    private Category newCategory;
//
//
//    @BeforeEach
//    public void init() {
//        control = new GetTransactionsOfAccountWithinPeriodController();
//
//
//        Manuel = new Person("manuel", "Manuel Pires Correia", "Vila Real", "1991-02-09", null, null);
//
//        personalAccount1 = new Account("Conta_Pessoal", "Conta Pessoal do Manuel");
//        personalAccount2 = new Account("Conta_Pessoal", "Conta pessoal do Manuel");
//
//        Manuel.addAccount("Conta_Pessoal", "Conta Pessoal do Manuel");
//        Manuel.addAccount("Conta_Pessoal", "Conta pessoal do Manuel");
//
//        newCategory = new Category("Comida");
//        Manuel.addCategory("Comida");
//
//
//        group1 = new Group("Grupo do Manuel", "2019-01-01", Manuel);
//
//        groupAccount1 = new Account("Conta_Pessoal", "Conta Pessoal do Manuel");
//        groupAccount2 = new Account("Conta_Pessoal", "Conta pessoal do Manuel");
//
//        group1.addAccount("Conta_Pessoal", "Conta Pessoal do Manuel");
//        group1.addAccount("Conta_Pessoal", "Conta pessoal do Manuel");
//
//        group1.addCategory("Comida");
//
//    }
//
//    /**
//     * Test for getTransactionOfAccountWithinPeriodController
//     * Happy Case
//     */
//    @Test
//    @DisplayName("Test for getTransactionsOfPersonAccountWithinPeriod - HappyCase")
//    void getTransactionsOfPersonAccountWithinPeriodHappyCaseTest() {
//        //ARRANGE
//
//        Manuel.addTransaction(20, Type.DEBIT, "2020-01-01 00:00:00", "Oreos", newCategory, personalAccount1, personalAccount2);
//        Manuel.addTransaction(100, Type.CREDIT, "2020-01-01 23:59:59", "Peixe", newCategory, personalAccount1, personalAccount2);
//
//        Transaction firstTransaction = new Transaction(20, Type.DEBIT, "2020-01-01 00:00:00", "Oreos", newCategory, personalAccount1, personalAccount2);
//        Transaction secondTransaction = new Transaction(100, Type.CREDIT, "2020-01-01 23:59:59", "Peixe", newCategory, personalAccount1, personalAccount2);
//        Set<Transaction> transactions = new HashSet<>();
//
//        transactions.add(secondTransaction);
//        transactions.add(firstTransaction);
//
//        Period period = new Period("2020-01-01 00:00:00", "2020-01-01 23:59:59");
//
//        String expected = transactions.toString();
//
//
//        //ACT
//        String result = control.getTransactionsOfPersonAccountWithinPeriodController(Manuel, personalAccount2, period);
//
//        //ASSERT
//        assertEquals(expected, result);
//    }
//
//    /**
//     * Test for getTransactionOfAccountWithinPeriodController
//     * Fail Case - transaction outside the given period.
//     */
//    @Test
//    @DisplayName("Test for getTransactionsOfPersonAccountWithinPeriod - Fail Case")
//    void getTransactionsOfPersonAccountWithinPeriodFailCaseTest() {
//        //ARRANGE
//
//        Manuel.addTransaction(20, Type.DEBIT, "2020-01-01 00:00:00", "Oreos", newCategory, personalAccount1, personalAccount2);
//        Manuel.addTransaction(100, Type.CREDIT, "2020-02-01 00:00:00", "Peixe", newCategory, personalAccount1, personalAccount2);
//        Manuel.addTransaction(200, Type.DEBIT,"2020-03-01 10:00:00","Filet mignon", newCategory,personalAccount1,personalAccount2);
//
//        Transaction firstTransaction = new Transaction(20, Type.DEBIT, "2020-01-01 00:00:00", "Oreos", newCategory, personalAccount1, personalAccount2);
//        Transaction secondTransaction = new Transaction(100, Type.CREDIT, "2020-02-01 00:00:00", "Peixe", newCategory, personalAccount1, personalAccount2);
//        Transaction thirdTransaction = new Transaction(200, Type.DEBIT, "2020-03-01 10:00:00", "Filet mignon", newCategory, personalAccount1, personalAccount2);
//        Set<Transaction> transactions = new HashSet<>();
//
//        transactions.add(thirdTransaction);
//        transactions.add(secondTransaction);
//        transactions.add(firstTransaction);
//
//        Period period = new Period("2020-01-01 00:00:00", "2020-02-29 23:59:59");
//
//        String expected = transactions.toString();
//
//        //ACT
//        String result = control.getTransactionsOfPersonAccountWithinPeriodController(Manuel, personalAccount2, period);
//
//        //ASSERT
//        assertNotEquals(expected, result);
//    }
//
//    /**
//     * Test for getTransactionOfAccountWithinPeriodController
//     * Happy Case
//     */
//    @Test
//    @DisplayName("Test for getTransactionsOfGroupAccountWithinPeriod - HappyCase")
//    void getTransactionsOfGroupAccountWithinPeriodHappyCaseTest() {
//        //ARRANGE
//
//        group1.addTransaction(20, Type.DEBIT, "2020-01-01 00:00:00", "Oreos", newCategory, personalAccount1, personalAccount2);
//        group1.addTransaction(100, Type.CREDIT, "2020-01-10 00:00:00", "Peixe", newCategory, personalAccount1, personalAccount2);
//
//        Transaction firstTransaction = new Transaction(20, Type.DEBIT, "2020-01-01 00:00:00", "Oreos", newCategory, personalAccount1, personalAccount2);
//        Transaction secondTransaction = new Transaction(100, Type.CREDIT, "2020-01-10 00:00:00", "Peixe", newCategory, personalAccount1, personalAccount2);
//        Set<Transaction> transactions = new HashSet<>();
//
//        transactions.add(secondTransaction);
//        transactions.add(firstTransaction);
//
//        Period period = new Period("2019-01-01 00:00:00", "2021-01-01 23:59:59");
//
//        String expected = transactions.toString();
//
//        //ACT
//        String result = control.getTransactionsOfGroupAccountWithinPeriodController(group1, personalAccount2, period);
//
//        //ASSERT
//        assertEquals(expected, result);
//    }
//
//    /**
//     * Test for getTransactionOfAccountWithinPeriodController
//     * Fail Case - transaction outside the given period.
//     */
//    @Test
//    @DisplayName("Test for getTransactionsOfGroupAccountWithinPeriod - Fail Case")
//    void getTransactionsOfGroupAccountWithinPeriodFailCaseTest() {
//        //ARRANGE
//
//        group1.addTransaction(20, Type.DEBIT, "2020-01-01 00:00:00", "Oreos", newCategory, groupAccount1, groupAccount2);
//        group1.addTransaction(100, Type.CREDIT, "2020-02-01 00:00:00", "Peixe", newCategory, groupAccount1, groupAccount2);
//        group1.addTransaction(40, Type.DEBIT, "2020-03-01 00:00:00","Carne", newCategory,groupAccount1,groupAccount2);
//
//        Transaction thirdTransaction = new Transaction(40, Type.DEBIT, "2020-03-01 00:00:00", "Carne", newCategory, groupAccount1, groupAccount2);
//        Transaction firstTransaction = new Transaction(20, Type.DEBIT, "2020-01-01 00:00:00", "Oreos", newCategory, groupAccount1, groupAccount2);
//        Transaction secondTransaction = new Transaction(100, Type.CREDIT, "2020-02-01 00:00:00", "Peixe", newCategory, groupAccount1, groupAccount2);
//        Set<Transaction> transactions = new HashSet<>();
//
//        transactions.add(secondTransaction);
//        transactions.add(firstTransaction);
//        transactions.add(thirdTransaction);
//
//        Period period = new Period("2020-01-01 00:00:00", "2020-02-01 23:59:59");
//
//        String expected = transactions.toString();
//
//        //ACT
//        String result = control.getTransactionsOfGroupAccountWithinPeriodController(group1, groupAccount2, period);
//
//        //ASSERT
//        assertNotEquals(expected, result);
//    }
//
//
//}