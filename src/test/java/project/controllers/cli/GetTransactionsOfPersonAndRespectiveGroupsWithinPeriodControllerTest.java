//package project.controllers;
//
//import org.junit.jupiter.api.DisplayName;
//import org.junit.jupiter.api.Test;
//import project.model.*;
//import project.model.domainentities.account.Account;
//import project.model.domainentities.group.Group;
//import project.model.domainentities.ledger.Transaction;
//import project.model.domainentities.ledger.Type;
//import project.model.domainentities.person.Person;
//import project.model.domainentities.shared.Category;
//import project.model.domainentities.shared.Period;
//
//import java.util.HashSet;
//import java.util.Set;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//
//class GetTransactionsOfPersonAndRespectiveGroupsWithinPeriodControllerTest {
//
//    /**
//     * Test for getTransactionsOfPersonAndRespectiveGroupsWithinPeriod
//     * Happy Case
//     */
//    @Test
//    @DisplayName("Test for getTransactionsOfPersonAndRespectiveGroupsWithinPeriodController - Happy Case")
//    void getTransactionsOfPersonAndRespectiveGroupsWithinPeriodHappyCaseTest() {
//        //Arrange
//
//        GetTransactionsOfPersonAndRespectiveGroupsWithinPeriodController control = new GetTransactionsOfPersonAndRespectiveGroupsWithinPeriodController();
//
//        Person maria = new Person("Maria", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null);
//        Person paulo = new Person("Paulo", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null);
//        Person andre = new Person("Andre", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", maria, paulo);
//
//        Account smithsGroceriesAccount = new Account("Smith's Groceries", "Account just for Smith family's groceries");
//        andre.addAccount("Smith's Groceries", "Account just for Smith family's groceries");
//
//        Account zeCreditAccount = new Account("Zé & Associados", "Current account of the 'A Loja do Zé' grocery shop");
//        andre.addAccount("Zé & Associados", "Current account of the 'A Loja do Zé' grocery shop");
//
//        Category newCategory = new Category("Groceries");
//        andre.addCategory("Groceries");
//
//        andre.addTransaction(25, Type.DEBIT, "2020-01-07 00:00:00", "Groceries for the week", newCategory, smithsGroceriesAccount, zeCreditAccount);
//        andre.addTransaction(149.49, Type.DEBIT, "2018-12-31 23:59:59", "New year's champagne", newCategory, smithsGroceriesAccount, zeCreditAccount);
//
//
//        Group switchGroup = new Group("Andre's switch group", "2019-10-07", andre);
//
//        Account switchGroupAccount = new Account("Current", "ProgrammersGroup's current account");
//        switchGroup.addAccount("Current", "ProgrammersGroup's current account");
//
//        Account computerStoreAccount = new Account("ScreenTime", "Account for switchGroup's technology purchases at ScreenTime");
//        switchGroup.addAccount("ScreenTime", "Account for switchGroup's technology purchases at ScreenTime");
//
//        Category electronicsCategory = new Category("Electronics");
//        switchGroup.addCategory("Electronics");
//
//        switchGroup.addTransaction(100, Type.DEBIT, "2019-10-01 00:00:00", "Electronic equipment", electronicsCategory, switchGroupAccount, computerStoreAccount);
//        switchGroup.addTransaction(380.00, Type.DEBIT, "1999-12-31 23:59:59", "Y2K software fix", electronicsCategory, switchGroupAccount, computerStoreAccount);
//
//
//        Group afterLunchGroup = new Group("Andre's after lunch group", "2019-10-02", andre);
//
//        Account afterLunchAccount = new Account("Budget", "Account for afterLunchGroup's budget for coffee");
//        afterLunchGroup.addAccount("Budget", "Account for afterLunchGroup's budget for coffee");
//
//        Account coffeeAccount = new Account("Zespresso", "Account just for afterLunchGroup coffee purchases");
//        afterLunchGroup.addAccount("Zespresso", "Account just for afterLunchGroup coffee purchases");
//
//        Category coffeeCategory = new Category("Coffee");
//        afterLunchGroup.addCategory("Coffee");
//
//        afterLunchGroup.addTransaction(20, Type.DEBIT, "2020-01-01 00:00:00", "Coffee for the month", coffeeCategory, afterLunchAccount, coffeeAccount);
//        afterLunchGroup.addTransaction(10, Type.DEBIT, "2018-08-01 00:00:00", "Coffee for August", coffeeCategory, afterLunchAccount, coffeeAccount);
//
//
//        Groups thisGroups = new Groups();
//        thisGroups.addGroup(afterLunchGroup);
//        thisGroups.addGroup(switchGroup);
//
//        Period period = new Period("2019-01-01 00:00:00", "2021-01-01 00:00:00");
//
//        Set<Transaction> expected = new HashSet<>();
//        expected.add(new Transaction(25, Type.DEBIT, "2020-01-07 00:00:00", "Groceries for the week", newCategory, smithsGroceriesAccount, zeCreditAccount));
//        expected.add(new Transaction(100, Type.DEBIT, "2019-10-01 00:00:00", "Electronic equipment", electronicsCategory, switchGroupAccount, computerStoreAccount));
//        expected.add(new Transaction(20, Type.DEBIT, "2020-01-01 00:00:00", "Coffee for the month", coffeeCategory, afterLunchAccount, coffeeAccount));
//
//        String expectedString = expected.toString();
//
//        //Act
//        String actual = control.getTransactionsOfPersonAndRespectiveGroupsWithinPeriod(andre, thisGroups, period);
//
//        //Assert
//        assertEquals(expectedString,actual);
//    }
//
//    /**
//     * Test for getTransactionsOfPersonAndRespectiveGroupsWithinPeriod
//     * List of groups has another group from a different person
//     */
//    @Test
//    @DisplayName("Test for getTransactionsOfPersonAndRespectiveGroupsWithinPeriod - Second Happy Case")
//    void getTransactionsOfPersonAndRespectiveGroupsWithinPeriodControllerSecondHappyCaseTest() {
//        //Arrange
//
//        GetTransactionsOfPersonAndRespectiveGroupsWithinPeriodController control = new GetTransactionsOfPersonAndRespectiveGroupsWithinPeriodController();
//
//        Person maria = new Person("Maria", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null);
//        Person paulo = new Person("Paulo", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null);
//        Person andre = new Person("Andre", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", maria, paulo);
//        Person joao = new Person("João", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", maria, paulo);
//
//
//        Account smithsGroceriesAccount = new Account("Smith's Groceries", "Account just for Smith family's groceries");
//        andre.addAccount("Smith's Groceries", "Account just for Smith family's groceries");
//
//        Account zeCreditAccount = new Account("Zé & Associados", "Current account of the 'A Loja do Zé' grocery shop");
//        andre.addAccount("Zé & Associados", "Current account of the 'A Loja do Zé' grocery shop");
//
//        Category newCategory = new Category("Groceries");
//        andre.addCategory("Groceries");
//
//        andre.addTransaction(25, Type.DEBIT, "2020-01-07 00:00:00", "Groceries for the week", newCategory, smithsGroceriesAccount, zeCreditAccount);
//
//
//        Group switchGroup = new Group("Andre's switch group", "2019-10-07", andre);
//
//        Account switchGroupAccount = new Account("Current", "ProgrammersGroup's current account");
//        switchGroup.addAccount("Current", "ProgrammersGroup's current account");
//
//        Account computerStoreAccount = new Account("ScreenTime", "Account for switchGroup's technology purchases at ScreenTime");
//        switchGroup.addAccount("ScreenTime", "Account for switchGroup's technology purchases at ScreenTime");
//
//        Category electronicsCategory = new Category("Electronics");
//        switchGroup.addCategory("Electronics");
//
//        switchGroup.addTransaction(100, Type.DEBIT, "2019-10-01 00:00:00", "Electronic equipment", electronicsCategory, switchGroupAccount, computerStoreAccount);
//
//
//        Group afterLunchGroup = new Group("João's after lunch group", "2019-10-02", joao);
//
//        Account afterLunchAccount = new Account("Budget", "Account for afterLunchGroup's budget for coffee");
//        afterLunchGroup.addAccount("Budget", "Account for afterLunchGroup's budget for coffee");
//
//        Account coffeeAccount = new Account("Zespresso", "Account just for afterLunchGroup coffee purchases");
//        afterLunchGroup.addAccount("Zespresso", "Account just for afterLunchGroup coffee purchases");
//
//        Category coffeeCategory = new Category("Coffee");
//        afterLunchGroup.addCategory("Coffee");
//
//        afterLunchGroup.addTransaction(20, Type.DEBIT, "2020-01-10 00:00:00", "Coffee for the month", coffeeCategory, afterLunchAccount, coffeeAccount);
//
//        Groups thisGroups = new Groups();
//        thisGroups.addGroup(afterLunchGroup);
//        thisGroups.addGroup(switchGroup);
//
//        Period period = new Period("2018-01-01 00:00:00", "2021-01-01 00:00:00");
//
//        Set<Transaction> expected = new HashSet<>();
//        expected.add(new Transaction(25, Type.DEBIT, "2020-01-07 00:00:00", "Groceries for the week", newCategory, smithsGroceriesAccount, zeCreditAccount));
//        expected.add(new Transaction(100, Type.DEBIT, "2019-10-01 00:00:00", "Electronic equipment", electronicsCategory, switchGroupAccount, computerStoreAccount));
//
//        String expectedString = expected.toString();
//
//        //Act
//        String actual = control.getTransactionsOfPersonAndRespectiveGroupsWithinPeriod(andre, thisGroups, period);
//
//        //Assert
//        assertEquals(expectedString,actual);
//    }
//
//    /**
//     * Test for getTransactionsOfPersonAndRespectiveGroupsWithinPeriod
//     * Test for a given period in which there were no transactions.
//     */
//    @Test
//    @DisplayName("Test for getTransactionsOfPersonAndRespectiveGroupsWithinPeriod - Period Without Transactions")
//    void getTransactionsOfPersonAndRespectiveGroupsWithinPeriodControllerPeriodWithoutTransactionsTest() {
//        //Arrange
//
//        GetTransactionsOfPersonAndRespectiveGroupsWithinPeriodController control = new GetTransactionsOfPersonAndRespectiveGroupsWithinPeriodController();
//
//        Person maria = new Person("Maria", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null);
//        Person paulo = new Person("Paulo", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null);
//        Person andre = new Person("Andre", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", maria, paulo);
//        Person joao = new Person("João", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", maria, paulo);
//
//
//        Account smithsGroceriesAccount = new Account("Smith's Groceries", "Account just for Smith family's groceries");
//        andre.addAccount("Smith's Groceries", "Account just for Smith family's groceries");
//
//        Account zeCreditAccount = new Account("Zé & Associados", "Current account of the 'A Loja do Zé' grocery shop");
//        andre.addAccount("Zé & Associados", "Current account of the 'A Loja do Zé' grocery shop");
//
//        Category newCategory = new Category("Groceries");
//        andre.addCategory("Groceries");
//
//        andre.addTransaction(25, Type.DEBIT, "2020-01-07 00:00:00", "Groceries for the week", newCategory, smithsGroceriesAccount, zeCreditAccount);
//
//
//        Group switchGroup = new Group("Andre's switch group", "2019-10-07", andre);
//
//        Account switchGroupAccount = new Account("Current", "ProgrammersGroup's current account");
//        switchGroup.addAccount("Current", "ProgrammersGroup's current account");
//
//        Account computerStoreAccount = new Account("ScreenTime", "Account for switchGroup's technology purchases at ScreenTime");
//        switchGroup.addAccount("ScreenTime", "Account for switchGroup's technology purchases at ScreenTime");
//
//        Category electronicsCategory = new Category("Electronics");
//        switchGroup.addCategory("Electronics");
//
//        switchGroup.addTransaction(100, Type.DEBIT, "2019-10-01 00:00:00", "Electronic equipment", electronicsCategory, switchGroupAccount, computerStoreAccount);
//
//
//        Group afterLunchGroup = new Group("João's after lunch group", "2019-10-02", joao);
//
//        Account afterLunchAccount = new Account("Budget", "Account for afterLunchGroup's budget for coffee");
//        afterLunchGroup.addAccount("Budget", "Account for afterLunchGroup's budget for coffee");
//
//        Account coffeeAccount = new Account("Zespresso", "Account just for afterLunchGroup coffee purchases");
//        afterLunchGroup.addAccount("Zespresso", "Account just for afterLunchGroup coffee purchases");
//
//        Category coffeeCategory = new Category("Coffee");
//        afterLunchGroup.addCategory("Coffee");
//
//        afterLunchGroup.addTransaction(20, Type.DEBIT, "2020-01-10 00:00:00", "Coffee for the month", coffeeCategory, afterLunchAccount, coffeeAccount);
//
//        Groups thisGroups = new Groups();
//        thisGroups.addGroup(afterLunchGroup);
//        thisGroups.addGroup(switchGroup);
//
//        Period period = new Period("2016-01-01 00:00:00", "2017-01-01 00:00:00");
//
//        Set<Transaction> expected = new HashSet<>();
//
//        String expectedString = expected.toString();
//
//        //Act
//        String actual = control.getTransactionsOfPersonAndRespectiveGroupsWithinPeriod(andre, thisGroups, period);
//
//        //Assert
//        assertEquals(expectedString, actual);
//        System.out.println(expectedString);
//    }
//
//    /**
//     * Test for getTransactionsOfPersonAndRespectiveGroupsWithinPeriod
//     * Empty Ledgers
//     */
//    @Test
//    @DisplayName("Test for getTransactionsOfPersonAndRespectiveGroupsWithinPeriod - Empty Ledger Case")
//    void getTransactionsOfPersonAndRespectiveGroupsWithinPeriodControllerEmptyLedgerTest() {
//        //Arrange
//
//        GetTransactionsOfPersonAndRespectiveGroupsWithinPeriodController control = new GetTransactionsOfPersonAndRespectiveGroupsWithinPeriodController();
//
//        Person maria = new Person("Maria", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null);
//        Person paulo = new Person("Paulo", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null);
//        Person andre = new Person("Andre", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", maria, paulo);
//        Person joao = new Person("João", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", maria, paulo);
//
//
//        Account smithsGroceriesAccount = new Account("Smith's Groceries", "Account just for Smith family's groceries");
//        andre.addAccount("Smith's Groceries", "Account just for Smith family's groceries");
//
//        Account zeCreditAccount = new Account("Zé & Associados", "Current account of the 'A Loja do Zé' grocery shop");
//        andre.addAccount("Zé & Associados", "Current account of the 'A Loja do Zé' grocery shop");
//
//        Category newCategory = new Category("Groceries");
//        andre.addCategory("Groceries");
//
//        Group switchGroup = new Group("Andre's switch group", "2019-10-07", andre);
//
//        Account switchGroupAccount = new Account("Current", "ProgrammersGroup's current account");
//        switchGroup.addAccount("Current", "ProgrammersGroup's current account");
//
//        Account computerStoreAccount = new Account("ScreenTime", "Account for switchGroup's technology purchases at ScreenTime");
//        switchGroup.addAccount("ScreenTime", "Account for switchGroup's technology purchases at ScreenTime");
//
//        Category electronicsCategory = new Category("Electronics");
//        switchGroup.addCategory("Electronics");
//
//        Group afterLunchGroup = new Group("João's after lunch group", "2019-10-02", joao);
//
//        Account afterLunchAccount = new Account("Budget", "Account for afterLunchGroup's budget for coffee");
//        afterLunchGroup.addAccount("Budget", "Account for afterLunchGroup's budget for coffee");
//
//        Account coffeeAccount = new Account("Zespresso", "Account just for afterLunchGroup coffee purchases");
//        afterLunchGroup.addAccount("Zespresso", "Account just for afterLunchGroup coffee purchases");
//
//        Category coffeeCategory = new Category("Coffee");
//        afterLunchGroup.addCategory("Coffee");
//
//        Groups thisGroups = new Groups();
//        thisGroups.addGroup(afterLunchGroup);
//        thisGroups.addGroup(switchGroup);
//
//        Period period = new Period("2018-01-01 00:00:00", "2021-01-01 00:00:00");
//
//        Set<Transaction> expected = new HashSet<>();
//
//        String expectedString = expected.toString();
//
//        //Act
//        String actual = control.getTransactionsOfPersonAndRespectiveGroupsWithinPeriod(andre, thisGroups, period);
//
//        //Assert
//        assertEquals(expectedString, actual);
//    }
//}