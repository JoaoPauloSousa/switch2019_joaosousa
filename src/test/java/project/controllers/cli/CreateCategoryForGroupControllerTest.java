package project.controllers.cli;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import project.ProjectApplication;
import project.application.services.CreateCategoryForGroupService;
import project.dto.CreateCategoryForGroupRequestDTO;
import project.dto.CreateCategoryForGroupResponseDTO;

import static org.junit.jupiter.api.Assertions.assertEquals;


@SpringBootTest(classes = ProjectApplication.class)
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CreateCategoryForGroupControllerTest {

    @Autowired
    CreateCategoryForGroupService service;

    @Autowired
    CreateCategoryForGroupController controller;

    /**
     * createCategoryForGroup - Happy Case
     */
    @Test
    @DisplayName("Test for createCategoryForGroup - Happy Case")
    public void createCategoryForGroupHappyCaseTest() {

        //ARRANGE
        String personID = "51010@switch.pt";
        String groupID = "510200";
        String designation = "Bolas";
        String groupDescription = "Futebol";

        CreateCategoryForGroupRequestDTO requestDTO = new CreateCategoryForGroupRequestDTO(personID, groupID, designation);

        CreateCategoryForGroupResponseDTO expected = new CreateCategoryForGroupResponseDTO(groupID, groupDescription, designation);

        Mockito.when(service.createCategoryForGroup(requestDTO)).thenReturn(expected);

        //ACT
        CreateCategoryForGroupResponseDTO result = controller.createCategoryForGroup(personID, groupID, designation);

        //ASSERT
        assertEquals(expected, result);
    }

}
