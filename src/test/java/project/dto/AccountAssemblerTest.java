package project.dto;

import org.junit.jupiter.api.Test;
import project.model.entities.shared.AccountID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AccountAssemblerTest {

    /**
     * Constructor test
     */
    @Test
    void constructorTest() {
        //ARRANGE
        AccountAssembler assembler = new AccountAssembler();
        //ASSERT
        assertTrue(assembler instanceof AccountAssembler);
    }

    /**
     * Test for method mapToDTO
     * Happy Case
     */
    @Test
    void mapToDTOHappyCaseTest() {
        //ARRANGE
        AccountID accountID = new AccountID("1000");
        String denomination = "denomination";
        String description = "description";

        AccountDTO expectedDTO = new AccountDTO("denomination", "description", "1000");

        //ACT
        AccountDTO actualDTO = AccountAssembler.mapToResponseDTO(denomination, description, accountID.toStringDTO());

        //ASSERT
        assertEquals(expectedDTO, actualDTO);
    }
}