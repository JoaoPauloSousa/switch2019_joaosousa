package project.dto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AccountDTOTest {

    /**
     * Test for AccountResponseDTO Constructor
     * Necessary to ensure full coverage
     */
    @Test
    void constructorTest() {
        //ARRANGE
        String denomination = "denomination";
        String description = "description";
        String accountIDStg = "1000";
        AccountDTO dto = new AccountDTO(denomination, description, accountIDStg);

        //ASSERT
        assertTrue(dto instanceof AccountDTO);
    }

    /**
     * Test for getDenomination method
     * Happy Case
     */
    @Test
    void getDenominationHappyCaseTest() {
        //ARRANGE
        String denomination = "denomination";
        String description = "description";
        String accountIDStg = "1000";
        AccountDTO dto = new AccountDTO(denomination, description, accountIDStg);

        String expected = "denomination";

        //ACT
        String actual = dto.getDenomination();

        //ASSERT
        assertEquals(expected, actual);

    }

    /**
     * Test for getDenomination method
     * Ensure not equals
     */
    @Test
    void getDenominationEnsureNotEqualsTest() {
        //ARRANGE
        String denomination = "denomination";
        String description = "description";
        String accountIDStg = "1000";
        AccountDTO dto = new AccountDTO(denomination, description, accountIDStg);

        String expected = "other denomination";

        //ACT
        String actual = dto.getDenomination();

        //ASSERT
        assertNotEquals(expected, actual);

    }

    /**
     * Test for getDescription method
     * Happy Case
     */
    @Test
    void getDescriptionHappyCaseTest() {
        //ARRANGE
        String denomination = "denomination";
        String description = "description";
        String accountIDStg = "1000";
        AccountDTO dto = new AccountDTO(denomination, description, accountIDStg);

        String expected = "description";

        //ACT
        String actual = dto.getDescription();

        //ASSERT
        assertEquals(expected, actual);

    }

    /**
     * Test for getDescription method
     * Ensure not equals test
     */
    @Test
    void getDescriptionEnsureNotEqualsTest() {
        //ARRANGE
        String denomination = "denomination";
        String description = "description";
        String accountIDStg = "1000";
        AccountDTO dto = new AccountDTO(denomination, description, accountIDStg);

        String expected = "other description";

        //ACT
        String actual = dto.getDescription();

        //ASSERT
        assertNotEquals(expected, actual);

    }

    /**
     * Test for getAccountID
     * Happy Case
     */
    @Test
    void getAccountIDHappyCaseTest() {
        //ARRANGE
        String denomination = "denomination";
        String description = "description";
        String accountIDStg = "1000";
        AccountDTO dto = new AccountDTO(denomination, description, accountIDStg);

        String expected = "1000";

        //ACT
        String actual = dto.getAccountID();

        //ASSERT
        assertEquals(expected, actual);

    }

    /**
     * Test for getAccountID
     * Ensure not equals test
     */
    @Test
    void getAccountIDEnsureNotEqualsTest() {
        //ARRANGE
        String denomination = "denomination";
        String description = "description";
        String accountIDStg = "1000";
        AccountDTO dto = new AccountDTO(denomination, description, accountIDStg);

        String expected = "2000";

        //ACT
        String actual = dto.getAccountID();

        //ASSERT
        assertNotEquals(expected, actual);

    }

    /**
     * Test for equals
     * Ensure true when comparing same object
     */
    @Test
    void testEqualsEnsureTrueWithSameObject() {
        //ARRANGE
        String denomination = "denomination";
        String description = "description";
        String accountIDStg = "1000";
        AccountDTO dto = new AccountDTO(denomination, description, accountIDStg);

        //ACT
        boolean actual = dto.equals(dto);

        //ASSERT
        assertTrue(actual);
    }

    /**
     * Test for equals
     * Ensure false when o is null
     */
    @Test
    void testEqualsEnsureFalseWhenObjectIsNull() {
        //ARRANGE
        String denomination = "denomination";
        String description = "description";
        String accountIDStg = "1000";
        AccountDTO dto = new AccountDTO(denomination, description, accountIDStg);

        //ACT
        boolean actual = dto.equals(null);

        //ASSERT
        assertFalse(actual);
    }

    /**
     * Test for equals
     * Ensure false when Class is different
     */
    @Test
    void testEqualsEnsureFalseWhenClassIsDifferent() {
        //ARRANGE
        String denomination = "denomination";
        String description = "description";
        String accountIDStg = "1000";
        AccountDTO dto = new AccountDTO(denomination, description, accountIDStg);

        String other = "other";

        //ACT
        boolean actual = dto.equals(other);

        //ASSERT
        assertFalse(actual);
    }

    /**
     * Test for equals
     * Ensure true when all attributes are equal
     */
    @Test
    void testEqualsEnsureTrueWhenAllAtributesAreEqual() {
        //ARRANGE
        String denomination = "denomination";
        String description = "description";
        String accountIDStg = "1000";
        AccountDTO dto = new AccountDTO(denomination, description, accountIDStg);

        String otherDenomination = "denomination";
        String otherDescription = "description";
        String otherAccountIDStg = "1000";
        AccountDTO otherDto = new AccountDTO(otherDenomination, otherDescription, otherAccountIDStg);

        //ACT
        boolean actual = dto.equals(otherDto);

        //ASSERT
        assertTrue(actual);
    }

    /**
     * Test for equals
     * Ensure false when denomination is different
     */
    @Test
    void testEqualsEnsureFalseWhenDenominationIsDifferent() {
        //ARRANGE
        String denomination = "denomination";
        String description = "description";
        String accountIDStg = "1000";
        AccountDTO dto = new AccountDTO(denomination, description, accountIDStg);

        String otherDenomination = "other denomination";
        String otherDescription = "description";
        String otherAccountIDStg = "1000";
        AccountDTO otherDto = new AccountDTO(otherDenomination, otherDescription, otherAccountIDStg);

        //ACT
        boolean actual = dto.equals(otherDto);

        //ASSERT
        assertFalse(actual);
    }

    /**
     * Test for equals
     * Ensure false when description is different
     */
    @Test
    void testEqualsEnsureFalseWhenDescriptionIsDifferent() {
        //ARRANGE
        String denomination = "denomination";
        String description = "description";
        String accountIDStg = "1000";
        AccountDTO dto = new AccountDTO(denomination, description, accountIDStg);

        String otherDenomination = "denomination";
        String otherDescription = "other description";
        String otherAccountIDStg = "1000";
        AccountDTO otherDto = new AccountDTO(otherDenomination, otherDescription, otherAccountIDStg);

        //ACT
        boolean actual = dto.equals(otherDto);

        //ASSERT
        assertFalse(actual);
    }

    /**
     * Test for equals
     * Ensure false when accountID is different
     */
    @Test
    void testEqualsEnsureFalseWhenAccountIDIsDifferent() {
        //ARRANGE
        String denomination = "denomination";
        String description = "description";
        String accountIDStg = "1000";
        AccountDTO dto = new AccountDTO(denomination, description, accountIDStg);

        String otherDenomination = "denomination";
        String otherDescription = "description";
        String otherAccountIDStg = "2000";
        AccountDTO otherDto = new AccountDTO(otherDenomination, otherDescription, otherAccountIDStg);

        //ACT
        boolean actual = dto.equals(otherDto);

        //ASSERT
        assertFalse(actual);
    }

    @Test
    void testHashCode() {
        //ARRANGE
        String denomination = "denomination";
        String description = "description";
        String accountIDStg = "1000";
        AccountDTO dto = new AccountDTO(denomination, description, accountIDStg);

        int expected = -1885039981;

        //ACTUAL
        int actual = dto.hashCode();

        //ASSERT
        assertEquals(expected, actual);

    }

    @Test
    void testToString() {
        String denomination = "denomination";
        String description = "description";
        String accountIDStg = "1000";
        AccountDTO dto = new AccountDTO(denomination, description, accountIDStg);

        String expected = "AccountDTO{denomination='denomination', description='description', accountID='1000'}";

        //ACTUAL
        String actual = dto.toString();

        //ASSERT
        assertEquals(expected, actual);
    }
}