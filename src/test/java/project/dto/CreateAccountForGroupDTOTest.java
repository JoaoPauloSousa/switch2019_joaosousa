package project.dto;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.entities.group.GroupID;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertFalse;

class CreateAccountForGroupDTOTest {

    /**
     * Test for CreateAccountForGroupDTO constructor
     * Happy case
     */
    @Test
    void constructorHappyCaseTest() {
        Set<String> constructorSet = new HashSet<>();
        CreateAccountForGroupDTO dto = new CreateAccountForGroupDTO(constructorSet);

        //ASSERT
        assertTrue(dto instanceof CreateAccountForGroupDTO);
    }

    /**
     * Test for getAccountsIDs
     * Happy case
     */
    @Test
    void getAccountsIDsHappyCase() {

        //ARRANGE
        Set<String> accountsIDs = new HashSet<>();
        CreateAccountForGroupDTO dto = new CreateAccountForGroupDTO(accountsIDs);


        accountsIDs.add("654");
        accountsIDs.add("87");

        dto.setAccountsIDs(accountsIDs);

        //ACT
        Set<String> result = dto.getAccountsIDs();

        //ASSERT
        assertEquals(accountsIDs, result);
    }

    /**
     * Test for equals
     * Happy case
     */
    @Test
    @DisplayName("Test for equals - HappyCase")
    void equalsHappyCaseTest() {
        //Arrange
        Set<String> expected = new HashSet<>();
        CreateAccountForGroupDTO dto1 = new CreateAccountForGroupDTO(expected);
        CreateAccountForGroupDTO dto2 = new CreateAccountForGroupDTO(expected);
        //Act
        boolean result = dto1.equals(dto2);
        //Assert
        assertTrue(result);
    }

    /**
     * Test for equals
     * Ensure false with null object
     */
    @Test
    @DisplayName("Test for equals - Ensure false with null")
    void equalsEnsureFalseWithNullTest() {
        //Arrange
        Set<String> expected = new HashSet<>();
        CreateAccountForGroupDTO dto1 = new CreateAccountForGroupDTO(expected);
        CreateAccountForGroupDTO dto2 = null;
        //Act
        boolean result = dto1.equals(dto2);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false with diferent object
     */
    @Test
    @DisplayName("Test for equals - Ensure false with object from another class")
    void equalsEnsureFalseWithObjectFromAnotherClassTest() {
        //Arrange
        Set<String> expected = new HashSet<>();
        CreateAccountForGroupDTO dto = new CreateAccountForGroupDTO(expected);
        GroupID groupID = new GroupID("1");
        //Act
        boolean result = dto.equals(groupID);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure true if is same object
     */
    @Test
    @DisplayName("Test for equals - Ensure true with same object")
    void equalsEnsureTrueWithSameObjectTest() {
        //Arrange
        Set<String> expected = new HashSet<>();
        CreateAccountForGroupDTO dto = new CreateAccountForGroupDTO(expected);
        //Act
        boolean result = dto.equals(dto);
        //Assert
        assertTrue(result);
    }

    /**
     * Test for hashCode
     * Happy case
     *
     */
    @Test
    void hasCodeEnsureWorks () {

        Set<String> dtoSet = new HashSet<>();
        CreateAccountForGroupDTO dto = new CreateAccountForGroupDTO(dtoSet);

        int expected = 31;

        int result = dto.hashCode();

        assertEquals (expected,result);
    }

}