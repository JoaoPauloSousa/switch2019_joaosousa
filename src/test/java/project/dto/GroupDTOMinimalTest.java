package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GroupDTOMinimalTest {

    String groupID;
    String description;
    String creationDate;
    GroupDTOMinimal groupDTO;

    @BeforeEach
    void init() {
        groupID = "1L";
        description = "description";
        creationDate = "2020-01-01";
        groupDTO = new GroupDTOMinimal(groupID, description, creationDate);
    }

    /**
     * Test for GroupDTO constructor
     */
    @Test
    void constructorTest() {
        assertTrue(groupDTO instanceof GroupDTOMinimal);
    }

    /**
     * Test for getGroupID method
     * <p>
     * Happy Case
     */
    @Test
    void getGroupIDHappyCaseTest() {
        //Arrange
        String expected = "1L";

        //Act
        String result = groupDTO.getGroupID();

        //Assert
        assertEquals(expected, result);

    }

    /**
     * Test for getGroupID method
     * <p>
     * Ensure not equals
     */
    @Test
    void getGroupIDEnsureNotEquals() {
        //Arrange
        String expected = "2L";

        //Act
        String result = groupDTO.getGroupID();

        //Assert
        assertNotEquals(expected, result);

    }

    /**
     * Test for getDescription method
     * <p>
     * Happy Case
     */
    @Test
    void getDescriptionHappyCaseTest() {
        //Arrange
        String expected = "description";

        //Act
        String result = groupDTO.getDescription();

        //Assert
        assertEquals(expected, result);

    }

    /**
     * Test for getDescription method
     * <p>
     * Ensure not equals
     */
    @Test
    void getDescriptionEnsureNotEqualsTest() {
        //Arrange
        String expected = "different description";

        //Act
        String result = groupDTO.getDescription();

        //Assert
        assertNotEquals(expected, result);

    }

    /**
     * Test for getCreationDate method
     * <p>
     * Happy Case
     */
    @Test
    void getCreationDateHappyCaseTest() {
        //Arrange
        String expected = "2020-01-01";

        //Act
        String result = groupDTO.getCreationDate();

        //Assert
        assertEquals(expected, result);

    }

    /**
     * Test for getCreationDate method
     * <p>
     * Ensure not equals
     */
    @Test
    void getCreationDateEnsureNotEqualsTest() {
        //Arrange
        String expected = "1900-01-01";

        //Act
        String result = groupDTO.getCreationDate();

        //Assert
        assertNotEquals(expected, result);

    }

    /**
     * Test for equals
     * <p>
     * Happy Case - Comparing same object
     */
    @Test
    void testEqualsHappyCaseTest() {
        //Arrange
        //Act
        boolean result = groupDTO.equals(groupDTO);

        //Assert
        assertTrue(result);
    }

    /**
     * Test for equals
     * <p>
     * Happy Case - 2 different GroupDTO objects with same attributes
     */
    @Test
    void testEqualsHappyCaseDifferentObjectsSameAttributesTest() {
        //Arrange
        GroupDTOMinimal otherGroupDTO = new GroupDTOMinimal(groupID, description, creationDate);

        //Act
        boolean result = groupDTO.equals(otherGroupDTO);

        //Assert
        assertTrue(result);
    }

    /**
     * Test for equals
     * <p>
     * Ensure false if one of the objects is null
     */
    @Test
    void testEqualsEnsureFalseIfOtherIsNullTest() {
        //Arrange
        GroupDTOMinimal otherGroupDTO = null;

        //Act
        boolean result = groupDTO.equals(otherGroupDTO);

        //Assert
        assertFalse(result);

    }

    /**
     * Test for equals
     * <p>
     * Ensure false if class is different
     */
    @Test
    void testEqualsEnsureFalseIfClassIsDifferentTest() {
        //Arrange
        String other = "other";

        //Act
        boolean result = groupDTO.equals(other);

        //Assert
        assertFalse(result);

    }

    /**
     * Test for equals
     * <p>
     * Ensure false if groupID is different
     */
    @Test
    void testEqualsEnsureFalseDifferentGroupIDTest() {
        //Arrange
        String otherGroupID = "10L";
        GroupDTOMinimal otherGroupDTO = new GroupDTOMinimal(otherGroupID, description, creationDate);

        //Act
        boolean result = groupDTO.equals(otherGroupDTO);

        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * <p>
     * Ensure false if description is different
     */
    @Test
    void testEqualsEnsureFalseDifferentDescriptionTest() {
        //Arrange
        String otherDescription = "other description";
        GroupDTOMinimal otherGroupDTO = new GroupDTOMinimal(groupID, otherDescription, creationDate);

        //Act
        boolean result = groupDTO.equals(otherGroupDTO);

        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * <p>
     * Ensure false if creationDate is different
     */
    @Test
    void testEqualsEnsureFalseDifferentCreationDateTest() {
        //Arrange
        String otherCreationDate = "1900-01-01";
        GroupDTOMinimal otherGroupDTO = new GroupDTOMinimal(groupID, description, otherCreationDate);

        //Act
        boolean result = groupDTO.equals(otherGroupDTO);

        //Assert
        assertFalse(result);
    }

    /**
     * Test for hascode
     * <p>
     * Ensure false if creationDate is different
     */
    @Test
    void testHashCode() {
        //Arrange
        int expected = 1468366550;

        //Act
        int result = groupDTO.hashCode();

        //Assert
        assertEquals(expected, result);

    }

    @Test
    void testToString() {
        //Arrange
        String expected = "GroupDTOMinimal{groupID='1L', description='description', creationDate='2020-01-01'}";

        //Act
        String result = groupDTO.toString();

        //Assert
        assertEquals(expected, result);
    }
}