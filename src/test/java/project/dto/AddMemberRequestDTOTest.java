package project.dto;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class AddMemberRequestDTOTest {

    /**
     * Test for AddMemberRequestDTO constructor
     * Happy case
     */
    @DisplayName("Test for constructor - Happy case")
    @Test
    void constructorHappyCaseTest() {
        // Arrange
        String newMemberID = "11";
        String groupID = "22";
        AddMemberRequestDTO dto = new AddMemberRequestDTO(newMemberID, groupID);

        // Assert
        assertTrue(dto instanceof AddMemberRequestDTO);
    }

    /**
     * Test for getNewMemberID
     */
    @Test
    @DisplayName("Test for getNewMemberID - Happy case")
    void getNewMemberIDHappyCaseTest() {
        // Arrange
        String newMemberID = "11";
        String groupID = "22";
        AddMemberRequestDTO dto = new AddMemberRequestDTO(newMemberID, groupID);
        String expected = "11";

        // Act
        String result = dto.getNewMemberEmail();
        // Assert
        assertEquals(expected, result);
    }

    /**
     * Test for getNewMemberID
     */
    @Test
    @DisplayName("Test for getNewMemberID - Sad case")
    void getNewMemberIDSadCaseTest() {
        // Arrange
        String newMemberID = "11";
        String groupID = "22";
        AddMemberRequestDTO dto = new AddMemberRequestDTO(newMemberID, groupID);
        String expected = "33";

        // Act
        String result = dto.getNewMemberEmail();
        // Assert
        assertNotEquals(expected, result);
    }

    /**
     * Test for getGroupID
     */
    @Test
    @DisplayName("Test for getGroupID - Happy case")
    void getGroupIDHappyCaseTest() {
        // Arrange
        String newMemberID = "11";
        String groupID = "22";
        AddMemberRequestDTO dto = new AddMemberRequestDTO(newMemberID, groupID);
        String expected = "22";

        // Act
        String result = dto.getGroupID();
        // Assert
        assertEquals(expected, result);
    }

    /**
     * Test for getGroupID
     */
    @Test
    @DisplayName("Test for getGroupID - Sad case")
    void getGroupIDSadCaseTest() {
        // Arrange
        String newMemberID = "11";
        String groupID = "22";
        AddMemberRequestDTO dto = new AddMemberRequestDTO(newMemberID, groupID);
        String expected = "33";

        // Act
        String result = dto.getGroupID();
        // Assert
        assertNotEquals(expected, result);
    }

    /**
     * Test for Equals
     * Happy case
     */
    @DisplayName("Test for Equals - Happy case")
    @Test
    void equalsHappyCaseTest() {
        //ARRANGE
        String expectedMemberID = "11";
        String expectedgroupID = "22";
        AddMemberRequestDTO expected = new AddMemberRequestDTO(expectedMemberID, expectedgroupID);

        String newMemberID = "11";
        String groupID = "22";
        AddMemberRequestDTO dto = new AddMemberRequestDTO(newMemberID, groupID);

        //ACT
        boolean result = dto.equals(expected);

        //ASSERT
        assertTrue(result);
    }

    /**
     * Test for Equals
     * Sad case
     */
    @DisplayName("Test for Equals - Ensure false with different personIDs")
    @Test
    void equalsDifferentPersonIDsCaseTest() {
        //ARRANGE
        String expectedMemberID = "11";
        String expectedgroupID = "22";
        AddMemberRequestDTO expected = new AddMemberRequestDTO(expectedMemberID, expectedgroupID);

        String newMemberID = "33";
        String groupID = "22";
        AddMemberRequestDTO dto = new AddMemberRequestDTO(newMemberID, groupID);

        //ACT
        boolean result = dto.equals(expected);

        //ASSERT
        assertFalse(result);
    }

    /**
     * Test for Equals
     * Sad case
     */
    @DisplayName("Test for Equals - Ensure false with different groupIDs")
    @Test
    void equalsDifferentGroupIDsCaseTest() {
        //ARRANGE
        String expectedMemberID = "11";
        String expectedgroupID = "22";
        AddMemberRequestDTO expected = new AddMemberRequestDTO(expectedMemberID, expectedgroupID);

        String newMemberID = "11";
        String groupID = "33";
        AddMemberRequestDTO dto = new AddMemberRequestDTO(newMemberID, groupID);

        //ACT
        boolean result = dto.equals(expected);

        //ASSERT
        assertFalse(result);
    }

    /**
     * Test for Equals
     * Sad case
     */
    @DisplayName("Test for Equals - Ensure false with null personID")
    @Test
    void equalsNullPersonIDCaseTest() {
        //ARRANGE
        String expectedMemberID = "11";
        String expectedgroupID = "22";
        AddMemberRequestDTO expected = new AddMemberRequestDTO(expectedMemberID, expectedgroupID);

        String groupID = "33";
        AddMemberRequestDTO dto = new AddMemberRequestDTO(null, groupID);

        //ACT
        boolean result = dto.equals(expected);

        //ASSERT
        assertFalse(result);
    }

    /**
     * Test for Equals
     * Sad case
     */
    @DisplayName("Test for Equals - Ensure false with null groupID")
    @Test
    void equalsNullGroupIDsCaseTest() {
        //ARRANGE
        String expectedMemberID = "11";
        String expectedgroupID = "22";
        AddMemberRequestDTO expected = new AddMemberRequestDTO(expectedMemberID, expectedgroupID);

        String newMemberID = "11";
        AddMemberRequestDTO dto = new AddMemberRequestDTO(newMemberID, null);

        //ACT
        boolean result = dto.equals(expected);

        //ASSERT
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false with null object
     */
    @Test
    @DisplayName("Test for Equals - Ensure false with null object")
    void equalsEnsureFalseWithNullTest() {
        //Arrange
        String newMemberID = "11";
        String groupID = "22";
        AddMemberRequestDTO dto = new AddMemberRequestDTO(newMemberID, groupID);

        MembersDTO dto2 = null;

        //Act
        boolean result = dto.equals(dto2);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false with different types of objects
     */
    @Test
    @DisplayName("Test for Equals - Ensure false with different types of objects")
    void equalsEnsureFalseWithDifferentTypesOfObjectsTest() {
        //Arrange
        String newMemberID = "11";
        String groupID = "22";
        AddMemberRequestDTO dto = new AddMemberRequestDTO(newMemberID, groupID);

        List<String> expectedMembersIDs = new ArrayList<>();
        List<String> expectedMembersNames = new ArrayList<>();
        List<Boolean> expectedManagers = new ArrayList<>();
        MembersDTO dto2 = new MembersDTO(expectedMembersIDs, expectedMembersNames, expectedManagers);

        //Act
        boolean result = dto.equals(dto2);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for Equals
     * Same object
     */
    @DisplayName("Test for Equals - Ensure true with same object")
    @Test
    void equalsSameObjectCaseTest() {
        //ARRANGE
        String newMemberID = "11";
        String groupID = "22";
        AddMemberRequestDTO dto = new AddMemberRequestDTO(newMemberID, groupID);

        //ACT
        boolean result = dto.equals(dto);

        //ASSERT
        assertTrue(result);
    }

    /**
     * Test for hashcode
     * Ensure works
     */
    @Test
    @DisplayName("Test for hashCode - Ensure works")
    void hashCodeEnsureWorksTest() {
        //Arrange
        String memberID = "11";
        String groupID = "22";
        AddMemberRequestDTO dto = new AddMemberRequestDTO(memberID, groupID);

        int expected = 51169;
        //Act
        int result = dto.hashCode();
        //Assert
        assertEquals(expected, result);
    }
}