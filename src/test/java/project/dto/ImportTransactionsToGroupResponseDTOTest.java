package project.dto;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ImportTransactionsToGroupResponseDTOTest {

    /**
     * Test for ImportTransactionsToGroupResponseDTO constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for ImportTransactionsToGroupResponseDTO constructor - Happy case")
    void constructorImportTransactionsToGroupResponseDTOHappyCaseTest() {
        //Arrange
        String groupID = "100";
        String groupDescription = "group description";
        String transactionDescription = "transaction description";
        ImportTransactionsToGroupResponseDTO responseDTO = new ImportTransactionsToGroupResponseDTO(groupID, groupDescription, transactionDescription);

        //Act
        //Assert
        assertTrue(responseDTO instanceof ImportTransactionsToGroupResponseDTO);

    }

    /**
     * Test for getGroupID method
     * Happy Case
     */
    @Test
    void getGroupIDHappyCaseTest() {
        //Arrange
        String groupID = "100";
        String groupDescription = "group description";
        String transactionDescription = "transaction description";
        ImportTransactionsToGroupResponseDTO responseDTO = new ImportTransactionsToGroupResponseDTO(groupID, groupDescription, transactionDescription);


        String expected = "100";

        //Act
        String actual = responseDTO.getGroupID();

        //Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for getGroupID method
     * Ensure not equals
     */
    @Test
    void getGroupIDEnsureNotEqualsTest() {
        //Arrange
        String groupID = "100";
        String groupDescription = "group description";
        String transactionDescription = "transaction description";
        ImportTransactionsToGroupResponseDTO responseDTO = new ImportTransactionsToGroupResponseDTO(groupID,
                groupDescription, transactionDescription);

        String expected = "200";

        //Act
        String actual = responseDTO.getGroupID();

        //Assert
        assertNotEquals(expected, actual);

    }

    /**
     * Test for getGroupDescription method
     * Happy Case
     */
    @Test
    void getGroupDescriptionHappyCaseTest() {
        //Arrange
        String groupID = "100";
        String groupDescription = "group description";
        String transactionDescription = "transaction description";
        ImportTransactionsToGroupResponseDTO responseDTO = new ImportTransactionsToGroupResponseDTO(groupID,
                groupDescription, transactionDescription);

        String expected = "group description";

        //Act
        String actual = responseDTO.getGroupDescription();

        //Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for getGroupDescription method
     * Ensure not equals
     */
    @Test
    void getGroupDescriptionEnsureNotEqualsTest() {
        //Arrange
        String groupID = "100";
        String groupDescription = "group description";
        String transactionDescription = "transaction description";
        ImportTransactionsToGroupResponseDTO responseDTO = new ImportTransactionsToGroupResponseDTO(groupID,
                groupDescription, transactionDescription);

        String expected = "different group description";

        //Act
        String actual = responseDTO.getGroupDescription();

        //Assert
        assertNotEquals(expected, actual);
    }

    /**
     * Test for getType method
     * Happy Case
     */
    @Test
    void getTransactionDescriptionHappyCaseTest() {
        //Arrange
        String groupID = "100";
        String groupDescription = "group description";
        String transactionDescription = "transaction description";
        ImportTransactionsToGroupResponseDTO responseDTO = new ImportTransactionsToGroupResponseDTO(groupID,
                groupDescription, transactionDescription);

        String expected = "transaction description";

        //Act
        String actual = responseDTO.getTransactionDescription();

        //Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for getType method
     * Ensure not equals
     */
    @Test
    void getTransactionDescriptionEnsureNotEqualsTest() {
        //Arrange
        String groupID = "100";
        String groupDescription = "group description";
        String transactionDescription = "transaction description";
        ImportTransactionsToGroupResponseDTO responseDTO = new ImportTransactionsToGroupResponseDTO(groupID,
                groupDescription, transactionDescription);

        String expected = "different transaction description";

        //Act
        String actual = responseDTO.getTransactionDescription();

        //Assert
        assertNotEquals(expected, actual);
    }
}

