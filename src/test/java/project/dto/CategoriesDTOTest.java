package project.dto;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.entities.shared.AccountID;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class CategoriesDTOTest {

    /**
     * Test for CreateCategoryForGroupDTO constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for CreateCategoryForGroupDTO constructor - Happy case")
    void constructorCreateCategoryForGroupDTOHappyCaseTest() {
        //ARRANGE
        CategoryDTO category1 = new CategoryDTO("equipamento");
        Set<CategoryDTO> categoriesDTO = new HashSet<>();
        categoriesDTO.add(category1);
        
        CategoriesDTO dto = new CategoriesDTO(categoriesDTO);
        //ACT
        //ASSERT
        assertTrue(dto instanceof CategoriesDTO);
    }

    /**
     * Test for getCategories
     * Happy case
     */
    @Test
    @DisplayName("Test for getCategories - Happy case")
    void getCategoriesHappyCaseTest() {
        //ARRANGE

        CategoryDTO category1 = new CategoryDTO("equipamento");
        CategoryDTO category2 = new CategoryDTO("balls");
        Set<CategoryDTO> categoriesDTO = new HashSet<>();
        categoriesDTO.add(category1);
        categoriesDTO.add(category2);
        
        CategoriesDTO dto = new CategoriesDTO(categoriesDTO);

        //ACT
        Set<CategoryDTO> result = dto.getCategories();

        //ASSERT
        assertEquals(categoriesDTO, result);
    }

    /**
     * Test for Equals
     * Test for equals - Ensure true with same object
     */
    @Test
    @DisplayName("Test for equals - Ensure true with same object")
    void equalsEnsureTrueWithSameObjectTest() {
        //ARRANGE
        Set<CategoryDTO> categoriesDTO = new HashSet<>();

        CategoriesDTO dto = new CategoriesDTO(categoriesDTO);

        //ACT
        boolean result = dto.equals(dto);

        //ASSERT
        assertTrue(result);
    }

    /**
     * Test for Equals
     * Ensure true with two objects with the same attribute
     */
    @Test
    @DisplayName("Test for equals - Ensure true with two objects with the same attribute ")
    void equalsEnsureTrueWithObjectsWithSameAttributesTest() {
        //ARRANGE
        Set<CategoryDTO> categoriesDTO = new HashSet<>();
        CategoryDTO category1 = new CategoryDTO("futebol");
        categoriesDTO.add(category1);
        CategoriesDTO dto = new CategoriesDTO(categoriesDTO);
        CategoriesDTO dto1 = new CategoriesDTO(categoriesDTO);

        //ACT
        boolean result = dto.equals(dto1);

        //ASSERT
        assertTrue(result);
    }

    /**
     * Test for Equals
     * Ensure false with different objects
     */
    @Test
    @DisplayName("Test for equals - Ensure false with different objects")
    void equalsEnsureFalseWithDifferentObjectsTest() {
        //ARRANGE
        Set<CategoryDTO> categories = new HashSet<>();
        CategoryDTO category1 = new CategoryDTO("futebol");
        categories.add(category1);
        Set<CategoryDTO> categories1 = new HashSet<>();
        CategoryDTO category2 = new CategoryDTO("futebol");
        categories.add(category2);
        CategoriesDTO dto = new CategoriesDTO(categories);
        CategoriesDTO dto1 = new CategoriesDTO(categories1);

        //ACT
        boolean result = dto.equals(dto1);

        //ASSERT
        assertFalse(result);
    }

    /**
     * Test for Equals
     * null object
     */
    @Test
    @DisplayName("Test for equals - null ")
    void equalsNullTest() {

        //ARRANGE
        Set<CategoryDTO> categories = new HashSet<>();
        CategoriesDTO dto = new CategoriesDTO(categories);
        CategoriesDTO dto2 = null;

        //ACT
        boolean result = dto.equals(dto2);

        //ASSERT
        assertFalse(result);
    }

    /**
     * Test for Equals
     * different object
     */
    @Test
    @DisplayName("Test for equals - different class Object ")
    void equalsDifferentObjectTest() {

        //ARRANGE
        Set<CategoryDTO> categories = new HashSet<>();
        CategoriesDTO dto = new CategoriesDTO(categories);

        AccountID accountID = new AccountID("11");

        //ACT
        boolean result = dto.equals(accountID);

        //ASSERT
        assertFalse(result);
    }

    /**
     * Test for hashCode
     * Happy Case
     */
    @Test
    @DisplayName("Test for hashCode - Happy Case ")
    void hashCodeHappyCaseTest() {

        //ARRANGE
        Set<CategoryDTO> categories = new HashSet<>();
        CategoriesDTO dto = new CategoriesDTO(categories);
        int expected = 59;

        //ACT
        int result = dto.hashCode();

        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for toString
     * Happy Case
     */
    @Test
    @DisplayName("Test for toString - Happy Case ")
    void toStringHappyCaseTest() {

        //ARRANGE
        Set<CategoryDTO> categories = new HashSet<>();
        CategoriesDTO dto = new CategoriesDTO(categories);
        String expected = "CategoriesDTO{categories=[]}";

        //ACT
        String result = dto.toString();

        //ASSERT
        assertEquals(expected, result);
    }

}