package project.dto;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.entities.shared.PersonID;

import static org.junit.jupiter.api.Assertions.*;

class CreateCategoryInfoDTOTest {

    /**
     * Test for CreateCategoryInfoDTO constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for CreateCategoryInfoDTO constructor - Happy case")
    void constructorCreateCategoryInfoDTOHappyCaseTest() {
        //ARRANGE
        CreateCategoryInfoDTO dto = new CreateCategoryInfoDTO("switch");
        //ACT
        //ASSERT
        assertTrue(dto instanceof CreateCategoryInfoDTO);
    }

    /**
     * Test for CreateCategoryInfoDTO constructor empty
     * Happy case
     */
    @Test
    @DisplayName("Test for CreateCategoryInfoDTO constructor empty - Happy case")
    void constructorEmptyCreateCategoryInfoDTOHappyCaseTest() {
        //ARRANGE
        CreateCategoryInfoDTO dto = new CreateCategoryInfoDTO();
        //ACT
        //ASSERT
        assertTrue(dto instanceof CreateCategoryInfoDTO);
    }

    /**
     * Test for getDesignation
     * Happy case
     */
    @Test
    @DisplayName("Test for getDesignation - Happy case")
    void getDesignationHappyCaseTest() {
        //ARRANGE
        CreateCategoryInfoDTO dto = new CreateCategoryInfoDTO("switch");
        String expected = "switch";

        //ACT
        String result = dto.getDesignation();

        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for Equals
     * Happy case - same object
     */
    @Test
    @DisplayName("Test for Equals - Happy case")
    void equalsHappyCaseTest() {
        //ARRANGE
        CreateCategoryInfoDTO dto = new CreateCategoryInfoDTO("switch");

        //ACT
        boolean result = dto.equals(dto);

        //ASSERT
        assertTrue(result);
    }

    /**
     * Test for Equals
     * Happy case - same attributes
     */
    @Test
    @DisplayName("Test for Equals - Same attributes")
    void equalsHappyCaseSameAttributesTest() {
        //ARRANGE
        CreateCategoryInfoDTO dto = new CreateCategoryInfoDTO("switch");
        CreateCategoryInfoDTO outroDto = new CreateCategoryInfoDTO("switch");

        //ACT
        boolean result = dto.equals(outroDto);

        //ASSERT
        assertTrue(result);
    }

    /**
     * Test for Equals
     * Null object
     */
    @Test
    @DisplayName("Test for Equals - Null Object")
    void equalsNullTest() {
        //ARRANGE
        CreateCategoryInfoDTO dto = new CreateCategoryInfoDTO("switch");

        //ACT
        boolean result = dto.equals(null);

        //ASSERT
        assertFalse(result);
    }

    /**
     * Test for Equals
     * Different object
     */
    @Test
    @DisplayName("Test for Equals - Different Object")
    void equalsDifferentObjectTest() {
        //ARRANGE
        CreateCategoryInfoDTO dto = new CreateCategoryInfoDTO("switch");
        PersonID personID = new PersonID("111@switch.pt");

        //ACT
        boolean result = dto.equals(personID);

        //ASSERT
        assertFalse(result);
    }

    /**
     * Test for hashCode
     * Happy Case
     */
    @Test
    @DisplayName("Test for hasCode - Happy Case")
    void hashCodeHappyCaseTest() {
        //ARRANGE
        CreateCategoryInfoDTO dto = new CreateCategoryInfoDTO("carros");
        int expected = -1367590495;

        //ACT
        int result = dto.hashCode();

        //ASSERT
        assertEquals(expected, result);
    }
}