package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import project.model.entities.group.Group;
import project.model.entities.group.GroupID;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.PersonID;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CreateGroupAssemblerTest {

    private PersonID creatorID;
    private LedgerID ledgerID;
    private GroupID groupID;

    @BeforeEach
    void init() {
        creatorID = new PersonID("1@switch.pt");
        ledgerID = new LedgerID("1");
        groupID = new GroupID("1");
    }

    /**
     * Test for GroupAssembler constructor
     */
    @Test
    void constructorTest() {
        CreateGroupAssembler createGroupAssembler = new CreateGroupAssembler();
        assertTrue(createGroupAssembler instanceof CreateGroupAssembler);
    }

    /**
     * Test for mapToDTO method
     * Happy Case
     */
    @Test
    void mapToDTOHappyCaseTest() {
        //ARRANGE
        Group newGroup = new Group(groupID, "description", "2020-01-01", creatorID, ledgerID);

        Set<String> expectedMembers = new HashSet<>();
        expectedMembers.add("1@switch.pt");

        Set<String> expectedManagers = new HashSet<>();
        expectedManagers.add("1@switch.pt");

        String expectedGroupID = "1";
        String expectedLedgerID = "1";
        String expectedDescription = "description";
        String expectedCreationDate = "2020-01-01";

        GroupDTO expected = new GroupDTO(expectedMembers, expectedManagers, expectedGroupID,
                expectedLedgerID, expectedDescription, expectedCreationDate);

        //ACT
        GroupDTO result = CreateGroupAssembler.mapToDTO(newGroup);

        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for mapToRequestDTO method
     * Happy Case
     */
    @Test
    void mapToRequestDTOHappyCaseTest() {
        //ARRANGE
        String groupID = "1";
        String description = "description";
        String creationDate = "2020-01-01";
        String creatorID = "1@switch.pt";
        String groupLedgerID = "1";

        CreateGroupRequestDTO expected = new CreateGroupRequestDTO(groupID, description, creationDate, creatorID, groupLedgerID);

        //ACT
        CreateGroupRequestDTO result = CreateGroupAssembler.mapToRequestDTO(groupID, description, creationDate, creatorID, groupLedgerID);

        //ASSERT
        assertEquals(expected, result);
    }

}