package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.entities.ledger.Transaction;
import project.model.entities.ledger.Type;
import project.model.entities.shared.AccountID;
import project.model.entities.shared.Category;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TransactionsAssemblerTest {

    List<Transaction> transactions;
    List<TransactionDTO> transactionsDTO;


    @BeforeEach
    public void init() {
        //Variables
        String debitAccountIDStg = "1002";
        AccountID debitAccountId = new AccountID(debitAccountIDStg);
        String creditAccountIDStg = "1004";
        AccountID creditAccountId = new AccountID(creditAccountIDStg);
        Category category = new Category("comida e bebida");


        Transaction transaction1 = new Transaction("500", Type.CREDIT, "2000-01-01 23:20:58", "Pacote de Oreos", category, debitAccountId, creditAccountId);
        Transaction transaction2 = new Transaction("400", Type.DEBIT, "2010-01-01 11:39:23", "Pacote de Belgas", category, debitAccountId, creditAccountId);

        transactions = new ArrayList<>();

        transactions.add(transaction1);
        transactions.add(transaction2);

        TransactionDTO tdto1 = new TransactionDTO("500", "CREDIT", "2000-01-01 23:20:58", "Pacote de Oreos", "comida e bebida", "1002", "1004");
        TransactionDTO tdto2 = new TransactionDTO("400", "DEBIT", "2010-01-01 11:39:23", "Pacote de Belgas", "comida e bebida", "1002", "1004");

        transactionsDTO = new ArrayList<>();

        transactionsDTO.add(tdto1);
        transactionsDTO.add(tdto2);
    }


    /**
     * Test for Constructor - Happy Case
     */
    @Test
    @DisplayName("Test for TransactionsAssembler Constructor - Happy Case")
    void instanceOfGetPersonsGroupsAssembler() {
        TransactionsAssembler assembler = new TransactionsAssembler();
        assertTrue(assembler instanceof TransactionsAssembler);
    }

    /**
     * Test for mapToDTO Method - Happy case
     */
    @Test
    @DisplayName("MapToDTO- Happy Case")
    void mapToDTOHappyCase() {
        //Arrange
        TransactionsResponseDTO expected = new TransactionsResponseDTO(transactionsDTO);

        //Act
        TransactionsResponseDTO result = TransactionsAssembler.mapToDTO(transactions);

        //Assert
        assertEquals(expected, result);
    }


}