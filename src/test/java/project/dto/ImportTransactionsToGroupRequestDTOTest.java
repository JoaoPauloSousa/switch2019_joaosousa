package project.dto;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ImportTransactionsToGroupRequestDTOTest {

    /**
     * Test for ImportTransactionsToGroupRequestDTO constructor
     */
    @Test
    @DisplayName("Test for ImportTransactionsToGroupRequestDTO constructor")
    void constructorImportTransactionsToGroupRequestDTOTest() {
        //Arrange
        String personEmail = "100@switch.pt";
        String groupID = "200";
        String transactions = "xpto";
        ImportTransactionsToGroupRequestDTO requestDTO = new ImportTransactionsToGroupRequestDTO(personEmail, groupID,
                transactions);

        //Act
        //Assert
        assertTrue(requestDTO instanceof ImportTransactionsToGroupRequestDTO);
    }

    /**
     * Test for getPersonID method
     * Ensure not equals
     */
    @Test
    void getPersonIDEnsureNotEqualsTest() {
        //Arrange
        String personEmail = "100@switch.pt";
        String groupID = "200";
        String transactions = "xpto";
        ImportTransactionsToGroupRequestDTO requestDTO = new ImportTransactionsToGroupRequestDTO(personEmail, groupID,
                transactions);

        String expected = "200@switch.pt";

        //Act
        String actual = requestDTO.getPersonEmail();

        //Assert
        assertNotEquals(expected, actual);
    }

}