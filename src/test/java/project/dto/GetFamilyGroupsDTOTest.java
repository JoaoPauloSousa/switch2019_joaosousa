package project.dto;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class GetFamilyGroupsDTOTest {

    /**
     * Test for GetFamilyGroupsDTO constructor
     * Happy Case
     */
    @Test
    void constructorHappyCaseTest() {
        Set<String> familyGroupsIDs = new HashSet<>();
        GetFamilyGroupsDTO familyGroupsDTO = new GetFamilyGroupsDTO(familyGroupsIDs);

        //Assert
        assertTrue(familyGroupsDTO instanceof GetFamilyGroupsDTO);
    }

    /**
     * Test for getGroupsIDs method
     * Happy Case
     */
    @Test
    void getGroupsIDsHappyCase() {

        //Arrange
        Set<String> familyGroupsIDs = new HashSet<>();
        familyGroupsIDs.add("542");
        familyGroupsIDs.add("2154");

        GetFamilyGroupsDTO familyGroupsDTO = new GetFamilyGroupsDTO(familyGroupsIDs);

        //Act
        Set<String> result = familyGroupsDTO.getFamilyGroupsIDs();

        //Assert
        assertEquals(familyGroupsIDs, result);
    }

    /**
     * Test for getGroupIDs method
     * Ensure not equals
     */
    @Test
    void getGroupsIDSEnsureNotEqualsTest() {
        //Arrange

        Set<String> familyGroupsIDs = new HashSet<>();
        familyGroupsIDs.add("542");

        GetFamilyGroupsDTO familyGroupsDTO = new GetFamilyGroupsDTO(familyGroupsIDs);

        Set<String> expected = new HashSet<>();
        expected.add("245");

        //Act
        Set<String> result = familyGroupsDTO.getFamilyGroupsIDs();

        //Assert
        assertNotEquals(expected, result);

    }

    /**
     * Test for getGroupIDs method
     * Ensure Equals for empty Set of members
     */
    @Test
    void getMembersEnsureEqualsForEmptyMembersTest() {
        //Arrange
        Set<String> familyGroupsIDs = new HashSet<>();

        GetFamilyGroupsDTO groupDTO = new GetFamilyGroupsDTO(familyGroupsIDs);

        Set<String> expected = new HashSet<>();

        //Act
        Set<String> result = groupDTO.getFamilyGroupsIDs();

        //Assert
        assertEquals(expected, result);

    }

    /**
     * Test for setFamilyGroups method
     * ensure does not include repeated familyGroupsIDs
     */
    @Test
    void setGroupsIDSEnsureNotAddExistentID() {
        //Arrange
        Set<String> familyGroupsIDs = new HashSet<>();
        familyGroupsIDs.add("542");
        familyGroupsIDs.add("542");

        GetFamilyGroupsDTO familyGroupsDTO = new GetFamilyGroupsDTO(familyGroupsIDs);

        Set<String> expected = new HashSet<>();
        expected.add("542");

        //Act
        Set<String> result = familyGroupsDTO.getFamilyGroupsIDs();

        //Assert
        assertEquals(familyGroupsIDs, result);
    }

    /**
     * Test for hashcode
     * Ensure it works
     */
    @Test
    @DisplayName("Test for hashCode - Ensure it works")
    void hashCodeEnsureItWorksTest() {
        //Arrange
        Set<String> familyGroupsIDs = new HashSet<>();
        GetFamilyGroupsDTO familyGroupsDTO = new GetFamilyGroupsDTO(familyGroupsIDs);
        int expected = 31;

        //Act
        int result = familyGroupsDTO.hashCode();

        //Assert
        assertEquals(expected, result);
    }

    /**
     * Test for equals
     * Ensure true if is same object
     */
    @Test
    @DisplayName("Test for equals - Ensure true with same object")
    void equalsEnsureTrueWithSameObjectTest() {
        //Arrange
        Set<String> familyGroupsIDs = new HashSet<>();
        GetFamilyGroupsDTO familyGroupsDTO = new GetFamilyGroupsDTO(familyGroupsIDs);

        //Act
        boolean result = familyGroupsDTO.equals(familyGroupsDTO);

        //Assert
        assertTrue(result);
    }

    /**
     * Test for equals
     * Ensure false with different object
     */
    @Test
    @DisplayName("Test for equals - Ensure false with object from another class")
    void equalsEnsureFalseWithObjectFromAnotherClassTest() {
        //Arrange
        Set<String> familyGroupsIDs = new HashSet<>();
        GetFamilyGroupsDTO familyGroupsDTO = new GetFamilyGroupsDTO(familyGroupsIDs);
        Set<String> stringDTO = new HashSet<>();
        CreateAccountForGroupDTO groupDTO = new CreateAccountForGroupDTO(stringDTO);

        //Act
        boolean result = familyGroupsDTO.equals(groupDTO);

        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false with null object
     */
    @Test
    @DisplayName("Test for equals - Ensure false with null")
    void equalsEnsureFalseWithNullTest() {
        //Arrange
        Set<String> familyGroupsIDs = new HashSet<>();
        GetFamilyGroupsDTO familyGroupsDTO = new GetFamilyGroupsDTO(familyGroupsIDs);
        GetFamilyGroupsDTO familyGroupsDTO1 = null;
        //Act
        boolean result = familyGroupsDTO.equals(familyGroupsDTO1);
        //Assert
        assertFalse(result);
    }
}