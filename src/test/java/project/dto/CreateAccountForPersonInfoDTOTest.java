package project.dto;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.entities.group.GroupID;

import static org.junit.jupiter.api.Assertions.*;

class CreateAccountForPersonInfoDTOTest {

    /**
     * Test for CreateAccountForPersonInfoDTO constructor
     */
    @Test
    @DisplayName("Test for CreateAccountForPersonInfoDTO constructor")
    void constructorHappyCaseTest() {
        CreateAccountForPersonInfoDTO dto = new CreateAccountForPersonInfoDTO("1", "food", "food");
        //ASSERT
        assertTrue(dto instanceof CreateAccountForPersonInfoDTO);
    }

    /**
     * Test for getAccountID
     * Happy case
     */
    @Test
    @DisplayName(" Test for getAccountsIDs - Happy Case")
    void getAccountIDHappyCaseTest() {
        //ARRANGE
        CreateAccountForPersonInfoDTO dto = new CreateAccountForPersonInfoDTO("1", "food", "food");
        String expected = "1";
        //ACT
        String result = dto.getAccountID();
        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for getDenomination
     * Happy case
     */
    @Test
    @DisplayName(" Test for getDenomination- Happy Case")
    void getDenominationHappyCaseTest() {
        //ARRANGE
        CreateAccountForPersonInfoDTO dto = new CreateAccountForPersonInfoDTO("1", "food", "food");
        String expected = "food";
        //ACT
        String result = dto.getDenomination();
        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for getDescription
     * Happy case
     */
    @Test
    @DisplayName(" Test for getDescription Happy Case")
    void getDescriptionHappyCaseTest() {
        //ARRANGE
        CreateAccountForPersonInfoDTO dto = new CreateAccountForPersonInfoDTO("1", "food", "food");
        String expected = "food";
        //ACT
        String result = dto.getDescription();
        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for equals
     * Happy case
     */
    @Test
    @DisplayName("Test for equals - HappyCase")
    void equalsHappyCaseTest() {
        //Arrange
        CreateAccountForPersonInfoDTO dto1 = new CreateAccountForPersonInfoDTO("1", "food", "food");
        CreateAccountForPersonInfoDTO dto2 = new CreateAccountForPersonInfoDTO("1", "food", "food");
        //Act
        boolean result = dto1.equals(dto2);
        //Assert
        assertTrue(result);
    }

    /**
     * Test for equals
     * Not Equals - Different accountID
     */
    @Test
    @DisplayName("Test for equals - Not Equals - Different accountID")
    void equalsDifferentAccountIDTest() {
        //Arrange
        CreateAccountForPersonInfoDTO dto1 = new CreateAccountForPersonInfoDTO("1", "food", "food");
        CreateAccountForPersonInfoDTO dto2 = new CreateAccountForPersonInfoDTO("2", "food", "food");
        //Act
        boolean result = dto1.equals(dto2);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Not Equals - Different denomination
     */
    @Test
    @DisplayName("Test for equals - Not Equals - Different denomination")
    void equalsDifferentDenominationTest() {
        //Arrange
        CreateAccountForPersonInfoDTO dto1 = new CreateAccountForPersonInfoDTO("1", "food", "food");
        CreateAccountForPersonInfoDTO dto2 = new CreateAccountForPersonInfoDTO("1", "comida", "food");
        //Act
        boolean result = dto1.equals(dto2);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Not Equals - Different description
     */
    @Test
    @DisplayName("Test for equals - Not Equals - Different description")
    void equalsDifferentDescriptionTest() {
        //Arrange
        CreateAccountForPersonInfoDTO dto1 = new CreateAccountForPersonInfoDTO("1", "food", "food");
        CreateAccountForPersonInfoDTO dto2 = new CreateAccountForPersonInfoDTO("1", "food", "comida");
        //Act
        boolean result = dto1.equals(dto2);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false with null object
     */
    @Test
    @DisplayName("Test for equals - Ensure false with null")
    void equalsEnsureFalseWithNullTest() {
        //Arrange
        CreateAccountForPersonInfoDTO dto = new CreateAccountForPersonInfoDTO("1", "food", "food");
        CreateAccountForPersonInfoDTO dto2 = null;
        //Act
        boolean result = dto.equals(dto2);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false with diferent object
     */
    @Test
    @DisplayName("Test for equals - Ensure false with object from another class")
    void equalsEnsureFalseWithObjectFromAnotherClassTest() {
        //Arrange
        CreateAccountForPersonInfoDTO dto = new CreateAccountForPersonInfoDTO("1", "food", "food");
        GroupID groupID = new GroupID("1");
        //Act
        boolean result = dto.equals(groupID);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure true if is same object
     */
    @Test
    @DisplayName("Test for equals - Ensure true with same object")
    void equalsEnsureTrueWithSameObjectTest() {
        //Arrange
        CreateAccountForPersonInfoDTO dto = new CreateAccountForPersonInfoDTO("1", "food", "food");
        //Act
        boolean result = dto.equals(dto);
        //Assert
        assertTrue(result);
    }

    /**
     * Test for hashcode
     * Ensure works
     */
    @Test
    @DisplayName("Test for hashCode - Ensure works")
    void hashCodeEnsureWorksTest() {
        //Arrange
        CreateAccountForPersonInfoDTO dto = new CreateAccountForPersonInfoDTO("1", "food", "food");
        int expected = 100841488;
        //Act
        int result = dto.hashCode();
        //Assert
        assertEquals(expected, result);
    }
}