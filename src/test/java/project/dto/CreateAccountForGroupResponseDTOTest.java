package project.dto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CreateAccountForGroupResponseDTOTest {

    /**
     * Constructor test
     */
    @Test
    void constructorTest() {
        //ARRANGE
        String groupIDStg = "1000";
        String groupDescription = "description";
        String accountIDStg = "1500";
        String accountDenomination = "denomination";
        CreateAccountForGroupResponseDTO dto = new CreateAccountForGroupResponseDTO(groupIDStg, groupDescription, accountIDStg, accountDenomination);

        //ASSERT
        assertTrue(dto instanceof CreateAccountForGroupResponseDTO);
    }

    /**
     * Test for getGroupID
     * Happy Case
     */
    @Test
    void getGroupIDHappyCaseTest() {
        String groupIDStg = "1000";
        String groupDescription = "description";
        String accountIDStg = "1500";
        String accountDenomination = "denomination";
        CreateAccountForGroupResponseDTO dto = new CreateAccountForGroupResponseDTO(groupIDStg, groupDescription, accountIDStg, accountDenomination);

        String expected = "1000";

        //ACT
        String actual = dto.getGroupID();

        //ASSERT
        assertEquals(expected, actual);
    }

    /**
     * Test for getGroupID
     * Ensure not equals
     */
    @Test
    void getGroupIDEnsureNotEqualsTest() {
        String groupIDStg = "1000";
        String groupDescription = "description";
        String accountIDStg = "1500";
        String accountDenomination = "denomination";
        CreateAccountForGroupResponseDTO dto = new CreateAccountForGroupResponseDTO(groupIDStg, groupDescription, accountIDStg, accountDenomination);

        String expected = "2000";

        //ACT
        String actual = dto.getGroupID();

        //ASSERT
        assertNotEquals(expected, actual);
    }

    /**
     * Test for getGroupDescription
     * Happy Case
     */
    @Test
    void getGroupDescriptionHappyCaseTest() {
        String groupIDStg = "1000";
        String groupDescription = "description";
        String accountIDStg = "1500";
        String accountDenomination = "denomination";
        CreateAccountForGroupResponseDTO dto = new CreateAccountForGroupResponseDTO(groupIDStg, groupDescription, accountIDStg, accountDenomination);

        String expected = "description";

        //ACT
        String actual = dto.getGroupDescription();

        //ASSERT
        assertEquals(expected, actual);
    }

    /**
     * Test for getGroupDescription
     * Ensure not equals test
     */
    @Test
    void getGroupDescriptionEnsureNotEqualsTest() {
        String groupIDStg = "1000";
        String groupDescription = "description";
        String accountIDStg = "1500";
        String accountDenomination = "denomination";
        CreateAccountForGroupResponseDTO dto = new CreateAccountForGroupResponseDTO(groupIDStg, groupDescription, accountIDStg, accountDenomination);

        String expected = "other description";

        //ACT
        String actual = dto.getGroupDescription();

        //ASSERT
        assertNotEquals(expected, actual);
    }

    /**
     * Test for getAccountID
     * Happy Case
     */
    @Test
    void getAccountIDHappyCaseTest() {
        String groupIDStg = "1000";
        String groupDescription = "description";
        String accountIDStg = "1500";
        String accountDenomination = "denomination";
        CreateAccountForGroupResponseDTO dto = new CreateAccountForGroupResponseDTO(groupIDStg, groupDescription, accountIDStg, accountDenomination);

        String expected = "1500";

        //ACT
        String actual = dto.getAccountID();

        //ASSERT
        assertEquals(expected, actual);
    }

    /**
     * Test for getAccountID
     * Ensure not equals
     */
    @Test
    void getAccountIDEnsureNotEqualsTest() {
        String groupIDStg = "1000";
        String groupDescription = "description";
        String accountIDStg = "1500";
        String accountDenomination = "denomination";
        CreateAccountForGroupResponseDTO dto = new CreateAccountForGroupResponseDTO(groupIDStg, groupDescription, accountIDStg, accountDenomination);

        String expected = "2000";

        //ACT
        String actual = dto.getAccountID();

        //ASSERT
        assertNotEquals(expected, actual);
    }

    /**
     * Test for getAccountDenomination
     * Happy Case
     */
    @Test
    void getAccountDenominationHappyCaseTest() {
        String groupIDStg = "1000";
        String groupDescription = "description";
        String accountIDStg = "1500";
        String accountDenomination = "denomination";
        CreateAccountForGroupResponseDTO dto = new CreateAccountForGroupResponseDTO(groupIDStg, groupDescription, accountIDStg, accountDenomination);

        String expected = "denomination";

        //ACT
        String actual = dto.getAccountDenomination();

        //ASSERT
        assertEquals(expected, actual);
    }

    /**
     * Test for getAccountDenomination
     * Ensure not equals
     */
    @Test
    void getAccountDenominationEnsureNotEqualsTest() {
        String groupIDStg = "1000";
        String groupDescription = "description";
        String accountIDStg = "1500";
        String accountDenomination = "denomination";
        CreateAccountForGroupResponseDTO dto = new CreateAccountForGroupResponseDTO(groupIDStg, groupDescription, accountIDStg, accountDenomination);

        String expected = "other denomination";

        //ACT
        String actual = dto.getAccountDenomination();

        //ASSERT
        assertNotEquals(expected, actual);
    }

    /**
     * Test for equals
     * Ensure true when comparing same object
     */
    @Test
    void testEqualsEnsureTrueWithSameObject() {
        //ARRANGE
        String groupIDStg = "1000";
        String groupDescription = "description";
        String accountIDStg = "1500";
        String accountDenomination = "denomination";
        CreateAccountForGroupResponseDTO dto = new CreateAccountForGroupResponseDTO(groupIDStg, groupDescription, accountIDStg, accountDenomination);

        //ACT
        boolean actual = dto.equals(dto);

        //ASSERT
        assertTrue(actual);
    }

    /**
     * Test for equals
     * Ensure false when o is null
     */
    @Test
    void testEqualsEnsureFalseWhenObjectIsNull() {
        //ARRANGE
        String groupIDStg = "1000";
        String groupDescription = "description";
        String accountIDStg = "1500";
        String accountDenomination = "denomination";
        CreateAccountForGroupResponseDTO dto = new CreateAccountForGroupResponseDTO(groupIDStg, groupDescription, accountIDStg, accountDenomination);

        //ACT
        boolean actual = dto.equals(null);

        //ASSERT
        assertFalse(actual);
    }

    /**
     * Test for equals
     * Ensure false when Class is different
     */
    @Test
    void testEqualsEnsureFalseWhenClassIsDifferent() {
        //ARRANGE
        String groupIDStg = "1000";
        String groupDescription = "description";
        String accountIDStg = "1500";
        String accountDenomination = "denomination";
        CreateAccountForGroupResponseDTO dto = new CreateAccountForGroupResponseDTO(groupIDStg, groupDescription, accountIDStg, accountDenomination);

        String other = "other";

        //ACT
        boolean actual = dto.equals(other);

        //ASSERT
        assertFalse(actual);
    }

    /**
     * Test for equals
     * Ensure true when all attributes are equal
     */
    @Test
    void testEqualsEnsureTrueWhenAllAtributesAreEqual() {
        //ARRANGE
        String groupIDStg = "1000";
        String groupDescription = "description";
        String accountIDStg = "1500";
        String accountDenomination = "denomination";
        CreateAccountForGroupResponseDTO dto = new CreateAccountForGroupResponseDTO(groupIDStg, groupDescription, accountIDStg, accountDenomination);

        String otherGroupIDStg = "1000";
        String otherGroupDescription = "description";
        String otherAccountIDStg = "1500";
        String otherAccountDenomination = "denomination";
        CreateAccountForGroupResponseDTO otherDto = new CreateAccountForGroupResponseDTO(otherGroupIDStg, otherGroupDescription, otherAccountIDStg, otherAccountDenomination);

        //ACT
        boolean actual = dto.equals(otherDto);

        //ASSERT
        assertTrue(actual);
    }

    /**
     * Test for equals
     * Ensure false when groupID is different
     */
    @Test
    void testEqualsEnsureFalseWhenGroupIDIsDifferent() {
        //ARRANGE
        String groupIDStg = "1000";
        String groupDescription = "description";
        String accountIDStg = "1500";
        String accountDenomination = "denomination";
        CreateAccountForGroupResponseDTO dto = new CreateAccountForGroupResponseDTO(groupIDStg, groupDescription, accountIDStg, accountDenomination);

        String otherGroupIDStg = "2000";
        String otherGroupDescription = "description";
        String otherAccountIDStg = "1500";
        String otherAccountDenomination = "denomination";
        CreateAccountForGroupResponseDTO otherDto = new CreateAccountForGroupResponseDTO(otherGroupIDStg, otherGroupDescription, otherAccountIDStg, otherAccountDenomination);

        //ACT
        boolean actual = dto.equals(otherDto);

        //ASSERT
        assertFalse(actual);
    }

    /**
     * Test for equals
     * Ensure false when groupDescription is different
     */
    @Test
    void testEqualsEnsureFalseWhenGroupDescriptionIsDifferent() {
        //ARRANGE
        String groupIDStg = "1000";
        String groupDescription = "description";
        String accountIDStg = "1500";
        String accountDenomination = "denomination";
        CreateAccountForGroupResponseDTO dto = new CreateAccountForGroupResponseDTO(groupIDStg, groupDescription, accountIDStg, accountDenomination);

        String otherGroupIDStg = "1000";
        String otherGroupDescription = "other description";
        String otherAccountIDStg = "1500";
        String otherAccountDenomination = "denomination";
        CreateAccountForGroupResponseDTO otherDto = new CreateAccountForGroupResponseDTO(otherGroupIDStg, otherGroupDescription, otherAccountIDStg, otherAccountDenomination);

        //ACT
        boolean actual = dto.equals(otherDto);

        //ASSERT
        assertFalse(actual);
    }

    /**
     * Test for equals
     * Ensure false when accountID is different
     */
    @Test
    void testEqualsEnsureFalseWhenAccountIDIsDifferent() {
        //ARRANGE
        String groupIDStg = "1000";
        String groupDescription = "description";
        String accountIDStg = "1500";
        String accountDenomination = "denomination";
        CreateAccountForGroupResponseDTO dto = new CreateAccountForGroupResponseDTO(groupIDStg, groupDescription, accountIDStg, accountDenomination);

        String otherGroupIDStg = "1000";
        String otherGroupDescription = "description";
        String otherAccountIDStg = "2000";
        String otherAccountDenomination = "denomination";
        CreateAccountForGroupResponseDTO otherDto = new CreateAccountForGroupResponseDTO(otherGroupIDStg, otherGroupDescription, otherAccountIDStg, otherAccountDenomination);

        //ACT
        boolean actual = dto.equals(otherDto);

        //ASSERT
        assertFalse(actual);
    }

    /**
     * Test for equals
     * Ensure false when accountDenomination is different
     */
    @Test
    void testEqualsEnsureFalseWhenAccountDenominationIsDifferent() {
        //ARRANGE
        String groupIDStg = "1000";
        String groupDescription = "description";
        String accountIDStg = "1500";
        String accountDenomination = "denomination";
        CreateAccountForGroupResponseDTO dto = new CreateAccountForGroupResponseDTO(groupIDStg, groupDescription, accountIDStg, accountDenomination);

        String otherGroupIDStg = "1000";
        String otherGroupDescription = "description";
        String otherAccountIDStg = "1500";
        String otherAccountDenomination = "other denomination";
        CreateAccountForGroupResponseDTO otherDto = new CreateAccountForGroupResponseDTO(otherGroupIDStg, otherGroupDescription, otherAccountIDStg, otherAccountDenomination);

        //ACT
        boolean actual = dto.equals(otherDto);

        //ASSERT
        assertFalse(actual);
    }

    @Test
    void testHashCode() {
        String groupIDStg = "1000";
        String groupDescription = "description";
        String accountIDStg = "1500";
        String accountDenomination = "denomination";
        CreateAccountForGroupResponseDTO dto = new CreateAccountForGroupResponseDTO(groupIDStg, groupDescription, accountIDStg, accountDenomination);

        int expected = 1251047838;

        //ACTUAL
        int actual = dto.hashCode();

        //ASSERT
        assertEquals(expected,actual);
    }
}