package project.datamodel;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.entities.ledger.Amount;
import project.model.entities.ledger.Ledger;
import project.model.entities.ledger.Type;
import project.model.entities.shared.AccountID;
import project.model.entities.shared.Category;
import project.model.entities.shared.Description;
import project.model.entities.shared.LedgerID;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LedgerDomainDataAssemblerTest {

    /**
     * Test forLedgerDomainDataAssemblerTest - toData
     * Testing ledger without transactions
     */
    @Test
    @DisplayName("Test for LedgerDomainDataAssemblerTest - toData - Ledger without transactions ")
    void toDataLedgerWithoutTransactionsTest() {
        //Assert
        LedgerID ledgerID = new LedgerID("1");
        Ledger ledger = new Ledger(ledgerID);
        LedgerJpa expected = new LedgerJpa("1");

        //Act
        LedgerJpa result = LedgerDomainDataAssembler.toData(ledger);

        //Assert
        assertEquals(expected, result);
    }

    /**
     * Test forLedgerDomainDataAssemblerTest - toData
     * Testing ledger with transactions
     */
    @Test
    @DisplayName("Test for LedgerDomainDataAssemblerTest - toData - Ledger with transactions")
    void toDataLedgerWithTransactionsTest() {
        //Assert
        LedgerID ledgerID = new LedgerID("1");
        Ledger ledger = new Ledger(ledgerID);
        Type type = Type.DEBIT;
        ledger.addTransaction("1000",
                type,
                "2020-01-01 12:12:12",
                "description",
                new Category("designation"),
                new AccountID("10"),
                new AccountID("20"));

        LedgerJpa expectedLedgerJpa = new LedgerJpa("1");

        LedgerJpa ledgerJpa = new LedgerJpa("1");
        List<TransactionJpa> transactionsJpa = new ArrayList<>();
        TransactionJpa transactionJpa = new TransactionJpa(ledgerJpa, new Amount("1000"),
                "2020-01-01 12:12:12",
                "Debit",
                new Description("description"),
                new Category("designation"),
                new AccountID("10"),
                new AccountID("20"));
        transactionsJpa.add(transactionJpa);

        expectedLedgerJpa.setTransactions(transactionsJpa);
        String expectedLedgerJpaStg = expectedLedgerJpa.toString();

        //Act
        LedgerJpa actualLedgerJpa = LedgerDomainDataAssembler.toData(ledger);
        String actualLedgerJpaStg = actualLedgerJpa.toString();

        //Assert
        assertEquals(expectedLedgerJpaStg, actualLedgerJpaStg);
    }

    /**
     * Test for LedgerDomainDataAssemblerTest - toDomain
     * Testing ledger without transactions
     */
    @Test
    @DisplayName("Test for LedgerDomainDataAssemblerTest - toDomain ")
    void toDomainLedgerWithoutTransactionsTest() {
        //Assert
        LedgerID ledgerID = new LedgerID("1");
        LedgerJpa ledgerJpa = new LedgerJpa("1");
        Ledger expected = new Ledger(ledgerID);

        //Act
        Ledger result = LedgerDomainDataAssembler.toDomain(ledgerJpa);

        //Assert
        assertEquals(expected, result);

    }

    /**
     * Test for LedgerDomainDataAssemblerTest - toDomain
     * Testing ledger with transactions
     */
    @Test
    @DisplayName("Test for LedgerDomainDataAssemblerTest - toDomain - Ledger with transactions ")
    void toDomainLedgerWithTransactionsTest() {
        //Assert
        LedgerID ledgerID = new LedgerID("1");
        LedgerJpa ledgerJpa = new LedgerJpa("1");

        List<TransactionJpa> transactionsJpa = new ArrayList<>();
        TransactionJpa transactionJpa = new TransactionJpa(ledgerJpa, new Amount("1000"),
                "2020-01-01 12:12:12",
                "Debit",
                new Description("description"),
                new Category("designation"),
                new AccountID("10"),
                new AccountID("20"));
        transactionsJpa.add(transactionJpa);

        ledgerJpa.setTransactions(transactionsJpa);

        Ledger expectedLedger = new Ledger(ledgerID);
        Type type = Type.DEBIT;
        expectedLedger.addTransaction("1000",
                type,
                "2020-01-01 12:12:12",
                "description",
                new Category("designation"),
                new AccountID("10"),
                new AccountID("20"));

        //Act
        Ledger actualLedger = LedgerDomainDataAssembler.toDomain(ledgerJpa);

        //Assert
        assertEquals(expectedLedger, actualLedger);

    }
}