package project.datamodel;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.entities.shared.Category;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CategoriesListConverterTest {

    @Test
    @DisplayName("Test for CategoriesListConverter - convert To Data base Column ")
    void convertToDatabaseColumn() {
        //Arrange
        CategoriesListConverter converter = new CategoriesListConverter();
        List<Category> categories = new ArrayList<>();
        categories.add(new Category("Bills"));
        categories.add(new Category("Bar"));

        String expected = "Bills,Bar";

        //Act
        String result = converter.convertToDatabaseColumn(categories);

        //Assert
        assertEquals(expected, result);
    }

    @Test
    @DisplayName("Test for CategoriesListConverter - convert To Entity Attribute ")
    void convertToEntityAttribute() {
        //Arrange
        String categories = "Bills,Bar";

        List<Category> expected = new ArrayList<>();
        expected.add(new Category("Bills"));
        expected.add(new Category("Bar"));
        CategoriesListConverter categoriesListConverter = new CategoriesListConverter();

        //Act
        List<Category> result = categoriesListConverter.convertToEntityAttribute(categories);

        //Assert
        assertEquals(expected, result);
    }

    @Test
    @DisplayName("Test for CategoriesListConverter - convert To Entity Attribute Ensure works with no data")
    void convertToEntityAttributeEnsureWorksWithNoData() {
        //Arrange
        String categories = "";

        List<Category> expected = new ArrayList<>();
        CategoriesListConverter categoriesListConverter = new CategoriesListConverter();

        //Act
        List<Category> result = categoriesListConverter.convertToEntityAttribute(categories);

        //Assert
        assertEquals(expected, result);
    }
}