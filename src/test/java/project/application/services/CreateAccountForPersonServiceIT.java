package project.application.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import project.ProjectApplication;
import project.dto.AccountDTO;
import project.dto.AccountsDTO;
import project.dto.CreateAccountForPersonRequestDTO;
import project.dto.CreateAccountForPersonResponseDTO;
import project.exceptions.AccountAlreadyExistsException;
import project.exceptions.PersonNotFoundException;
import project.frameworkddd.IUSCreateAccountForPersonService;
import project.model.specifications.repositories.AccountRepository;
import project.model.specifications.repositories.PersonRepository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Transactional
@SpringBootTest(classes = ProjectApplication.class)
class CreateAccountForPersonServiceIT {

    @Autowired
    PersonRepository personRepo;

    @Autowired
    AccountRepository accountRepo;

    @Autowired
    IUSCreateAccountForPersonService service;

    /**
     * Test for CreateAccountForPersonService Constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for constructor- Happy case")
    void CreateAccountForPersonServiceConstructorTest() {
        //ASSERT
        assertTrue(service instanceof CreateAccountForPersonService);
    }

    /**
     * Test for createAccountForPerson
     * Happy Case
     */
    @Test
    @DisplayName("Test for createAccountForPerson - Happy case")
    void createAccountForPersonHappyCaseTest() {
        //ARRANGE
        CreateAccountForPersonRequestDTO requestDTO = new CreateAccountForPersonRequestDTO("66610", "6661@switch.pt", "food", "food");

        String expectedPersonID = "6661@switch.pt";
        String expectedPersonName = "Rambo, John";
        String expectedAccountID = "66610";
        String expectedDenomination = "food";

        CreateAccountForPersonResponseDTO expected = new CreateAccountForPersonResponseDTO
                (expectedPersonID, expectedPersonName, expectedAccountID, expectedDenomination);
        //ACT
        CreateAccountForPersonResponseDTO result = service.createAccountForPerson(requestDTO);
        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for createAccountForPerson
     * Happy Case More than One Account
     */
    @Test
    @DisplayName("Test for createAccountForPerson - Happy case - More Than one Account")
    void createAccountForPersonHappyCaseMoreThanOneAccountTest() {
        //ARRANGE
        CreateAccountForPersonRequestDTO requestDTO = new CreateAccountForPersonRequestDTO("66620", "6661@switch.pt", "bills", "bills");

        String expectedPersonID = "6661@switch.pt";
        String expectedPersonName = "Rambo, John";
        String expectedAccountID = "66620";
        String expectedDenomination = "bills";

        CreateAccountForPersonResponseDTO expected = new CreateAccountForPersonResponseDTO(expectedPersonID, expectedPersonName, expectedAccountID, expectedDenomination);
        //ACT
        CreateAccountForPersonResponseDTO result = service.createAccountForPerson(requestDTO);
        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for createAccountForPerson
     * PersonID doesn't exist in the person repository
     */
    @Test
    @DisplayName("Test for createAccountForPerson - PersonID doesn't exist in the person repository")
    void createAccountForPersonDontExistTest() {
        //ARRANGE
        CreateAccountForPersonRequestDTO requestDTO = new CreateAccountForPersonRequestDTO("66620", "6002@switch.pt", "bills", "bills");

        //ACT
        //ASSERT
        Assertions.assertThrows(PersonNotFoundException.class, () -> {
            service.createAccountForPerson(requestDTO);
        });
    }

    /**
     * Test for createAccountForPerson
     * Account already exists in the accountRepository
     */
    @Test
    @DisplayName("Test for createAccountForPerson - Account already exists in the accountRepository")
    void createAccountForPersonAccountAlreadyExistsInTheRepositoryTest() {
        //ARRANGE
        CreateAccountForPersonRequestDTO requestDTO = new CreateAccountForPersonRequestDTO("66601", "6661@switch.pt", "bills", "bills");

        //ACT
        //ASSERT
        Assertions.assertThrows(AccountAlreadyExistsException.class, () -> {
            service.createAccountForPerson(requestDTO);
        });
    }

    /**
     * Test for getAccountByPersonHappyCase
     * Happy Case
     */
    @Test
    @DisplayName("Test for getAccountByPersonID - Happy Case")
    void getAccountByPersonIDHappyCaseTest() {
        //ARRANGE
        List<AccountDTO> expectedList = new ArrayList<>();
        expectedList.add(new AccountDTO("arrows", "War assets", "66601"));
        expectedList.add(new AccountDTO("knife sharpeners", "More war assets", "66606"));

        AccountsDTO expected = new AccountsDTO(expectedList);

        String expectedPersonID = "6661@switch.pt";

        //ACT
        AccountsDTO result = service.getAccountsByPersonID(expectedPersonID);
        //ASSERT
        assertEquals(expected, result);
    }
}