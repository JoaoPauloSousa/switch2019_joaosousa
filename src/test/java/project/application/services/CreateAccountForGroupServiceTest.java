package project.application.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.AccountDTO;
import project.dto.AccountsDTO;
import project.dto.CreateAccountForGroupRequestDTO;
import project.dto.CreateAccountForGroupResponseDTO;
import project.exceptions.AccountAlreadyExistsException;
import project.exceptions.AccountNotFoundException;
import project.exceptions.GroupConflictException;
import project.exceptions.GroupNotFoundException;
import project.frameworkddd.IUSCreateAccountForGroupService;
import project.model.entities.account.Account;
import project.model.entities.account.Denomination;
import project.model.entities.group.Group;
import project.model.entities.group.GroupID;
import project.model.entities.shared.*;
import project.model.specifications.repositories.AccountRepository;
import project.model.specifications.repositories.GroupRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("serviceTest")
@ExtendWith(SpringExtension.class)
public class CreateAccountForGroupServiceTest {

    @Autowired
    IUSCreateAccountForGroupService service;

    @Autowired
    AccountRepository accountRepo;

    @Autowired
    GroupRepository groupRepo;

    private GroupID groupID;
    private AccountID accountID;
    private PersonID personID;

    @BeforeEach
    public void init() {
        groupID = new GroupID("1");
        personID = new PersonID("1@switch.pt");
        accountID = new AccountID("1");
    }

    /**
     * createAccountForGroup Unit Test
     * Happy Case
     */
    @Test
    @DisplayName("Test for createAccountForGroup - HappyCase")
    public void createAccountForGroupHappyCaseTest() {

        //ARRANGE
        String accountIDStg = "1";
        String accountDenomination = "shopping";
        String accountDescription = "food";
        String groupIDStg = "1";
        String groupDescription = "family";
        String personIDStg = "1@switch.pt";
        Denomination denomination = new Denomination("shopping");
        GroupID groupID = new GroupID("1");
        PersonID personID = new PersonID("1@switch.pt");
        AccountID accountID = new AccountID("1");

        CreateAccountForGroupRequestDTO requestDTO = new CreateAccountForGroupRequestDTO(accountIDStg, groupIDStg, accountDenomination, accountDescription, personIDStg);
        CreateAccountForGroupResponseDTO responseDTO = new CreateAccountForGroupResponseDTO(groupIDStg, groupDescription, accountIDStg, accountDenomination);

        Account savedAccount = new Account(accountID, "food", "food");
        Group group = new Group(groupID, "family", "2020-03-21", personID, new LedgerID("1"));

        Group mockedGroup = Mockito.mock(Group.class);
        Account mockedAccount = Mockito.mock(Account.class);

        Mockito.when(groupRepo.findById(groupID)).thenReturn(Optional.of(mockedGroup));
        Mockito.when(accountRepo.findById(accountID)).thenReturn(Optional.empty());

        Mockito.when(mockedGroup.hasManagerID(personID)).thenReturn(true);
        Mockito.when(accountRepo.save(savedAccount)).thenReturn(mockedAccount);

        Mockito.when(mockedGroup.addAccount(accountID)).thenReturn(true);

        Mockito.when(groupRepo.save(mockedGroup)).thenReturn(group);

        Mockito.when(mockedAccount.getID()).thenReturn(accountID);
        Mockito.when(mockedAccount.getDenomination()).thenReturn(denomination);

        //ACT
        CreateAccountForGroupResponseDTO actual = service.createAccountForGroup(requestDTO);

        //ASSERT
        assertEquals(responseDTO, actual);
    }

    /**
     * createAccountForGroup Unit Test
     * Ensure throws exception when group doesn't exist in group repository
     */
    @Test
    @DisplayName("Test for createAccountForGroup - Ensure throws exception when group doesn't exist in group repository")
    public void createAccountForGroupGroupDoesNotExistInRepoTest() {

        //ARRANGE
        String accountIDStg = "1";
        String accountDenomination = "shopping";
        String accountDescription = "food";
        String groupIDStg = "1";
        String personIDStg = "1@switch.pt";

        CreateAccountForGroupRequestDTO requestDTO = new CreateAccountForGroupRequestDTO(accountIDStg, groupIDStg, accountDenomination, accountDescription, personIDStg);

        Mockito.when(groupRepo.findById(groupID)).thenReturn(Optional.empty());

        //ACT
        //ASSERT
        Assertions.assertThrows(GroupNotFoundException.class, () -> {
            service.createAccountForGroup(requestDTO);
        });
    }

    /**
     * createAccountForGroup Unit Test
     * Ensure throws exception when the account to be created already exists in account repository
     */
    @Test
    @DisplayName("Test for createAccountForGroup - Ensure throws exception when the account to be created already exists in account repository")
    public void createAccountForGroupAccountAlreadyExistsInRepoTest() {

        //ARRANGE
        String accountIDStg = "1";
        String accountDenomination = "shopping";
        String accountDescription = "food";
        String groupIDStg = "1";
        String personIDStg = "1@switch.pt";

        CreateAccountForGroupRequestDTO requestDTO = new CreateAccountForGroupRequestDTO(accountIDStg, groupIDStg, accountDenomination, accountDescription, personIDStg);

        Group mockedGroup = Mockito.mock(Group.class);
        Account mockedAccount = Mockito.mock(Account.class);

        Mockito.when(groupRepo.findById(groupID)).thenReturn(Optional.of(mockedGroup));
        Mockito.when(accountRepo.findById(accountID)).thenReturn(Optional.of(mockedAccount));

        //ACT
        //ASSERT
        Assertions.assertThrows(AccountAlreadyExistsException.class, () -> {
            service.createAccountForGroup(requestDTO);
        });
    }

    /**
     * createAccountForGroup Unit Test
     * Ensure throws exception when the person who is creating and adding the account to the group is not a manager of the group
     */
    @Test
    @DisplayName("Test for createAccountForGroup - Ensure throws exception when the person who is creating and adding the account to the group is not a manager of the group")
    public void createAccountForGroupPersonIsNotManagerOfTheGroupTest() {

        //ARRANGE
        String accountIDStg = "1";
        String accountDenomination = "shopping";
        String accountDescription = "food";
        String groupIDStg = "1";
        String personIDStg = "1@switch.pt";

        CreateAccountForGroupRequestDTO requestDTO = new CreateAccountForGroupRequestDTO(accountIDStg, groupIDStg, accountDenomination, accountDescription, personIDStg);

        Group mockedGroup = Mockito.mock(Group.class);

        Mockito.when(groupRepo.findById(groupID)).thenReturn(Optional.of(mockedGroup));
        Mockito.when(accountRepo.findById(accountID)).thenReturn(Optional.empty());
        Mockito.when(mockedGroup.hasManagerID(personID)).thenReturn(false);

        //ACT
        //ASSERT
        Assertions.assertThrows(GroupConflictException.class, () -> {
            service.createAccountForGroup(requestDTO);
        });
    }

    /**
     * getAccountByID Unit Test
     * Happy Case
     */
    @Test
    @DisplayName("Test for getAccountByID - Happy Case")
    public void getAccountByIDHappyCaseTest() {

        //ARRANGE
        String accountIDStg = "1";
        String accountDenomination = "shopping";
        String accountDescription = "food";
        Denomination denomination = new Denomination("shopping");
        Description description = new Description("food");
        AccountID accountID = new AccountID("1");

        AccountDTO responseDTO = new AccountDTO(accountDenomination, accountDescription, accountIDStg);

        Account mockedAccount = Mockito.mock(Account.class);
        Mockito.when(accountRepo.findById(accountID)).thenReturn(Optional.of(mockedAccount));
        Mockito.when(mockedAccount.getDenomination()).thenReturn(denomination);
        Mockito.when(mockedAccount.getDescription()).thenReturn(description);
        Mockito.when(mockedAccount.getID()).thenReturn(accountID);

        //ACT
        AccountDTO actual = service.getAccountByID(accountIDStg);

        //ASSERT
        assertEquals(responseDTO, actual);
    }

    /**
     * getAccountByID Unit Test
     * Ensure throws exception when account doesn't exist in account repository
     */
    @Test
    @DisplayName("Test for getAccountByID - Ensure throws exception when account doesn't exist in account repository")
    public void getAccountByIDAccountDoesNotExistInRepoTest() {

        //ARRANGE
        String accountIDStg = "1";
        AccountID accountID = new AccountID("1");

        Mockito.when(accountRepo.findById(accountID)).thenReturn(Optional.empty());

        //ACT
        //ASSERT
        Assertions.assertThrows(AccountNotFoundException.class, () -> {
            service.getAccountByID(accountIDStg);
        });
    }

    /**
     * getAccountByID Unit Test
     * Ensure throws exception when account doesn't exist in account repository
     */
    @Test
    @DisplayName("Test for getAccountByID second method- Ensure throws exception when account doesn't exist in account repository")
    public void getAccountByIDAccountDoesNotExistInRepoSecondMethodTest() {

        //ARRANGE
        String accountIDStg = "1";
        AccountID accountID = new AccountID("1");

        Mockito.when(accountRepo.findById(accountID)).thenReturn(Optional.empty());

        //ACT
        //ASSERT
        Assertions.assertThrows(AccountNotFoundException.class, () -> {
            service.getAccountByID(accountID);
        });
    }

    /**
     * getAccountsByGroupID Unit Test
     * Happy Case
     */
    @Test
    @DisplayName("Test for getAccountsByGroupID - Happy Case")
    public void getAccountsByGroupIDHappyCaseTest() {

        //ARRANGE
        String groupIDStg = "1";
        GroupID groupID = new GroupID(groupIDStg);

        Group mockedGroup = Mockito.mock(Group.class);
        String denomination = "bla";
        String description = "ble";
        String accountIDStr = "1";
        AccountID accountID = new AccountID(accountIDStr);
        Account account = new Account(accountID, denomination, description);

        List<AccountDTO> expectedList = new ArrayList<>();
        expectedList.add(new AccountDTO(denomination, description, accountIDStr));
        AccountsDTO responseDTO = new AccountsDTO(expectedList);

        AccountsIDs accountsIDs = new AccountsIDs();
        accountsIDs.addAccountID(accountID);

        Mockito.when(groupRepo.findById(groupID)).thenReturn(Optional.of(mockedGroup));
        Mockito.when(accountRepo.findById(accountID)).thenReturn(Optional.of(account));
        Mockito.when(mockedGroup.getAccountsIDs()).thenReturn(accountsIDs);

        //ACT
        AccountsDTO actual = service.getAccountsByGroupID(groupIDStg);

        //ASSERT
        assertEquals(responseDTO, actual);
    }

    /**
     * getAccountsByGroupID Unit Test
     * Ensure throws exception when group doesn't exist in group repository
     */
    @Test
    @DisplayName("Test for getAccountsByGroupID - Ensure throws exception when group doesn't exist in group repository")
    public void getAccountsByGroupIDGroupDoesNotExistInRepoTest() {

        //ARRANGE
        String groupIDStg = "1";

        Mockito.when(groupRepo.findById(groupID)).thenReturn(Optional.empty());

        //ACT
        //ASSERT
        Assertions.assertThrows(GroupNotFoundException.class, () -> {
            service.getAccountsByGroupID(groupIDStg);
        });
    }

}
