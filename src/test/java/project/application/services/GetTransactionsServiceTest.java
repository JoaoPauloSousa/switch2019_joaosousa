package project.application.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.TransactionDTO;
import project.dto.TransactionsAssembler;
import project.dto.TransactionsResponseDTO;
import project.exceptions.*;
import project.frameworkddd.IUSGetTransactionsService;
import project.model.entities.Transactions;
import project.model.entities.account.Account;
import project.model.entities.group.Group;
import project.model.entities.group.GroupID;
import project.model.entities.ledger.Ledger;
import project.model.entities.ledger.Transaction;
import project.model.entities.ledger.Type;
import project.model.entities.person.Person;
import project.model.entities.shared.*;
import project.model.specifications.repositories.AccountRepository;
import project.model.specifications.repositories.GroupRepository;
import project.model.specifications.repositories.LedgerRepository;
import project.model.specifications.repositories.PersonRepository;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("serviceTest")
@ExtendWith(SpringExtension.class)
class GetTransactionsServiceTest {

    @Autowired
    IUSGetTransactionsService service;

    @Autowired
    PersonRepository personRepository;

    @Autowired
    GroupRepository groupRepository;

    @Autowired
    LedgerRepository ledgerRepository;

    @Autowired
    AccountRepository accountRepository;


    /**
     * Unit test for method getTransactionsByPersonID
     * Happy Case
     */
    @Test
    @DisplayName("getTransactionsByPersonID - Happy Case Test")
    void getTransactionsByPersonIDHappyCaseTest() {

        //Variables
        String personIdStg = "1001@switch.pt";
        PersonID personID = new PersonID(personIdStg);

        String debitAccountIDStg = "1001";
        AccountID debitAccountId = new AccountID(debitAccountIDStg);

        String creditAccountIDStg = "1003";
        AccountID creditAccountId = new AccountID(creditAccountIDStg);

        String ledgerGroupIdStg = "1001";
        LedgerID personLedgerID = new LedgerID(ledgerGroupIdStg);

        String initialDateTime = "1999-01-01 12:00:00";
        String finalDateTime = "2021-01-01 12:00:00";

        Type typeDebit = Type.DEBIT;
        Type typeCredit = Type.CREDIT;

        Category category = new Category("comida e bebida");

        //TransactionsD for expectedTransactionSet
        Transaction t1 = new Transaction("500", typeCredit, "2000-01-01 23:20:58", "Pacote de Oreos", category, debitAccountId, creditAccountId);
        Transaction t2 = new Transaction("400", typeDebit, "2010-01-01 11:39:23", "Pacote de Belgas", category, debitAccountId, creditAccountId);

        List<Transaction> expectedTransactionList = new ArrayList<>();
        expectedTransactionList.add(t1);
        expectedTransactionList.add(t2);


        //TransactionsDTO for expectedTransactionDTOList to build expectedResponseDTO
        TransactionDTO tdto1 = new TransactionDTO("500", "CREDIT", "2000-01-01 23:20:58", "Pacote de Oreos", "comida e bebida", "1002", "1004");
        TransactionDTO tdto2 = new TransactionDTO("400", "DEBIT", "2010-01-01 11:39:23", "Pacote de Belgas", "comida e bebida", "1002", "1004");

        List<TransactionDTO> expectedTransactionDTOList = new ArrayList<>();
        expectedTransactionDTOList.add(tdto1);
        expectedTransactionDTOList.add(tdto2);


        //Mocks
        Person mockPerson = Mockito.mock(Person.class);
        Account mockAccount = Mockito.mock(Account.class);
        Ledger mockLedger = Mockito.mock(Ledger.class);
        Transactions mockTransactions = Mockito.mock(Transactions.class);


        Mockito.when(personRepository.findById(personID)).thenReturn(Optional.of(mockPerson));
        Mockito.when(mockPerson.getLedgerID()).thenReturn(personLedgerID);

        //getTransactionsByLedgerID
        Mockito.when(ledgerRepository.findById(personLedgerID)).thenReturn(Optional.of(mockLedger));
        Mockito.when(mockLedger.getTransactions()).thenReturn(mockTransactions);
        Mockito.when(mockTransactions.getAll()).thenReturn(expectedTransactionList);

        //Expected Response DTO
        TransactionsResponseDTO expectedResponseDTO = new TransactionsResponseDTO(expectedTransactionDTOList);

        TransactionsResponseDTO actualResponseDTO = service.getTransactionsByPersonID(personIdStg);

        assertEquals(expectedResponseDTO, actualResponseDTO);

    }

    /**
     * Unit test for method getTransactionsByPersonID - Ensure Throws Exception When Person Does Not Exist In Repo Test
     */
    @Test
    @DisplayName("getTransactionsByPersonID - Ensure Throws Exception When Person Does Not Exist In Repo Test")
    void getTransactionsByPersonIDEnsureThrowsExceptionWhenPersonDoesNotExistInRepoTest() {

        //Variables
        String personIdStg = "1005@switch.pt";
        PersonID personID = new PersonID(personIdStg);

        Mockito.when(personRepository.findById(personID)).thenReturn(Optional.empty());

        //Act
        //Assert
        Assertions.assertThrows(PersonNotFoundException.class, () -> {
            service.getTransactionsByPersonID(personIdStg);
        });

    }


    /**
     * Unit test for method getTransactionsByGroupID
     * HappyCase
     */
    @Test
    void getTransactionsByGroupIDHappyCaseTest() {
        //Arrange
        String personIdStg = "10@switch.pt";
        PersonID personID = new PersonID(personIdStg);
        String groupIdStg = "100";
        GroupID groupID = new GroupID(groupIdStg);
        LedgerID ledgerID = new LedgerID("1000");

        Person mockedPerson = Mockito.mock(Person.class);
        Group mockedGroup = Mockito.mock(Group.class);
        Ledger mockedLedger = Mockito.mock(Ledger.class);
        Transactions mockedTransactions = Mockito.mock(Transactions.class);

        Mockito.when(personRepository.findById(personID)).thenReturn(Optional.of(mockedPerson));
        Mockito.when(groupRepository.findById(groupID)).thenReturn(Optional.of(mockedGroup));
        Mockito.when(mockedGroup.hasMemberID(personID)).thenReturn(true);
        Mockito.when(mockedGroup.getLedgerID()).thenReturn(ledgerID);
        Mockito.when(ledgerRepository.findById(ledgerID)).thenReturn(Optional.of(mockedLedger));

        Transaction transaction = new Transaction("1000", Type.DEBIT, "2020-01-01 12:00:00", "description",
                new Category("category"), new AccountID("500"), new AccountID("600"));
        List<Transaction> transactionsSet = new ArrayList<>();
        transactionsSet.add(transaction);
        Mockito.when(mockedLedger.getTransactions()).thenReturn(mockedTransactions);
        Mockito.when(mockedTransactions.getAll()).thenReturn(transactionsSet);

        TransactionsResponseDTO expectedResponseDTO = TransactionsAssembler.mapToDTO(transactionsSet);

        System.out.println(expectedResponseDTO);

        //Act
        TransactionsResponseDTO actualResponseDTO = service.getTransactionsByGroupID(personIdStg, groupIdStg);

        //Assert
        assertEquals(expectedResponseDTO, actualResponseDTO);

    }

    /**
     * Unit test for method getTransactionsByGroupID - Ensure throws exception when person doesn't exist in group repo
     */
    @Test
    void getTransactionsByGroupIDEnsureThrowsExceptionWhenPersonDoesNotExistInRepoTest() {
        //Arrange
        String personIdStg = "10@switch.pt";
        String groupIdStg = "100";
        PersonID personID = new PersonID(personIdStg);

        Mockito.when(personRepository.findById(personID)).thenReturn(Optional.empty());

        //Act
        //Assert
        Assertions.assertThrows(PersonNotFoundException.class, () -> {
            service.getTransactionsByGroupID(personIdStg, groupIdStg);
        });

    }

    /**
     * Unit test for method getTransactionsByGroupID - Ensure throws exception when group doesn't exist in group repo
     */
    @Test
    void getTransactionsByGroupIDEnsureThrowsExceptionWhenGroupDoesNotExistInRepoTest() {
        //Arrange
        String personIdStg = "10@switch.pt";
        PersonID personID = new PersonID(personIdStg);
        String groupIdStg = "100";
        GroupID groupID = new GroupID(groupIdStg);

        Person mockedPerson = Mockito.mock(Person.class);
        Mockito.when(personRepository.findById(personID)).thenReturn(Optional.of(mockedPerson));
        Mockito.when(groupRepository.findById(groupID)).thenReturn(Optional.empty());

        //Act
        //Assert
        Assertions.assertThrows(GroupNotFoundException.class, () -> {
            service.getTransactionsByGroupID(personIdStg, groupIdStg);
        });
    }

    /**
     * Unit test for method getTransactionsByGroupID
     * Ensure throws exception when member doesn't exist in group
     */
    @Test
    void getTransactionsByGroupIDEnsureThrowsExceptionWhenMemberDoesNotExistInGroupTest() {
        //Arrange
        String personIdStg = "10@switch.pt";
        PersonID personID = new PersonID(personIdStg);
        String groupIdStg = "100";
        GroupID groupID = new GroupID(groupIdStg);

        Person mockedPerson = Mockito.mock(Person.class);
        Group mockedGroup = Mockito.mock(Group.class);

        Mockito.when(personRepository.findById(personID)).thenReturn(Optional.of(mockedPerson));
        Mockito.when(groupRepository.findById(groupID)).thenReturn(Optional.of(mockedGroup));

        //Act
        //Assert
        Assertions.assertThrows(GroupConflictException.class, () -> {
            service.getTransactionsByGroupID(personIdStg, groupIdStg);
        });
    }

    /**
     * Unit test for method getTransactionsByGroupID
     * Ensure throws exception when ledger doesn't exist in ledger repo
     */
    @Test
    void getTransactionsByGroupIDEnsureThrowsExceptionWhenLedgerDoesNotExistInRepoTest() {
        //Arrange
        String personIdStg = "10@switch.pt";
        PersonID personID = new PersonID(personIdStg);
        String groupIdStg = "100";
        GroupID groupID = new GroupID(groupIdStg);
        LedgerID ledgerID = new LedgerID("1000");

        Group mockedGroup = Mockito.mock(Group.class);
        Person mockedPerson = Mockito.mock(Person.class);

        Mockito.when(personRepository.findById(personID)).thenReturn(Optional.of(mockedPerson));
        Mockito.when(groupRepository.findById(groupID)).thenReturn(Optional.of(mockedGroup));
        Mockito.when(mockedGroup.hasMemberID(personID)).thenReturn(true);
        Mockito.when(mockedGroup.getLedgerID()).thenReturn(ledgerID);
        Mockito.when(ledgerRepository.findById(ledgerID)).thenReturn(Optional.empty());

        //Act
        //Assert
        Assertions.assertThrows(LedgerNotFoundException.class, () -> {
            service.getTransactionsByGroupID(personIdStg, groupIdStg);
        });
    }


    /**
     * Unit test for method getTransactionOfAccountWithinPeriodByPerson - Happy Case
     */
    @Test
    @DisplayName("getTransactionOfAccountWithinPeriodByPerson - Happy Case Test")
    void getTransactionOfAccountWithinPeriodServiceByPersonIDHappyCaseTest() {

        //ARRANGE

        //Variables
        String personIdStg = "1001@switch.pt";
        PersonID personID = new PersonID(personIdStg);

        String ledgerGroupIdStg = "1001";
        LedgerID personLedgerID = new LedgerID(ledgerGroupIdStg);

        Type typeDebit = Type.DEBIT;
        Type typeCredit = Type.CREDIT;


        String debitAccountIDStg = "1001";
        AccountID debitAccountId = new AccountID(debitAccountIDStg);

        String creditAccountIDStg = "1003";
        AccountID creditAccountId = new AccountID(creditAccountIDStg);


        String initialDateTime = "1999-01-01 12:00:00";
        String finalDateTime = "2021-01-01 12:00:00";
        Period period = new Period(initialDateTime, finalDateTime);

        Category category = new Category("comida e bebida");


        //TransactionsDTO for expectedTransactionDTOList to build expectedResponseDTO
        TransactionDTO tdto1 = new TransactionDTO("500", "CREDIT", "2000-01-01 23:20:58", "Pacote de Oreos", "comida e bebida", "1002", "1004");
        TransactionDTO tdto2 = new TransactionDTO("400", "DEBIT", "2010-01-01 11:39:23", "Pacote de Belgas", "comida e bebida", "1002", "1004");
        TransactionDTO tdto3 = new TransactionDTO("100", "CREDIT", "2020-01-01 14:12:15", "Pacote de Maria", "comida e bebida", "1002", "1004");

        List<TransactionDTO> expectedTransactionDTOList = new ArrayList<>();
        expectedTransactionDTOList.add(tdto1);
        expectedTransactionDTOList.add(tdto2);
        expectedTransactionDTOList.add(tdto3);


        //TransactionsD for expectedTransactionSet
        Transaction t1 = new Transaction("500", typeCredit, "2000-01-01 23:20:58", "Pacote de Oreos", category, debitAccountId, creditAccountId);
        Transaction t2 = new Transaction("400", typeDebit, "2010-01-01 11:39:23", "Pacote de Belgas", category, debitAccountId, creditAccountId);
        Transaction t3 = new Transaction("100", typeCredit, "2020-01-01 14:12:15", "Pacote de Maria", category, debitAccountId, creditAccountId);

        Set<Transaction> expectedTransactionSet = new HashSet<>();
        expectedTransactionSet.add(t1);
        expectedTransactionSet.add(t2);
        expectedTransactionSet.add(t3);

        List<Transaction> expectedTransactionList = new ArrayList<>();
        expectedTransactionList.add(t1);
        expectedTransactionList.add(t2);
        expectedTransactionList.add(t3);


        //Mocks
        Person mockPerson = Mockito.mock(Person.class);
        Account mockAccount = Mockito.mock(Account.class);
        Ledger mockLedger = Mockito.mock(Ledger.class);


        //ACT
        Mockito.when(personRepository.findById(personID)).thenReturn(Optional.of(mockPerson));
        Mockito.when(mockPerson.hasAccountID(debitAccountId)).thenReturn(true);
        Mockito.when(accountRepository.findById(debitAccountId)).thenReturn(Optional.of(mockAccount));
        Mockito.when(mockPerson.getLedgerID()).thenReturn(personLedgerID);

        //Calls getTransactionsOfAccountWithinPeriodByLedgerID
        Mockito.when(ledgerRepository.findById(personLedgerID)).thenReturn(Optional.of(mockLedger));
        Mockito.when(mockLedger.getTransactionsOfAccountWithinPeriod(debitAccountId, period)).thenReturn(expectedTransactionSet);

        //Expected Response DTO
        TransactionsResponseDTO expectedResponseDTO = new TransactionsResponseDTO(expectedTransactionDTOList);

        //ACT
        TransactionsResponseDTO actualResponseDTO = service.getTransactionsOfAccountWithinPeriodByPersonID(personIdStg, debitAccountIDStg, initialDateTime, finalDateTime);

        //ASSERT
        assertEquals(expectedResponseDTO, actualResponseDTO);
    }

    /**
     * Unit test for method getTransactionOfAccountWithinPeriodByPerson - Ensure Throws Exception When Person Does Not Exist In Repo Test
     */
    @Test
    @DisplayName("getTransactionOfAccountWithinPeriodByPerson - Ensure Throws Exception When Person Does Not Exist In Repo")
    void getTransactionOfAccountWithinPeriodServiceByPersonIDEnsureThrowsExceptionWhenPersonDoesNotExistInRepoTest() {

        //ARRANGE

        //Variables
        String personIdStg = "1001@switch.pt";
        PersonID personID = new PersonID(personIdStg);

        String debitAccountIDStg = "1001";

        String initialDateTime = "1999-01-01 12:00:00";
        String finalDateTime = "2021-01-01 12:00:00";

        Mockito.when(personRepository.findById(personID)).thenReturn(Optional.empty());

        //Act
        //Assert
        Assertions.assertThrows(PersonNotFoundException.class, () -> {
            service.getTransactionsOfAccountWithinPeriodByPersonID(personIdStg, debitAccountIDStg, initialDateTime, finalDateTime);
        });
    }

    /**
     * Unit test for method getTransactionOfAccountWithinPeriodByPerson - Ensure Throws Exception When Account Does Not Exist In Person
     */
    @Test
    @DisplayName("getTransactionOfAccountWithinPeriodByPerson - Ensure Throws Exception When Account Does Not Exist In Person")
    void getTransactionOfAccountWithinPeriodServiceByPersonIDEnsureThrowsExceptionWhenAccountDoesNotExistInPersonTest() {

        //ARRANGE

        //Variables
        String personIdStg = "1001@switch.pt";
        PersonID personID = new PersonID(personIdStg);

        String debitAccountIDStg = "1001";
        AccountID debitAccountId = new AccountID(debitAccountIDStg);

        String initialDateTime = "1999-01-01 12:00:00";
        String finalDateTime = "2021-01-01 12:00:00";

        //Mocks
        Person mockPerson = Mockito.mock(Person.class);

        //ACT
        Mockito.when(personRepository.findById(personID)).thenReturn(Optional.of(mockPerson));
        Mockito.when(mockPerson.hasAccountID(debitAccountId)).thenReturn(false);


        //Act
        //Assert
        Assertions.assertThrows(AccountConflictException.class, () -> {
            service.getTransactionsOfAccountWithinPeriodByPersonID(personIdStg, debitAccountIDStg, initialDateTime, finalDateTime);
        });
    }

    /**
     * Unit test for method getTransactionOfAccountWithinPeriodByPerson - Ensure Throws Exception When Account Does Not Exist In Repo Test
     */
    @Test
    @DisplayName("getTransactionOfAccountWithinPeriodByPerson - Ensure Throws Exception When Account Does Not Exist In Repo Test")
    void getTransactionOfAccountWithinPeriodServiceByPersonIDEnsureThrowsExceptionWhenAccountDoesNotExistInRepoTest() {

        //ARRANGE

        //Variables
        String personIdStg = "1001@switch.pt";
        PersonID personID = new PersonID(personIdStg);

        String debitAccountIDStg = "1001";
        AccountID debitAccountId = new AccountID(debitAccountIDStg);

        String initialDateTime = "1999-01-01 12:00:00";
        String finalDateTime = "2021-01-01 12:00:00";

        //Mocks
        Person mockPerson = Mockito.mock(Person.class);

        //ACT
        Mockito.when(personRepository.findById(personID)).thenReturn(Optional.of(mockPerson));
        Mockito.when(mockPerson.hasAccountID(debitAccountId)).thenReturn(true);
        Mockito.when(accountRepository.findById(debitAccountId)).thenReturn(Optional.empty());

        //Act
        //Assert
        Assertions.assertThrows(AccountNotFoundException.class, () -> {
            service.getTransactionsOfAccountWithinPeriodByPersonID(personIdStg, debitAccountIDStg, initialDateTime, finalDateTime);
        });
    }

    /**
     * Unit test for method getTransactionOfAccountWithinPeriodByGroupID
     * Happy Case
     */
    @Test
    @DisplayName("getTransactionOfAccountWithinPeriodByGroupID - Happy Case Test")
    void getTransactionOfAccountWithinPeriodServiceByGroupIDHappyCaseTest() {

        //ARRANGE

        //Variables
        String personIdStg = "1001@switch.pt";
        PersonID personID = new PersonID(personIdStg);

        String groupIdStg = "1002";
        GroupID groupID = new GroupID(groupIdStg);


        String ledgerGroupIdStg = "1002";
        LedgerID groupLedgerID = new LedgerID(ledgerGroupIdStg);

        Type typeDebit = Type.DEBIT;
        Type typeCredit = Type.CREDIT;


        String debitAccountIDStg = "1002";
        AccountID debitAccountId = new AccountID(debitAccountIDStg);

        String creditAccountIDStg = "1004";
        AccountID creditAccountId = new AccountID(creditAccountIDStg);


        String initialDateTime = "1999-01-01 12:00:00";
        String finalDateTime = "2021-01-01 12:00:00";
        Period period = new Period(initialDateTime, finalDateTime);

        Category category = new Category("comida e bebida");


        //TransactionsDTO for expectedTransactionDTOList to build expectedResponseDTO
        TransactionDTO tdto1 = new TransactionDTO("500", "CREDIT", "2000-01-01 23:20:58", "Pacote de Oreos", "comida e bebida", "1002", "1004");
        TransactionDTO tdto2 = new TransactionDTO("400", "DEBIT", "2010-01-01 11:39:23", "Pacote de Belgas", "comida e bebida", "1002", "1004");
        TransactionDTO tdto3 = new TransactionDTO("100", "CREDIT", "2020-01-01 14:12:15", "Pacote de Maria", "comida e bebida", "1002", "1004");

        List<TransactionDTO> expectedTransactionDTOList = new ArrayList<>();
        expectedTransactionDTOList.add(tdto1);
        expectedTransactionDTOList.add(tdto2);
        expectedTransactionDTOList.add(tdto3);


        //TransactionsD for expectedTransactionSet
        Transaction t1 = new Transaction("500", typeCredit, "2000-01-01 23:20:58", "Pacote de Oreos", category, debitAccountId, creditAccountId);
        Transaction t2 = new Transaction("400", typeDebit, "2010-01-01 11:39:23", "Pacote de Belgas", category, debitAccountId, creditAccountId);
        Transaction t3 = new Transaction("100", typeCredit, "2020-01-01 14:12:15", "Pacote de Maria", category, debitAccountId, creditAccountId);

        Set<Transaction> expectedTransactionSet = new HashSet<>();
        expectedTransactionSet.add(t1);
        expectedTransactionSet.add(t2);
        expectedTransactionSet.add(t3);

        List<Transaction> expectedTransactionList = new ArrayList<>();
        expectedTransactionList.add(t1);
        expectedTransactionList.add(t2);
        expectedTransactionList.add(t3);


        //Mocks
        Person mockPerson = Mockito.mock(Person.class);
        Group mockGroup = Mockito.mock(Group.class);
        Account mockAccount = Mockito.mock(Account.class);
        Ledger mockLedger = Mockito.mock(Ledger.class);


        //ACT
        Mockito.when(personRepository.findById(personID)).thenReturn(Optional.of(mockPerson));
        Mockito.when(groupRepository.findById(groupID)).thenReturn(Optional.of(mockGroup));
        Mockito.when(mockGroup.hasMemberID(personID)).thenReturn(true);
        Mockito.when(mockGroup.hasAccountID(debitAccountId)).thenReturn(true);
        Mockito.when(accountRepository.findById(debitAccountId)).thenReturn(Optional.of(mockAccount));
        Mockito.when(mockGroup.getLedgerID()).thenReturn(groupLedgerID);

        //Calls getTransactionsOfAccountWithinPeriodByLedgerID
        Mockito.when(ledgerRepository.findById(groupLedgerID)).thenReturn(Optional.of(mockLedger));
        Mockito.when(mockLedger.getTransactionsOfAccountWithinPeriod(debitAccountId, period)).thenReturn(expectedTransactionSet);

        //Expected Response DTO
        TransactionsResponseDTO expectedResponseDTO = new TransactionsResponseDTO(expectedTransactionDTOList);

        //ACT
        TransactionsResponseDTO actualResponseDTO = service.getTransactionsOfAccountWithinPeriodByGroupID(personIdStg, groupIdStg, debitAccountIDStg, initialDateTime, finalDateTime);

        //ASSERT
        assertEquals(expectedResponseDTO, actualResponseDTO);
    }

    /**
     * Unit test for method getTransactionOfAccountWithinPeriodByGroupID - Ensure Throws Exception When Member Does Not Exist In Repo
     */
    @Test
    @DisplayName("getTransactionOfAccountWithinPeriodByGroupID - EnsureThrowsExceptionWhenMemberDoesNotExistInRepo")
    void getTransactionOfAccountWithinPeriodServiceByGroupIDEnsureThrowsExceptionWhenMemberDoesNotExistInRepoTest() {

        //ARRANGE
        String personIdStg = "1005@switch.pt";
        PersonID personID = new PersonID(personIdStg);
        String groupIdStg = "1002";
        String debitAccountIDStg = "1002";
        String initialDateTime = "1999-01-01 12:00:00";
        String finalDateTime = "2021-01-01 12:00:00";

        Mockito.when(personRepository.findById(personID)).thenReturn(Optional.empty());

        //Act
        //Assert
        Assertions.assertThrows(PersonNotFoundException.class, () -> {
            service.getTransactionsOfAccountWithinPeriodByGroupID(personIdStg, groupIdStg, debitAccountIDStg, initialDateTime, finalDateTime);
        });
    }

    /**
     * Unit test for method getTransactionOfAccountWithinPeriodByGroupID - Ensure Throws Exception When Group Does Not Exist In Repo
     */
    @Test
    @DisplayName("getTransactionOfAccountWithinPeriodByGroupID - EnsureThrowsExceptionWhenGroupDoesNotExistInRepo")
    void getTransactionOfAccountWithinPeriodServiceByGroupIDEnsureThrowsExceptionWhenGroupDoesNotExistInRepoTest() {

        //ARRANGE
        //Variables
        String personIdStg = "1005@switch.pt";
        PersonID personID = new PersonID(personIdStg);
        String groupIdStg = "1002";
        GroupID groupID = new GroupID(groupIdStg);
        String debitAccountIDStg = "1002";
        String initialDateTime = "1999-01-01 12:00:00";
        String finalDateTime = "2021-01-01 12:00:00";

        //Mocks
        Person mockPerson = Mockito.mock(Person.class);
        Group mockGroup = Mockito.mock(Group.class);

        //ACT
        Mockito.when(personRepository.findById(personID)).thenReturn(Optional.of(mockPerson));
        Mockito.when(groupRepository.findById(groupID)).thenReturn(Optional.empty());

        //ASSERT
        Assertions.assertThrows(GroupNotFoundException.class, () -> {
            service.getTransactionsOfAccountWithinPeriodByGroupID(personIdStg, groupIdStg, debitAccountIDStg, initialDateTime, finalDateTime);
        });
    }

    /**
     * Unit test for method getTransactionOfAccountWithinPeriodByGroupID - Ensure Throws Exception When Group Does Not Exist In Group
     */
    @Test
    @DisplayName("getTransactionOfAccountWithinPeriodByGroupID - EnsureThrowsExceptionWhenGroupDoesNotExistInGroup")
    void getTransactionOfAccountWithinPeriodServiceByGroupIDEnsureThrowsExceptionWhenMemberDoesNotExistInGroupTest() {

        //ARRANGE

        //Variables
        String personIdStg = "1001@switch.pt";
        PersonID personID = new PersonID(personIdStg);

        String groupIdStg = "1002";
        GroupID groupID = new GroupID(groupIdStg);

        String debitAccountIDStg = "1002";

        String initialDateTime = "1999-01-01 12:00:00";
        String finalDateTime = "2021-01-01 12:00:00";

        //Mocks
        Person mockPerson = Mockito.mock(Person.class);
        Group mockGroup = Mockito.mock(Group.class);

        //ACT
        Mockito.when(personRepository.findById(personID)).thenReturn(Optional.of(mockPerson));
        Mockito.when(groupRepository.findById(groupID)).thenReturn(Optional.of(mockGroup));
        Mockito.when(mockGroup.hasMemberID(personID)).thenReturn(false);

        //ASSERT
        Assertions.assertThrows(GroupConflictException.class, () -> {
            service.getTransactionsOfAccountWithinPeriodByGroupID(personIdStg, groupIdStg, debitAccountIDStg, initialDateTime, finalDateTime);
        });
    }

    /**
     * Unit test for method getTransactionOfAccountWithinPeriodByGroupID - Ensure Throws Exception When Account Does Not Exist In Group
     */
    @Test
    @DisplayName("getTransactionOfAccountWithinPeriodByGroupID - EnsureThrowsExceptionWhenAccountDoesNotExistInGroup")
    void getTransactionOfAccountWithinPeriodServiceByGroupIDEnsureThrowsExceptionWhenAccountDoesNotExistInGroupTest() {

        //ARRANGE

        //Variables
        String personIdStg = "1001@switch.pt";
        PersonID personID = new PersonID(personIdStg);

        String groupIdStg = "1002";
        GroupID groupID = new GroupID(groupIdStg);

        String debitAccountIDStg = "1002";
        AccountID debitAccountId = new AccountID(debitAccountIDStg);

        String initialDateTime = "1999-01-01 12:00:00";
        String finalDateTime = "2021-01-01 12:00:00";

        //Mocks
        Person mockPerson = Mockito.mock(Person.class);
        Group mockGroup = Mockito.mock(Group.class);

        //ACT
        Mockito.when(personRepository.findById(personID)).thenReturn(Optional.of(mockPerson));
        Mockito.when(groupRepository.findById(groupID)).thenReturn(Optional.of(mockGroup));
        Mockito.when(mockGroup.hasMemberID(personID)).thenReturn(true);
        Mockito.when(mockGroup.hasAccountID(debitAccountId)).thenReturn(false);

        //ASSERT
        Assertions.assertThrows(AccountConflictException.class, () -> {
            service.getTransactionsOfAccountWithinPeriodByGroupID(personIdStg, groupIdStg, debitAccountIDStg, initialDateTime, finalDateTime);
        });
    }

    /**
     * Unit test for method getTransactionOfAccountWithinPeriodByGroupID - Ensure Throws Exception When Account Does Not Exist In Repo
     */
    @Test
    @DisplayName("getTransactionOfAccountWithinPeriodByGroupID - EnsureThrowsExceptionWhenAccountDoesNotExistInRepo")
    void getTransactionOfAccountWithinPeriodServiceByGroupIDEnsureThrowsExceptionWhenAccountDoesNotExistInRepoTest() {

        //ARRANGE

        //Variables
        String personIdStg = "1001@switch.pt";
        PersonID personID = new PersonID(personIdStg);

        String groupIdStg = "1002";
        GroupID groupID = new GroupID(groupIdStg);

        String ledgerGroupIdStg = "1002";

        String debitAccountIDStg = "1002";
        AccountID debitAccountId = new AccountID(debitAccountIDStg);

        String initialDateTime = "1999-01-01 12:00:00";
        String finalDateTime = "2021-01-01 12:00:00";

        //Mocks
        Person mockPerson = Mockito.mock(Person.class);
        Group mockGroup = Mockito.mock(Group.class);
        Account mockAccount = Mockito.mock(Account.class);

        //ACT
        Mockito.when(personRepository.findById(personID)).thenReturn(Optional.of(mockPerson));
        Mockito.when(groupRepository.findById(groupID)).thenReturn(Optional.of(mockGroup));
        Mockito.when(mockGroup.hasMemberID(personID)).thenReturn(true);
        Mockito.when(mockGroup.hasAccountID(debitAccountId)).thenReturn(true);
        Mockito.when(accountRepository.findById(debitAccountId)).thenReturn(Optional.empty());

        //ASSERT
        Assertions.assertThrows(AccountNotFoundException.class, () -> {
            service.getTransactionsOfAccountWithinPeriodByGroupID(personIdStg, groupIdStg, debitAccountIDStg, initialDateTime, finalDateTime);
        });
    }

    /**
     * Unit test for method getTransactionsOfAccountWithinPeriodByLedgerID
     * Happy Case
     */
    @Test
    @DisplayName("getTransactionsOfAccountWithinPeriodByLedgerID - Happy Case Test")
    void getTransactionsOfAccountWithinPeriodByLedgerIDHappyCaseTest() {

        //ARRANGE

        //Variables
        String personIdStg = "1001@switch.pt";
        PersonID personID = new PersonID(personIdStg);

        String groupIdStg = "1002";
        GroupID groupID = new GroupID(groupIdStg);


        String ledgerGroupIdStg = "1002";
        LedgerID groupLedgerID = new LedgerID(ledgerGroupIdStg);

        Type typeDebit = Type.DEBIT;
        Type typeCredit = Type.CREDIT;


        String debitAccountIDStg = "1002";
        AccountID debitAccountId = new AccountID(debitAccountIDStg);

        String creditAccountIDStg = "1004";
        AccountID creditAccountId = new AccountID(creditAccountIDStg);


        String initialDateTime = "1999-01-01 12:00:00";
        String finalDateTime = "2021-01-01 12:00:00";
        Period period = new Period(initialDateTime, finalDateTime);

        Category category = new Category("comida e bebida");


        //TransactionsDTO for expectedTransactionDTOList to build expectedResponseDTO
        TransactionDTO tdto1 = new TransactionDTO("500", "CREDIT", "2000-01-01 23:20:58", "Pacote de Oreos", "comida e bebida", "1002", "1004");
        TransactionDTO tdto2 = new TransactionDTO("400", "DEBIT", "2010-01-01 11:39:23", "Pacote de Belgas", "comida e bebida", "1002", "1004");
        TransactionDTO tdto3 = new TransactionDTO("100", "CREDIT", "2020-01-01 14:12:15", "Pacote de Maria", "comida e bebida", "1002", "1004");

        List<TransactionDTO> expectedTransactionDTOList = new ArrayList<>();
        expectedTransactionDTOList.add(tdto1);
        expectedTransactionDTOList.add(tdto2);
        expectedTransactionDTOList.add(tdto3);


        //TransactionsD for expectedTransactionSet
        Transaction t1 = new Transaction("500", typeCredit, "2000-01-01 23:20:58", "Pacote de Oreos", category, debitAccountId, creditAccountId);
        Transaction t2 = new Transaction("400", typeDebit, "2010-01-01 11:39:23", "Pacote de Belgas", category, debitAccountId, creditAccountId);
        Transaction t3 = new Transaction("100", typeCredit, "2020-01-01 14:12:15", "Pacote de Maria", category, debitAccountId, creditAccountId);

        Set<Transaction> expectedTransactionSet = new HashSet<>();
        expectedTransactionSet.add(t1);
        expectedTransactionSet.add(t2);
        expectedTransactionSet.add(t3);

        List<Transaction> expectedTransactionList = new ArrayList<>();
        expectedTransactionList.add(t1);
        expectedTransactionList.add(t2);
        expectedTransactionList.add(t3);


        //Mocks
        Person mockPerson = Mockito.mock(Person.class);
        Group mockGroup = Mockito.mock(Group.class);
        Account mockAccount = Mockito.mock(Account.class);
        Ledger mockLedger = Mockito.mock(Ledger.class);


        //ACT
        Mockito.when(personRepository.findById(personID)).thenReturn(Optional.of(mockPerson));
        Mockito.when(groupRepository.findById(groupID)).thenReturn(Optional.of(mockGroup));
        Mockito.when(mockGroup.hasMemberID(personID)).thenReturn(true);
        Mockito.when(mockGroup.hasAccountID(debitAccountId)).thenReturn(true);
        Mockito.when(accountRepository.findById(debitAccountId)).thenReturn(Optional.of(mockAccount));
        Mockito.when(mockGroup.getLedgerID()).thenReturn(groupLedgerID);

        //Calls getTransactionsOfAccountWithinPeriodByLedgerID
        Mockito.when(ledgerRepository.findById(groupLedgerID)).thenReturn(Optional.of(mockLedger));
        Mockito.when(mockLedger.getTransactionsOfAccountWithinPeriod(debitAccountId, period)).thenReturn(expectedTransactionSet);

        //Expected Response DTO
        TransactionsResponseDTO expectedResponseDTO = new TransactionsResponseDTO(expectedTransactionDTOList);

        //ACT
        TransactionsResponseDTO actualResponseDTO = service.getTransactionsOfAccountWithinPeriodByLedgerID(groupLedgerID, debitAccountId, period);

        //ASSERT
        assertEquals(expectedResponseDTO, actualResponseDTO);
    }

    /**
     * Unit test for method getTransactionsOfAccountWithinPeriodByLedgerID - Ensure Throws Exception When LedgerID Does Not Exist In Repo
     */
    @Test
    @DisplayName("getTransactionsOfAccountWithinPeriodByLedgerID - Ensure Throws Exception When LedgerID Does Not Exist In Repo")
    void getTransactionsOfAccountWithinPeriodByLedgerIDEnsureThrowsExceptionWhenLedgerDoesNotExistInRepoTest() {

        //ARRANGE

        //Variables
        String personIdStg = "1001@switch.pt";

        String groupIdStg = "1002";

        String ledgerGroupIdStg = "1002";
        LedgerID groupLedgerID = new LedgerID(ledgerGroupIdStg);

        String debitAccountIDStg = "1002";
        AccountID debitAccountId = new AccountID(debitAccountIDStg);

        String initialDateTime = "1999-01-01 12:00:00";
        String finalDateTime = "2021-01-01 12:00:00";
        Period period = new Period(initialDateTime, finalDateTime);

        //Mocks
        Ledger mockLedger = Mockito.mock(Ledger.class);

        //Calls getTransactionsOfAccountWithinPeriodByLedgerID
        Mockito.when(ledgerRepository.findById(groupLedgerID)).thenReturn(Optional.empty());

        //ASSERT
        Assertions.assertThrows(LedgerNotFoundException.class, () -> {
            service.getTransactionsOfAccountWithinPeriodByLedgerID(groupLedgerID, debitAccountId, period);
        });
    }

}