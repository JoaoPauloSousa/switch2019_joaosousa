package project.application.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import project.ProjectApplication;
import project.dto.CreateCategoryForPersonRequestDTO;
import project.dto.CreateCategoryForPersonResponseDTO;
import project.exceptions.CategoryAlreadyExistsException;
import project.exceptions.InvalidFieldException;
import project.exceptions.PersonNotFoundException;
import project.frameworkddd.IUSCreateCategoryForPersonService;
import project.model.entities.ledger.Ledger;
import project.model.entities.person.Person;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.PersonID;
import project.model.specifications.repositories.LedgerRepository;
import project.model.specifications.repositories.PersonRepository;

import static org.junit.jupiter.api.Assertions.*;

@Transactional
@SpringBootTest(classes = ProjectApplication.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CreateCategoryForPersonServiceIT {

    @Autowired
    PersonRepository personRepository;

    @Autowired
    LedgerRepository ledgerRepository;

    @Autowired
    IUSCreateCategoryForPersonService service;

    private PersonID motherPersonID;
    private PersonID fatherPersonID;
    private LedgerID ledgerID;
    private PersonID personID;
    private Person manuel;

    @BeforeEach
    public void init() {
        String prefix = "5000";
        personID = new PersonID(prefix + "1@switch.pt");
        motherPersonID = new PersonID(prefix + "2@switch.pt");
        fatherPersonID = new PersonID(prefix + "3@switch.pt");
        ledgerID = new LedgerID(prefix + "1");
        Ledger manuelLedger = new Ledger(ledgerID);
        manuel = new Person(personID, "Manuel", "Rua Santa Catarina 88", "Vila Real", "2020-01-20", motherPersonID, fatherPersonID, ledgerID);
        manuel.addCategory("food");
        manuel.addCategory("cenas");
        ledgerRepository.save(manuelLedger);
        personRepository.save(manuel);
    }

    /**
     * Test for Constructor CreateCategoryForPerson Service
     * Happy case
     */
    @Test
    @DisplayName("Test for Constructor CreateCategoryForPerson - Happy Case")
    void createCategoryPersonControllerConstructorTest() {
        assertTrue(service instanceof IUSCreateCategoryForPersonService);
    }

    /**
     * Test for CreateCategoryForPerson - HappyCase
     * Happy case
     */
    @Test
    @DisplayName("Test for CreateCategoryForPerson - Happy Case")
    void createCategoryForPersonHappyCaseTest() {

        //Arrange
        CreateCategoryForPersonRequestDTO requestDTO = new CreateCategoryForPersonRequestDTO(personID.toStringDTO(), "things");

        CreateCategoryForPersonResponseDTO expected = new CreateCategoryForPersonResponseDTO(personID.toStringDTO(), "things");

        //Act
        CreateCategoryForPersonResponseDTO result = service.createCategoryForPerson(requestDTO);

        //Assert
        assertEquals(expected, result);
    }

    /**
     * Test for CreateCategoryForPerson Service
     * Ensure throws a CategoryAlreadyExistsException when added a Category that already exists
     */
    @Test
    @DisplayName("Ensure throws a CategoryAlreadyExistsException when added a Category that already exists")
    void createCategoryForPersonEnsureExceptionWhenCategoryAlreadyExistsTest() {

        //Arrange
        CreateCategoryForPersonRequestDTO requestDTO = new CreateCategoryForPersonRequestDTO(personID.toStringDTO(), "food");
        //ACT-ASSERT
        assertThrows(CategoryAlreadyExistsException.class, () -> {
            service.createCategoryForPerson(requestDTO);
        });
    }

    /**
     * Test for CreateCategoryForPerson Service
     * Ensure PersonNotFoundException is thrown when addCategory to a personID that is not in personRepository
     */
    @Test
    @DisplayName("Ensure PersonNotFoundException is thrown when addCategory to a personID that is not in personRepository")
    void createCategoryForPersonEnsureExceptionIsThrownWhenPersonIDDontExistInTheRepositoryTest() {

        //Arrange
        CreateCategoryForPersonRequestDTO requestDTO = new CreateCategoryForPersonRequestDTO("50002@switch.pt", "things");

        //ACT-ASSERT
        assertThrows(PersonNotFoundException.class, () -> {
            service.createCategoryForPerson(requestDTO);
        });
    }

    /**
     * Test for CreateCategoryForPerson Service
     * Ensure InvalidFieldException when the designation input is invalid
     */
    @Test
    @DisplayName("Ensure InvalidFieldException when the designation input is invalid ")
    void createCategoryForPersonEnsureExceptionDesignationInvalidInputTest() {

        //Arrange
        CreateCategoryForPersonRequestDTO requestDTO = new CreateCategoryForPersonRequestDTO(personID.toStringDTO(), "");

        //ACT-ASSERT
        assertThrows(InvalidFieldException.class, () -> {
            service.createCategoryForPerson(requestDTO);
        });
    }

}
