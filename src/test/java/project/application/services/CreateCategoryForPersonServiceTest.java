package project.application.services;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.CreateCategoryForPersonRequestDTO;
import project.dto.CreateCategoryForPersonResponseDTO;
import project.exceptions.CategoryAlreadyExistsException;
import project.exceptions.PersonNotFoundException;
import project.model.entities.person.Person;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.PersonID;
import project.model.specifications.repositories.PersonRepository;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("serviceTest")
@ExtendWith(SpringExtension.class)
public class CreateCategoryForPersonServiceTest {

    @Autowired
    PersonRepository personRepository;

    @Autowired
    CreateCategoryForPersonService service;

    private PersonID personID;
    private PersonID motherPersonID;
    private PersonID fatherPersonID;
    private LedgerID ledgerID;
    private Person manuel;

    @BeforeEach
    public void init() {
        String prefix = "5000";
        personID = new PersonID(prefix + "1@switch.pt");
        motherPersonID = new PersonID(prefix + "2@switch.pt");
        fatherPersonID = new PersonID(prefix + "3@switch.pt");
        ledgerID = new LedgerID(prefix + "1");
        manuel = new Person(personID, "Manuel", "Rua Santa Catarina 88", "Vila Real", "2020-01-20", motherPersonID, fatherPersonID, ledgerID);
        manuel.addCategory("food");
        personRepository.save(manuel);
    }

    /**
     * Test for createCategoryForPerson Unit Test
     * Happy case
     */
    @Test
    @DisplayName("Test for create category for person - Happy Case")
    public void createCategoryForPersonHappyCaseTest() {

        //ARRANGE
        String designation = "videogames";
        String personIDStr = personID.toStringDTO();

        CreateCategoryForPersonRequestDTO requestDTO = new CreateCategoryForPersonRequestDTO(personIDStr, designation);
        CreateCategoryForPersonResponseDTO expected = new CreateCategoryForPersonResponseDTO(personIDStr, designation);
        Person person = new Person(personID, "Manuel", "Rua Santa Catarina 88", "Vila Real", "2020-01-20", motherPersonID, fatherPersonID, ledgerID);

        Person mockedPerson = Mockito.mock(Person.class);
        CreateCategoryForPersonRequestDTO mockedRequestDTO = Mockito.mock(CreateCategoryForPersonRequestDTO.class);


        Mockito.when(personRepository.findById(personID)).thenReturn(Optional.of(mockedPerson));
        Mockito.when(mockedRequestDTO.getDesignation()).thenReturn(designation);
        Mockito.when(mockedPerson.addCategory(designation)).thenReturn(true);
        Mockito.when(personRepository.save(mockedPerson)).thenReturn(person);

        //ACT
        CreateCategoryForPersonResponseDTO actual = service.createCategoryForPerson(requestDTO);

        //ASSERT
        assertEquals(expected, actual);
    }

    /**
     * Test for createCategoryForPerson Unit Test
     * Ensure exception is thrown when added a Category that already exists
     */
    @Test
    @DisplayName("Ensure exception is thrown when added a Category that already exists")
    public void createCategoryForPersonEnsureExceptionWhenCategoryAlreadyExistsTest() {

        //ARRANGE
        String designation = "food";

        CreateCategoryForPersonRequestDTO requestDTO = new CreateCategoryForPersonRequestDTO(personID.toStringDTO(), "food");

        Person mockedPerson = Mockito.mock(Person.class);
        CreateCategoryForPersonRequestDTO mockedRequestDTO = Mockito.mock(CreateCategoryForPersonRequestDTO.class);

        Mockito.when(personRepository.findById(personID)).thenReturn(Optional.of(mockedPerson));

        Mockito.when(mockedRequestDTO.getDesignation()).thenReturn(designation);

        Mockito.when(mockedPerson.addCategory(designation)).thenReturn(false);

        //ACT
        //ASSERT
        Assertions.assertThrows(CategoryAlreadyExistsException.class, () -> {
            service.createCategoryForPerson(requestDTO);
        });
    }

    /**
     * Test for createCategoryForPerson Unit Test
     * Ensure exception is thrown when person doesn't exist
     */
    @Test
    @DisplayName("Ensure exception is thrown when person doesn't exist")
    public void createCategoryForPersonPersonNotPresentTest() {

        //ARRANGE
        CreateCategoryForPersonRequestDTO requestDTO = new CreateCategoryForPersonRequestDTO(personID.toStringDTO(), "food");

        Mockito.when(personRepository.findById(personID)).thenReturn(Optional.empty());

        //ACT
        //ASSERT
        Assertions.assertThrows(PersonNotFoundException.class, () -> {
            service.createCategoryForPerson(requestDTO);
        });
    }

}

