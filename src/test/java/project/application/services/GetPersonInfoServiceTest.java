package project.application.services;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.GetPersonInfoResponseDTO;
import project.frameworkddd.IUSAddMemberToGroupService;
import project.frameworkddd.IUSGetPersonInfoService;
import project.model.entities.person.Person;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.PersonID;
import project.model.specifications.repositories.PersonRepository;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("serviceTest")
@ExtendWith(SpringExtension.class)
public class GetPersonInfoServiceTest {

    @Autowired
    IUSGetPersonInfoService service;

    @Autowired
    IUSAddMemberToGroupService ius003Service;

    @Autowired
    PersonRepository personRepository;

    @Autowired
    AddMemberToGroupService otherService;

    @Test
    @DisplayName("Test for GetPersonInfo - Happy Case")
    void getPersonInfoHappyCaseTest() {
        //Arrange
        String email = "21001@switch.pt";
        String name = "João";
        String address = "Travessa Santa Bárbara";
        String birthDate = "1987-04-17";
        String birthplace = "Estarreja";

        GetPersonInfoResponseDTO expectedResponseDto = new GetPersonInfoResponseDTO(email, name,
                address, birthDate, birthplace);

        PersonID personID = new PersonID(email);
        Person person = new Person(personID, name, address, birthplace, birthDate, null, null,
                new LedgerID("1"));

        Mockito.when(personRepository.findById(personID)).thenReturn(Optional.of(person));

        //Act
        GetPersonInfoResponseDTO result = service.getPersonInfo(email);

        //Assert
        assertEquals(expectedResponseDto, result);

    }
}