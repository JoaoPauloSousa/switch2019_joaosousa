package project.application.services;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import project.infrastructure.repositories.AccountRepositoryDB;
import project.infrastructure.repositories.GroupRepositoryDB;
import project.infrastructure.repositories.LedgerRepositoryDB;
import project.infrastructure.repositories.PersonRepositoryDB;
import project.model.entities.person.Person;

@Profile("serviceTest")
@Configuration
public class TestConfiguration {
    @Bean
    @Primary
    public PersonRepositoryDB personRepositoryMock() {
        return Mockito.mock(PersonRepositoryDB.class);
    }

    @Bean
    @Primary
    public GroupRepositoryDB groupRepositoryMock() {
        return Mockito.mock(GroupRepositoryDB.class);
    }

    @Bean
    @Primary
    public LedgerRepositoryDB ledgerRepositoryMock() {
        return Mockito.mock(LedgerRepositoryDB.class);
    }

    @Bean
    @Primary
    public Person personMock() {
        return Mockito.mock(Person.class);
    }

//    @Bean
//    @Primary
//    public CreateAccountForGroupAssembler CreateAccountForGroupAssemblerMock() {
//        return Mockito.mock(CreateAccountForGroupAssembler.class);
//    }


//    @Bean
//    @Primary
//    public CreateAccountForPersonAssembler CreateAccountForPersonAssemblerMock() {
//        return Mockito.mock(CreateAccountForPersonAssembler.class);
//    }

    @Bean
    @Primary
    public AccountRepositoryDB accountRepositoryMock() {
        return Mockito.mock(AccountRepositoryDB.class);
    }

}
