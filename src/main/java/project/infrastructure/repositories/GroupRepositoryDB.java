package project.infrastructure.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import project.datamodel.GroupDomainDataAssembler;
import project.datamodel.GroupJpa;
import project.infrastructure.repositories.jpa.GroupJpaRepository;
import project.model.entities.group.Group;
import project.model.entities.group.GroupID;
import project.model.specifications.repositories.GroupRepository;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Component
public class GroupRepositoryDB implements GroupRepository {

    @Autowired
    GroupJpaRepository groupJpaRepository;

    /**
     * Constructor of Class GroupRepository
     */
    public GroupRepositoryDB() {
        //This constructor is intentional empty
    }

    /**
     * Method findById for group
     *
     * @param groupID GroupID
     * @return Optional<Group> containing group in repository
     */
    public Optional<Group> findById(GroupID groupID) {

        Optional<Group> result = Optional.empty();

        if (groupID != null) {

            Optional<GroupJpa> opGroupJpa = groupJpaRepository.findById(groupID);

            if (opGroupJpa.isPresent()) {
                GroupJpa groupJpa = opGroupJpa.get();

                Group group = GroupDomainDataAssembler.toDomain(groupJpa);

                result = Optional.of(group);
            }
        }
        return result;
    }

    /**
     * Method to save group
     *
     * @param group Group
     * @return Group that was saved to repository
     */
    public Group save(Group group) {
        GroupJpa groupJpa = GroupDomainDataAssembler.toData(group);
        GroupJpa savedGroupJpa = groupJpaRepository.save(groupJpa);
        return GroupDomainDataAssembler.toDomain(savedGroupJpa);
    }

    /**
     * Method to find all groups for GetFamilyGroupsService
     *
     * @return
     */
    public Set<Group> findAll() {
        Iterable<GroupJpa> groups = this.groupJpaRepository.findAll();
        Set<Group> group = new HashSet<>();
        for (GroupJpa groupJpa : groups) {
            group.add(GroupDomainDataAssembler.toDomain(groupJpa));
        }
        return group;
    }
}