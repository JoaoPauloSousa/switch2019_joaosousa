package project.infrastructure.repositories.jpa;

import org.springframework.data.repository.CrudRepository;
import project.datamodel.GroupJpa;
import project.model.entities.group.GroupID;

public interface GroupJpaRepository extends CrudRepository<GroupJpa, GroupID> {
}