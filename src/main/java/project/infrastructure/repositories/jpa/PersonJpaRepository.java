package project.infrastructure.repositories.jpa;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import project.datamodel.PersonJpa;
import project.model.entities.shared.PersonID;

@Component
public interface PersonJpaRepository extends CrudRepository<PersonJpa, PersonID> {
}