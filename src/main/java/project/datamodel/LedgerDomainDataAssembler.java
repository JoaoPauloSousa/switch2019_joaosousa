package project.datamodel;

import org.springframework.stereotype.Service;
import project.model.entities.Transactions;
import project.model.entities.ledger.Ledger;
import project.model.entities.ledger.Transaction;

import java.util.ArrayList;
import java.util.List;

@Service
public class LedgerDomainDataAssembler {

    /**
     * Constructor
     */
    private LedgerDomainDataAssembler() {
        //Constructor is intentionally empty
    }

    /**
     * Method to convert Ledger(Model) to LedgerJpa(Data Model)
     *
     * @param ledger Model Ledger
     * @return Data Model LedgerJpa
     */
    public static LedgerJpa toData(Ledger ledger) {

        LedgerJpa ledgerJpa = new LedgerJpa(ledger.getID());

        List<TransactionJpa> transactionsJpa = new ArrayList<>();

        List<Transaction> ledgerTransactions = ledger.getTransactions().getAll();

        if (!ledgerTransactions.isEmpty()) {
            Transaction lastTransaction = ledgerTransactions.get(ledgerTransactions.size() - 1);
            TransactionJpa lastTransactionJPA = TransactionDomainDataAssembler.toData(ledgerJpa, lastTransaction);
            transactionsJpa.add(lastTransactionJPA);
        }

        ledgerJpa.setTransactions(transactionsJpa);

        return ledgerJpa;
    }

    /**
     * Method to convert LedgerJpa(Data Model) to Ledger(Model)
     *
     * @param ledgerJpa Data Model LedgerJpa
     * @return Model Ledger
     */
    public static Ledger toDomain(LedgerJpa ledgerJpa) {

        Ledger ledger = new Ledger(ledgerJpa.getId());

        Transactions transactions = new Transactions();
        for (TransactionJpa transactionJpa : ledgerJpa.getTransactions()) {
            transactions.addTransaction(TransactionDomainDataAssembler.toDomain(transactionJpa));
        }

        ledger.setTransactions(transactions);

        return ledger;
    }
}
