package project.datamodel;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import project.model.entities.shared.LedgerID;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "Ledgers")
public class LedgerJpa {

    @EmbeddedId
    private LedgerID id;

    @OneToMany(mappedBy = "ledgerJpa", cascade = CascadeType.ALL)
    private List<TransactionJpa> transactions;

    public LedgerJpa(String id) {
        this.id = new LedgerID(id);
        this.transactions = new ArrayList<>();
    }

    public LedgerJpa(LedgerID ledgerID) {
        this.id = ledgerID;
        this.transactions = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "LedgerJpa{" +
                "id=" + id +
                ", transactions=" + transactions +
                '}';
    }
}
