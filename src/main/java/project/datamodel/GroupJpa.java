package project.datamodel;

import lombok.Data;
import lombok.NoArgsConstructor;
import project.model.entities.CreationDate;
import project.model.entities.group.GroupID;
import project.model.entities.shared.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor

@Entity
@Table(name = "groups")
public class GroupJpa {

    @EmbeddedId
    private GroupID id;

    private Description description;
    private CreationDate creationDate;
    private LedgerID ledgerID;

    @ElementCollection
    @CollectionTable(name = "groupAccounts", joinColumns = @JoinColumn(name = "group_id"))
    private List<AccountID> accountsIds;

    @ElementCollection
    @CollectionTable(name = "groupCategories", joinColumns = @JoinColumn(name = "group_id"))
    private List<Category> categories;

    @ElementCollection
    @CollectionTable(name = "members", joinColumns = @JoinColumn(name = "group_id"))
    private List<PersonID> members;

    @ElementCollection
    @CollectionTable(name = "managers", joinColumns = @JoinColumn(name = "group_id"))
    private List<PersonID> managers;


    /**
     * Constructor with Strings as parameters
     *
     * @param id           GroupID
     * @param description  Description
     * @param creationDate CreationDate
     * @param creatorID    CreatorID
     * @param ledgerID     Ledger Id
     */
    public GroupJpa(String id, String description, String creationDate, String creatorID, String ledgerID) {
        this.id = new GroupID(id);

        this.description = new Description(description);
        this.creationDate = new CreationDate(creationDate);
        PersonID personID = new PersonID(creatorID);

        this.accountsIds = new ArrayList<>();
        this.categories = new ArrayList<>();
        this.members = new ArrayList<>();
        this.managers = new ArrayList<>();

        this.members.add(personID);
        this.managers.add(personID);
        this.ledgerID = new LedgerID(ledgerID);
    }

    /**
     * Constructor with Vos as parameters
     *
     * @param id           GroupID
     * @param description  Description
     * @param creationDate CreationDate
     * @param creatorID    CreatorID
     * @param ledgerID     Ledger Id
     */
    public GroupJpa(GroupID id, Description description, CreationDate creationDate, PersonID creatorID, LedgerID ledgerID) {
        this.id = id;

        this.description = description;
        this.creationDate = creationDate;

        this.accountsIds = new ArrayList<>();
        this.categories = new ArrayList<>();
        this.members = new ArrayList<>();
        this.managers = new ArrayList<>();

        this.members.add(creatorID);
        this.managers.add(creatorID);
        this.ledgerID = ledgerID;
    }
}
