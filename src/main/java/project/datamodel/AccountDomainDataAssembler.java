package project.datamodel;

import org.springframework.stereotype.Service;
import project.model.entities.account.Account;

@Service
public class AccountDomainDataAssembler {

    /**
     * Constructor
     */
    private AccountDomainDataAssembler() {
        //Constructor is intentionally empty
    }

    /**
     * toData method
     * Converts an instance of Account (Domain object) into an instance of AccountJpa (Data Object)
     *
     * @param account instance of the domain class Account to be converted
     * @return an instance of the data model class AccountJpa
     */
    public static AccountJpa toData(Account account) {

        return new AccountJpa(account.getID().toStringDTO(),
                account.getDenomination().getDenominationValue(),
                account.getDescription().getDescriptionValue());
    }

    /**
     * toDomain method
     * Converts an instance of AccountJpa (Data object) into an instance of Account (Domain Object)
     *
     * @param accountJpa instance of the data model class AccountJpa to be converted
     * @return an instance of the domain class Account
     */
    public static Account toDomain(AccountJpa accountJpa) {

        return new Account(accountJpa.getId(),
                accountJpa.getDenomination(),
                accountJpa.getDescription());
    }
}
