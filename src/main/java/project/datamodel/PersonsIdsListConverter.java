package project.datamodel;

import project.model.entities.shared.PersonID;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Converter
public class PersonsIdsListConverter implements AttributeConverter<List<PersonID>, String> {


    /**
     * Method convertToDatabaseColumn
     * Convert a list of PersonsISs in a String to Be Represented in a column in the database
     * @param personIDS
     * @return
     */
    @Override
    public String convertToDatabaseColumn(List<PersonID> personIDS) {

        List<String> personsIdsStg = new ArrayList<>();
        for (PersonID personID : personIDS) {
            personsIdsStg.add(personID.getPersonEmail());
        }

        return String.join(",", personsIdsStg);
    }

    /**
     * Method convertToEntityAttribute
     * Convert the information of the column PersonsIDs in the database to a List<PersonID>
     * @param s
     * @return
     */
    @Override
    public List<PersonID> convertToEntityAttribute(String s) {

        List<String> personsIdsStg = new ArrayList<>(Arrays.asList(s.split(",")));

        List<PersonID> personsIds = new ArrayList<>();

        if (!personsIdsStg.get(0).equals("")) {
            for (String personIdStg : personsIdsStg) {
                personsIds.add(new PersonID(personIdStg));
            }
        }
        return personsIds;
    }
}
