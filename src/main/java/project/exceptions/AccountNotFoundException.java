package project.exceptions;

import static project.exceptions.Messages.ACCOUNTNOTFOUND;

public class AccountNotFoundException extends RuntimeException {
    static final long serialVersionUID = -3112987815529734725L;

    /**
     * Exception for when account not found
     * Uses exceptions.Messages for exception message
     */
    public AccountNotFoundException() {
        super(ACCOUNTNOTFOUND);
    }

    /**
     * Exception for when account not found
     * Uses provided message for exception message
     */
    public AccountNotFoundException(String message) {
        super(message);
    }

}
