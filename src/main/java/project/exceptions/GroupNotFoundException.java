package project.exceptions;

import static project.exceptions.Messages.GROUPNOTFOUND;

public class GroupNotFoundException extends RuntimeException {
    static final long serialVersionUID = -1159385135552734815L;

    /**
     * Exception for when group not found
     * Uses exceptions.Messages for exception message
     */
    public GroupNotFoundException() {
        super(GROUPNOTFOUND);
    }

    /**
     * Exception for when group not found
     * Uses provided message for exception message
     */
    public GroupNotFoundException(String message) {
        super(message);
    }

}
