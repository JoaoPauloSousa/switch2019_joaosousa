package project.exceptions;

import static project.exceptions.Messages.PERSONNOTMANAGER;

public class PersonIsNotManagerOfTheGroupException extends RuntimeException {

    static final long serialVersionUID = -6548256817502095918L;

    /**
     * Exception for when person is not manager of the group
     * Uses exceptions.Messages for exception message
     */
    public PersonIsNotManagerOfTheGroupException() {
        super(PERSONNOTMANAGER);
    }

    /**
     * Exception for when person is not manager of the group
     * Uses provided message for exception message
     */
    public PersonIsNotManagerOfTheGroupException(String message) {
        super(message);
    }
}
