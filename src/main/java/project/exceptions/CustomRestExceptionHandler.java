package project.exceptions;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CustomRestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({PersonNotFoundException.class})
    public ResponseEntity<Object> handlePersonNotFound(final PersonNotFoundException ex) {
        final String error = ex.getMessage();
        final ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, ex.getLocalizedMessage(), error);
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    @ExceptionHandler({MemberAlreadyExistsException.class})
    public ResponseEntity<Object> handleMemberAlreadyExists(final MemberAlreadyExistsException ex) {
        final String error = ex.getMessage();
        final ApiError apiError = new ApiError(HttpStatus.UNPROCESSABLE_ENTITY, ex.getLocalizedMessage(), error);
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    @ExceptionHandler({GroupAlreadyExistsException.class})
    public ResponseEntity<Object> handleGroupAlreadyExists(final GroupAlreadyExistsException ex) {
        final String error = ex.getMessage();
        final ApiError apiError = new ApiError(HttpStatus.UNPROCESSABLE_ENTITY, ex.getLocalizedMessage(), error);
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    @ExceptionHandler({GroupNotFoundException.class})
    public ResponseEntity<Object> handleGroupNotFound(final GroupNotFoundException ex) {
        final String error = ex.getMessage();
        final ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, ex.getLocalizedMessage(), error);
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    @ExceptionHandler({GroupConflictException.class})
    public ResponseEntity<Object> handleGroupConflict(final GroupConflictException ex) {
        final String error = ex.getMessage();
        final ApiError apiError = new ApiError(HttpStatus.UNPROCESSABLE_ENTITY, ex.getLocalizedMessage(), error);
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    @ExceptionHandler({AccountNotFoundException.class})
    public ResponseEntity<Object> handleAccountNotFound(final AccountNotFoundException ex) {
        final String error = ex.getMessage();
        final ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, ex.getLocalizedMessage(), error);
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    @ExceptionHandler({AccountAlreadyExistsException.class})
    public ResponseEntity<Object> handleAccountAlreadyExists(final AccountAlreadyExistsException ex) {
        final String error = ex.getMessage();
        final ApiError apiError = new ApiError(HttpStatus.UNPROCESSABLE_ENTITY, ex.getLocalizedMessage(), error);
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    @ExceptionHandler({CategoryAlreadyExistsException.class})
    public ResponseEntity<Object> handleCategoryAlreadyExists(final CategoryAlreadyExistsException ex) {
        final String error = ex.getMessage();
        final ApiError apiError = new ApiError(HttpStatus.UNPROCESSABLE_ENTITY, ex.getLocalizedMessage(), error);
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    @ExceptionHandler({CategoryNotFoundException.class})
    public ResponseEntity<Object> handleCategoryNotFound(final CategoryNotFoundException ex) {
        final String error = ex.getMessage();
        final ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, ex.getLocalizedMessage(), error);
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    @ExceptionHandler({PersonIsNotManagerOfTheGroupException.class})
    public ResponseEntity<Object> handlePersonIsNotManagerOfTheGroup(final PersonIsNotManagerOfTheGroupException ex) {
        final String error = ex.getMessage();
        final ApiError apiError = new ApiError(HttpStatus.FORBIDDEN, ex.getLocalizedMessage(), error);
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    @ExceptionHandler({InvalidFieldException.class})
    public ResponseEntity<Object> handleInvalidField(final InvalidFieldException ex) {
        final String error = ex.getMessage();
        final ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), error);
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    @ExceptionHandler({GroupLedgerAlreadyExistsException.class})
    public ResponseEntity<Object> handleGroupLedgerAlreadyExists(final GroupLedgerAlreadyExistsException ex) {
        final String error = ex.getMessage();
        final ApiError apiError = new ApiError(HttpStatus.UNPROCESSABLE_ENTITY, ex.getLocalizedMessage(), error);
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    @ExceptionHandler({LedgerNotFoundException.class})
    public ResponseEntity<Object> handleLedgerNotFound(final LedgerNotFoundException ex) {
        final String error = ex.getMessage();
        final ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, ex.getLocalizedMessage(), error);
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    @ExceptionHandler({TransactionAlreadyExistsException.class})
    public ResponseEntity<Object> handleTransactionAlreadyExists(final TransactionAlreadyExistsException ex) {
        final String error = ex.getMessage();
        final ApiError apiError = new ApiError(HttpStatus.UNPROCESSABLE_ENTITY, ex.getLocalizedMessage(), error);
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    @ExceptionHandler({AccountConflictException.class})
    public ResponseEntity<Object> handleAccountConflict(final AccountConflictException ex) {
        final String error = ex.getMessage();
        final ApiError apiError = new ApiError(HttpStatus.UNPROCESSABLE_ENTITY, ex.getLocalizedMessage(), error);
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }
}
