package project;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;
import project.model.entities.account.Account;
import project.model.entities.group.Group;
import project.model.entities.group.GroupID;
import project.model.entities.ledger.Ledger;
import project.model.entities.ledger.Type;
import project.model.entities.person.Person;
import project.model.entities.shared.AccountID;
import project.model.entities.shared.Category;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.PersonID;
import project.model.specifications.repositories.AccountRepository;
import project.model.specifications.repositories.GroupRepository;
import project.model.specifications.repositories.LedgerRepository;
import project.model.specifications.repositories.PersonRepository;

import java.util.Arrays;
import java.util.List;

@SpringBootApplication
@RestController
public class ProjectApplication implements ApplicationRunner {

    @Autowired
    PersonRepository personRepository;

    @Autowired
    LedgerRepository ledgerRepository;

    @Autowired
    GroupRepository groupRepository;

    @Autowired
    AccountRepository accountRepository;

    // String literal employed often
    String suffix = "@switch.pt";
    String porto = "Porto";
    String saude = "Saude";

    public static void main(String[] args) {
        SpringApplication.run(ProjectApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments arg0) throws Exception {
        bootstrapping();
    }

/*    private PersonID buildPersonID(String id) {
        return new PersonID(id + suffix);
    }*/

    private PersonID buildPersonID(String prefix, String number) {
        return new PersonID(prefix + number + suffix);
    }

    private GroupID buildGroupID(String prefix, String number) {
        return new GroupID(prefix + number);
    }

    private LedgerID buildLedgerID(String prefix, String number) {
        return new LedgerID(prefix + number);
    }

    private AccountID buildAccountID(String prefix, String number) {
        return new AccountID(prefix + number);
    }

    private void bootstrapping() {
        bootstrappingUS1();
        bootstrappingUS2();
        bootstrappingUS3();
        bootstrappingUS4();
        bootstrappingUS5();
        bootstrappingUS6();
        bootstrappingUS7();
        bootstrappingUS8();
        bootstrappingUS10();
    }

    private void bootstrappingUS1() {
        String prefix = "1000";

        // Create PersonIDs
        PersonID pID1 = buildPersonID(prefix, "1");
        PersonID pID2 = buildPersonID(prefix, "2");
        PersonID pID3 = buildPersonID(prefix, "3");
        PersonID pID4 = buildPersonID(prefix, "4");
        PersonID pID5 = buildPersonID(prefix, "5");
        PersonID pID6 = buildPersonID(prefix, "6");
        PersonID pID7 = buildPersonID(prefix, "7");
        PersonID pID8 = buildPersonID(prefix, "8");

        // Create LedgerIDs
        LedgerID lID1 = buildLedgerID(prefix, "1");
        LedgerID lID2 = buildLedgerID(prefix, "2");
        LedgerID lID3 = buildLedgerID(prefix, "3");
        LedgerID lID4 = buildLedgerID(prefix, "4");
        LedgerID lID5 = buildLedgerID(prefix, "5");
        LedgerID lID6 = buildLedgerID(prefix, "6");
        LedgerID lID7 = buildLedgerID(prefix, "7");
        LedgerID lID8 = buildLedgerID(prefix, "8");

        // A -> Same mother: Carla is mother of Carlinhos & Carlota
        // B -> Same father: Miguel is father of Carlinhos & Carlao
        // C -> Same siblings list: Carlota & Carlao are siblings by SIBLINGS LIST (no common parents)
        // Carlota's mother = Carla, father = Joao & Carlao's mother = Paula, father = Miguel

        // Create people (using common address, birthPlace and birthDate, no need to differentiate for this US)
        String address = "Rua Dr. Roberto Frias s/n";
        String birthPlace = "Viseu";
        String birthDate = "1991-12-22";
        Person carla = new Person(pID1, "Carla", address, birthPlace, birthDate, null, null, lID1);
        Person miguel = new Person(pID2, "Miguel", address, birthPlace, birthDate, null, null, lID2);
        Person paula = new Person(pID3, "Paula", address, birthPlace, birthDate, null, null, lID3);
        Person joao = new Person(pID4, "Joao", address, birthPlace, birthDate, null, null, lID4);
        Person carlinhos = new Person(pID5, "Carlinhos", address, birthPlace, birthDate, pID1, pID2, lID5);
        Person carlota = new Person(pID6, "Carlota", address, birthPlace, birthDate, pID1, pID4, lID6);
        Person carlao = new Person(pID7, "Carlao", address, birthPlace, birthDate, pID3, pID2, lID7);
        Person carlona = new Person(pID8, "Carlona", address, birthPlace, birthDate, pID1, pID4, lID8);
        carlota.addSibling(carlao);

        // Save people in DB
        List<Person> people = Arrays.asList(carla, miguel, paula, joao, carlinhos, carlota, carlao, carlona);
        for (Person person : people) {
            personRepository.save(person);
        }

        // Save ledgers in DB
        List<LedgerID> ledgersIDs = Arrays.asList(lID1, lID2, lID3, lID4, lID5, lID6, lID7, lID8);
        for (LedgerID ledgerID : ledgersIDs) {
            ledgerRepository.save(new Ledger(ledgerID));
        }

    }

    private void bootstrappingUS2() {
        // Creators Id's : 2100x
        // Groups Id's : 2101x
        // Ledger Creators Id's : 2102x
        // Ledger Groups Id's : 2103x

        String prefix = "210";

        PersonID creator1ID = buildPersonID(prefix, "01");
        PersonID creator2ID = buildPersonID(prefix, "02");
        GroupID group1ID = buildGroupID(prefix, "11");

        LedgerID ledgerCreator1ID = buildLedgerID(prefix, "21");
        LedgerID ledgerCreator2ID = buildLedgerID(prefix, "22");
        LedgerID ledgerGroup1ID = buildLedgerID(prefix, "31");

        Ledger ledgerCreator1 = new Ledger(ledgerCreator1ID);
        Ledger ledgerCreator2 = new Ledger(ledgerCreator2ID);
        Ledger ledgerGroup1 = new Ledger(ledgerGroup1ID);

        Person creator1 = new Person(creator1ID, "João", "Travessa Santa Bárbara", "Estarreja",
                "1987-04-17", null, null, ledgerCreator1ID);
        Person creator2 = new Person(creator2ID, "João2", "Travessa Santa Bárbara2", "Estarreja2",
                "2020-04-17", null, null, ledgerCreator2ID);
        Group group1 = new Group(group1ID, "Switchadas", "1987-04-17", creator1ID, ledgerGroup1ID);

        ledgerRepository.save(ledgerCreator1);
        ledgerRepository.save(ledgerCreator2);
        ledgerRepository.save(ledgerGroup1);
        personRepository.save(creator1);
        personRepository.save(creator2);
        groupRepository.save(group1);
    }

    private void bootstrappingUS3() {
        // Bootstrapping for US3
        String address = "Rua Dr. Roberto Frias s/n";

        PersonID isabelID = new PersonID("111" + suffix);
        LedgerID isabelLedgerID = new LedgerID("11111");
        Person isabel = new Person(isabelID, "Isabel", address, porto, "1991-12-22", null, null, isabelLedgerID);

        PersonID joaoID = new PersonID("222" + suffix);
        LedgerID joaoLedgerID = new LedgerID("22222");
        Person joao = new Person(joaoID, "João", address, "Gaia", "1990-11-01", null, null, joaoLedgerID);

        PersonID andreID = new PersonID("333" + suffix);
        LedgerID andreLedgerID = new LedgerID("33333");
        Person andre = new Person(andreID, "André", address, "Penafiel", "1989-09-09", null, null, andreLedgerID);

        PersonID diogoID = new PersonID("1996" + suffix);
        LedgerID diogoLedgerID = new LedgerID("1997");
        Person diogo = new Person(diogoID, "Diogo", address, "Paranhos", "1996-05-27", null, null, diogoLedgerID);

        ledgerRepository.save(new Ledger(isabelLedgerID));
        ledgerRepository.save(new Ledger(joaoLedgerID));
        ledgerRepository.save(new Ledger(andreLedgerID));
        ledgerRepository.save(new Ledger(diogoLedgerID));
        personRepository.save(isabel);
        personRepository.save(joao);
        personRepository.save(andre);
        personRepository.save(diogo);

        GroupID alvesGroupID = new GroupID("4444");
        LedgerID alvesGroupLedgerID = new LedgerID("444444");
        Group alvesFamily = new Group(alvesGroupID, "Grupo da familia Alves", "2020-01-01", new PersonID("333" + suffix), alvesGroupLedgerID);
        alvesFamily.addMemberID(joaoID);
        alvesFamily.addMemberID(diogoID);

        ledgerRepository.save(new Ledger(alvesGroupLedgerID));
        groupRepository.save(alvesFamily);
    }

    private void bootstrappingUS4() {
        String prefix1 = "40";
        String prefix2 = "45";

        //Group A - Family Group
        PersonID carlosID = buildPersonID(prefix1, "01");
        LedgerID carlosLedgerID = buildLedgerID(prefix1, "01");
        PersonID carlaID = buildPersonID(prefix1, "02");
        LedgerID carlaLedgerID = buildLedgerID(prefix1, "02");
        PersonID carlitosID = buildPersonID(prefix1, "03");
        LedgerID carlitosLedgerID = buildLedgerID(prefix1, "03");
        PersonID mariaID = buildPersonID(prefix1, "04");
        LedgerID mariaLedgerID = buildLedgerID(prefix1, "04");
        GroupID groupAID = buildGroupID(prefix2, "01");
        LedgerID groupALedgerID = buildLedgerID(prefix2, "01");

        //Group B - Not family group
        PersonID joaoID = buildPersonID(prefix1, "05");
        LedgerID joaoLedgerID = buildLedgerID(prefix1, "05");
        PersonID carlaoID = buildPersonID(prefix1, "06");
        LedgerID carlaoLedgerID = buildLedgerID(prefix1, "06");
        PersonID carlotaID = buildPersonID(prefix1, "07");
        LedgerID carlotaLedgerID = buildLedgerID(prefix1, "07");
        GroupID groupBID = buildGroupID(prefix2, "02");
        LedgerID groupBLedgerID = buildLedgerID(prefix2, "02");

        //Group C - Not family group
        GroupID groupCID = buildGroupID(prefix2, "03");
        LedgerID groupCLedgerID = buildLedgerID(prefix2, "03");

        //Group D - Not family group
        GroupID groupDID = buildGroupID(prefix2, "04");
        LedgerID groupDLedgerID = buildLedgerID(prefix2, "04");

        //Group E - Not family group
        GroupID groupEID = buildGroupID(prefix2, "05");
        LedgerID groupELedgerID = buildLedgerID(prefix2, "05");

        //Group F - Not family group
        GroupID groupFID = buildGroupID(prefix2, "06");
        LedgerID groupFLedgerID = buildLedgerID(prefix2, "06");

        //Group G - Family group
        PersonID patriciaID = buildPersonID(prefix1, "08");
        LedgerID patriciaLedgerID = buildLedgerID(prefix1, "08");
        PersonID catarinaID = buildPersonID(prefix1, "09");
        LedgerID catarinaLedgerID = buildLedgerID(prefix1, "09");
        GroupID groupGID = buildGroupID(prefix2, "07");
        LedgerID groupGLedgerID = buildLedgerID(prefix2, "07");

        //Group H - Family group
        PersonID pedroID = buildPersonID(prefix1, "10");
        LedgerID pedroLedgerID = buildLedgerID(prefix1, "10");
        PersonID carinaID = buildPersonID(prefix1, "11");
        LedgerID carinaLedgerID = buildLedgerID(prefix1, "11");
        GroupID groupHID = buildGroupID(prefix2, "08");
        LedgerID groupHLedgerID = buildLedgerID(prefix2, "08");

        //Group I - Not Family group
        PersonID catiaID = buildPersonID(prefix1, "12");
        LedgerID catiaLedgerID = buildLedgerID(prefix1, "12");
        GroupID groupIID = buildGroupID(prefix2, "09");
        LedgerID groupILedgerID = buildLedgerID(prefix2, "09");

        //Group J - Not Family group
        GroupID groupJID = buildGroupID(prefix2, "10");
        LedgerID groupJLedgerID = buildLedgerID(prefix2, "10");

        String address = "Rua Santa Catarina 35";
        String birthPlace = porto;
        String birthDate = "1989-12-18";
        String creationDate = "2019-12-18";
        String description = "general";

        // Group A (Carlos, Carla, Carlitos, Maria | Carlos + Carla = Carlitos & Maria) - Family Group
        Person carlos = new Person(carlosID, "carlos", address, birthPlace, birthDate, null, null, carlosLedgerID);
        Person carla = new Person(carlaID, "carla", address, birthPlace, birthDate, null, null, carlaLedgerID);
        Person carlitos = new Person(carlitosID, "carlitos", address, birthPlace, birthDate, carlosID, carlaID, carlitosLedgerID);
        Person maria = new Person(mariaID, "maria", address, birthPlace, birthDate, carlosID, carlaID, mariaLedgerID);

        personRepository.save(carlos);
        personRepository.save(carla);
        personRepository.save(carlitos);
        personRepository.save(maria);

        Group groupA = new Group(groupAID, description, creationDate, carlosID, groupALedgerID);

        groupA.addMemberID(carlaID);
        groupA.addMemberID(carlitosID);
        groupA.addMemberID(mariaID);

        groupRepository.save(groupA);
        ledgerRepository.save(new Ledger(carlosLedgerID));
        ledgerRepository.save(new Ledger(carlaLedgerID));
        ledgerRepository.save(new Ledger(carlitosLedgerID));
        ledgerRepository.save(new Ledger(mariaLedgerID));
        ledgerRepository.save(new Ledger(groupALedgerID));

        // Group B (Carlos, Carla, Carlao, Carlota | Carlos + Joao = Carlota & Carla + Joao = Carlao) - Not family group

        Person joao = new Person(joaoID, "joao", address, birthPlace, birthDate, null, null, joaoLedgerID);
        Person carlao = new Person(carlaoID, "carla", address, birthPlace, birthDate, carlaID, joaoID, carlaoLedgerID);
        Person carlota = new Person(carlotaID, "carlota", address, birthPlace, birthDate, carlosID, joaoID, carlotaLedgerID);

        personRepository.save(joao);
        personRepository.save(carlao);
        personRepository.save(carlota);

        Group groupB = new Group(groupBID, description, creationDate, carlosID, groupBLedgerID);

        groupB.addMemberID(carlaID);
        groupB.addMemberID(carlaoID);
        groupB.addMemberID(carlotaID);

        groupRepository.save(groupB);
        ledgerRepository.save(new Ledger(joaoLedgerID));
        ledgerRepository.save(new Ledger(carlaoLedgerID));
        ledgerRepository.save(new Ledger(carlotaLedgerID));
        ledgerRepository.save(new Ledger(groupBLedgerID));

        // Group C (Carlitos | Carlos + Carla = Carlitos) - Not family Group

        Group groupC = new Group(groupCID, description, creationDate, carlitosID, groupCLedgerID);

        groupRepository.save(groupC);
        ledgerRepository.save(new Ledger(groupCLedgerID));

        // Group D (Carlos, Carlitos, Maria | Carlos + Carla = Carlitos & Maria) - Not family group

        Group groupD = new Group(groupDID, description, creationDate, carlosID, groupDLedgerID);

        groupD.addMemberID(carlitosID);
        groupD.addMemberID(mariaID);

        groupRepository.save(groupD);
        ledgerRepository.save(new Ledger(groupDLedgerID));

        // Group E (Carla, Carlitos, Maria | Carlos + Carla = Carlitos & Maria) - Not family group

        Group groupE = new Group(groupEID, description, creationDate, carlaID, groupELedgerID);

        groupE.addMemberID(carlitosID);
        groupE.addMemberID(mariaID);

        groupRepository.save(groupE);
        ledgerRepository.save(new Ledger(groupELedgerID));

        // Group F (Carlitos, Maria | Carlos + Carla = Carlitos & Maria) - Not family group

        Group groupF = new Group(groupFID, description, creationDate, carlitosID, groupFLedgerID);

        groupF.addMemberID(mariaID);

        groupRepository.save(groupF);
        ledgerRepository.save(new Ledger(groupFLedgerID));

        // Group G (Carlos, Carla, Carlitos, Patricia | Carlos + Carla = Carlitos & Carlos + Patricia = Catarina) - Family group

        Person patricia = new Person(patriciaID, "patricia", address, birthPlace, birthDate, null, null, patriciaLedgerID);
        Person catarina = new Person(catarinaID, "catarina", address, birthPlace, birthDate, patriciaID, carlosID, catarinaLedgerID);

        personRepository.save(patricia);
        personRepository.save(catarina);

        Group groupG = new Group(groupGID, description, creationDate, carlaID, groupGLedgerID);

        groupG.addMemberID(carlosID);
        groupG.addMemberID(carlitosID);
        groupG.addMemberID(catarinaID);

        groupRepository.save(groupG);
        ledgerRepository.save(new Ledger(patriciaLedgerID));
        ledgerRepository.save(new Ledger(catarinaLedgerID));
        ledgerRepository.save(new Ledger(groupGLedgerID));

        // Group H (Carlos, Carla, Carlitos, Pedro | Carlos + Carla = Carlitos & Carla + Pedro = Carina) - Family group

        Person pedro = new Person(pedroID, "pedro", address, birthPlace, birthDate, null, null, pedroLedgerID);
        Person carina = new Person(carinaID, "carina", address, birthPlace, birthDate, carlaID, pedroID, carinaLedgerID);

        personRepository.save(pedro);
        personRepository.save(carina);

        Group groupH = new Group(groupHID, description, creationDate, carlaID, groupHLedgerID);

        groupH.addMemberID(carlosID);
        groupH.addMemberID(carlitosID);
        groupH.addMemberID(carinaID);

        groupRepository.save(groupH);
        ledgerRepository.save(new Ledger(pedroLedgerID));
        ledgerRepository.save(new Ledger(carinaLedgerID));
        ledgerRepository.save(new Ledger(groupHLedgerID));

        // Group I (Carlos, Carla, Carlitos, Catia | Carlos + Carla = Carlitos & Pedro + Patricia = Catia) - Not Family group

        Person catia = new Person(catiaID, "catia", address, birthPlace, birthDate, patriciaID, pedroID, catiaLedgerID);

        personRepository.save(catia);

        Group groupI = new Group(groupIID, description, creationDate, carlaID, groupILedgerID);

        groupI.addMemberID(carlosID);
        groupI.addMemberID(carlitosID);
        groupI.addMemberID(catiaID);

        groupRepository.save(groupI);
        ledgerRepository.save(new Ledger(catiaLedgerID));
        ledgerRepository.save(new Ledger(groupILedgerID));

        // Group J (Carlos, Carla, Catarina, Carina | Carlos + Patricia = Catarina & Carla + Pedro = Carina) - Not Family group

        Group groupJ = new Group(groupJID, description, creationDate, carlaID, groupJLedgerID);

        groupJ.addMemberID(carlosID);
        groupJ.addMemberID(carlitosID);
        groupJ.addMemberID(catiaID);

        groupRepository.save(groupJ);
        ledgerRepository.save(new Ledger(groupJLedgerID));

    }

    private void bootstrappingUS5() {
        String prefix = "510";
        GroupID groupID = buildGroupID(prefix, "200");
        PersonID creatorID = buildPersonID(prefix, "10");
        LedgerID ledgerID = buildLedgerID(prefix, "20");
        LedgerID ledgerID1 = buildLedgerID(prefix, "1");

        Ledger ledger1 = new Ledger(ledgerID1);
        Ledger ledger = new Ledger(ledgerID);
        Person joao = new Person(creatorID, "João", "Mirandela", "Mirandela", "1982-05-27", null, null, ledgerID1);

        Group groupUS5 = new Group(groupID, "Futebol", "2020-01-20", creatorID, ledgerID);

        joao.addCategory("Cerveja");
        groupUS5.addCategory("Arbitros");

        personRepository.save(joao);
        ledgerRepository.save(ledger1);
        ledgerRepository.save(ledger);
        groupRepository.save(groupUS5);
    }

    private void bootstrappingUS6() {

        // Create US prefix for identifier
        String prefix = "666";

        // Create PersonIDs
        PersonID pID61 = buildPersonID(prefix, "1");
        PersonID pID62 = buildPersonID(prefix, "2");
        PersonID pID63 = buildPersonID(prefix, "3");
        PersonID pID64 = buildPersonID(prefix, "4");
        PersonID pID65 = buildPersonID(prefix, "5");

        // Create LedgerIDs
        LedgerID lID61 = buildLedgerID(prefix, "1");
        LedgerID lID62 = buildLedgerID(prefix, "2");
        LedgerID lID63 = buildLedgerID(prefix, "3");
        LedgerID lID64 = buildLedgerID(prefix, "4");
        LedgerID lID65 = buildLedgerID(prefix, "5");

        // Create people (using common address, birthPlace, no need to differentiate for this US)
        String address = "Hollywood";
        String birthPlace = "USA";
        String ripleyBirthDate = "1979-05-25";
        String ramboBirthDate = "1982-10-22";
        String terminatorBirthDate = "1984-10-26";
        String mcclaneBirthDate = "1988-07-12";
        Person rambo = new Person(pID61, "Rambo, John", address, birthPlace, ramboBirthDate, null, null, lID61);
        Person ripley = new Person(pID62, "Ripley, Ellen", address, birthPlace, ripleyBirthDate, null, null, lID62);
        Person terminator = new Person(pID63, "T-800, Terminator", address, birthPlace, terminatorBirthDate, pID61, pID62, lID63);
        Person connor = new Person(pID64, "Connor, Sarah", address, birthPlace, terminatorBirthDate, pID61, pID62, lID64);
        Person mcclane = new Person(pID65, "McClane, John", address, birthPlace, mcclaneBirthDate, pID62, pID62, lID65);

        // Create AccountID's
        AccountID aID61 = buildAccountID(prefix, "01");
        AccountID aID62 = buildAccountID(prefix, "02");
        AccountID aID63 = buildAccountID(prefix, "03");
        AccountID aID64 = buildAccountID(prefix, "04");
        AccountID aID65 = buildAccountID(prefix, "05");
        AccountID aID66 = buildAccountID(prefix, "06");

        // Create Account's
        Account ramboAccount = new Account(aID61, "arrows", "War assets");
        Account ripleyAccount = new Account(aID62, "spaceships", "Space race");
        Account terminatorAccount = new Account(aID63, "machines", "Humanity eradication plan");
        Account connorAccount = new Account(aID64, "training", "Humanity survival plan");
        Account mcclaneAccount = new Account(aID65, "boots", "Never again!");
        Account ramboAccount2 = new Account(aID66, "knife sharpeners", "More war assets");
        rambo.addAccount(aID61);
        ripley.addAccount(aID62);
        terminator.addAccount(aID63);
        connor.addAccount(aID64);
        mcclane.addAccount(aID65);
        rambo.addAccount(aID66);

        // Save people
        List<Person> people = Arrays.asList(rambo, ripley, terminator, connor, mcclane);
        for (Person person : people) {
            personRepository.save(person);
        }

        // Save ledgers
        List<LedgerID> ledgersIDs = Arrays.asList(lID61, lID62, lID63, lID64, lID65);
        for (LedgerID ledgerID : ledgersIDs) {
            ledgerRepository.save(new Ledger(ledgerID));
        }

        // Save account's
        List<Account> accounts = Arrays.asList(ramboAccount, ramboAccount2, ripleyAccount, terminatorAccount,
                connorAccount, mcclaneAccount);
        for (Account account : accounts) {
            accountRepository.save(account);
        }
    }

    private void bootstrappingUS7() {
        String prefix = "700";

        // Create AccountID's
        AccountID newAccountID = buildAccountID(prefix, "1");
        AccountID newAccountID1 = buildAccountID(prefix, "10");
        AccountID newAccountID2 = buildAccountID(prefix, "0");
        AccountID newAccountID3 = buildAccountID(prefix, "9");

        // Create PersonIDs
        PersonID newPersonID = buildPersonID(prefix, "1");
        PersonID newPersonID1 = buildPersonID(prefix, "2");

        // Create GroupIDs
        GroupID newGroupID = buildGroupID(prefix, "1");
        GroupID newGroupID1 = buildGroupID(prefix, "2");

        // Create LedgerIDs
        LedgerID newLedgerPersonID = buildLedgerID(prefix, "1");
        LedgerID newLedgerPersonID1 = buildLedgerID(prefix, "3");
        LedgerID newLedgerGroupID = buildLedgerID(prefix, "2");
        LedgerID newLedgerGroupID1 = buildLedgerID(prefix, "4");

        // Create Account's
        Account newAccount = new Account(newAccountID, "football", "sports");
        Account newAccount1 = new Account(newAccountID1, "potatoes", "groceries");
        Account newAccount2 = new Account(newAccountID2, "bills", "bills");
        Account newAccount3 = new Account(newAccountID3, "party", "party");
        // Create people
        Person newPerson = new Person(newPersonID, "Gonçalo", porto, porto, "1993-03-15", null, null, newLedgerPersonID);
        Person alfredo = new Person(newPersonID1, "Alfredo", "Braganca", "Braganca", "2000-07-02", null, null, newLedgerPersonID1);
        // Create Groups
        Group newGroup = new Group(newGroupID, "isep students", "2019-09-05", newPersonID, newLedgerGroupID);
        Group group4 = new Group(newGroupID1, "group4", "2019-12-12", newPersonID1, newLedgerGroupID1);

        newGroup.addAccount(newAccountID);
        newGroup.addAccount(newAccountID2);
        group4.addAccount(newAccountID3);

        // Create Ledgers
        Ledger personLedger = new Ledger(newLedgerPersonID);
        Ledger groupLedger = new Ledger(newLedgerGroupID);
        Ledger personLedger1 = new Ledger(newLedgerPersonID1);
        Ledger groupLedger1 = new Ledger(newLedgerGroupID1);

        // Save account's
        List<Account> accounts = Arrays.asList(newAccount, newAccount1, newAccount2, newAccount3);
        for (Account account : accounts) {
            accountRepository.save(account);
        }
        // Save ledgers
        List<Ledger> ledgers = Arrays.asList(groupLedger, personLedger, personLedger1, groupLedger1);
        for (Ledger ledger : ledgers) {
            ledgerRepository.save(ledger);
        }
        // Save people
        List<Person> people = Arrays.asList(newPerson, alfredo);
        for (Person person : people) {
            personRepository.save(person);
        }
        // Save Groups
        List<Group> groups = Arrays.asList(newGroup, group4);
        for (Group group : groups) {
            groupRepository.save(group);
        }
    }

    private void bootstrappingUS8() {

        // Create US prefix for identifier
        String prefix = "810";

        //Create PersonIDs
        PersonID personID1 = buildPersonID(prefix, "1");
        PersonID personID2 = buildPersonID(prefix, "2");

        //Create GroupIDs
        GroupID groupID1 = buildGroupID(prefix, "1");
        GroupID groupID2 = buildGroupID(prefix, "2");

        //Create AccountIDs
        AccountID accountID1 = buildAccountID(prefix, "1");
        AccountID accountID2 = buildAccountID(prefix, "2");
        AccountID accountID3 = buildAccountID(prefix, "3");
        AccountID accountID4 = buildAccountID(prefix, "4");
        AccountID accountID5 = buildAccountID(prefix, "5");

        //Create Ledger IDs
        LedgerID ledgerID1 = buildLedgerID(prefix, "1");
        LedgerID ledgerID2 = buildLedgerID(prefix, "2");
        LedgerID ledgerID3 = buildLedgerID(prefix, "3");
        LedgerID ledgerID4 = buildLedgerID(prefix, "4");

        //Create Ledgers
        Ledger ledger1 = new Ledger(ledgerID1);
        Ledger ledger2 = new Ledger(ledgerID2);
        Ledger ledger3 = new Ledger(ledgerID3);

        //Create Persons
        Person person1 = new Person(personID1, "Miguel", "Rua de Tanger, 265", "Lisboa", "1989-12-20", null, null,
                ledgerID1);
        Person person2 = new Person(personID2, "Teresa", "Rua de Direita de Campinas 67, 2 andar", porto, "1989-09" +
                "-22",
                null, null, ledgerID2);

        //Create Accounts
        Account account1 = new Account(accountID1, "Geral", "Despesas gerais");
        Account account2 = new Account(accountID2, saude, "Utilizada para despesas de saude");
        Account account3 = new Account(accountID3, "Recreativa", "Utilizada para despesas de entretenimento");
        Account account4 = new Account(accountID4, "Conta de ninguém", "Esta conta não tem owner");
        Account account5 = new Account(accountID5, "Conta do Grupo sem ledger", "Esta conta pertence ao grupo sem " +
                "ledger");

        //Create Groups
        Group group1 = new Group(groupID1, "Grupo de amigos do ISEP", "2020-01-01", personID1, ledgerID3);
        //group2 has a ledgerID which corresponds to a ledger(ledger4) that doesn't exist in ledger repository (is
        // not saved)
        Group group2 = new Group(groupID2, "Grupo dos solteiros", "2010-01-01", personID2, ledgerID4);

        //Create and add Category
        group1.addCategory("comida");

        //Add accounts to groups and persons
        group1.addAccount(accountID1);
        group1.addAccount(accountID2);
        person2.addAccount(accountID3);
        group2.addAccount(accountID5);

        //Save Ledgers
        ledgerRepository.save(ledger1);
        ledgerRepository.save(ledger2);
        ledgerRepository.save(ledger3);

        //Save Accounts
        accountRepository.save(account1);
        accountRepository.save(account2);
        accountRepository.save(account3);
        accountRepository.save(account4);
        accountRepository.save(account5);

        //Save Persons
        personRepository.save(person1);
        personRepository.save(person2);

        //Save Groups
        groupRepository.save(group1);
        groupRepository.save(group2);
    }

    private void bootstrappingUS10() {

        /*
         * ID'S AND NOMENCLATURE
         *
         * Manuel - id 1001@switch.pt (1001) , instance xxx1 or xxxManuel
         * Group1 - id 1002 , instance xxx2 or xxxGroup1
         * Manuel is the creator of Group1
         *
         * Daniela - id 1003@switch.pt (1003), , instance xxx3 or xxxDaniela
         * Group2 - id 1004 , instance xxx4 or xxxGroup2
         * Daniela is the creator of Group2
         *
         * Transactions for group have:
         * Group1 with debitAccount
         * Group2 with creditAccount
         *
         * Transactions for person are always:
         * Manuel with debitAccount
         * Daniela with creditAccount
         *
         * Transactions are the same, only creditor and debitor change
         *
         * Note: save to repository has to be done after each transaction!!!
         * It does not work if multiple transactions are done before save!!!
         */

        // Create US prefix for identifier
        String prefix = "100";

        //Create PersonID
        PersonID personIDManuel = buildPersonID(prefix, "1");
        PersonID personIDDaniela = buildPersonID(prefix, "3");


        //Create Ledger ID
        LedgerID ledgerIDManuel = buildLedgerID(prefix, "1");
        LedgerID ledgerIDGroup1 = buildLedgerID(prefix, "2");
        LedgerID ledgerIDDaniela = buildLedgerID(prefix, "3");
        LedgerID ledgerIDGroup2 = buildLedgerID(prefix, "4");


        //Create GroupID
        GroupID groupID1 = buildGroupID(prefix, "2");
        GroupID groupID2 = buildGroupID(prefix, "4");


        //Create AccountID
        AccountID accountIDManuel = buildAccountID(prefix, "1");
        AccountID accountIDGroup1 = buildAccountID(prefix, "2");
        AccountID accountIDDaniela = buildAccountID(prefix, "3");
        AccountID accountIDGroup2 = buildAccountID(prefix, "4");

        //Create Type
        Type debit = Type.DEBIT;
        Type credit = Type.CREDIT;

        //Create Ledger
        Ledger ledger1 = new Ledger(ledgerIDManuel);
        Ledger ledger2 = new Ledger(ledgerIDGroup1);
        Ledger ledger3 = new Ledger(ledgerIDDaniela);
        Ledger ledger4 = new Ledger(ledgerIDGroup2);

        //Create Person
        Person personManuel = new Person(personIDManuel, "Manuel", "Rua Real Vila", "Vila Real Santo António", "1993-02-09", null, null, ledgerIDManuel);
        Person personDaniela = new Person(personIDDaniela, "Daniela", "Rua Vila Real", "Vila Real", "1993-02-26", null, null, ledgerIDDaniela);

        //Create Category to person
        String category1Strg = "comida e bebida";
        Category category1 = new Category(category1Strg);
        personManuel.addCategory(category1Strg);

        //Create Group
        Group group1 = new Group(groupID1, "Grupo de pessoas de bem", "2020-06-09", personIDManuel, ledgerIDGroup1);
        Group group2 = new Group(groupID2, "Grupo de pessoas de mal", "2020-06-09", personIDDaniela, ledgerIDGroup2);

        //Create Category to group
        Category category2 = new Category(category1Strg);
        group1.addCategory(category1Strg);

        //Create Account
        Account accountManuel = new Account(accountIDManuel, "Conta manuel ", "Utilizada para despesas saúde");
        Account accountGrupo1 = new Account(accountIDGroup1, "Conta grupo1", "Utilizada para despesas Grupo1");
        Account accountDaniela = new Account(accountIDDaniela, "Conta daniela ", "Utilizada para despesas educação");
        Account accountGrupo2 = new Account(accountIDGroup2, "Conta grupo2", "Utilizada para despesas Grupo2");


        //Add Accounts
        personManuel.addAccount(accountIDManuel);
        group1.addAccount(accountIDGroup1);
        personDaniela.addAccount(accountIDDaniela);
        group2.addAccount(accountIDGroup2);

        //Create Transactions for Group
        ledger2.addTransaction("500", credit, "2000-01-01 23:20:58", "Pacote de Oreos", category2, accountIDGroup1, accountIDGroup2);
        ledgerRepository.save(ledger2);
        ledger2.addTransaction("400", debit, "2010-01-01 11:39:23", "Pacote de Belgas", category2, accountIDGroup1, accountIDGroup2);
        ledgerRepository.save(ledger2);
        ledger2.addTransaction("100", credit, "2020-01-01 14:12:15", "Pacote de Maria", category2, accountIDGroup1, accountIDGroup2);
        ledgerRepository.save(ledger2);

        //Create Transactions for Person
        ledger1.addTransaction("500", credit, "2000-01-01 23:20:58", "Pacote de Oreos", category1, accountIDManuel, accountIDDaniela);
        ledgerRepository.save(ledger1);
        ledger1.addTransaction("400", debit, "2010-01-01 11:39:23", "Pacote de Belgas", category1, accountIDManuel, accountIDDaniela);
        ledgerRepository.save(ledger1);


        //Save Person to PersonRepository
        personRepository.save(personManuel);
        personRepository.save(personDaniela);

        //Save Ledger to LedgerRepository
        ledgerRepository.save(ledger3);
        ledgerRepository.save(ledger4);

        //Save Group to GroupRepository
        groupRepository.save(group1);
        groupRepository.save(group2);

        //Save Account to AccountRepository
        List<Account> accounts = Arrays.asList(accountManuel, accountGrupo1, accountDaniela, accountGrupo2);
        for (Account account : accounts) {
            accountRepository.save(account);
        }
    }

}
