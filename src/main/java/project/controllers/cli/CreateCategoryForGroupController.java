package project.controllers.cli;

import org.springframework.stereotype.Controller;
import project.dto.CreateCategoryForGroupAssembler;
import project.dto.CreateCategoryForGroupRequestDTO;
import project.dto.CreateCategoryForGroupResponseDTO;
import project.frameworkddd.IUSCreateCategoryForGroupService;

/**
 * US005.1- Como responsável de grupo, quero criar categoria e associá-la ao grupo.
 */
@Controller
public class CreateCategoryForGroupController {

    private final IUSCreateCategoryForGroupService service;

    /**
     * Constructor for group creating category controller class
     *
     * @param service
     */
    public CreateCategoryForGroupController(IUSCreateCategoryForGroupService service) {
        this.service = service;
    }

    /**
     * Method createCategoryGroup
     *
     * @param groupID
     * @param designation
     * @return CreateCategoryForGroupRequestDTO
     */
    public CreateCategoryForGroupResponseDTO createCategoryForGroup(String personEmail, String groupID, String designation) {

        CreateCategoryForGroupRequestDTO requestDTO = CreateCategoryForGroupAssembler.mapToRequestDTO(personEmail, groupID, designation);
        return this.service.createCategoryForGroup(requestDTO);

    }

}
