package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import project.dto.CopyCategoryBetweenGroupsInfoDTO;
import project.dto.CopyCategoryBetweenGroupsRequestDTO;
import project.dto.CopyCategoryBetweenGroupsResponseDTO;
import project.dto.CopyCategoryForGroupsAssembler;
import project.frameworkddd.IUSCopyCategoryBetweenGroupsService;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;


@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class CopyCategoryBetweenGroupsRestController {

    @Autowired
    private IUSCopyCategoryBetweenGroupsService service;

    @PostMapping("/people/{personEmail}/groups/copy/categories")
    public ResponseEntity<Object> copyCategoryBetweenGroups(@RequestBody CopyCategoryBetweenGroupsInfoDTO info, @PathVariable String personEmail) {

        CopyCategoryBetweenGroupsRequestDTO requestDTO = CopyCategoryForGroupsAssembler.mapToRequestDTO(info.getDesignation(), info.getGroupID1(), info.getGroupID2(), personEmail);

        CopyCategoryBetweenGroupsResponseDTO result = service.copyCategoryBetweenGroups(requestDTO);

        Link linkToCategories = linkTo(methodOn(GetCategoriesRESTController.class).getCategoriesByGroupID(info.getGroupID2())).withSelfRel();
        result.add(linkToCategories);

        return new ResponseEntity<>(result, HttpStatus.CREATED);

    }
}


