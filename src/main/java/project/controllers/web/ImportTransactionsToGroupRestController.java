package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import project.dto.ImportTransactionsToGroupAssembler;
import project.dto.ImportTransactionsToGroupInfoDTO;
import project.dto.ImportTransactionsToGroupRequestDTO;
import project.dto.ImportTransactionsToGroupResponseDTO;
import project.frameworkddd.IUSImportTransactionsToGroupService;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class ImportTransactionsToGroupRestController {

    @Autowired
    private IUSImportTransactionsToGroupService service;

    /**
     * Method createGroup
     * Operates through RequestMapping of the Post type
     *
     * @param info an instance of CreateGroupInfoDTO which represents a DTO from the client
     * @return ResponseEntity with body and HttpStatus
     */
    @PostMapping("/people/{personEmail}/groups/{groupID}/import/transactions")
    public ResponseEntity<Object> importTransactionsToGroup(@RequestBody ImportTransactionsToGroupInfoDTO info, @PathVariable String personEmail, @PathVariable String groupID) {

        ImportTransactionsToGroupRequestDTO requestDTO = ImportTransactionsToGroupAssembler.mapToRequestDTO(info.getTransactions(), groupID, personEmail);
        ImportTransactionsToGroupResponseDTO result = service.importTransactionsToGroup(requestDTO);

        Link linkToTransactions = linkTo(methodOn(GetTransactionsRESTController.class).getTransactionsByGroupID(personEmail, groupID)).withSelfRel();
        result.add(linkToTransactions);

        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }
}
