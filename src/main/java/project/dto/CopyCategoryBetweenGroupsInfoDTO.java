package project.dto;

import lombok.Data;

import java.util.Objects;

@Data
public class CopyCategoryBetweenGroupsInfoDTO {

    final String designation;
    final String groupID1;
    final String groupID2;


    public CopyCategoryBetweenGroupsInfoDTO(String designation, String groupID1, String groupID2) {
        this.groupID1 = groupID1;
        this.groupID2 = groupID2;
        this.designation = designation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CopyCategoryBetweenGroupsInfoDTO)) return false;
        CopyCategoryBetweenGroupsInfoDTO that = (CopyCategoryBetweenGroupsInfoDTO) o;
        return Objects.equals(designation, that.designation) &&
                Objects.equals(groupID1, that.groupID1) &&
                Objects.equals(groupID2, that.groupID2);
    }

    @Override
    public int hashCode() {
        return Objects.hash(designation, groupID1, groupID2);
    }
}

