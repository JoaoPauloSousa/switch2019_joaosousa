package project.dto;

public final class CopyCategoryForGroupsAssembler {


    CopyCategoryForGroupsAssembler() {
        //Intentionally empty
    }

    /**
     * Method mapToDtoRequest
     *
     * @param personEmail personEmail
     * @param groupID1    groupID
     * @param groupID2    groupID
     * @param designation designation of category
     * @return instance of CopyCategoryBetweenGroupsRequestDTO
     */
    public static CopyCategoryBetweenGroupsRequestDTO mapToRequestDTO(String designation, String groupID1, String groupID2, String personEmail) {
        return new CopyCategoryBetweenGroupsRequestDTO(designation, groupID1, groupID2, personEmail);
    }

    /**
     * Method mapToResponseDTO
     *
     * @param groupID
     * @param groupDescription
     * @param newCategory
     * @return instance of CopyCategoryBetweenGroupsResponseDTO
     */
    public static CopyCategoryBetweenGroupsResponseDTO mapToResponseDTO(String groupID, String groupDescription, String newCategory) {
        return new CopyCategoryBetweenGroupsResponseDTO(groupID, groupDescription, newCategory);
    }
}
