package project.dto;

import lombok.Getter;
import org.springframework.hateoas.RepresentationModel;

@Getter
public class TransactionDTO extends RepresentationModel<TransactionDTO> {
    private String amount;
    private String dateTime;
    private String type;
    private String description;
    private String category;
    private String debitAccountID;
    private String creditAccountID;

    /**
     * Constructor for TransactionDTO
     *
     * @param amount          Amount of the Transaction
     * @param type            Type of the Transaction
     * @param dateTime        DateTime of the Transaction
     * @param description     Description of the Transaction
     * @param category        Category of the Transaction
     * @param debitAccountID  ID of the Debit Account of the Transaction
     * @param creditAccountID ID of the Credit Account of the Transaction
     */
    public TransactionDTO(String amount, String type, String dateTime, String description, String category,
                          String debitAccountID, String creditAccountID) {
        setAmount(amount);
        setType(type);
        setDateTime(dateTime);
        setDescription(description);
        setCategory(category);
        setDebitAccountID(debitAccountID);
        setCreditAccountID(creditAccountID);
    }

    /**
     * Create attribute dateTime
     * Checks if dateTime is appropriate (not null, not empty, not composed by just spaces, does not contain letters,
     * characters at index 5 & 8 are "-" or "/")
     *
     * @param dateTime Transaction dateTime
     */
    private void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    /**
     * Create attribute type
     *
     * @param type Transaction type
     */
    private void setType(String type) {
        this.type = type;
    }

    /**
     * Create attribute description
     *
     * @param description Transaction description
     */
    private void setDescription(String description) {
        this.description = description;
    }

    /**
     * Crete attribute category
     *
     * @param category category of transaction
     */
    private void setCategory(String category) {
        this.category = category;
    }

    /**
     * Create attribute debitAccountID
     *
     * @param debitAccountID Account from where the amount is going to be subtracted
     */
    private void setDebitAccountID(String debitAccountID) {
        this.debitAccountID = debitAccountID;
    }

    /**
     * Create attribute creditAccountID
     *
     * @param creditAccountID Account to where the amount is going to be added
     */
    private void setCreditAccountID(String creditAccountID) {
        this.creditAccountID = creditAccountID;
    }

    /**
     * Create attribute transactionAmount
     *
     * @param amount Value to be subtracted and added to debitAccount and creditAccount respectively
     */
    private void setAmount(String amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "TransactionDTO{" +
                "amount='" + amount + '\'' +
                ", dateTime='" + dateTime + '\'' +
                ", type='" + type + '\'' +
                ", description='" + description + '\'' +
                ", category='" + category + '\'' +
                ", debitAccountID='" + debitAccountID + '\'' +
                ", creditAccountID='" + creditAccountID + '\'' +
                '}';
    }
}
