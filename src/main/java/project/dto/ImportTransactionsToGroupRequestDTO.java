package project.dto;

import lombok.Data;

@Data
public class ImportTransactionsToGroupRequestDTO {

    private String transactions;
    private String groupID;
    private String personEmail;

    /**
     * Constructor of ImportTransactionsToGroupRequestDTO class
     *
     * @param groupID      The identity of the group to be added the transactions
     * @param transactions The transactions as a String
     * @param personEmail  The identity of the group's manager who is importing the string transactions
     */
    public ImportTransactionsToGroupRequestDTO(String transactions, String groupID, String personEmail) {
        this.transactions = transactions;
        this.groupID = groupID;
        this.personEmail = personEmail;
    }
}
