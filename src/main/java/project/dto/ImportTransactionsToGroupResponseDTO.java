package project.dto;

import lombok.Data;
import org.springframework.hateoas.RepresentationModel;

@Data
public class ImportTransactionsToGroupResponseDTO extends RepresentationModel<ImportTransactionsToGroupResponseDTO> {

    final String transactionDescription;
    private String groupID;
    private String groupDescription;

    /**
     * Constructor for ImportTransactionsToGroupResponseDTO
     *
     * @param groupID
     * @param groupDescription
     * @param transactionDescription
     */
    public ImportTransactionsToGroupResponseDTO(String groupID, String groupDescription, String transactionDescription) {
        this.transactionDescription = transactionDescription;
        this.groupDescription = groupDescription;
        this.groupID = groupID;
    }

    @Override
    public String toString() {
        return "ImportTransactionsToGroupResponseDTO{" +
                "transactions=" + transactionDescription +
                '}';
    }
}
