package project.dto;

import lombok.Data;

@Data
public class ImportTransactionsToGroupInfoDTO {

    final String transactions;

    /**
     * Constructor of ImportTransactionsToGroupInfoDTO class
     *
     * @param transactions The string of transactions to be imported
     */
    public ImportTransactionsToGroupInfoDTO(String transactions) {

        this.transactions = transactions;
    }

}
