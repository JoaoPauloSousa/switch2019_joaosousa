package project.dto;

public class CheckIfPeopleAreSiblingsAssembler {

    /**
     * Empty constructor of PeopleAreSiblingsDTOAssembler
     */
    CheckIfPeopleAreSiblingsAssembler() {
        // empty
    }

    /**
     * Method to assemble/map a RequestDTO with the IDs of two people
     *
     * @param p1Email Email of person 1
     * @param p2Email Email of person 2
     * @return DTO that contains the IDs of two people
     */
    public static CheckIfPeopleAreSiblingsRequestDTO mapToRequestDTO(String p1Email, String p2Email) {
        return new CheckIfPeopleAreSiblingsRequestDTO(p1Email, p2Email);
    }

    /**
     * Method to assemble/map a ResponseDTO with the response if two person are siblings (true/false)
     *
     * @param isSibling true/false
     * @return
     */
    public static CheckIfPeopleAreSiblingsResponseDTO mapToResponseDTO(String isSibling) {
        return new CheckIfPeopleAreSiblingsResponseDTO(isSibling);

    }

}
