package project.dto;

import java.util.Objects;

public class CreateTransactionInfoDTO {

    final String amount;
    final String dateTime;
    final String type;
    final String description;
    final String categoryDesignation;
    final String debitAccountID;
    final String creditAccountID;

    /**
     * Constructor of CreateTransactionInfoDTO class
     *
     * @param amount              The amount of the transaction to be created
     * @param dateTime            The date and time of the transaction to be created
     * @param type                The type of the transaction to be created (credit or debit)
     * @param description         The description of the transaction to be created
     * @param categoryDesignation The category's designation of the new transaction to be created
     * @param debitAccountID      The identity of the account to be debited
     * @param creditAccountID     The identity of the account to be credited
     */
    public CreateTransactionInfoDTO(String amount, String dateTime,
                                    String type, String description, String categoryDesignation,
                                    String debitAccountID, String creditAccountID) {

        this.amount = amount;
        this.dateTime = dateTime;
        this.type = type;
        this.description = description;
        this.categoryDesignation = categoryDesignation;
        this.debitAccountID = debitAccountID;
        this.creditAccountID = creditAccountID;
    }

    /**
     * Method getAmount
     * gets the amount attribute from requestDTO
     *
     * @return amount as a String
     */
    public String getAmount() {
        return amount;
    }

    /**
     * Method getDateTime
     * gets the dateTime attribute from requestDTO
     *
     * @return dateTime as a String
     */
    public String getDateTime() {
        return dateTime;
    }

    /**
     * Method getType
     * gets the transaction type attribute from requestDTO
     *
     * @return type as a String
     */
    public String getType() {
        return type;
    }

    /**
     * Method getDescription
     * gets the description attribute from requestDTO
     *
     * @return description as a String
     */
    public String getDescription() {
        return description;
    }

    /**
     * Method getCategoryDesignation
     * gets the categoryDesignation attribute from requestDTO
     *
     * @return categoryDesignation as a String
     */
    public String getCategoryDesignation() {
        return categoryDesignation;
    }

    /**
     * Method getDebitAccountID
     * gets the debitAccountID attribute from requestDTO
     *
     * @return debitAccountID as a String
     */
    public String getDebitAccountID() {
        return debitAccountID;
    }

    /**
     * Method getCreditAccountID
     * gets the creditAccountID attribute from requestDTO
     *
     * @return creditAccountID as a String
     */
    public String getCreditAccountID() {
        return creditAccountID;
    }

    /**
     * Override of equals method
     *
     * @param o Object to be compared to this
     * @return True if this is equal to Object o
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateTransactionInfoDTO that = (CreateTransactionInfoDTO) o;
        return Objects.equals(amount, that.amount) &&
                Objects.equals(dateTime, that.dateTime) &&
                Objects.equals(type, that.type) &&
                Objects.equals(description, that.description) &&
                Objects.equals(categoryDesignation, that.categoryDesignation) &&
                Objects.equals(debitAccountID, that.debitAccountID) &&
                Objects.equals(creditAccountID, that.creditAccountID);
    }

    /**
     * Override of hashCode method
     *
     * @return Integer hashCode
     */
    @Override
    public int hashCode() {
        return Objects.hash(amount, dateTime, type, description, categoryDesignation, debitAccountID, creditAccountID);
    }
}
