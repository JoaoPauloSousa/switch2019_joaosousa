package project.dto;

public class ImportTransactionsToGroupAssembler {

    /**
     * Constructor for ImportTransactionsToGroupAssembler
     */
    ImportTransactionsToGroupAssembler() {
        //Intentionally empty
    }

    /**
     * Method mapToDtoRequest
     *
     * @param personEmail  personEmail
     * @param groupID      groupID
     * @param transactions transactions
     * @return instance of ImportTransactionsToGroupRequestDTO
     */
    public static ImportTransactionsToGroupRequestDTO mapToRequestDTO(String transactions, String groupID, String personEmail) {
        return new ImportTransactionsToGroupRequestDTO(transactions, groupID, personEmail);
    }

    /**
     * Method mapToResponseDTO
     *
     * @param groupID
     * @param groupDescription
     * @param transactionDescription
     * @return instance of ImportTransactionsToGroupResponseDTO
     */
    public static ImportTransactionsToGroupResponseDTO mapToResponseDTO(String groupID, String groupDescription, String transactionDescription) {
        return new ImportTransactionsToGroupResponseDTO(groupID, groupDescription, transactionDescription);
    }
}
