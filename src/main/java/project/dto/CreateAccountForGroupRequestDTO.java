package project.dto;

import java.util.Objects;

public class CreateAccountForGroupRequestDTO {

    private String accountID;
    private String groupID;
    private String accountDenomination;
    private String accountDescription;
    private String personEmail;

    /**
     * Constructor of CreateAccountForGroupRequestDTO class
     *
     * @param accountID           The identity of the new account to be created as a String
     * @param groupID             The identity of the group to be added the new account a String
     * @param accountDenomination The denomination of the new account to be created as a String
     * @param accountDescription  The description of the new account to be created as a String
     * @param personEmail         The identity of the group's manager who is adding the new account a String
     */
    public CreateAccountForGroupRequestDTO(String accountID, String groupID, String accountDenomination, String accountDescription, String personEmail) {
        setAccountID(accountID);
        setGroupID(groupID);
        setAccountDenomination(accountDenomination);
        setAccountDescription(accountDescription);
        setPersonEmail(personEmail);
    }

    /**
     * Method getAccountID
     * gets the accountID attribute from requestDTO
     *
     * @return object AccountID
     */
    public String getAccountID() {
        return accountID;
    }

    /**
     * Method setAccountID
     * sets the accountID attribute of the requestDTO
     *
     * @param accountID String to be converted to long
     */
    public void setAccountID(String accountID) {
        this.accountID = accountID;
    }

    /**
     * Method getPersonID
     * gets the personID attribute from requestDTO
     *
     * @return object PersonID
     */
    public String getGroupID() {
        return groupID;
    }

    /**
     * Method setPersonID
     * sets the accountID attribute of the requestDTO
     *
     * @param groupID String to be converted to long
     */
    public void setGroupID(String groupID) {
        this.groupID = groupID;
    }

    /**
     * Method getAccountDenomination
     * gets the accountDenomination attribute from requestDTO
     *
     * @return denomination of the account
     */
    public String getAccountDenomination() {
        return accountDenomination;
    }

    /**
     * Method setAccountDenomination
     * sets the accountDenomination attribute of the requestDTO
     *
     * @param accountDenomination Denomination of the account
     */
    public void setAccountDenomination(String accountDenomination) {
        this.accountDenomination = accountDenomination;
    }

    /**
     * Method getAccountDescription
     * gets the accountDescription attribute from requestDTO
     *
     * @return description of the account
     */
    public String getAccountDescription() {
        return accountDescription;
    }

    /**
     * Method setAccountDescription
     * sets the accountDenomination attribute of the requestDTO
     *
     * @param accountDescription Description of the account
     */
    public void setAccountDescription(String accountDescription) {
        this.accountDescription = accountDescription;
    }

    /**
     * Method getPersonID
     * gets the personID attribute from requestDTO
     *
     * @return the identity of the group's administrator who is creating the account
     */
    public String getPersonEmail() {
        return personEmail;
    }

    /**
     * Method setPersonID
     * sets the personID attribute of the requestDTO
     *
     * @param personEmail the identity of the group's administrator who is creating the account
     */
    private void setPersonEmail(String personEmail) {
        this.personEmail = personEmail;
    }

    /**
     * Override of equals method
     *
     * @param o Object to be compared to this
     * @return True if this is equal to Object o
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateAccountForGroupRequestDTO that = (CreateAccountForGroupRequestDTO) o;
        return Objects.equals(accountID, that.accountID) &&
                Objects.equals(groupID, that.groupID) &&
                Objects.equals(accountDenomination, that.accountDenomination) &&
                Objects.equals(accountDescription, that.accountDescription) &&
                Objects.equals(personEmail, that.personEmail);
    }

    /**
     * Override of hashCode method
     *
     * @return Integer hashCode
     */
    @Override
    public int hashCode() {
        return Objects.hash(accountID, groupID, accountDenomination, accountDescription, personEmail);
    }
}
