package project.dto;

import org.springframework.hateoas.RepresentationModel;

import java.util.Objects;

public class CreateAccountForPersonResponseDTO extends RepresentationModel<CreateAccountForGroupResponseDTO> {

    private String personEmail;
    private String personName;
    private String accountID;
    private String accountDenomination;

    /**
     * Constructor for CreateAccountForPersonResponseDTO
     * <p>
     * This DTO is a response DTO for US006.
     *
     * @param personEmail         String of PersonID that owns the account
     * @param personName          String of the person's name
     * @param accountID           String of accountID created
     * @param accountDenomination String of the denomination of the account created
     */
    public CreateAccountForPersonResponseDTO(String personEmail, String personName, String accountID, String accountDenomination) {
        setPersonEmail(personEmail);
        setPersonName(personName);
        setAccountID(accountID);
        setAccountDenomination(accountDenomination);
    }

    /**
     * Method getPersonID
     *
     * @return String of PersonID
     */
    public String getPersonEmail() {
        return personEmail;
    }

    /**
     * Method setPersonID
     *
     * @param personEmail String of PersonID
     */
    private void setPersonEmail(String personEmail) {
        this.personEmail = personEmail;
    }

    /**
     * Method getPersonName
     *
     * @return personName
     */
    public String getPersonName() {
        return personName;
    }

    /**
     * Method setPersonName
     *
     * @param personName String of personName
     */
    private void setPersonName(String personName) {
        this.personName = personName;
    }

    /**
     * Method getAccountID
     *
     * @return String of accountID
     */
    public String getAccountID() {
        return accountID;
    }

    /**
     * Method setAccountID
     *
     * @param accountID String of accountID
     */
    private void setAccountID(String accountID) {
        this.accountID = accountID;
    }

    /**
     * Method getAccountDenomination
     *
     * @return String of accountDenomination
     */
    public String getAccountDenomination() {
        return accountDenomination;
    }

    /**
     * Method getAccountDenomination
     *
     * @param accountDenomination String of AccountDenomination
     */
    private void setAccountDenomination(String accountDenomination) {
        this.accountDenomination = accountDenomination;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateAccountForPersonResponseDTO that = (CreateAccountForPersonResponseDTO) o;
        return Objects.equals(personEmail, that.personEmail) &&
                Objects.equals(personName, that.personName) &&
                Objects.equals(accountID, that.accountID) &&
                Objects.equals(accountDenomination, that.accountDenomination);
    }

    @Override
    public int hashCode() {
        return Objects.hash(personEmail, personName, accountID, accountDenomination);
    }

}
