package project.dto;

import lombok.Data;
import org.springframework.hateoas.RepresentationModel;

import java.util.Objects;

@Data
public class CopyCategoryBetweenGroupsResponseDTO extends RepresentationModel<CopyCategoryBetweenGroupsResponseDTO> {

    private String groupID;
    private String groupDescription;
    private String newCategory;

    /**
     * Constructor for CreateCategoryResponseDTO
     *
     * @param groupID
     * @param groupDescription
     * @param newCategory
     */
    public CopyCategoryBetweenGroupsResponseDTO(String groupID, String groupDescription, String newCategory) {
        this.groupID = groupID;
        this.groupDescription = groupDescription;
        this.newCategory = newCategory;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CopyCategoryBetweenGroupsResponseDTO)) return false;
        if (!super.equals(o)) return false;
        CopyCategoryBetweenGroupsResponseDTO that = (CopyCategoryBetweenGroupsResponseDTO) o;
        return Objects.equals(groupID, that.groupID) &&
                Objects.equals(groupDescription, that.groupDescription) &&
                Objects.equals(newCategory, that.newCategory);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), groupID, groupDescription, newCategory);
    }
}
