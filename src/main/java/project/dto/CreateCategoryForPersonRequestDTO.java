package project.dto;

import java.util.Objects;

public class CreateCategoryForPersonRequestDTO {

    private String personID;
    private String designation;

    /**
     * Constructor for CreateCategoryForPersonRequestDTO
     *
     * @param personID    personID
     * @param designation designation of the Category
     */
    public CreateCategoryForPersonRequestDTO(String personID, String designation) {
        setPersonID(personID);
        setDesignation(designation);
    }


    /**
     * Method getPersonID
     *
     * @return personID
     */
    public String getPersonID() {
        return personID;
    }

    /**
     * Method setPersonID
     *
     * @param personID personID
     */
    private void setPersonID(String personID) {
        this.personID = personID;
    }

    /**
     * Method getDesignation
     *
     * @return category of Category
     */
    public String getDesignation() {
        return designation;
    }

    /**
     * Method setDesignation
     *
     * @param designation designation of Category
     */
    private void setDesignation(String designation) {
        this.designation = designation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateCategoryForPersonRequestDTO that = (CreateCategoryForPersonRequestDTO) o;
        return Objects.equals(personID, that.personID) &&
                Objects.equals(designation, that.designation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(personID, designation);
    }
}
