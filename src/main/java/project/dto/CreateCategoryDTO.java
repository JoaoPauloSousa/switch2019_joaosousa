package project.dto;

import org.springframework.hateoas.RepresentationModel;

import java.util.Collections;
import java.util.Objects;
import java.util.Set;

public class CreateCategoryDTO extends RepresentationModel<CreateCategoryDTO> {

    private Set<String> designation;

    /**
     * Constructor for CreateCategoryForGroupDTO
     */
    public CreateCategoryDTO(Set<String> designation) {
        setDesignation(designation);
    }

    public Set<String> getDesignation() {
        return Collections.unmodifiableSet(designation);
    }

    public void setDesignation(Set<String> designation) {
        this.designation = Collections.unmodifiableSet(designation);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateCategoryDTO that = (CreateCategoryDTO) o;
        return Objects.equals(designation, that.designation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), designation);
    }

    @Override
    public String toString() {
        return "CreateCategoryDTO{" +
                "designation=" + designation +
                '}';
    }
}
