package project.dto;

import project.model.entities.shared.Category;

import java.util.HashSet;
import java.util.Set;

public class CategoriesAssembler {

    /**
     * Constructor for CategoriesAssembler
     */
    CategoriesAssembler() {
        //Intentionally Empty
    }

    /**
     * mapToDTO method
     *
     * @param categories List of Categories
     * @return CategoriesDTO
     */
    public static CategoriesDTO mapToDTO(Set<Category> categories) {

        Set<CategoryDTO> categoriesDTOs = new HashSet<>();
        CategoryDTO categoryDTO;

        for (Category category : categories) {
            categoryDTO = new CategoryDTO(
                    category.getDesignation().toStringDTO());

            categoriesDTOs.add(categoryDTO);
        }
        return new CategoriesDTO(categoriesDTOs);
    }


}
