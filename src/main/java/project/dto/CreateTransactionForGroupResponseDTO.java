package project.dto;

import org.springframework.hateoas.RepresentationModel;

import java.util.Objects;

public class CreateTransactionForGroupResponseDTO extends RepresentationModel<CreateTransactionForGroupResponseDTO> {

    final String groupID;
    final String groupDescription;
    final String amount;
    final String type;
    final String transactionDescription;

    /**
     * Constructor of CreateTransactionForGroupResponseDTO class
     *
     * @param groupID                The identity of the group to whom the transaction was created
     * @param groupDescription       The description of the group to whom the transaction was created
     * @param amount                 The amount of the created transaction
     * @param type                   The type of the created transaction
     * @param transactionDescription The description of the created transaction
     */
    public CreateTransactionForGroupResponseDTO(String groupID, String groupDescription, String amount, String type,
                                                String transactionDescription) {

        this.groupID = groupID;
        this.groupDescription = groupDescription;
        this.amount = amount;
        this.type = type;
        this.transactionDescription = transactionDescription;
    }

    /**
     * Method getGroupID
     * gets the groupID attribute from responseDTO
     *
     * @return groupID as a String
     */
    public String getGroupID() {
        return groupID;
    }

    /**
     * Method getGroupDescription
     * gets the groupDescription attribute from responseDTO
     *
     * @return groupDescription as a String
     */
    public String getGroupDescription() {
        return groupDescription;
    }

    /**
     * Method getAmount
     * gets the amount attribute from responseDTO
     *
     * @return amount as a String
     */
    public String getAmount() {
        return amount;
    }

    /**
     * Method getType
     * gets the transaction type attribute from responseDTO
     *
     * @return type as a String
     */
    public String getType() {
        return type;
    }

    /**
     * Method getTransactionDescription
     * gets the transactionDescription attribute from responseDTO
     *
     * @return transactionDescription as a String
     */
    public String getTransactionDescription() {
        return transactionDescription;
    }

    /**
     * Override of equals method
     *
     * @param o Object to be compared to this
     * @return True if this is equal to Object o
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateTransactionForGroupResponseDTO that = (CreateTransactionForGroupResponseDTO) o;
        return Objects.equals(groupID, that.groupID) &&
                Objects.equals(groupDescription, that.groupDescription) &&
                Objects.equals(amount, that.amount) &&
                Objects.equals(type, that.type) &&
                Objects.equals(transactionDescription, that.transactionDescription);
    }

    /**
     * Override of hashCode method
     *
     * @return Integer hashCode
     */
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), groupID, groupDescription, amount, type, transactionDescription);
    }
}
