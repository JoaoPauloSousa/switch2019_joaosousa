package project.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.hateoas.RepresentationModel;

import java.util.Collections;
import java.util.List;

@Getter
@EqualsAndHashCode(callSuper = false)
public class AccountsDTO extends RepresentationModel<AccountsDTO> {

    private List<AccountDTO> accounts;

    /**
     * Constructor for AccountsDTO
     *
     * @param accounts List of accountSTOs
     */
    public AccountsDTO(List<AccountDTO> accounts) {
        this.accounts = Collections.unmodifiableList(accounts);
    }

    @Override
    public String toString() {
        return "AccountsDTO{" +
                "accounts=" + accounts +
                '}';
    }
}
