package project.dto;

import lombok.Data;

import java.util.Objects;

@Data
public class CopyCategoryBetweenGroupsRequestDTO {
    private String designation;
    private String groupID1;
    private String groupID2;
    private String personEmail;


    public CopyCategoryBetweenGroupsRequestDTO(String designation, String groupID1, String groupID2, String personEmail) {
        this.designation = designation;
        this.groupID1 = groupID1;
        this.groupID2 = groupID2;
        this.personEmail = personEmail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CopyCategoryBetweenGroupsRequestDTO)) return false;
        CopyCategoryBetweenGroupsRequestDTO that = (CopyCategoryBetweenGroupsRequestDTO) o;
        return Objects.equals(designation, that.designation) &&
                Objects.equals(groupID1, that.groupID1) &&
                Objects.equals(groupID2, that.groupID2) &&
                Objects.equals(personEmail, that.personEmail);
    }

    @Override
    public int hashCode() {
        return Objects.hash(designation, groupID1, groupID2, personEmail);
    }
}
