package project.dto;

import project.model.entities.Categories;
import project.model.entities.group.Group;
import project.model.entities.shared.Category;

import java.util.HashSet;
import java.util.Set;

public final class CreateCategoryForGroupAssembler {

    /**
     * Constructor for CreateCategoryForGroupAssembler
     * Created to override the public constructor Java creates automatically if there is not a constructor for a class
     */
    CreateCategoryForGroupAssembler() {
        //Intentionally empty
    }

    /**
     * method mapToResponseDTO
     *
     * @param group instance of group
     * @return instance of CategoriesDTO
     */
    public static CreateCategoryDTO mapToDTO(Group group) {

        Categories categoriesVO = group.getCategories();             // ir a group buscar o VO categories
        Set<Category> categories = categoriesVO.getCategoriesValue();     // ir ao VO categories buscar cada categoria

        Set<String> categoriesStrings = new HashSet<>();             // set onde vao ser guardadas a categorias

        for (Category category : categories) {
            categoriesStrings.add(category.toStringDTO());           // adiciona as categorias uma a uma ao Set criado, no formato string
        }

        return new CreateCategoryDTO(categoriesStrings);
    }

    /**
     * Method mapToDtoRequest
     *
     * @param personEmail personEmail
     * @param groupID     groupID
     * @param designation designation of category
     * @return instance of CreateCategoryForGroupRequestDTO
     */
    public static CreateCategoryForGroupRequestDTO mapToRequestDTO(String personEmail, String groupID, String designation) {
        return new CreateCategoryForGroupRequestDTO(personEmail, groupID, designation);
    }

    /**
     * Method mapToResponseDTO
     *
     * @param groupID
     * @param groupDescription
     * @param newCategory
     * @return instance of CreateCategoryForGroupResponseDTO
     */
    public static CreateCategoryForGroupResponseDTO mapToResponseDTO(String groupID, String groupDescription, String newCategory) {
        return new CreateCategoryForGroupResponseDTO(groupID, groupDescription, newCategory);
    }

}
