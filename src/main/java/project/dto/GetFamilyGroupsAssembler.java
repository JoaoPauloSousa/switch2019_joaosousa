package project.dto;

import project.model.entities.group.GroupID;

import java.util.HashSet;
import java.util.Set;

public final class GetFamilyGroupsAssembler {

    /**
     * Constructor for GetFamilyGroupsAssembler
     * Created to override the public constructor Java creates automatically if there is not a constructor for a class
     */
    GetFamilyGroupsAssembler() {
        //Intentionally empty
    }

    /**
     * mapToDTO method
     *
     * @param groupsIDS
     * @return
     */
    public static GetFamilyGroupsDTO mapToDTO(Set<GroupID> groupsIDS) {

        Set<String> groupsIDSInString = new HashSet<>();

        for (GroupID groupId : groupsIDS) {
            groupsIDSInString.add(groupId.toStringDTO());
        }

        return new GetFamilyGroupsDTO(groupsIDSInString);
    }
}
