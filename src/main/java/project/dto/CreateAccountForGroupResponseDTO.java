package project.dto;

import org.springframework.hateoas.RepresentationModel;

import java.util.Objects;

public class CreateAccountForGroupResponseDTO extends RepresentationModel<CreateAccountForGroupResponseDTO> {

    final String groupID;
    final String groupDescription;
    final String accountID;
    final String accountDenomination;

    /**
     * Constructor of CreateAccountForGroupResponseDTO class
     *
     * @param groupID             The identity of the group in which the new account was added
     * @param groupDescription    The identity of the group in which the new account was added a String
     * @param accountID           The identity of the new account created as a String
     * @param accountDenomination The denomination of the new account created as a String
     */
    public CreateAccountForGroupResponseDTO(String groupID, String groupDescription, String accountID, String accountDenomination) {
        this.groupID = groupID;
        this.groupDescription = groupDescription;
        this.accountID = accountID;
        this.accountDenomination = accountDenomination;
    }

    /**
     * Method getGroupID
     * gets the groupID attribute from responseDTO
     *
     * @return groupID as a String
     */
    public String getGroupID() {
        return groupID;
    }

    /**
     * Method getGroupDescription
     * gets the groupDescription attribute from responseDTO
     *
     * @return group's description as a String
     */
    public String getGroupDescription() {
        return groupDescription;
    }

    /**
     * Method getAccountID
     * gets the accountID attribute from responseDTO
     *
     * @return accountID as a String
     */
    public String getAccountID() {
        return accountID;
    }

    /**
     * Method getAccountDenomination
     * gets the accountDenomination attribute from responseDTO
     *
     * @return account's description as a String
     */
    public String getAccountDenomination() {
        return accountDenomination;
    }

    /**
     * Override of equals method
     *
     * @param o Object
     * @return boolean (True if this Object is equal to Object o and False otherwise)
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateAccountForGroupResponseDTO that = (CreateAccountForGroupResponseDTO) o;
        return Objects.equals(groupID, that.groupID) &&
                Objects.equals(groupDescription, that.groupDescription) &&
                Objects.equals(accountID, that.accountID) &&
                Objects.equals(accountDenomination, that.accountDenomination);
    }

    /**
     * Override of hashCode method
     *
     * @return integer
     */
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), groupID, groupDescription, accountID, accountDenomination);
    }
}
