package project.dto;

import project.model.entities.ledger.Transaction;

import java.util.ArrayList;
import java.util.List;

public final class TransactionsAssembler {

    /**
     * Constructor for TransactionsAssembler
     * Created to override the public constructor Java creates automatically if there is not a constructor for a class
     */
    TransactionsAssembler() {
        //Intentionally empty
    }

    /**
     * mapToDTO method
     *
     * @param transactions Set of transactions as strings
     * @return
     */
    public static TransactionsResponseDTO mapToDTO(List<Transaction> transactions) {

        List<TransactionDTO> transactionsDTOs = new ArrayList<>();

        TransactionDTO transactionDTO;

        for (Transaction transaction : transactions) {

            transactionDTO = new TransactionDTO(
                    transaction.getAmount().toStringDTO(),
                    transaction.getType().toStringDTO(),
                    transaction.getDateTime().toStringDTO(),
                    transaction.getDescription().toStringDTO(),
                    transaction.getCategory().toStringDTO(),
                    transaction.getDebitAccountID().toStringDTO(),
                    transaction.getCreditAccountID().toStringDTO()
            );

            transactionsDTOs.add(transactionDTO);

        }

        return new TransactionsResponseDTO(transactionsDTOs);
    }
}
