package project.application.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.CreateCategoryForPersonAssembler;
import project.dto.CreateCategoryForPersonRequestDTO;
import project.dto.CreateCategoryForPersonResponseDTO;
import project.exceptions.CategoryAlreadyExistsException;
import project.exceptions.PersonNotFoundException;
import project.frameworkddd.IUSCreateCategoryForPersonService;
import project.model.entities.person.Person;
import project.model.entities.shared.PersonID;
import project.model.specifications.repositories.PersonRepository;

import java.util.Optional;

@Service
public class CreateCategoryForPersonService implements IUSCreateCategoryForPersonService {

    @Autowired
    PersonRepository personRepository;

    /**
     * Constructor for CreateCategoryForPersonService
     *
     * @param personRepository Instance of PersonRepository containing all people
     */
    public CreateCategoryForPersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    /**
     * Method createCategoryForPerson
     * Add a Category to categories of the person
     * verify if personRepository contains personID
     *
     * @param requestDTO CreateCategoryForPersonRequestDTO
     * @return If the person exists in the personRepository, but the category already exists: throws CategoryAlreadyExistsException
     */
    public CreateCategoryForPersonResponseDTO createCategoryForPerson(CreateCategoryForPersonRequestDTO requestDTO) {
        PersonID personID = new PersonID(requestDTO.getPersonID());
        Optional<Person> optPerson = personRepository.findById(personID);

        if (optPerson.isPresent()) {
            Person person = optPerson.get();
            String designation = requestDTO.getDesignation();
            if (person.addCategory(designation)) {
                Person savedPerson = personRepository.save(person);
                return CreateCategoryForPersonAssembler.mapToResponseDTO(
                        savedPerson.getID().toStringDTO(),
                        designation);
            } else {
                throw new CategoryAlreadyExistsException();
            }
        } else {
            throw new PersonNotFoundException();
        }
    }

}
