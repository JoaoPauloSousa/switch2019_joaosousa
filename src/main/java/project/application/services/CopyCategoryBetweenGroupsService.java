package project.application.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.CopyCategoryBetweenGroupsRequestDTO;
import project.dto.CopyCategoryBetweenGroupsResponseDTO;
import project.dto.CopyCategoryForGroupsAssembler;
import project.exceptions.CategoryNotFoundException;
import project.exceptions.GroupConflictException;
import project.exceptions.GroupNotFoundException;
import project.exceptions.PersonNotFoundException;
import project.frameworkddd.IUSCopyCategoryBetweenGroupsService;
import project.model.entities.group.Group;
import project.model.entities.group.GroupID;
import project.model.entities.person.Person;
import project.model.entities.shared.Category;
import project.model.entities.shared.PersonID;
import project.model.specifications.repositories.GroupRepository;
import project.model.specifications.repositories.PersonRepository;

import java.util.Optional;

@Service
public class CopyCategoryBetweenGroupsService implements IUSCopyCategoryBetweenGroupsService {

    @Autowired
    GroupRepository groupRepo;

    @Autowired
    PersonRepository personRepo;

    /**
     * Method copyCategoryBetweenGroups
     *
     * @param requestDTO
     * @return
     */
    public CopyCategoryBetweenGroupsResponseDTO copyCategoryBetweenGroups(CopyCategoryBetweenGroupsRequestDTO requestDTO) {
        PersonID personID = new PersonID(requestDTO.getPersonEmail());
        Category designation = new Category(requestDTO.getDesignation());

        Optional<Person> optPerson = personRepo.findById(new PersonID(requestDTO.getPersonEmail()));
        if (!optPerson.isPresent())
            throw new PersonNotFoundException();

        Optional<Group> optGroup1 = groupRepo.findById(new GroupID(requestDTO.getGroupID1()));
        if (!optGroup1.isPresent())
            throw new GroupNotFoundException();
        Group group1 = optGroup1.get();

        Optional<Group> optGroup2 = groupRepo.findById(new GroupID(requestDTO.getGroupID2()));
        if (!optGroup2.isPresent())
            throw new GroupNotFoundException();
        Group group2 = optGroup2.get();

        if (!group1.hasManagerID(personID))
            throw new GroupConflictException("The person with " + personID + " doesn't have manager privileges");

        if (!group2.hasManagerID(personID))
            throw new GroupConflictException("The person with " + personID + " doesn't have manager privileges");

        if (!group1.getCategories().hasCategory(designation))
            throw new CategoryNotFoundException();

        group2.addCategory(requestDTO.getDesignation());

        Group savedGroup = groupRepo.save(group2);

        return CopyCategoryForGroupsAssembler.mapToResponseDTO(savedGroup.getID().toStringDTO(), savedGroup.getDescription().getDescriptionValue(), requestDTO.getDesignation());

    }
}


