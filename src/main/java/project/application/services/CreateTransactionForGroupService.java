package project.application.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.CreateTransactionForGroupAssembler;
import project.dto.CreateTransactionForGroupRequestDTO;
import project.dto.CreateTransactionForGroupResponseDTO;
import project.exceptions.*;
import project.frameworkddd.IUSCreateTransactionForGroupService;
import project.model.entities.group.Group;
import project.model.entities.group.GroupID;
import project.model.entities.ledger.Ledger;
import project.model.entities.ledger.Type;
import project.model.entities.shared.AccountID;
import project.model.entities.shared.Category;
import project.model.entities.shared.PersonID;
import project.model.specifications.repositories.AccountRepository;
import project.model.specifications.repositories.GroupRepository;
import project.model.specifications.repositories.LedgerRepository;
import project.model.specifications.repositories.PersonRepository;

import java.util.Optional;
import java.util.Set;

@Service
public class CreateTransactionForGroupService implements IUSCreateTransactionForGroupService {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private LedgerRepository ledgerRepository;

    @Autowired
    private AccountRepository accountRepository;

    /**
     * createTransactionForGroup method
     * <p>
     * This method intends to create a new transaction and add it to the group's ledger.
     * Throws exceptions with the following scenarios:
     * - Group to be added the new transaction doesn't exist
     * - Ledger doesn't exist in LedgerRepository
     * - The credit account doesn't exist in AccountRepository
     * - The debit account doesn't exist in AccountRepository
     * - The input type of the transaction is invalid
     *
     * @param requestDTO a data transfer object with the necessary information to create a transaction for the group
     * @return a response DTO with some information regarding the created transaction if successfully created
     */
    @Override
    public CreateTransactionForGroupResponseDTO createTransactionForGroup(CreateTransactionForGroupRequestDTO requestDTO) {

        Optional<Group> groupOpt = groupRepository.findById(new GroupID(requestDTO.getGroupID()));
        if (!groupOpt.isPresent())
            throw new GroupNotFoundException();

        Group group = groupOpt.get();
        Optional<Ledger> ledgerOpt = ledgerRepository.findById(group.getLedgerID());
        if (!ledgerOpt.isPresent())
            throw new LedgerNotFoundException();

        if (!group.hasMemberID(new PersonID(requestDTO.getPersonEmail())))
            throw new GroupConflictException("Person is not member of the group");

        AccountID debitAccountId = new AccountID(requestDTO.getDebitAccountID());
        if (!accountRepository.existsById(debitAccountId))
            throw new AccountNotFoundException("Debit account not found");

        AccountID creditAccountId = new AccountID(requestDTO.getCreditAccountID());
        if (!accountRepository.existsById(creditAccountId))
            throw new AccountNotFoundException("Credit account not found");

        Category category = new Category(requestDTO.getCategoryDesignation());
        if (!group.getCategories().hasCategory(category))
            throw new CategoryNotFoundException();

        Set<AccountID> groupAccountsIds = group.getAccountsIDs().getAccountsIDsValue();

        Type type;
        if (requestDTO.getType().equalsIgnoreCase("DEBIT")) {
            type = Type.DEBIT;
            if (!groupAccountsIds.contains(debitAccountId))
                throw new AccountNotFoundException("Debit account is not owned by the group");
        } else if (requestDTO.getType().equalsIgnoreCase("CREDIT")) {
            type = Type.CREDIT;
            if (!groupAccountsIds.contains(creditAccountId))
                throw new AccountNotFoundException("Credit account is not owned by the group");
        } else throw new InvalidFieldException();

        Ledger groupLedger = ledgerOpt.get();
        groupLedger.addTransaction(requestDTO.getAmount(),
                type,
                requestDTO.getDateTime(),
                requestDTO.getDescription(),
                new Category(requestDTO.getCategoryDesignation()),
                new AccountID(requestDTO.getDebitAccountID()),
                new AccountID(requestDTO.getCreditAccountID()));

        ledgerRepository.save(groupLedger);

        return CreateTransactionForGroupAssembler.mapToResponseDTO(group.getID().toStringDTO(),
                group.getDescription().getDescriptionValue(), requestDTO.getAmount(), requestDTO.getType(),
                requestDTO.getDescription());
    }

}
