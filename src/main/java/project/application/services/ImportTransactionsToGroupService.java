package project.application.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.ImportTransactionsToGroupRequestDTO;
import project.dto.ImportTransactionsToGroupResponseDTO;
import project.exceptions.GroupConflictException;
import project.exceptions.GroupNotFoundException;
import project.exceptions.LedgerNotFoundException;
import project.exceptions.PersonNotFoundException;
import project.frameworkddd.IUSImportTransactionsToGroupService;
import project.model.entities.group.Group;
import project.model.entities.group.GroupID;
import project.model.entities.ledger.Ledger;
import project.model.entities.person.Person;
import project.model.entities.shared.PersonID;
import project.model.specifications.repositories.GroupRepository;
import project.model.specifications.repositories.LedgerRepository;
import project.model.specifications.repositories.PersonRepository;

import java.util.Optional;

@Service
public class ImportTransactionsToGroupService implements IUSImportTransactionsToGroupService {

    @Autowired
    IUSImportTransactionsToGroupService service;

    @Autowired
    GroupRepository groupRepo;

    @Autowired
    PersonRepository personRepo;

    @Autowired
    private LedgerRepository ledgerRepository;


    //TODO: Não consegui acabar de implementar o serviço

    /**
     * Method importTransactionsToGroup
     *
     * @param requestDTO
     * @return
     */
    public ImportTransactionsToGroupResponseDTO importTransactionsToGroup(ImportTransactionsToGroupRequestDTO requestDTO) {

        PersonID personID = new PersonID(requestDTO.getPersonEmail());

        Optional<Person> optPerson = personRepo.findById(new PersonID(requestDTO.getPersonEmail()));
        if (!optPerson.isPresent())
            throw new PersonNotFoundException();

        Optional<Group> optGroup = groupRepo.findById(new GroupID(requestDTO.getGroupID()));
        if (!optGroup.isPresent())
            throw new GroupNotFoundException();
        Group group = optGroup.get();

        Optional<Ledger> ledgerOpt = ledgerRepository.findById(group.getLedgerID());
        if (!ledgerOpt.isPresent())
            throw new LedgerNotFoundException();

        if (!group.hasManagerID(personID))
            throw new GroupConflictException("The person with " + personID + " doesn't have manager privileges");


        return null;
    }
}
