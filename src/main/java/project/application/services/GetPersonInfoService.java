package project.application.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.GetPersonInfoResponseDTO;
import project.dto.PersonAssembler;
import project.frameworkddd.IUSAddMemberToGroupService;
import project.frameworkddd.IUSGetPersonInfoService;
import project.model.entities.person.Person;
import project.model.entities.shared.PersonID;

@Service
public class GetPersonInfoService implements IUSGetPersonInfoService {

    @Autowired
    IUSAddMemberToGroupService service;

    /**
     * Constructor for GetPersonInfoService
     */
    public GetPersonInfoService() {
        //Intentionally empty
    }

    /**
     * Method getPersonInfo
     * Get information of a person given the Id of that person
     *
     * @param personID
     */
    public GetPersonInfoResponseDTO getPersonInfo(String personID) {
        PersonID personIdDomain = new PersonID(personID);

        Person person = service.getPersonByID(personIdDomain);

        return PersonAssembler.mapToResponseDTO(person.getID().toStringDTO(),
                person.getName().toStringDTO(),
                person.getAddress().getAddressValue(),
                person.getBirthDate().getBirthdateValue(),
                person.getBirthPlace().getBirthplaceValue());
    }
}