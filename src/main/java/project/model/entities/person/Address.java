package project.model.entities.person;

import project.exceptions.InvalidFieldException;
import project.frameworkddd.ValueObject;

import java.util.Objects;

public final class Address implements ValueObject {

    String addressValue;

    public Address(String address) {
        setAddressValue(address);
    }

    /**
     * Create attribute address
     * Throws exception if string invalid (null, empty or composed by just spaces)
     *
     * @param addressValue address string
     */
    private void setAddressValue(String addressValue) {
        if (addressValue != null && !addressValue.isEmpty() && !addressValue.trim().isEmpty()) {
            this.addressValue = addressValue;
        } else {
            throw new InvalidFieldException("Input 'address' is invalid!");
        }
    }

    /**
     * Override of equals
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Address that = (Address) o;
        return Objects.equals(addressValue, that.addressValue);
    }

    /**
     * Override of hashCode
     */
    @Override
    public int hashCode() {
        return Objects.hash(addressValue);
    }

    /**
     * Override of toString
     */
    @Override
    public String toString() {
        return "Address{" +
                "address='" + addressValue + '\'' +
                '}';
    }

    public String getAddressValue() {
        return addressValue;
    }
}
