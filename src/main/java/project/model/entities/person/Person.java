package project.model.entities.person;

import org.apache.commons.lang3.Validate;
import project.frameworkddd.Root;
import project.model.entities.Birthdate;
import project.model.entities.Categories;
import project.model.entities.shared.*;

import java.util.Objects;

public class Person implements Root<PersonID> {

    private PersonsIDs siblingsIDs;
    private final AccountsIDs accountsIDs;
    private final Categories categories;
    private PersonID id;
    private Name name;
    private Address address;
    private Birthplace birthPlace;
    private Birthdate birthDate;
    private PersonID motherID;
    private PersonID fatherID;
    private LedgerID ledgerID;

    /**
     * Constructor for Class Person
     *
     * @param name       Name of person
     * @param address    Address of person
     * @param birthPlace Birthplace of person
     * @param birthDate  Birthdate of person
     * @param motherID   PersonID representing the ID of this Person's mother
     * @param fatherID   PersonID representing fatherID
     */
    public Person(PersonID personID, String name, String address, String birthPlace, String birthDate,
                  PersonID motherID, PersonID fatherID, LedgerID ledgerID) {
        setID(personID);
        setName(name);
        setAddress(address);
        setBirthPlace(birthPlace);
        setBirthDate(birthDate);
        setMother(motherID);
        setFather(fatherID);
        accountsIDs = new AccountsIDs();
        categories = new Categories();
        siblingsIDs = new PersonsIDs();
        setLedgerID(ledgerID);
    }

    public Address getAddress() {
        return address;
    }

    /**
     * Create attribute address
     *
     * @param address Address of person
     */
    private void setAddress(String address) {
        this.address = new Address(address);
    }

    public Birthplace getBirthPlace() {
        return birthPlace;
    }

    /**
     * Create attribute birthPlace
     *
     * @param birthPlace Birthplace of person
     */
    private void setBirthPlace(String birthPlace) {
        this.birthPlace = new Birthplace(birthPlace);
    }

    public Birthdate getBirthDate() {
        return birthDate;
    }

    /**
     * Create attribute birthDate
     *
     * @param birthDate Birth date of person
     */
    private void setBirthDate(String birthDate) {
        this.birthDate = new Birthdate(birthDate);
    }

    public LedgerID getLedgerID() {
        return ledgerID;
    }

    /**
     * Set LedgerId
     * Throws exception if null
     *
     * @param ledgerID ID of ledger
     */
    private void setLedgerID(LedgerID ledgerID) {
        Validate.notNull(ledgerID);
        this.ledgerID = ledgerID;
    }

    /**
     * Create motherID reference
     *
     * @param motherID Person to add to attribute
     */
    private void setMother(PersonID motherID) {
        this.motherID = motherID;
    }

    /**
     * Create fatherID reference
     *
     * @param fatherID father Person to add to attribute
     */
    private void setFather(PersonID fatherID) {
        this.fatherID = fatherID;
    }

    /**
     * Add sibling to a person
     *
     * @param sibling New sibling to add
     */
    public boolean addSibling(Person sibling) {
        PersonID siblingID = sibling.getID();
        boolean result = this.siblingsIDs.addPersonID(siblingID);
        if (!sibling.siblingsIDs.containsPersonID(this.id)) {
            sibling.addSibling(this);
        }
        return result;
    }

    /**
     * Check if newPerson is sibling of Person.
     * True if same mother OR same father OR contained in list of siblings
     *
     * @param newPerson Person to compare
     */
    public boolean isSibling(Person newPerson) {
        return (this.fatherID != null && this.motherID != null) && (this.fatherID.equals(newPerson.fatherID) || this.motherID.equals(newPerson.motherID) || this.siblingsIDs.containsPersonID(newPerson.id));
    }

    /**
     * Check if newPerson is father of Person.
     *
     * @param newPersonID PersonID to check if is father
     * @return True if newPerson is father
     */
    public boolean hasFather(PersonID newPersonID) {
        return this.fatherID != null && this.fatherID.equals(newPersonID);
    }

    /**
     * Check if newPerson is mother of Person.
     *
     * @param newPersonID PersonID to check if is mother
     * @return True if newPerson is mother
     */
    public boolean hasMother(PersonID newPersonID) {
        return this.motherID != null && this.motherID.equals(newPersonID);
    }

    /**
     * Add a category to listOfCategories
     * Delegates functionality to ListOfCategories class
     *
     * @param designation Designation of category to add
     * @return True/False if successful, Exception from ListOfCategories
     */
    public boolean addCategory(String designation) {
        return this.categories.addCategory(designation);
    }

    /**
     * Add an account to AccountID
     * Delegates functionality to AccountID class
     *
     * @param accountID ID of account
     * @return true/false if added
     */
    public boolean addAccount(AccountID accountID) {
        return accountsIDs.addAccountID(accountID);
    }

    /**
     * Method sameIdentityAs
     *
     * @param other New person to compare identity
     * @return True if this Person has same id as other
     */
    public boolean sameIdentityAs(Person other) {
        return this.id.sameValueAs(other.id);
    }

    /**
     * Method getPersonID()
     *
     * @return this person's id
     */
    public PersonID getID() {
        return id;
    }

    /**
     * Set PersonId
     * Throws exception if null
     *
     * @param id ID of person
     */
    private void setID(PersonID id) {
        Validate.notNull(id);
        this.id = id;
    }

    /**
     * Method getName()
     *
     * @return this person's name
     */
    public Name getName() {
        return name;
    }

    /**
     * Create attribute name
     *
     * @param name Name of person
     */
    private void setName(String name) {
        this.name = new Name(name);
    }

    /**
     * Method getAccountsIDs
     *
     * @return Accounts IDs of person
     */
    public AccountsIDs getAccountsIDs() {
        return this.accountsIDs;
    }

    /**
     * Method getCategories
     *
     * @return Categories of person
     */
    public Categories getCategories() {
        return this.categories;
    }

    /**
     * Method hasAccountID
     *
     * @param accountID accountID
     * @return true if contains, false if not
     */
    public boolean hasAccountID(AccountID accountID) {
        return accountsIDs.getAccountsIDsValue().contains(accountID);
    }

    public PersonID getMotherID() {
        return motherID;
    }

    public PersonID getFatherID() {
        return fatherID;
    }

    public PersonsIDs getSiblingsIDs() {
        return this.siblingsIDs;
    }

    /**
     * Override of Equals
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Person other = (Person) o;
        return sameIdentityAs(other);
    }

    /**
     * Override of HashCode
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, name, address, birthPlace, birthDate, motherID, fatherID, ledgerID);
    }

    @Override
    public boolean sameAs(Object other) {
        return false;
    }

    /**
     * Override of toString
     */
    @Override
    public String toString() {
        return "Person{" +
                "siblingsIDs=" + siblingsIDs +
                ", accountsIDs=" + accountsIDs +
                ", id=" + id +
                ", categories=" + categories +
                ", name=" + name +
                ", address=" + address +
                ", birthPlace=" + birthPlace +
                ", birthDate=" + birthDate +
                ", motherID=" + motherID +
                ", fatherID=" + fatherID +
                ", ledgerID=" + ledgerID +
                '}';
    }

    public void setSiblingsIDs(PersonsIDs siblingsIDs) {
        this.siblingsIDs = siblingsIDs;
    }
}