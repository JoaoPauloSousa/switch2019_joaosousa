package project.model.entities.ledger;

import project.frameworkddd.ValueObject;

public enum Type implements ValueObject {

    DEBIT(-1),
    CREDIT(1);

    int typeValue;


    /**
     * Constructor for Type
     *
     * @param type String of description
     */
    Type(int type) {

        this.typeValue = type;
    }

    /**
     * Override of toString.
     */
    @Override
    public String toString() {
        return "Type{" +
                "type=" + typeValue +
                '}';
    }

    public String toStringDTO() {
        return "" + typeValue;
    }
}