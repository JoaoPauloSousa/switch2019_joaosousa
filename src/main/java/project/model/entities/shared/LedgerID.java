package project.model.entities.shared;

import lombok.NoArgsConstructor;
import project.frameworkddd.ValueObject;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
@NoArgsConstructor
public class LedgerID implements ValueObject, Serializable {

    private Long ledgerIdValue;

    /**
     * Constructor for LedgerID
     *
     * @param ledgerID unique identifier number of LedgerID
     */
    public LedgerID(String ledgerID) {
        this.ledgerIdValue = Long.parseLong(ledgerID);
    }

    /**
     * Compares internal attribute id of two LedgerID objects
     * Condition to be verified in equals() override
     *
     * @param other
     * @return
     */
    public boolean sameValueAs(LedgerID other) {
        return other != null && this.ledgerIdValue.equals(other.ledgerIdValue);
    }

    /**
     * Get attribute id
     *
     * @return id attribute
     */
    public Long getLedgerIdValue() {
        return ledgerIdValue;
    }

    /**
     * Method toStringDTO
     *
     * @return id attribute converted to String
     */
    public String toStringDTO() {
        return ledgerIdValue.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LedgerID)) return false;
        LedgerID other = (LedgerID) o;
        return sameValueAs(other);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ledgerIdValue);
    }

    @Override
    public String toString() {
        return "LedgerID{" +
                "ledgerID=" + ledgerIdValue +
                '}';
    }
}
