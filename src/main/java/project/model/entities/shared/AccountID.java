package project.model.entities.shared;

import lombok.Data;
import lombok.NoArgsConstructor;
import project.frameworkddd.ValueObject;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Data
@Embeddable
@NoArgsConstructor
public class AccountID implements ValueObject, Serializable {

    private Long accountIdValue;

    /**
     * Constructor for AccountID
     *
     * @param accountID unique identifier number of AccountID
     */
    public AccountID(String accountID) {
        this.accountIdValue = Long.parseLong(accountID);
    }

    /**
     * Compares internal attribute id of two AccountID objects
     * Condition to be verified in equals() override
     *
     * @param other
     * @return
     */
    public boolean sameValueAs(AccountID other) {
        return other != null && this.accountIdValue.equals(other.accountIdValue);
    }

    /**
     * Get attribute id
     *
     * @return id attribute
     */
    public Long getAccountIdValue() {
        return accountIdValue;
    }

    /**
     * Method toStringDTO
     *
     * @return id attribute converted to String
     */
    public String toStringDTO() {
        return this.accountIdValue.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AccountID)) {
            return false;
        }
        AccountID other = (AccountID) o;
        return sameValueAs(other);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountIdValue);
    }

    @Override
    public String toString() {
        return "AccountID{" +
                "accountID=" + accountIdValue +
                '}';
    }
}
