package project.model.entities.shared;

import lombok.Data;
import lombok.NoArgsConstructor;
import project.exceptions.InvalidFieldException;
import project.frameworkddd.ValueObject;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Data
@NoArgsConstructor
@Embeddable
public class Description implements ValueObject, Serializable {

    private String descriptionValue;

    /**
     * Constructor for Description
     *
     * @param description String of description
     */
    public Description(String description) {
        setDescriptionValue(description);
    }

    /**
     * Override equals
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Description newDescription = (Description) o;
        return (this.descriptionValue.equals(newDescription.descriptionValue));
    }

    /**
     * Override hashCode
     */
    @Override
    public int hashCode() {
        return Objects.hash(this.descriptionValue);
    }

    /**
     * Override toString
     */
    @Override
    public String toString() {
        return "Description{" +
                "description='" + descriptionValue + '\'' +
                '}';
    }

    public String toStringDTO() {
        return descriptionValue;
    }

    public String getDescriptionValue() {
        return descriptionValue;
    }

    /**
     * Create attribute description
     * throw a exception if the string (null, empty or composed by just spaces)
     *
     * @param descriptionValue Description
     */
    private void setDescriptionValue(String descriptionValue) {
        if (descriptionValue != null && !descriptionValue.isEmpty() && !descriptionValue.trim().isEmpty()) {
            this.descriptionValue = descriptionValue;
        } else {
            throw new InvalidFieldException("Input 'description' is invalid!");
        }
    }
}
