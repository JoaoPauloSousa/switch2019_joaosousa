package project.model.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.model.entities.group.Group;
import project.model.entities.person.Person;
import project.model.entities.shared.PersonID;
import project.model.specifications.repositories.PersonRepository;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class IsFamilyService {

    @Autowired
    PersonRepository personRepository;

    public IsFamilyService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    /**
     * Method getMembers
     * Retrieves all members from repository that belong to a group
     *
     * @return Set<Person> containing all Person instances
     */
    public Set<Person> getMembers(Group group) {
        Set<PersonID> memberIDs = group.getMembersIDs().getPersonIDs();
        Set<Person> members = new HashSet<>();

        Person person;

        for (PersonID memberID : memberIDs) {
            Optional<Person> personOpt = this.personRepository.findById(memberID);
            if (personOpt.isPresent()) {
                person = personOpt.get();
                members.add(person);
            }
        }
        return members;
    }

    /**
     * Method checkIfGroupContainsBothParents
     * Checks if there is any member of a group that has both his mother and father in the group
     *
     * @return True if successful
     */
    public boolean checkIfGroupContainsBothParents(Group group) {
        Set<Person> groupMembers = getMembers(group);
        boolean parentsVerification = false;

        for (Person childMember : groupMembers) {
            for (Person fatherMember : groupMembers) {
                for (Person motherMember : groupMembers) {
                    if (childMember.hasFather(fatherMember.getID()) && childMember.hasMother(motherMember.getID())) {
                        parentsVerification = true;
                    }
                }
            }
        }

        return parentsVerification;
    }

    /**
     * Method get NumberOfChildren
     * Get the number of members of a group that have at least one of his parents inside the group
     *
     * @return integer of number of children
     */
    public int getNumberOfChildren(Group group) {
        Set<Person> groupMembers = getMembers(group);
        int childCounter = 0;

        for (Person childMember : groupMembers) {
            for (Person parentMember : groupMembers) {
                if (childMember.hasFather(parentMember.getID()) || childMember.hasMother(parentMember.getID())) {
                    childCounter++;
                    break;
                }
            }
        }

        return childCounter;
    }

    /**
     * Method isFamilyGroup
     * Checks if a group is a family group (There must be a child with both his mother and father within the group and
     * the group may contain other children of at least one of the these parents)
     *
     * @return true if the group is a family group
     */
    public boolean isFamily(Group group) {
        Set<Person> groupMembers = getMembers(group);
        int numberOfMembers = groupMembers.size();


        return checkIfGroupContainsBothParents(group) && numberOfMembers == getNumberOfChildren(group) + 2;
    }

}
