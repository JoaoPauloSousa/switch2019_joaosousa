package project.model.specifications.repositories;

import project.model.entities.account.Account;
import project.model.entities.shared.AccountID;

import java.util.Optional;

/**
 * Interface AccountRepository
 */
public interface AccountRepository {


    Account save(final Account object);

    boolean existsById(AccountID accountID);

    Optional<Account> findById(AccountID accountID);

}
