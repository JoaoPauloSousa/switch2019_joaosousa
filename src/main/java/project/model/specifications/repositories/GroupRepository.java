package project.model.specifications.repositories;

import project.model.entities.group.Group;
import project.model.entities.group.GroupID;

import java.util.Optional;
import java.util.Set;

/**
 * Interface GroupRepository
 */
public interface GroupRepository {

    Optional<Group> findById(GroupID groupID);

    Group save(final Group object);

    Set<Group> findAll();

}
