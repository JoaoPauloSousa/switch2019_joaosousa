package project.frameworkddd;

import project.dto.CreateGroupRequestDTO;
import project.dto.CreateGroupResponseDTO;
import project.dto.GroupDTO;

public interface IUSCreateGroupService {

    CreateGroupResponseDTO createGroup(CreateGroupRequestDTO requestDTO);

    GroupDTO getGroupByID(String id);
}
