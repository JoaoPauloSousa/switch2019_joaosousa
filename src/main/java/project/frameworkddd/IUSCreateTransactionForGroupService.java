package project.frameworkddd;


import project.dto.CreateTransactionForGroupRequestDTO;
import project.dto.CreateTransactionForGroupResponseDTO;

public interface IUSCreateTransactionForGroupService {
    CreateTransactionForGroupResponseDTO createTransactionForGroup(CreateTransactionForGroupRequestDTO requestDTO);
}
