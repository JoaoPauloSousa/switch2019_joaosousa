package project.frameworkddd;

import project.dto.CreateCategoryForPersonRequestDTO;
import project.dto.CreateCategoryForPersonResponseDTO;

public interface IUSCreateCategoryForPersonService {

    CreateCategoryForPersonResponseDTO createCategoryForPerson(CreateCategoryForPersonRequestDTO requestDTO);

}
