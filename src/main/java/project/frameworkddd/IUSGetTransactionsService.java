package project.frameworkddd;

import project.dto.TransactionsResponseDTO;
import project.model.entities.shared.AccountID;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.Period;

public interface IUSGetTransactionsService {

    TransactionsResponseDTO getTransactionsByGroupID(String personID, String groupID);

    TransactionsResponseDTO getTransactionsByPersonID(String personID);

    TransactionsResponseDTO getTransactionsOfAccountWithinPeriodByPersonID(String personID, String accountID, String initialDate, String finalDate);

    TransactionsResponseDTO getTransactionsOfAccountWithinPeriodByGroupID(String personID, String groupID, String accountID, String initialDate, String finalDate);

    TransactionsResponseDTO getTransactionsOfAccountWithinPeriodByLedgerID(LedgerID ledgerID, AccountID accountID, Period period);
}
