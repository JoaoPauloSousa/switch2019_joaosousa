package project.frameworkddd;

/**
 * Interface for an entity serving as a root for an aggregate (a cluster of domain objects that can be treated as
 * a single unit) which is a pattern in Domain-Drive Design (DDD).
 *
 * @param <I> generic type which represent the id of the root entity
 */
public interface Root<I extends ValueObject> extends Entity<I> {
}
