package project.frameworkddd;

import project.dto.CategoriesDTO;

public interface IUSGetCategoriesService {

    CategoriesDTO getCategoriesByGroupID(String groupID);

    CategoriesDTO getCategoriesByPersonID(String personID);

}
