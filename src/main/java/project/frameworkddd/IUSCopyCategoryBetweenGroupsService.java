package project.frameworkddd;

import project.dto.CopyCategoryBetweenGroupsRequestDTO;
import project.dto.CopyCategoryBetweenGroupsResponseDTO;

public interface IUSCopyCategoryBetweenGroupsService {

    CopyCategoryBetweenGroupsResponseDTO copyCategoryBetweenGroups(CopyCategoryBetweenGroupsRequestDTO requestDTO);
}
